import * as Y from 'yjs'
import level from 'level'
import * as encoding from 'lib0/dist/encoding'
import * as decoding from 'lib0/dist/decoding'
import * as syncProtocol from 'y-protocols/dist/sync.js'
import * as authProtocol from 'y-protocols/dist/auth.js'
import { createMutex } from 'lib0/dist/mutex.js'

import { logger } from '../server/log.js'

var request = require("request")

const mux = createMutex()

function getEmptyEncodedStateVector() {
  const encoder = encoding.createEncoder()
  encoding.writeVarUint(encoder, 0)
  return encoding.toUint8Array(encoder)
}

/*
 * Improves the uniqueness of timestamps.
 * We gamble with the fact that users won't create more than 10000 changes on a single document
 * within one millisecond (also assuming clock works correctly).
 */
let timestampIterator = 0
/**
 * @return {string} A random, time-based string starting with "${roomName}:"
 */
const getNextTimestamp = () => {
  timestampIterator = (timestampIterator + 1) % 10000
  return `${Date.now()}${timestampIterator.toString().padStart(4, '0')}`
}

/**
 * @param {string} docName
 * @return {string}
 */
const generateEntryKey = docName => `${docName}#${getNextTimestamp()}`

/**
 *
 * @param {any} db
 * @param {string} docName
 * @param {Uint8Array | ArrayBuffer} buf
 */
const writeEntry = (db, docName, buf) => db.put(generateEntryKey(docName), buf)

/**
 * @param {Uint8Array} arr
 * @param {Y.Y} ydocument
 */
const readEntry = (arr, ydocument) => mux(() =>
  syncProtocol.readSyncMessage(
    decoding.createDecoder(arr), encoding.createEncoder(), ydocument
  )
)

const maxNumSegments = process.env.MAX_NUM_SEGMENTS || 2000;
const hrString = "<hr>";

function setLoaded(ydocument) {
    let metaDataMap = ydocument.getMap("meta-data");
    metaDataMap.set("loaded", true);
}

function isLoaded(ydocument) {
    let metaDataMap = ydocument.getMap("meta-data");
    return metaDataMap.has("loaded") && metaDataMap.get("loaded");
}

/**
 *  Save a snapshot of the current ydocument state.
 *
 *  @param {Y.Y} ydocument
 *  */
function saveSnapshot(ydocument) {
    const versions = ydocument.getArray('versions')

    const snapshot = Y.snapshot(ydocument)

    versions.push([{
        date: new Date().getTime(),
        snapshot: Y.encodeSnapshot(snapshot),
        clientID: ydocument.clientID
    }]);

    // Assign a name to the changes coming from this server
    const permanentUserData = new Y.PermanentUserData(ydocument)
    permanentUserData.setUserMapping(ydocument, ydocument.clientID, "Lecture Translator")
}
/**
 *  Parse all data segments to create XML representations, then add these XML elements to the document.
 * @param {Y.Y} ydocument
 * */
function loadDocumentFromSegments(dataSegments, ydocument) {
  const xmlNodes = [];

  const numSegments = dataSegments.length;
  let segmentStep = 1;
  if (numSegments > maxNumSegments) {
    segmentStep = Math.floor(numSegments / maxNumSegments);
  }
  let curStep = 0;
  let curText = "";
  let curStart = 0;

  // Create and add an XML element for each data segment and include 'hr' where necessary
  dataSegments.forEach(function (entry) {
    // Create an XML element for the current segment including its start and stop time
    // i.e. <segment startTime="..." stopTime="..."></segment>
    if (curStep == 0) {
      // Start new segment
      curStart = entry.start;
      curText = "";
    }
    curText += entry.text;
    curStep++;
    let addHr = (entry.text.indexOf(hrString) !== -1);  // Does the current segment text contain a "line break"?

    if (curStep >= segmentStep || addHr) {
      const yxml = new Y.XmlElement();
      yxml.nodeName = "segment";
      yxml.setAttribute("startTime", curStart);
      yxml.setAttribute("stopTime", entry.stop);

      // Create a text element that will be the child of the segment element
      // i.e. <segment startTime="..." stopTime="...">[TEXT]</segment>
      const ytext = new Y.XmlText();
      if (addHr) {
        curText = curText.replace(hrString, "");  // Remove the hr from the segment text
      }
      ytext.applyDelta([{ insert: curText, attributes: null }]);

      // Insert the text element as a child of the segment element
      yxml.insert(0, [ytext]);
      xmlNodes.push(yxml);  // Add the segment element to the node list

      // If the segment text contained an hr, create a <hr/> element and add it
      if (addHr) {
        const hr = new Y.XmlElement();
        hr.nodeName = "horizontal_rule";
        xmlNodes.push(hr);
      }

      curStep = 0;
    }
  });
  // Add all XML elements to the document
  const type = ydocument.get("prosemirror", Y.XmlFragment);
  type.delete(0, type.length);
  type.insert(0, xmlNodes);
  saveSnapshot(ydocument);
  setLoaded(ydocument);
}

/**
 * Load the transcript specified by docName into the ydocument in such a way that it is later displayed by the prosemirror editor correctly.
 * @param {string} docName
 * @param {Y.Y} ydocument
 */
function loadFromDjango(docName, ydocument) {
  let streamInfo = docName.split("-");
  let sessionID = streamInfo[0];
  let streamIdent = streamInfo[1];
  let fingerprint = streamInfo[2];
  const correctionsUrl = (process.env.WEB_BASE_URL + process.env.WEB_CORRECTIONS_PATH).replace(/\/$/, "");  // Get corrections url and strip trailing slash
  let url = correctionsUrl + '/' + sessionID.replace('/', '') + '/' + fingerprint + '/';
  let body = process.env.WEBSOCKET_ACCESS_SECRET_KEY; // Need to send secret shared key to be able to access correction data
  mux(() => {
    var resultText = '';
    request(url, { json: true, body: body }, (err, res, body) => {
      if (err) {
        logger.error(err);
        return;
      }
      let data = res.body.data;
      if (data == null) {
          logger.error("Could not load data from Django.", {docName});
          return;
      }
      loadDocumentFromSegments(data, ydocument);
      logger.info("Loaded document from Django.", {docName});
    });
  });
}

/**
 * @param {any} db
 * @param {string} docName
 * @param {Y.Y} ydocument
 */
const loadFromPersistence = (db, docName, ydocument) => new Promise((resolve, reject) => {
  let empty = true  // Keeps track of whether or not a database entry already exists for the document
  db.createReadStream({
    gte: `${docName}#`,
    lte: `${docName}#Z`,
    keys: false,
    values: true
  })
    .on('data', data => {
      // Found the document in the database
      empty = false
      readEntry(data, ydocument)
    })
    .on('error', reject)
    .on('end', () => {
      if (empty) loadFromDjango(docName, ydocument)
      resolve();
    })
    .on('close', resolve)
}
)

const persistState = (db, docName, ydocument) => {
  if(!isLoaded(ydocument)) {
      logger.error("Tried to persist a non-loaded document.", {docName});
      logger.info("Trying to load the document now.", {docName});
      loadFromDjango(docName, ydocument);
      return new Promise((resolve, reject) => reject("Failure"));
  }
  const encoder = encoding.createEncoder()
  syncProtocol.writeSyncStep2(encoder, ydocument, getEmptyEncodedStateVector())
  const entryKey = generateEntryKey(docName)
  const entryPromise = db.put(entryKey, encoding.toUint8Array(encoder))
  const delOps = []
  return new Promise((resolve, reject) => db.createKeyStream({
    gte: `${docName}#`,
    lt: entryKey
  })
    .on('data', key => delOps.push({ type: 'del', key }))
    .on('error', reject)
    .on('end', resolve)
    .on('close', resolve)
  ).then(() => entryPromise).then(() => db.batch(delOps))
}

/**
 * Persistence layer for db.
 */
export class LevelDbPersistence {
  /**
   * @param {string} fpath Path to leveldb database
   */
  constructor(fpath, conf = {}) {
    this.db = level(fpath, { valueEncoding: 'binary' })
    this.conf = Object.assign({
      writeStateOnLoad: true
    }, conf)
  }
  /**
   * Retrieve all data from LevelDB and automatically persist all document updates to leveldb.
   *
   * @param {string} docName
   * @param {Y.Y} ydocument
   */
  bindState(docName, ydocument) {
    const broadcastUpdate = (update, origin, y) => {
      if (origin !== 'remote') {
        const encoder = encoding.createEncoder()
        encoding.writeVarUint(encoder, messageSync)
        syncProtocol.writeUpdate(encoder, update)
        const buf = encoding.toUint8Array(encoder)
        if (y.wsconnected) {
          // @ts-ignore We know that wsconnected = true
          y.ws.send(buf)
        }
      }
    }

    // write all updates received from other clients
    // - unless it is created by this persistence layer (e.g. loadFromPersistence, we we mux).
    ydocument.on('update', (update, origin, y) => {
      mux(() => {
        const encoder = encoding.createEncoder()
        syncProtocol.writeUpdate(encoder, update)
        writeEntry(this.db, docName, encoding.toUint8Array(encoder))
      })
    })
    // read all data from persistence
    return loadFromPersistence(this.db, docName, ydocument).then(() => {
      // write current state (just in case anything was added before state was bound)
      if (this.conf.writeStateOnLoad && isLoaded(ydocument)) {
        this.writeState(docName, ydocument)
        .catch(() => {
            logger.error("Could not write state of document after loading from persistence", {docName});
        });
      }
    })
  }
  /**
   * Write current state to persistence layer. Deletes all entries that were made before.
   * Call this method at any time - the recommended time to call this method is before the ydocument is destroyed.
   *
   * @param {string} docName
   * @param {Y.Y} ydocument
   */
  writeState(docName, ydocument) {
    return persistState(this.db, docName, ydocument)
  }
}

#!/usr/bin/env node

/**
 * @type {any}
 */
require('dotenv').config();
const logger = require('./log.js').logger;
const useHttps = (process.env.USE_HTTPS == "true");
const validateCookies = (process.env.VALIDATE_COOKIES == "true");
const WebSocket = require('ws');
const http = useHttps ? require('https') : require('http');
const fs = require('fs');
const request = require("request");

const wss = new WebSocket.Server({ noServer: true });
const setupWSConnection = require('./utils.js').setupWSConnection;

const port = process.env.PORT || 1234;

let httpOptions = {};
if (useHttps) {
  httpOptions = { key: fs.readFileSync(process.env.KEY_PATH), cert: fs.readFileSync(process.env.CERT_PATH) };
}

const server = http.createServer(httpOptions, (request, response) => {
  console.log(request);
  response.writeHead(200, { 'Content-Type': 'text/plain' });
  response.end('okay');
});

wss.on('connection', (conn, req) => setupWSConnection(conn, req, { gc: false }));

server.on('upgrade', (request, socket, head) => {
  // Check the authorization of the request
  /**
   * @param {any} ws
   */
  const handleAuth = ws => {
    // Callbacks are needed because an async request to the Django REST API will be made
    const onSuccess = () => {
      wss.emit('connection', ws, request);
    };
    const onFail = () => {
        logger.warn('Access denied', {url: request.url});
      setTimeout(() => {
        socket.destroy();
        socket = null;
      }, 10000);
      return;
    };
    authRequest(request.headers, onSuccess, onFail);
  };
  wss.handleUpgrade(request, socket, head, handleAuth);
});

server.listen(port);

logger.info('Running on port', {port});

function authRequest(headers, onSuccess, onFail) {
  if (!validateCookies) {
    onSuccess(); 
    return;
  }
  url = process.env.WEB_BASE_URL + process.env.WEB_IS_CORRECTOR_PATH;
  let newHeaders = {
    'Cookie': headers.cookie
  };
  options = {
    url: url,
    method: 'GET',
    headers: newHeaders,
    json: true,
    key: httpOptions.key,
    cert: httpOptions.cert,
  };
  request(options, (err, res, body) => {
    if (err) { logger.error("Error on auth request", {err}); onFail(); }
    let data = res.body;  // True iff user is staff
    if(data) {
        onSuccess();
    }
    else {
        onFail();
    }
  });
}

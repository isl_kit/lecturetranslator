# Editor Websocket
This subdirectory contains the websocket for the transcript editor. The editor allows users to collaboratively edit transcripts to correct mistakes etc.
To achieve realtime collaboration, messages need to be passed between clients in real time - this is what this websocket does.

## Install

    npm install

## Usage

### Set configuration

Ensure that you have set all necessary environment variables or created a `.env` file, containing those values (see [Config](#config)).

### Start the server

    node server/server.js

For keeping this server running in a production environment, you can use the npm package [forever](https://www.npmjs.com/package/forever):

    npm install forever -g
    forever start server/server.js

## Config

Configuration is done with environment variables defined in a `.env` file located in this directory.

The following shows a list of all available settings.

### PORT

The port under which the websocket should run:

    PORT=8085

### USE_HTTPS

    USE_HTTPS=true

### KEY_PATH

This parameter is only required when using `USE_HTTPS=true`. It defines the path to the SSL key file:

    KEY_PATH=/path/to/key.pem

### CERT_PATH
This parameter is only required when using `USE_HTTPS=true`. It defines the path to the SSL cert file:

    CERT_FILE=/path/to/cert.pem

### VALIDATE_COOKIES
Should the websocket validate the session cookie of the incoming request?
This checks whether the request is authorized or not.
In production, this setting should always be enabled to prevent attacks on the websocket.
For development, cookies sometimes aren't sent with the request, so this setting should be disabled.

    VALIDATE_COOKIES=true

### WEB_BASE_URL
The base URL of the LT website.

    WEB_BASE_URL=http://localhost:8000

### WEB_IS_CORRECTOR_PATH
The path to the `isCorrector` REST API endpoint.

    WEB_IS_CORRECTOR_PATH=/auth/isCorrector

### WEB_CORRECTIONS_PATH 
The path to the `corrections` REST API endpoint.

    WEB_CORRECTIONS_PATH=/streamdata/corrections

### MAX_NUM_SEGMENTS
This setting limits the maximum number of segments in an editor document.
In practice, this is not a hard limit - the number of segments might be slightly above the limit to distribute segment lengths more evenly.

    MAX_NUM_SEGMENTS=2000

### DB_PATH
The path to the LevelDB where the editor documents are stored. The path is specified relative to this directory.
Note that the database will be created for you if it doesn't exist already, you simply need to set its path here.

    DB_PATH=./db

# Lecture Translator Website

This project contains the [Lecture Translator](https://lecture-translator.kit.edu/#/) web application, developed as an [AngularJS (1.x)](https://angularjs.org/) single page application with a RESTful backend ([Django](https://www.djangoproject.com/), [Django REST framework](http://www.django-rest-framework.org/)).

The project is structured loosely based on the [cookiecutter-django boilerplate](https://github.com/pydanny/cookiecutter-django). It uses [bower](https://bower.io/) as a package manager and [Gulp](http://gulpjs.com/) as a build-tool for static files. All JavaScript files are written in accordance with the [AngularJS style guide by John Papa](https://github.com/johnpapa/angular-styleguide/blob/master/a1/README.md) and are linted using [JSHint](http://jshint.com/about/). Python files are linted with [Pylint](https://www.pylint.org/).

All documentation can be found in the [wiki](https://bitbucket.org/isl_kit/lecturetranslator/wiki/Home) that is part of the Bitbucket repository.
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.db import models

from lecturetranslator.session.models import Session

class Connection(models.Model):
    ip_address = models.GenericIPAddressField()

    def __unicode__(self):
        return str(self.ip_address)


class Device(models.Model):
    fingerprint = models.CharField(max_length=32, primary_key=True)
    browser = models.TextField()
    language = models.TextField()

    def __unicode__(self):
        return self.fingerprint


class PageAccess(models.Model):
    id = models.CharField(max_length=32, primary_key=True)
    device = models.ForeignKey(Device, on_delete=models.CASCADE) # keeping implicit on_delete behaviour from Django version <= 1.10
    connection = models.ForeignKey(Connection, on_delete=models.CASCADE, null=True)
    start = models.DateTimeField()
    stop = models.DateTimeField(null=True)

    def __unicode__(self):
        return self.id


class SessionAccess(models.Model):
    id = models.CharField(max_length=32, primary_key=True)
    page_access = models.ForeignKey(PageAccess, on_delete=models.CASCADE)
    session = models.ForeignKey(Session, on_delete=models.CASCADE, null=True)
    start = models.DateTimeField()
    stop = models.DateTimeField(null=True)

    def __unicode__(self):
        return self.id

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime

from lecturetranslator.session.models import Session

from .models import Connection, Device, PageAccess, SessionAccess


class LogType:
    OPEN = 0
    CLOSE = 1
    JOIN = 2
    LEAVE = 3


def log(tab_id, join_id, type, hash, browser, language, session_id, ip):
    now = datetime.now()

    if type == 0:
        print(("OPEN " + tab_id))
    elif type == 1:
        print(("CLOSE " + tab_id))
    elif type == 2:
        print(("JOIN " + tab_id + " / " + join_id))
    elif type == 3:
        print(("LEAVE " + tab_id + " / " + join_id))

    try:
        page_access = PageAccess.objects.get(id=tab_id)
        device = page_access.device
        if type == LogType.OPEN:
            return
    except PageAccess.DoesNotExist:
        if type == LogType.CLOSE:
            return
        try:
            device = Device.objects.get(fingerprint=hash)
        except Device.DoesNotExist:
            device = Device(fingerprint=hash, browser=browser, language=language)
            device.save()
        if ip:
            try:
                connection = Connection.objects.get(ip_address=ip)
            except Connection.DoesNotExist:
                connection = Connection(ip_address=ip)
                connection.save()
        else:
            connection = None
        page_access = PageAccess(id=tab_id, device=device, connection=connection, start=now)
        page_access.save()

    if type == LogType.CLOSE:
        page_access.stop = now
        page_access.save()
        page_access.sessionaccess_set.filter(stop=None).update(stop=now)
    else:
        if page_access.stop:
            page_access.stop = None
            page_access.save()
        if not session_id:
            return
        try:
            session = Session.objects.get(id=session_id)
        except Session.DoesNotExist:
            return
        try:
            session_access = SessionAccess.objects.get(id=join_id)
            if type == LogType.JOIN:
                return
            if session_access.page_access != page_access:
                return
        except SessionAccess.DoesNotExist:
            if type == LogType.LEAVE:
                return
            session_access = SessionAccess(id=join_id, page_access=page_access, session=session, start=now)
            session_access.save()
        if type == LogType.LEAVE:
            session_access.stop = now
            session_access.save()

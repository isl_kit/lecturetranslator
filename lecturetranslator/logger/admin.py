#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import Connection, Device, PageAccess, SessionAccess

admin.site.register(Connection)
admin.site.register(Device)
admin.site.register(PageAccess)
admin.site.register(SessionAccess)

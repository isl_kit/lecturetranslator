#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json

from ipware.ip import get_real_ip as get_ip

from django.http import JsonResponse
from django.views.decorators.http import require_POST

from .loggingService import log as _log


@require_POST
def log(request):
    try:
        data = json.loads(request.body)
    except:
        data = request.POST
    try:
        tab_id = data["tab_id"]
        join_id = data["join_id"]
        type = int(data["type"])
        hash = data["hash"]
        browser = data["browser"]
        language = data["language"]
        session_id = data["session_id"]
    except KeyError:
        return JsonResponse({}, status=404)

    session_id = int(session_id) if session_id else None
    _log(tab_id, join_id, type, hash, browser, language, session_id, get_ip(request))
    return JsonResponse({"message": "OK"})

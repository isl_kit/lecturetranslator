#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.LecturerList.as_view()),
    url(r'^(?P<pk>[0-9]+)/$', views.LecturerDetail.as_view()),
    url(r'^(?P<pk>[0-9]+)/user/$', views.LecturerUser.as_view()),
    url(r'me/$', views.LecturerMe.as_view())
]

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.contrib.auth import get_user_model

from rest_framework.serializers import ModelSerializer, SlugRelatedField

from .models import Lecturer


class LecturerListSerializer(ModelSerializer):
    class Meta:
        model = Lecturer
        fields = ('id', 'title', 'name')


class LecturerDetailSerializer(ModelSerializer):
    user = SlugRelatedField(
        allow_null=True,
        queryset=get_user_model().objects.all(),
        slug_field='name',
        write_only=True
    )
    class Meta:
        model = Lecturer
        fields = ('id', 'title', 'name', 'user')
        read_only_fields = ('id',)


class LecturerUserSerializer(ModelSerializer):
    user = SlugRelatedField(
        read_only=True,
        slug_field='name',
    )
    class Meta:
        model = Lecturer
        fields = ('id', 'user')


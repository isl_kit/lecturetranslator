#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from typing import Sequence, Type

from rest_framework.generics import (
    ListCreateAPIView,
    RetrieveAPIView,
    RetrieveUpdateDestroyAPIView
)
from rest_framework.permissions import BasePermission
from rest_framework.filters import SearchFilter, OrderingFilter

from lecturetranslator.common.permissions import IsStaffOrReadOnly, IsStaff

from .models import Lecturer
from .serializers import (
    LecturerDetailSerializer,
    LecturerListSerializer,
    LecturerUserSerializer
)


class LecturerList(ListCreateAPIView):
    queryset = Lecturer.objects.all()
    filter_backends = (SearchFilter, OrderingFilter)
    search_fields = ('name', 'title')
    ordering_fields = ('name', 'title')
    ordering = ('name',)
    permission_classes = [IsStaffOrReadOnly]

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return LecturerDetailSerializer
        else:
            return LecturerListSerializer


class LecturerDetail(RetrieveUpdateDestroyAPIView):
    queryset = Lecturer.objects.all()
    serializer_class = LecturerDetailSerializer
    permission_classes = [IsStaffOrReadOnly]


class LecturerUser(RetrieveAPIView):
    queryset = Lecturer.objects.all()
    serializer_class = LecturerUserSerializer
    permission_classes = [IsStaff]


class LecturerMe(RetrieveAPIView):
    queryset = Lecturer.objects.all()
    serializer_class = LecturerListSerializer
    permission_classes: Sequence[Type[BasePermission]] = []

    def get_object(self):
        try:
            return Lecturer.objects.get(user=self.request.user)
        except Lecturer.DoesNotExist:
            return None

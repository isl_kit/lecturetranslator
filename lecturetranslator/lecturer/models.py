#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.conf import settings
from django.db import models


class Lecturer(models.Model):
    title = models.CharField(max_length=100, blank=True)
    name = models.CharField(max_length=200)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, models.SET_NULL,
        null=True, blank=True, related_name='lecturers'
    )

    def __unicode__(self):
        return '{} {}'.format(self.title, self.name)

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import logging

from twisted.internet import reactor, ssl
from twisted.web.static import File
from twisted.web.server import Site
from autobahn.twisted.websocket import WebSocketServerFactory, WebSocketServerProtocol, listenWS

from pythonrecordingclient.session import Session, on_session_ready
from pythonrecordingclient.config import Config

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings


class AudioServerProtocol(WebSocketServerProtocol):

    def __init__(self):
        super(AudioServerProtocol, self).__init__()
        self.sessions = {}
        self.session = None

    def get_session_by_id(self, session_id):
        for key, session in self.sessions.items():
            if session and session_id == session.id:
                return session
        return None

    def get_session(self):
        key = self.get_key()
        if key not in self.sessions:
            return None
        return self.sessions[key]

    def get_key(self):
        return self.http_headers["sec-websocket-key"]

    def onConnect(self, request):
        self.sessions[self.get_key()] = None
        print(("Client connecting: {}".format(request.peer)))

    def connectionLost(self, reason):
        key = self.get_key()
        if key in self.sessions:
            del self.sessions[key]
        print("#### connection lost ####")
        print(reason)

    def onOpen(self):
        print("WebSocket connection open.")

    def write(self, data):
        key = self.get_key().replace("/", "")
        with open("/home/bkrueger/web_client/{}.raw".format(key), "ab+") as f:
            f.write(data)

    def handle_start(self, msg):
        print("START")
        params = []
        js_params = msg["data"]["params"]
        for key, val in js_params.items():
            params.append(str(key) + "=" + str(val))
        if settings.SESSION_NAMESPACE:
            if type(settings.SESSION_NAMESPACE) is list:
                ns = settings.SESSION_NAMESPACE[0]
            else:
                ns = settings.SESSION_NAMESPACE
            params.append("N=" + ns)
        if params:
            params = "?" + "?".join(params)
        else:
            params = ""
        title = msg["data"]["title"] + params
        fingerprint = msg["data"]["fingerprint"]
        output_fingerprint = fingerprint.split("-")[0]
        desc = msg["data"]["desc"]
        password = msg["data"]["password"]
        logging = msg["data"]["logging"]
        print("START SESSION")
        session = Session(name=title, description=desc, password=password, input_fingerprint=fingerprint, output_fingerprint=[output_fingerprint], logging=logging)
        session.start(settings.MEDIATOR_HOST, settings.MEDIATOR_PORT)
        print(session)
        self.sessions[self.get_key()] = session

    def handle_stop(self):
        session = self.get_session()
        if session:
            session.stop()

    def handle_reconnect(self, msg):
        session_id = msg["data"]["sessionid"]
        session = self.get_session_by_id(session_id)
        if session:
            self.sessions[self.get_key()] = session

    def handle_status(self):
        session = self.get_session()
        if session and session.ready:
            self.sendMessage(json.dumps({"ready": True, "session_id": session.id}))
        elif session and session.closed:
            self.sendMessage(json.dumps({"ready": False, "closed": True}))
        else:
            self.sendMessage(json.dumps({"ready": False, "closed": False}))

    def handle_data(self, msg):
        session = self.get_session()
        if session and session.ready:
            #self.write(msg)
            session.send_audio(msg)

    def onMessage(self, msg, binary):
        if not binary:
            msg = json.loads(msg)
            if msg["type"] == "start":
                self.handle_start(msg)
            elif msg["type"] == "stop":
                self.handle_stop()
            elif msg["type"] == "reconnect":
                self.handle_reconnect(msg)
            elif msg["type"] == "status":
                self.handle_status()
        else:
            self.handle_data(msg)

def configure_logging(level):
    logger = logging.getLogger("mepy-client")
    logger.setLevel(level)
    ch = logging.StreamHandler()
    ch.setLevel(level)
    formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)

class Command(BaseCommand):

    def handle(self, *args, **options):
        Config.set(CHUNK_SIZE=16384, BUFFER_CHUNKS=False, CORRECT_TIME=False)
        configure_logging("INFO")
        protocol = settings.WEBSOCKET_INT_URL.split(":")[0]
        if protocol == "wss":
            contextFactory = ssl.DefaultOpenSSLContextFactory(settings.LOCALHOST_KEY_FILE, settings.LOCALHOST_CERT_FILE)
        else:
            contextFactory = None
        factory = WebSocketServerFactory(settings.WEBSOCKET_INT_URL, debug=True)
        factory.protocol = AudioServerProtocol
        listenWS(factory, contextFactory)
        #reactor.listenTCP(settings.WEBSOCKET_INT_PORT, factory)
        reactor.run()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from rest_framework import serializers

from lecturetranslator.common.fields import CreatableSlugRelatedField
from lecturetranslator.authentication.serializers import AuthEntitySerializer
from lecturetranslator.event.models import Event
from lecturetranslator.lecturehall.models import LectureHall
from lecturetranslator.lecturer.models import Lecturer
from lecturetranslator.lecturer.serializers import LecturerListSerializer

from .models import Lecture, LectureTerm


class LectureListSerializer(serializers.ModelSerializer):
    lecture_terms = serializers.SlugRelatedField(read_only=True, many=True, slug_field='term')

    class Meta:
        model = Lecture
        fields = ('slug', 'title', 'lecture_terms')


class LectureDetailSerializer(serializers.ModelSerializer):
    lecture_terms = serializers.SlugRelatedField(read_only=True, many=True, slug_field='term')

    class Meta:
        model = Lecture
        fields = ('slug', 'title', 'lecture_terms')


class LectureTermRelatedSerializer(serializers.ModelSerializer):
    lecture = LectureListSerializer(read_only=True)

    class Meta:
        model = LectureTerm
        fields = (
            'id', 'lecture', 'term', 'description',
            'archive', 'media_access'
        )


class LectureTermListSerializer(serializers.ModelSerializer):
    lecture = LectureListSerializer(read_only=True)
    auth = AuthEntitySerializer(read_only=True)
    lecturers = LecturerListSerializer(many=True, read_only=True)

    class Meta:
        model = LectureTerm
        fields = (
            'id', 'auth', 'lecture', 'term', 'lecturers',
            'description', 'archive', 'media_access'
        )


class EventCreateRelatedSerializer(serializers.ModelSerializer):
    lecture_hall = CreatableSlugRelatedField(
        queryset=LectureHall.objects.all(),
        slug_field='name',
        allow_null=True
    )
    class Meta:
        model = Event
        fields = ('lecture_hall', 'start', 'stop', 'fingerprint')


class LectureTermCreateSerializer(serializers.ModelSerializer):
    lecture = serializers.SlugRelatedField(queryset=Lecture.objects.all(), slug_field='slug')
    auth = AuthEntitySerializer()
    lecturers = serializers.PrimaryKeyRelatedField(many=True, queryset=Lecturer.objects.all())
    events = EventCreateRelatedSerializer(many=True)

    class Meta:
        model = LectureTerm
        fields = (
            'auth', 'lecture', 'term', 'lecturers',
            'description', 'archive', 'media_access', 'events'
        )

    def validate(self, data):
        term = data['term']
        lecture = data['lecture']
        if not data['archive']:
            data['media_access'] = False
        if LectureTerm.objects.filter(term=term, lecture=lecture).exists():
            raise serializers.ValidationError('LectureTerm already exists')
        return data

    def create(self, validated_data):
        lecture = validated_data.pop('lecture')
        auth_data = validated_data.pop('auth')
        auth = None
        if auth_data['password']:
            auth_serializer = AuthEntitySerializer(data=auth_data)
            if auth_serializer.is_valid():
                auth = auth_serializer.save()
        events_data = validated_data.pop('events')
        lecturers = validated_data.pop('lecturers')
        lecture_term = LectureTerm.objects.create(
            auth=auth, lecture=lecture,
            **validated_data
        )
        lecture_term.lecturers.set(lecturers)
        for event_data in events_data:
            Event.objects.create(lecture_term=lecture_term, **event_data)
        return lecture_term

    def to_representation(self, obj):
        return LectureTermListSerializer(obj).data


class LectureTermDetailSerializer(serializers.ModelSerializer):
    lecture = LectureListSerializer(read_only=True)
    auth = AuthEntitySerializer(read_only=True)
    lecturers = LecturerListSerializer(many=True, read_only=True)
    lecture_halls = serializers.SerializerMethodField()
    min_date = serializers.DateTimeField()

    def get_lecture_halls(self, obj):
        return LectureHall.objects.filter(events__lecture_term=obj).distinct().values('id', 'name')

    class Meta:
        model = LectureTerm
        fields = (
            'id', 'auth', 'lecture', 'term', 'lecturers',
            'lecture_halls', 'description', 'archive', 'media_access',
            'min_date'
        )


class LectureTermUpdateSerializer(serializers.ModelSerializer):
    lecturers = serializers.PrimaryKeyRelatedField(many=True, queryset=Lecturer.objects.all())

    class Meta:
        model = LectureTerm
        fields = (
            'lecturers', 'description', 'archive', 'media_access'
        )

    def to_representation(self, obj):
        return LectureTermListSerializer(obj).data

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.db import models

from lecturetranslator.authentication.models import AuthEntity
from lecturetranslator.lecturer.models import Lecturer


class Lecture(models.Model):
    slug = models.SlugField(unique=True)
    title = models.CharField(max_length=100)

    def __unicode__(self):
        return self.slug + ': ' + self.title


class LectureTerm(models.Model):
    lecture = models.ForeignKey(
        Lecture, models.CASCADE,
        related_name='lecture_terms'
    )
    auth = models.ForeignKey(
        AuthEntity, models.SET_NULL,
        null=True, blank=True
    )
    term = models.SlugField(max_length=50)
    description = models.TextField(blank=True)
    lecturers = models.ManyToManyField(Lecturer, blank=True)
    archive = models.BooleanField(default=False)
    media_access = models.BooleanField(default=False)

    class Meta:
        permissions = (
            ('has_archive_access', 'Can access archived lectures'),
            ('has_media_access', 'Can access archived media'),
            ('can_upload', 'Can upload files'),
        )

    def __unicode__(self):
        return self.lecture.slug + ': ' + self.term

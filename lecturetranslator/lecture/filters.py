#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.db.models import Q

from django_filters.rest_framework import FilterSet, BooleanFilter, CharFilter, ModelMultipleChoiceFilter

from lecturetranslator.authentication.auth import get_all_authenticated
from lecturetranslator.lecturer.models import Lecturer
from lecturetranslator.lecturehall.models import LectureHall

from .models import LectureTerm, Lecture


class LectureFilter(FilterSet):
    hall = ModelMultipleChoiceFilter(
        field_name='lecture_terms__events__lecture_hall',
        to_field_name='id',
        queryset=LectureHall.objects.all(),
        help_text='ID of related LectureHall (multiple values allowed)'
    )
    lecturer = ModelMultipleChoiceFilter(
        field_name='lecture_terms__lecturers',
        to_field_name='id',
        queryset=Lecturer.objects.all(),
        help_text='ID of related Lecturer (multiple values allowed)'
    )

    class Meta:
        model = Lecture
        fields = ['hall', 'lecturer']


class LectureTermFilter(FilterSet):
    term = CharFilter(
        help_text='term name (i.e. SS2016)'
    )
    accessible = BooleanFilter(
        label='Accessible',
        method='accessible_filter',
        help_text='Show only LectureTerms, the current user can access'
    )

    class Meta:
        model = LectureTerm
        fields = ['term', 'accessible']

    def accessible_filter(self, queryset, name, value):
        auth_entity_ids = get_all_authenticated(self.request)
        if value is True:
            if self.request.user.is_authenticated:
                queryset = queryset.filter(Q(auth_id__in=auth_entity_ids) | Q(lecturers__user=self.request.user)).distinct()
            else:
                queryset = queryset.filter(auth_id__in=auth_entity_ids).distinct()
        else:
            if self.request.user.is_authenticated:
                queryset = queryset.exclude(auth_id__in=auth_entity_ids, lecturers__user=self.request.user).distinct()
            else:
                queryset = queryset.exclude(auth_id__in=auth_entity_ids).distinct()
        return queryset

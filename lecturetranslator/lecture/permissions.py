#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.conf import settings


def has_archive_access(request, lecture_term):
    return settings.GRANT_ARCHIVE_TO_ALL or lecture_term.archive or \
        request.user.has_perm('has_archive_access', lecture_term) or \
        is_lecturer(request.user, lecture_term)


def has_media_access(request, lecture_term):
    return settings.GRANT_MEDIA_TO_ALL or lecture_term.media_access or \
        request.user.has_perm('has_media_access', lecture_term) or \
        is_lecturer(request.user, lecture_term)


def can_upload_files(request, lecture_term):
    return request.user.has_perm('can_upload', lecture_term) or \
        is_lecturer(request.user, lecture_term)


def is_lecturer(user, lecture_term):
    # See authentication.auth:12 for explanation of is_authenticated == True
    return (user.is_authenticated == True) and lecture_term.lecturers.filter(user=user).exists()

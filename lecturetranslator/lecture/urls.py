#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.conf.urls import url
from .views import (
    LectureDetail,
    LectureList,
    LectureTermDetail,
    LectureTermList,
)

urlpatterns = [
    url(r'^$', LectureList.as_view()),
    url(r'^terms/$', LectureTermList.as_view()),
    url(r'^(?P<slug>[A-Za-z0-9_\-]+)/$', LectureDetail.as_view()),
    url(r'^(?P<slug>[A-Za-z0-9_\-]+)/(?P<term>[A-Za-z0-9_\-]+)/$', LectureTermDetail.as_view()),
]

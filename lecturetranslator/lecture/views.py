#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.db.models import Min, Prefetch
from django.http import Http404

from django_filters.rest_framework import DjangoFilterBackend

from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from lecturetranslator.common.permissions import IsStaffOrReadOnly

from .filters import (
    LectureTermFilter,
    LectureFilter
)
from .models import Lecture, LectureTerm
from .serializers import (
    LectureListSerializer,
    LectureDetailSerializer,
    LectureTermCreateSerializer,
    LectureTermUpdateSerializer,
    LectureTermListSerializer,
    LectureTermDetailSerializer,
)


class LectureList(ListCreateAPIView):
    serializer_class = LectureListSerializer
    queryset = Lecture.objects.prefetch_related(
        Prefetch(
            'lecture_terms',
            queryset=LectureTerm.objects \
                .annotate(min_date=Min('events__start')) \
                .order_by('min_date')
        )
    ).all()
    permission_classes = [IsStaffOrReadOnly]
    filter_backends = (
        DjangoFilterBackend,
        SearchFilter,
        OrderingFilter
    )
    filterset_class = LectureFilter
    search_fields = ('title',)
    ordering_fields = ('title',)
    ordering = ('title',)


class LectureDetail(RetrieveUpdateDestroyAPIView):
    queryset = Lecture.objects.prefetch_related(
        Prefetch(
            'lecture_terms',
            queryset=LectureTerm.objects \
                .annotate(min_date=Min('events__start')) \
                .order_by('min_date')
        )
    ).all()
    serializer_class = LectureDetailSerializer
    lookup_field = 'slug'
    permission_classes = [IsStaffOrReadOnly]


class LectureTermList(ListCreateAPIView):
    queryset = LectureTerm.objects \
        .select_related('auth', 'lecture') \
        .prefetch_related('lecturers')
    permission_classes = [IsStaffOrReadOnly]
    filter_backends = (
        DjangoFilterBackend,
        SearchFilter,
        OrderingFilter
    )
    filterset_class = LectureTermFilter
    search_fields = ('lecture__title',)
    ordering_fields = ('lecture__title',)
    ordering = ('lecture__title',)

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return LectureTermCreateSerializer
        return LectureTermListSerializer

    def get_serializer_context(self):
        return {
            'request': self.request
        }


class LectureTermDetail(RetrieveUpdateDestroyAPIView):
    queryset = LectureTerm.objects.all()
    permission_classes = [IsStaffOrReadOnly]

    def get_object(self):
        slug = self.kwargs['slug']
        term = self.kwargs['term']
        try:
            return LectureTerm.objects \
                .annotate(min_date=Min('events__start')) \
                .get(term=term, lecture__slug=slug)
        except LectureTerm.DoesNotExist:
            raise Http404

    def get_serializer_class(self):
        if self.request.method in ['PUT', 'PATCH']:
            return LectureTermUpdateSerializer
        return LectureTermDetailSerializer

    def get_serializer_context(self):
        return {
            'request': self.request
        }

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import socket

from django.conf import settings


class MSocket(object):

    def __init__(self):
        self.socket = None

    def __connect__(self):
        self.socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        try:
            print(("Connecting to DisplayClient socket [" + settings.UNIX_IN_SOCKET + "]"))
            self.socket.connect(settings.UNIX_IN_SOCKET)
            print(("Connection to DisplayClient socket [" + settings.UNIX_IN_SOCKET + "] successful!"))
        except (socket.error, socket.timeout) as err:
            print(("Connection to DisplayClient socket [" + settings.UNIX_IN_SOCKET + "] failed! Error message follows."))
            print("====================")
            print(err)
            print("====================")
            print("")

    def __send__(self, msg):
        self.socket.sendall((msg + "\r\n").encode());

    def send(self, msg):
        if self.socket is None:
            self.__connect__()
        try:
            self.__send__(msg)
        except Exception as e:
            print(("Sending of message to DisplayClient socket [" + settings.UNIX_IN_SOCKET + "] failed. Retrying connection and resending."))
            print("Reason: " + str(e))
            self.__connect__()
            try:
                self.__send__(msg)
            except:
                print("Sending failed, message will be lost.")


m_socket = MSocket()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import re

from socketiodjangoapp.conf import io_auth
from socketiodjangoapp.api import on_join, on_leave, on_disconnect

from lecturetranslator.session.auth import check_session_auth
from lecturetranslator.session.models import Stream

from .msocket import m_socket
from .preprocess.preprocess import preproc


def check_login_io(request, room):
    session_id = int(room.split('_')[0][1:])
    return check_session_auth(request, session_id)


def stream_action(action, socketid, room):
    m = re.match('^s([0-9]+):(.+)$', room)
    if m:
        session_id = int(m.group(1))
        ident = m.group(2)
        if not Stream.objects.filter(session_id=session_id, ident=ident).exists():
            return
        socketid, session_id, stream_id = preproc.join(socketid, session_id, ident)
        print(('{} -> {}'.format(action, stream_id)))
        m_socket.send(json.dumps({
            'action': action,
            'client_id': socketid,
            'session_id': session_id,
            'stream_id': stream_id
        }))


@on_join()
def on_join_handler(socketid, data, request):
    stream_action('join', socketid, data['room'])


@on_leave()
def on_leave_handler(socketid, data, request):
    stream_action('leave', socketid, data['room'])


@on_disconnect
def on_disconnect_handler(socketid, data, request):
    socketid = preproc.disconnect(socketid)
    m_socket.send(json.dumps({
        'action': 'disconnect',
        'client_id': socketid
    }))


io_auth.set_data({
    '^s([0-9]+)_([a-zA-Z]+)$': check_login_io
})

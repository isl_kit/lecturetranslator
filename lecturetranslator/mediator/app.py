#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

class MediatorConfig(AppConfig):
    name = 'lecturetranslator.mediator'
    verbose_name = _('mediator')

    def ready(self):
        import lecturetranslator.mediator.events  # noqa
        import lecturetranslator.mediator.signals  # noqa

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json

from django.core.management.base import BaseCommand

from lecturetranslator.session.models import DataSegment
from ...serializers import DataSegmentCreateUpdateSerializer


class Command(BaseCommand):

    def handle(self, *args, **options):
        data = dict(
            stream_id='de_text',
            session_id=123456789,
            text='how are you?<br><br>',
            start_timestamp=250,
            stop_timestamp=350,
            startoffset=50,
            creator='ASR'
        )
        try:
            data_segment = DataSegment.objects.get(
                stream__ident=data['stream_id'], stream__session_id=data['session_id'], start=data['start_timestamp']
            )
            serializer = DataSegmentCreateUpdateSerializer(data_segment, data=data)
        except DataSegment.DoesNotExist:
            serializer = DataSegmentCreateUpdateSerializer(data=data)
        if not serializer.is_valid():
            print("INVALID")
            return
        serializer.save()


#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re

from django.conf import settings
from django.core.management.base import BaseCommand

from lecturetranslator.session.models import Session, Stream, DataSegment


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('sessionid', type=int)
        parser.add_argument('fingerprint', type=str)
        parser.add_argument('type', type=str)
        parser.add_argument('source', type=str)

    def handle(self, *args, **options):
        stream_type = options['type']
        fingerprint = options['fingerprint']
        session = Session.objects.get(id=options['sessionid'])
        with open(options['source'], 'r') as f:
            lines = [t.strip() for t in f.readlines()]
        try:
            stream = Stream.objects.get(
                session=session,
                type=stream_type,
                fingerprint=fingerprint
            )
        except Stream.DoesNotExist:
            stream = Stream.objects.create(
                session=session,
                ident='{}_{}'.format(fingerprint, stream_type),
                type=stream_type,
                fingerprint=fingerprint,
                label=settings.GET_STREAM_LABEL(fingerprint),
                display_name='MANUAL {}/{}'.format(fingerprint, stream_type),
                control='l',
                sid='speech'
            )
        stream.data_segments.all().delete()
        for idx, line in enumerate(lines):
            self.handle_line(stream, idx, line, (idx + 1) % 5 == 0)

    def handle_line(self, stream, index, line, end_of_paragraph):
        parts = line.split(' ', 2)
        start = int(float(parts[0]) * 1000)
        stop = int(float(parts[1]) * 1000)
        text = parts[2]
        text = re.sub(r'\s+(\.|,|!|\?)', '\\1', text)
        text = re.sub(r'(¿)\s+', '\\1', text)
        text = re.sub(r'^(¿)?.', capitalize, text)
        DataSegment.objects.create(
            index=index,
            stream=stream,
            start_abs=start,
            stop_abs=stop,
            start=start,
            stop=stop,
            offset=start,
            text=text,
            creator='MANUAL',
            end_of_paragraph=end_of_paragraph,
            version=1
        )


def capitalize(match):
    return match.group(0).upper()

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import time
from datetime import datetime

from twisted.internet import reactor, task
from twisted.internet.protocol import ReconnectingClientFactory
from twisted.protocols.basic import LineReceiver

import django.db

from django.core.management.base import BaseCommand

from django.conf    import settings
from django.db      import connection, connections
from ...msg_handler import handle_message
from ...db_handler import cleanup, connect


class SocketClientProtocol(LineReceiver):

    MAX_LENGTH = 64*1024*1024

    def __init__(self):
        self.setLineMode()

    def connectionMade(self):
        connect()
        task.deferLater(reactor, 10, cleanup)
        print("CONNECTION ESTABLISHED")

    def connectionLost(self, reason):
        print("CONNECTION LOST")
        print(reason.getErrorMessage())

    def lengthLimitExceeded(self, length):
        print("EXCEED")
        print(length)

    def lineReceived(self, msg):
        data = json.loads(msg.decode("utf-8", "replace"))
        if data["type"] == 2:
            n = datetime.now()
            data["data"]["created"] = int(time.mktime(n.timetuple()) * 1000000 + n.microsecond)
            #print("{}".format(data["data"]["created"]%100000000))
        # Schedule thread-safe run of handle_message()
        # I am not sure if we're already in the main loop so this might be slightly worse than colling handle_message() directly
        #reactor.callInThread(handle_message, data["type"], data["data"])
        reactor.callFromThread(handle_message, data["type"], data["data"])
        #handle_message(data["type"], data["data"])


class SocketClientFactory(ReconnectingClientFactory):

    def startedConnecting(self, connector):
        print('Started to connect.')

    def buildProtocol(self, addr):
        print('Connected')
        self.resetDelay()
        return SocketClientProtocol()

    def clientConnectionLost(self, connector, reason):
        print('Lost connection.  Reason:', reason)
        ReconnectingClientFactory.clientConnectionLost(self, connector, reason)

    def clientConnectionFailed(self, connector, reason):
        print('Connection failed. Reason:', reason)
        ReconnectingClientFactory.clientConnectionFailed(self, connector, reason)


class Command(BaseCommand):

    def handle(self, *args, **options):
        # This is a good place to check for additional LT instances on the same database
        # which might produce very ugly sideffects
        print("Checking for users in database...")
        if connection.vendor == 'postgresql':
            con = connections[django.db.DEFAULT_DB_ALIAS]
            db_name = con.settings_dict["NAME"]
            query = "SELECT usename, pid, application_name, client_addr, client_hostname, backend_start, backend_type FROM pg_stat_activity WHERE datname = '{}';".format(db_name)

            try:
                with connection.cursor() as cursor:
                    cursor.execute(query)

                    line = cursor.fetchone()
                    i = 0
                    print("    Username   PID        AppName    Address    Host       start      type")
                    while line:
                        print("{} - {:<10} {:<10} {:<10} {:<10} {} {} {:<10}".format(i, *line))
                        line = cursor.fetchone()
                        i += 1
                    if i > 1:
                        print("WARNING! There are possibly multiple LT instances currently connected to this database.")
            except Exception as e:
                print("Could not check databse connection. Printing error and continuing.")
                print(e)
        else:
            print("I do not know how to deal with database type {}.".format(connection.vendor))
        print("")
        time.sleep(2)

        # Actual command
        client_factory = SocketClientFactory()
        reactor.connectUNIX(settings.UNIX_OUT_SOCKET, client_factory)
        reactor.run()

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json

from django.core.management.base import BaseCommand

from lecturetranslator.session.models import Session
from ...serializers import SessionCreateUpdateSerializer


class Command(BaseCommand):

    def handle(self, *args, **options):
        data = dict(
            id=1814003514,
            title='my session 313',
            desc='this is a test session 32',
            input={
                'fingerprint': 'de-DE-lecture_KIT',
                'type': 'audio'
            },
            auth={
                'password': '12345xyz',
                'salt': 'salty'
            },
            source={
                'host': 'i13srv30.ira.uka.de',
                'port': 60019
            },
            meta={},
            extras={},
            params={
                'N': 'Y',
                'D': '2017-03-16 12:25',
                'S': 'GBI'
            },
            audio_file='/path/to/audio/file',
            streams=[
                dict(
                    id='de_text',
                    fingerprint='de',
                    displayname='German MT Text',
                    sid='speech',
                    control='*',
                    type='text',
                    path=[]
                ),
                dict(
                    id='hu_text',
                    fingerprint='hu',
                    displayname='Hungarian MT Text',
                    sid='speech',
                    control='l',
                    type='text',
                    path=[]
                ),
                dict(
                    id='es_text',
                    fingerprint='es',
                    displayname='Spanish MT Text',
                    sid='speech',
                    control='l',
                    type='text',
                    path=[]
                ),
                dict(
                    id='de_unseg-text',
                    fingerprint='de',
                    displayname='German Unsegmented Text',
                    sid='speech',
                    control='l',
                    type='unseg-text',
                    path=[]
                )
            ]
        )
        try:
            session = Session.objects.get(id=data['id'])
            serializer = SessionCreateUpdateSerializer(session, data=data)
        except Session.DoesNotExist:
            serializer = SessionCreateUpdateSerializer(data=data)
        if not serializer.is_valid():
            print("INVALID")
            return
        serializer.save()


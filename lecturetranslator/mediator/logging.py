#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os.path
from datetime import datetime

from django.conf import settings


def log_session_start(data):
    log(data['id'], 'SESSION START')


def log_data(data):
    log(data['session_id'], '{creator} ({start_timestamp} / {stop_timestamp}) : ({startoffset} / {stopoffset}) / {text}'.format(**data))


def log_session_end(data):
    log(data['id'], 'SESSION END\n')


def log(session_id, message):
    now = datetime.now()
    msg = '{}:: {}'.format(now.strftime('%H:%M:%S.%f'), message)
    if settings.MEDIATOR_PRINT_LOG:
        print(msg)
    logfile = os.path.join(settings.MEDIATOR_LOG_DIR, '{}.log'.format(session_id))
    try:
        with open(logfile, 'a+') as f:
            f.write(msg.encode('utf-8') + '\n')
    except:
        pass

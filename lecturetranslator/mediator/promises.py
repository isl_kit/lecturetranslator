#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class Deferred(object):
    def __init__(self):
        self.state = 0
        self.value = None
        self.promise = Promise(self)

    def resolve(self, value):
        if self.promise.set:
            self.promise.success_handler(value)
        self.value = value
        self.state = 1

    def reject(self, reason):
        if self.promise.set and self.promise.error_handler:
            self.promise.error_handler(reason)
        self.value = reason
        self.state = -1


class Promise(object):

    def __init__(self, deferred):
        self.deferred = deferred
        self.success_handler = None
        self.error_handler = None
        self.set = False

    def then(self, success_handler, error_handler=None):
        if self.deferred.state == 1:
            success_handler(self.deferred.value)
        elif self.deferred.state == -1 and error_handler:
            error_handler(self.deferred.value)
        self.success_handler = success_handler
        self.error_handler = error_handler
        self.set = True

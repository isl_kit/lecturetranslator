#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.db.utils import IntegrityError

from lecturetranslator.session.models import Session, DataSegment

from .serializers import SessionCreateUpdateSerializer, DataSegmentCreateUpdateSerializer
from .signals import session_create, session_destroy

def connect():
    Session.objects.filter(active=True, touched=True).update(touched=False)


def cleanup():
    sessions = Session.objects.filter(active=True, touched=False)
    for session in sessions:
        deactivate_session(session)


def touch_session(session):
    if not session.touched:
        session.touched = True
        session.save()


def activate_session(session):
    if session.active:
        return
    session.active = True
    session.save()
    session_create.send(sender=None, session=session)


def deactivate_session(session):
    if not session.active:
        return
    session.active = False
    session.save()
    session_destroy.send(sender=None, session=session)


def update_or_create_session(data):
    session_id = data["id"]
    try:
        session = Session.objects.get(id=session_id)
        serializer = SessionCreateUpdateSerializer(session, data=data)
    except Session.DoesNotExist:
        serializer = SessionCreateUpdateSerializer(data=data)
    if not serializer.is_valid():
        print("WARNING! Could not create/update session. Ignoring. Reason:")
        print(serializer.errors)
        return
    serializer.save()
    return serializer.data


def update_or_create_data_segment(data):
    kwargs = {
        'stream__ident': data["stream_id"],
        'stream__session_id': data["session_id"],
        'start_abs': data["start_timestamp"]
    }
    try:
        data_segment = DataSegment.objects.get(**kwargs)
#        if data_segment.created - data['created'] > 50000:
        if data_segment.created - data['created'] > 0:
            print("!!WARNING!! Gotten datasegment older than got before. Printing database and gotten segments")
            print("{}: c:{} v:{} +:{}: {}".format(data_segment, data_segment.created, data_segment.version, data_segment.offset, repr(data_segment.text)))
            print(repr(data))
            return
        serializer = DataSegmentCreateUpdateSerializer(data_segment, data=data)
    except DataSegment.DoesNotExist as e:
        serializer = DataSegmentCreateUpdateSerializer(data=data)
    if not serializer.is_valid():
        print("WARNING! Could not create DataSegment. Segment and errors will follow.")
        print(data)
        print(serializer.errors)
        return
    try:
        serializer.save()
    except IntegrityError as e:
        print("!Warning! Integrity error on creating/updating Data segment. Possibly multiple database users?")
        return update_or_create_data_segment(data, c=c+1)
    return serializer.data

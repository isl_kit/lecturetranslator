#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json

from django.conf import settings
from django.dispatch import Signal, receiver

from socketiodjangoapp.api import emit_to

from lecturetranslator.session.serializers import (
    SessionBroadcastSerializer,
    DataSegmentListSerializer
)

from .events import stream_action


session_create = Signal(providing_args=['session'])
session_destroy = Signal(providing_args=['session'])
session_update = Signal(providing_args=['session'])

data_segment_create = Signal(providing_args=['data_segment'])
data_segment_update = Signal(providing_args=['data_segment'])


@receiver(session_create, dispatch_uid='mediator_session_create')
def session_create_handler(sender, session, **kwargs): #pylint: disable=W0613
    apply_autosub(session)
    if session.hidden and not settings.SHOW_HIDDEN_SESSIONS:
        return
    data = SessionBroadcastSerializer(session).data
    emit_to({
        'type': 'sessions_add',
        'data': data
    }, 'sessions')


@receiver(session_update, dispatch_uid='mediator_session_update')
def session_update_handler(sender, session, **kwargs): #pylint: disable=W0613
    apply_autosub(session)
    if session.hidden and not settings.SHOW_HIDDEN_SESSIONS:
        return
    data = SessionBroadcastSerializer(session).data
    emit_to({
        'type': 'sessions_add',
        'data': data
    }, 'sessions')

@receiver(session_destroy, dispatch_uid='mediator_session_destroy')
def session_destroy_handler(sender, session, **kwargs): #pylint: disable=W0613
    if session.hidden and not settings.SHOW_HIDDEN_SESSIONS:
        return
    data = SessionBroadcastSerializer(session).data
    emit_to({
        'type': 'sessions_end',
        'data': data
    }, 'sessions')
    emit_to({
        'type': 'session_end',
    }, 's{}'.format(session.id))


@receiver(data_segment_create, dispatch_uid='mediator_data_segment_create')
def data_segment_create_handler(sender, data_segment, **kwargs): #pylint: disable=W0613
    data = DataSegmentListSerializer(data_segment).data
    emit_to({
        'type': 'data',
        'data': data
    }, 's{}:{}'.format(data_segment.stream.session_id, data_segment.stream.ident))


@receiver(data_segment_update, dispatch_uid='mediator_data_segment_update')
def data_segment_update_handler(sender, data_segment, **kwargs): #pylint: disable=W0613
    data = DataSegmentListSerializer(data_segment).data
    emit_to({
        'type': 'data',
        'data': data
    }, 's{}:{}'.format(data_segment.stream.session_id, data_segment.stream.ident))


def apply_autosub(session):
    params = json.loads(session.params)
    if 'SUB' not in params:
        return
    language_tags = params['SUB'].split(',')
    for stream in session.streams.filter(type='text'):
        if stream.fingerprint.split('-')[0] not in language_tags:
            continue
        stream_action('join', 'AUTOSUB', 's{}:{}'.format(session.id, stream.ident))

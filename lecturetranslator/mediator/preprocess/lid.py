#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json

from socketiodjangoapp.api import get_sockets_of_rooms

from lecturetranslator.session.models import Session

from ..msocket import m_socket

from .preprocess import preproc, PreProcTask


class LIDTask(PreProcTask):

    name = 'LIDTask'

    def _get_transcript_stream(self, session):
        for stream in session['streams']:
            langs = stream['fingerprint'].split('-')[0]
            base = langs[0:2]
            target = langs[2:]
            if base == target and stream['type'] == 'text':
                return stream
        return None

    def session_create(self, session):
        session['meta']['multi'] = False
        input_fingerprint = session['input']['fingerprint']
        if input_fingerprint.split('-')[0] != 'mx':
            return session
        transcript_stream = self._get_transcript_stream(session)
        if not transcript_stream:
            return None
        session['extras']['multi'] = True
        session['meta']['multi'] = True
        session['meta']['current_base'] = transcript_stream['fingerprint'][:2]
        session['meta']['first_base'] = session['meta']['current_base']
        lid_streams = [s for s in session['streams'] if s['fingerprint'] == 'mx']
        session['meta']['lid_stream'] = lid_streams[0] if len(lid_streams) > 0 else None
        session['meta']['lid_hash_forw'] = {}
        session['meta']['lid_hash_back'] = {}
        session['meta']['raw_streams'] = session['streams']
        session['meta']['mx_counter'] = 0
        all_targets = []
        streams = []
        for stream in session['streams']:
            if stream == session['meta']['lid_stream']:
                session['meta']['lid_hash_back'][stream['id']] = stream['id']
                session['meta']['lid_hash_forw'][stream['id']] = stream['id']
                streams.append(stream)
                continue
            langs = stream['fingerprint'].split('-')[0]
            base = langs[0:2]
            target = langs[2:] if stream['type'] != 'unseg-text' else session['meta']['first_base']

            new_stream_id = '_'.join([target, stream['sid'], stream['type']])
            session['meta']['lid_hash_forw'][stream['id']] = new_stream_id

            if session['meta']['current_base'] == base:
                session['meta']['lid_hash_back'][new_stream_id] = stream['id']
            stream_key = target + ':' + stream['type']
            if stream_key not in all_targets:
                all_targets.append(stream_key)
                streams.append({
                    'id': new_stream_id,
                    'fingerprint': target,
                    'type': stream['type'],
                    'sid': stream['sid'],
                    'displayname': 'Multi (%s/%s)' % (target, stream['type']),
                    'control': '*' if stream == transcript_stream else 'l',
                    'autosub': False,
                    'path': []
                })
        session['streams'] = streams
        if session['meta']['lid_stream']:
            socketid, session_id, stream_id = preproc.join(
                'LID' + str(session['id']),
                session['id'],
                session['meta']['lid_stream']['id'],
                exclude=['LIDTask']
            )
            m_socket.send(json.dumps({
                'action': 'join',
                'client_id': socketid,
                'session_id': session_id,
                'stream_id': stream_id
            }))
        return session

    def data_create(self, data):
        try:
            session = Session.objects.get(id=data['session_id'])
        except Session.DoesNotExist:
            return data
        meta = json.loads(session.meta)
        if not meta['multi']:
            return data
        if meta['lid_stream'] and data['stream_id'] == meta['lid_stream']['id']:
            new_base = data['text'].strip().lower()[:2]
            if new_base != '<b' and new_base != meta['current_base']:
                old_transcript_stream = self._get_orig_by_new_id(
                    session.id, session.transcript_stream.stream_id
                )
                new_transcript_stream = old_transcript_stream
                meta['current_base'] = new_base
                lid_hash_back = {}
                swap_hash = {}
                for stream in meta['raw_streams']:
                    langs = stream['fingerprint'].split('-')[0]
                    base = langs[0:2]
                    target = langs[2:] if stream['type'] != 'unseg-text' else meta['first_base']
                    new_stream_id = '_'.join([target, stream['sid'], stream['type']])
                    if new_base == base:
                        lid_hash_back[new_stream_id] = stream['id']
                        swap_hash[new_stream_id] = {
                            'old': meta['lid_hash_back'].get(new_stream_id, None),
                            'new': stream['id']
                        }
                        if target == base and stream['type'] == 'text':
                            session.transcript_stream_id = '%d:%s' % (session.id, new_stream_id)
                            new_transcript_stream = stream['id']
                meta['lid_hash_back'] = lid_hash_back
                meta['mx_counter'] += 1
                data['start_timestamp'] += meta['mx_counter']
                session.meta = json.dumps(meta)
                session.save()
                self._rejoin_streams(
                    session.id, swap_hash, old_transcript_stream, new_transcript_stream
                )

        else:
            data['stream_id'] = meta['lid_hash_forw'][data['stream_id']]
        return data

    def join(self, client_id, session_id, stream_id):
        orig_id = self._get_orig_by_new_id(session_id, stream_id)
        if not orig_id:
            return (client_id, session_id, stream_id)
        return (client_id, session_id, orig_id)

    def leave(self, client_id, session_id, stream_id):
        orig_id = self._get_orig_by_new_id(session_id, stream_id)
        if not orig_id:
            return (client_id, session_id, stream_id)
        return (client_id, session_id, orig_id)

    def _get_orig_by_new_id(self, session_id, new_id):
        try:
            session = Session.objects.get(id=session_id)
        except Session.DoesNotExist:
            return None
        meta = json.loads(session.meta)
        if meta['multi']:
            return meta['lid_hash_back'][new_id]
        return None

    def _rejoin_streams(self, session_id, swap_hash, old_transcript_stream, new_transcript_stream):
        rooms = []
        rooms_hash = {}
        for stream_id, obj in swap_hash.items():
            room_name = 's%d:%s' % (session_id, stream_id)
            rooms.append(room_name)
            rooms_hash[room_name] = obj
        sockets_in_rooms = get_sockets_of_rooms(rooms)
        socketid_leave, session_id_leave, stream_id_leave = preproc.leave(
            'LID' + str(session_id),
            session_id,
            old_transcript_stream,
            exclude=['LIDTask']
        )
        m_socket.send(json.dumps({
            'action': 'leave',
            'client_id': socketid_leave,
            'session_id': session_id_leave,
            'stream_id': stream_id_leave
        }))
        socketid_join, session_id_join, stream_id_join = preproc.join(
            'LID' + str(session_id),
            session_id,
            new_transcript_stream,
            exclude=['LIDTask']
        )
        m_socket.send(json.dumps({
            'action': 'join',
            'client_id': socketid_join,
            'session_id': session_id_join,
            'stream_id': stream_id_join
        }))
        for room, sockets in sockets_in_rooms.items():
            for socket_id in sockets:
                if rooms_hash[room]['old']:
                    socketid_leave, session_id_leave, stream_id_leave = preproc.leave(
                        socket_id, session_id, rooms_hash[room]['old'], exclude=['LIDTask']
                    )
                    m_socket.send(json.dumps({
                        'action': 'leave',
                        'client_id': socketid_leave,
                        'session_id': session_id_leave,
                        'stream_id': stream_id_leave
                    }))
                socketid_join, session_id_join, stream_id_join = preproc.join(
                    socket_id, session_id, rooms_hash[room]['new'], exclude=['LIDTask']
                )
                m_socket.send(json.dumps({
                    'action': 'join',
                    'client_id': socketid_join,
                    'session_id': session_id_join,
                    'stream_id': stream_id_join
                }))

#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf import settings

from .preprocess import PreProcTask


class FilterTask(PreProcTask):

    name = "FilterTask"

    def session_create(self, session):
        if not settings.FILTER_SESSIONS(session):
            print("Filtering session {}".format(session))
            return None
        print("Showing session {}".format(session))
        return session

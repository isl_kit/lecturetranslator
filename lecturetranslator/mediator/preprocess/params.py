#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .preprocess import PreProcTask


class ParamsTask(PreProcTask):

    name = "ParamsTask"

    def session_create(self, session):
        title = session["title"]
        title_parts = title.split("?")
        session["title"] = title_parts[0]
        session["params"] = {}
        session["meta"] = {}
        for param in title_parts[1:]:
            param_parts = param.split("=")
            name = param_parts[0]
            value = param_parts[1] if len(param_parts) > 1 else True
            session["params"][name] = value
        return session

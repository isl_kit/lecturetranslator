#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .preprocess import preproc
from .compound import CompoundTask
from .filter import FilterTask
from .params import ParamsTask
from .lid import LIDTask
from .offline import OfflineTask

preproc.add_task(ParamsTask())
preproc.add_task(FilterTask())
preproc.add_task(CompoundTask())
preproc.add_task(LIDTask())
#preproc.add_task(OfflineTask())


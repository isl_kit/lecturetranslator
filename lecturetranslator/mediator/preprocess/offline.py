#!/usr/bin/env python
# -*- coding: utf-8 -*-

from lecturetranslator.session.models import Session, DataSegment

from .preprocess import PreProcTask


class OfflineTask(PreProcTask):

    name = "OfflineTask"

    def session_delete(self, session_data):
        try:
            session = Session.objects.get(id=session_data["id"])
        except Session.DoesNotExist:
            return session_data
        if not self.sanity_check(session):
            return session_data
        if session.event and session.event == session.offline_event:
            session.offline_to_online()
            session.make_default()
            if session.offline_event.media_src:
                session.ext_video_url = session.offline_event.media_src
                session.save()
        return session_data

    def sanity_check(self, session):
        if not session.event or session.event != session.offline_event:
            return False
        old_session = session.offline_event.origin.session
        if not session.transcript_stream:
            return False
        if not old_session.transcript_stream:
            return True
        if DataSegment.objects.filter(stream=old_session.transcript_stream).count() == 0:
            return True
        try:
            first_d1 = DataSegment.objects.filter(stream=session.transcript_stream) \
                .order_by("start").first().start
            last_d1 = DataSegment.objects.filter(stream=session.transcript_stream) \
                .order_by("start").last().start
            first_d2 = DataSegment.objects.filter(stream=old_session.transcript_stream) \
                .order_by("start").first().start
            last_d2 = DataSegment.objects.filter(stream=old_session.transcript_stream) \
                .order_by("start").last().start
        except DataSegment.DoesNotExist:
            return False
        delta1 = last_d1 - first_d1
        delta2 = last_d2 - first_d2
        if delta1 - delta2 < -1200000 and delta1 < 4200000:
            return False
        return True

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import random
from threading import Timer
from functools import partial

from socketiodjangoapp.api import get_sockets_of_rooms

from lecturetranslator.session.models import Session

from ..msocket import m_socket
from ..promises import Deferred

from .preprocess import preproc, PreProcTask


class CompoundTask(PreProcTask):

    name = 'CompoundTask'

    def __init__(self):
        self.timer = {}
        self.id_map_forw = {}

    def session_create(self, session):
        session['meta']['compound'] = False
        if 'C' not in session['params']:
            return session
        try:
            s = Session.objects.filter(compound_key=session['params']['C']).latest('created')
            new_id = s.id
            append = True
        except Session.DoesNotExist:
            new_id = random.getrandbits(62) + 1
            append = False
        session['meta']['compound'] = True
        session['meta']['orig_id'] = session['id']
        session['id'] = new_id
        self._add_to_id_map(session['meta']['orig_id'], new_id)
        if new_id in self.timer:
            self.timer[new_id].cancel()
            del self.timer[new_id]
        if append:
            self._rejoin_streams(session)

        return session

    def session_delete(self, session):
        new_id = self._get_new_by_orig_id(session['id'])
        if new_id:
            deferred = Deferred()
            callback = partial(self.session_delete_finish, {'id': new_id}, deferred)
            self.timer[new_id] = Timer(60, callback)
            self.timer[new_id].daemon = True
            self.timer[new_id].start()
            return deferred.promise
        return session

    def session_delete_finish(self, session, deferred):
        session = preproc.session_delete(session, exclude=['CompoundTask'])
        deferred.resolve(session)

    def data_create(self, data):
        new_id = self._get_new_by_orig_id(data['session_id'])
        if new_id:
            try:
                session = Session.objects.get(id=new_id)
                meta = json.loads(session.meta)
                if meta['compound']:
                    data['session_id'] = new_id
            except Session.DoesNotExist:
                return None
        return data

    def join(self, client_id, session_id, stream_id):
        orig_id = self._get_orig_by_new_id(session_id)
        if not orig_id:
            return (client_id, session_id, stream_id)
        return (client_id, orig_id, stream_id)

    def leave(self, client_id, session_id, stream_id):
        orig_id = self._get_orig_by_new_id(session_id)
        if not orig_id:
            return (client_id, session_id, stream_id)
        return (client_id, orig_id, stream_id)

    def _add_to_id_map(self, orig_id, new_id):
        self.id_map_forw[orig_id] = new_id

    def _get_orig_by_new_id(self, new_id):
        try:
            session = Session.objects.get(id=new_id)
        except Session.DoesNotExist:
            return None
        meta = json.loads(session.meta)
        if meta['compound']:
            return meta['orig_id']
        return None

    def _get_new_by_orig_id(self, orig_id):
        return self.id_map_forw.get(orig_id, None)

    def _rejoin_streams(self, session):
        rooms = []
        rooms_hash = {}
        for stream in session['streams']:
            room_name = 's{}:{}'.format(session['id'], stream['id'])
            rooms.append(room_name)
            rooms_hash[room_name] = stream['id']
        sockets_in_rooms = get_sockets_of_rooms(rooms)
        for room, sockets in sockets_in_rooms.items():
            for socket_id in sockets:
                stream_id = rooms_hash[room]
                m_socket.send(json.dumps({
                    'action': 'join',
                    'client_id': socket_id,
                    'session_id': session['meta']['orig_id'],
                    'stream_id': stream_id
                }))

#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ..promises import Promise


class PreProcessor(object):

    def __init__(self):
        self.tasks = []

    def add_task(self, task):
        self.tasks.append(task)

    def session_create(self, session, exclude=[]):
        for task in self.tasks:
            if task.name in exclude:
                continue
            session = task.session_create(session)
            if not session:
                return None
        return session

    def session_delete(self, session, exclude=[]):
        for task in self.tasks:
            if task.name in exclude:
                continue
            session = task.session_delete(session)
            if not session:
                return None
            if type(session) is Promise:
                break
        return session

    def data_create(self, data, exclude=[]):
        for task in self.tasks:
            if task.name in exclude:
                continue
            data = task.data_create(data)
            if not data:
                return None
        return data

    def join(self, client_id, session_id, stream_id, exclude=[]):
        for task in self.tasks:
            if task.name in exclude:
                continue
            client_id, session_id, stream_id = task.join(client_id, session_id, stream_id)
        return (client_id, session_id, stream_id)

    def leave(self, client_id, session_id, stream_id, exclude=[]):
        for task in self.tasks:
            if task.name in exclude:
                continue
            client_id, session_id, stream_id = task.leave(client_id, session_id, stream_id)
        return (client_id, session_id, stream_id)

    def disconnect(self, client_id, exclude=[]):
        for task in self.tasks:
            if task.name in exclude:
                continue
            client_id = task.disconnect(client_id)
        return client_id


class PreProcTask(object):

    def session_create(self, session):
        return session

    def session_delete(self, session):
        return session

    def data_create(self, data):
        return data

    def join(self, client_id, session_id, stream_id):
        return (client_id, session_id, stream_id)

    def leave(self, client_id, session_id, stream_id):
        return (client_id, session_id, stream_id)

    def disconnect(self, client_id):
        return client_id

preproc = PreProcessor()

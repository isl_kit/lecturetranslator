#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import re
from datetime import datetime

from django.conf import settings
from django.db.models import Max

from rest_framework.serializers import (
    CharField,
    DateTimeField,
    DictField,
    IntegerField,
    Serializer,
    ValidationError
)

from lecturetranslator.authentication.models import AuthEntity
from lecturetranslator.event.models import Event
from lecturetranslator.mediaprocessor.models import MediaEvent
from lecturetranslator.session.models import Session, SessionPart, Stream, DataSegment, ComputeNode, ComputeStep
from lecturetranslator.session.serializers import DataSegmentListSerializer, SessionDetailSerializer

from .signals import session_create, session_update, data_segment_create, data_segment_update


class SessionInputSerializer(Serializer):
    fingerprint = CharField(max_length=200)
    type = CharField(max_length=50)


class SessionSourceSerializer(Serializer):
    host = CharField(max_length=50, allow_blank=True)
    port = IntegerField()


class ComputeNodeCreateUpdateSerializer(Serializer):
    name = CharField(max_length=200)
    creator = CharField(max_length=100)
    input_fingerprint = CharField(max_length=200)
    input_type = CharField(max_length=50)
    output_fingerprint = CharField(max_length=200)
    output_type = CharField(max_length=50)


class ComputeStepCreateUpdateSerializer(Serializer):
    name = CharField(max_length=200)
    type = CharField(max_length=50)
    fingerprint = CharField(max_length=200)
    node = ComputeNodeCreateUpdateSerializer(required=False, allow_null=True)


class StreamCreateUpdateSerializer(Serializer):
    id = CharField(max_length=251)
    fingerprint = CharField(max_length=200)
    displayname = CharField(max_length=200)
    sid = CharField(max_length=50)
    control = CharField(max_length=50)
    type = CharField(max_length=50)
    path = ComputeStepCreateUpdateSerializer(many=True)

    def validate(self, data):
        data['ident'] = data.pop('id')
        data['label'] = settings.GET_STREAM_LABEL(data['fingerprint'])
        return data


class SessionCreateUpdateSerializer(Serializer):
    id = IntegerField()
    title = CharField(max_length=200)
    desc = CharField(allow_blank=True)
    input = SessionInputSerializer()
    password = CharField(allow_blank=True)
    # This next field is overwriting a DRF field but we cannot easily change it
    source = SessionSourceSerializer() # type: ignore
    meta = DictField()
    extras = DictField()
    params = DictField()
    audio_file = CharField(max_length=4096, allow_blank=True)
    streams = StreamCreateUpdateSerializer(many=True)

    def validate(self, data):
        params = data['params']
        media_event = None
        event = None
        video_url = ''
        if 'S' in params and 'D' in params:
            event = self.get_event(params['S'], params['D'])
        elif 'ME' in params:
            media_event = self.get_media_event(int(params['ME']))
            if media_event is not None and media_event.media_type == MediaEvent.URL:
                video_url = media_event.path
            if media_event is not None:
                event = media_event.event
        source = '{}:{}'.format(data['source']['host'], data['source']['port'])
        password = data['password']
        password = password if password and password != settings.SESSION_DEFAULT_PASSWORD else None
        has_transcript = False
        for stream_data in data['streams']:
            has_transcript = has_transcript or self.is_transcript(
                data['input']['fingerprint'], stream_data['fingerprint'], stream_data['type']
            )
        if not has_transcript:
            raise ValidationError('No transcript stream found!')
        return dict(
            id=data['id'],
            title=data['title'],
            desc=data['desc'],
            input_fingerprint=data['input']['fingerprint'],
            input_type=data['input']['type'],
            source=source,
            compound_key=params.get('C', ''),
            meta=json.dumps(data['meta']),
            extras=json.dumps(data['extras']),
            params=json.dumps(data['params']),
            hidden=params.get('INV', False),
            audio_file=data['audio_file'],
            event=event,
            media_event=media_event,
            streams=data['streams'],
            password=password,
            ext_video_url=video_url
        )

    def fingerprint_match(self, f1, f2):
        return f1.split('-')[0].lower() == f2.split('-')[0].lower()

    def is_transcript(self, input_fingerprint, fingerprint, type):
        return type == 'text' and self.fingerprint_match(input_fingerprint, fingerprint)

    def get_event(self, slug, date_string):
        try:
            date = datetime.strptime(date_string, '%Y-%m-%d %H:%M')
        except ValueError:
            return None
        try:
            return Event.objects.select_related(
                'lecture_term',
                'lecture_term__lecture',
                'lecture_hall',
            ) \
            .get(start=date, lecture_term__lecture__slug=slug)
        except Event.DoesNotExist:
            return None

    def get_media_event(self, id):
        try:
            return MediaEvent.objects.get(id=id)
        except MediaEvent.DoesNotExist:
            return None

    def clear_compute_items(self, stream):
        ComputeNode.objects.filter(compute_steps__stream=stream).delete()
        ComputeStep.objects.filter(stream=stream).delete()

    def set_compute_items(self, stream, steps_data):
        for index, step_data in enumerate(steps_data):
            node_data = step_data.pop('node')
            if node_data:
                node = ComputeNode.objects.create(**node_data)
            else:
                node = None
            ComputeStep.objects.create(index=index, node=node, stream=stream, **step_data)

    def get_auth_entity(self, password):
        if password is None:
            return None
        auth = AuthEntity()
        auth.set_password(password)
        auth.save()
        return auth

    def create(self, validated_data):
        streams_data = validated_data.pop('streams')
        password = validated_data.pop('password')
        media_event = validated_data.pop('media_event')
        event = validated_data['event']
        if event:
            auth = event.lecture_term.auth
        else:
            auth = self.get_auth_entity(password)
        session = Session.objects.create(auth=auth, **validated_data)
        transcript_stream = None
        for stream_data in streams_data:
            steps_data = stream_data.pop('path')
            stream = Stream.objects.create(session=session, **stream_data)
            is_transcript = self.is_transcript(
                session.input_fingerprint, stream.fingerprint, stream.type
            )
            if transcript_stream is None and is_transcript:
                transcript_stream = stream
            self.set_compute_items(stream, steps_data)
        session.transcript_stream = transcript_stream
        session.save()
        if media_event:
            media_event.session = session
            media_event.save()
        elif event and not event.session:
            event.session = session
            event.save()
        self.create_session_part(session)
        session_create.send(sender=self.__class__, session=session)
        return session

    def update(self, session, validated_data):
        streams_data = validated_data.pop('streams')
        password = validated_data.pop('password')
        event = validated_data['event']
        if event:
            auth = event.lecture_term.auth
        else:
            auth = self.get_auth_entity(password)
        transcript_stream = None
        for stream_data in streams_data:
            steps_data = stream_data.pop('path')
            try:
                stream = Stream.objects.get(
                    session=session,
                    type=stream_data['type'],
                    fingerprint=stream_data['fingerprint']
                )
                for key, val in list(stream_data.items()):
                    setattr(stream, key, val)
                stream.save()
                self.clear_compute_items(stream)
            except Stream.DoesNotExist:
                stream = Stream.objects.create(session=session, **stream_data)
            is_transcript = self.is_transcript(
                session.input_fingerprint, stream.fingerprint, stream.type
            )
            if transcript_stream is None and is_transcript:
                transcript_stream = stream
            self.set_compute_items(stream, steps_data)
        session.auth = auth
        session.touched = True
        session.active = True
        session.transcript_stream = transcript_stream
        for key, val in list(validated_data.items()):
            setattr(session, key, val)
        session.save()
        if event and not event.session:
            event.session = session
            event.save()
        self.create_session_part(session)
        session_update.send(sender=self.__class__, session=session)
        return session

    def create_session_part(self, session):
        last_start = get_last_start(session.transcript_stream)
        try:
            sessionPart = SessionPart.objects.get(session=session, start_greater=last_start)
            sessionPart.audio_file = session.audio_file
            sessionPart.save()
        except SessionPart.DoesNotExist:
            SessionPart.objects.create(
                session=session,
                audio_file=session.audio_file,
                start_greater=last_start
            )

    def to_representation(self, obj):
        return SessionDetailSerializer(obj).data


class DataSegmentCreateUpdateSerializer(Serializer):
    stream_id = CharField(max_length=251)
    session_id = IntegerField()
    text = CharField()
    start_timestamp = IntegerField()
    stop_timestamp = IntegerField()
    startoffset = IntegerField()
    stopoffset = IntegerField()
    creator = CharField(max_length=100)
    created = IntegerField()

    def validate(self, data):
        try:
            stream = Stream.objects.select_related('session').get(
                ident=data['stream_id'],
                session_id=data['session_id']
            )
        except Stream.DoesNotExist:
            raise ValidationError('Stream does not exist!')
        session = stream.session
        is_from_transcript = session.transcript_stream_id == stream.id
        if not is_from_transcript and session.start == 0:
            raise ValidationError('Missing initial transcript data!')
        if is_from_transcript and session.start == 0:
            session.start = data['start_timestamp'] - data['startoffset']
            session.save()
        return {
            'stream': stream,
            'start_abs': data['start_timestamp'],
            'stop_abs': data['stop_timestamp'],
            'start': data['start_timestamp'] - session.start,
            'stop': data['stop_timestamp'] - session.start,
            'offset': data['startoffset'],
            'end_of_paragraph': self.is_end_of_paragraph(data['text']),
            'text': self.sanitize_text(data['text']),
            'creator': data['creator'],
            'created': data['created']
        }

    def is_end_of_paragraph(self, text):
        return False

    def sanitize_text(self, text):
        return re.sub(r' {2,}', ' ', text.replace('<br><br>', '<hr>').replace('<br>', '<hr>')).strip()

    def create(self, validated_data):
        data_segment = DataSegment.objects.create(**validated_data)
        data_segment_create.send(sender=self.__class__, data_segment=data_segment)
        return data_segment

    def update(self, data_segment, validated_data):
        data_segment = DataSegment.objects.get(id=data_segment.id)
        data_segment.stop = validated_data['stop']
        data_segment.stop_abs = validated_data['stop_abs']
        data_segment.text = validated_data['text']
        data_segment.end_of_paragraph = validated_data['end_of_paragraph']
        data_segment.creator = validated_data['creator']
        data_segment.created = validated_data['created']
        data_segment.version += 1
        data_segment.save()
        data_segment_update.send(sender=self.__class__, data_segment=data_segment)
        return data_segment

    def to_representation(self, obj):
        return DataSegmentListSerializer(obj).data


def get_last_start(stream):
    m = DataSegment.objects \
        .filter(stream=stream) \
        .aggregate(max_start=Max('start_abs'))
    return m['max_start'] if m['max_start'] is not None else 0

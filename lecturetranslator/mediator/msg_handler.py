#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from lecturetranslator.session.models import Session

from .logging import log_session_start, log_session_end, log_data
from .preprocess.preprocess import preproc
from .db_handler import update_or_create_session, deactivate_session, update_or_create_data_segment
from .promises import Promise


def handle_message(msgtype, data):
    if msgtype == 0:
        if isinstance(data, list):
            for d in data:
                handle_session_create(d)
        else:
            handle_session_create(data)
    elif msgtype == 1:
        handle_session_delete(data)
    elif msgtype == 2:
        handle_new_data(data)


def handle_session_create(session_data):
    log_session_start(session_data)
    session_data["extras"] = {}
    session_data = preproc.session_create(session_data)
    if not session_data:
        return
    update_or_create_session(session_data)


def handle_session_delete(session_data):
    log_session_end(session_data)
    session_data = preproc.session_delete(session_data)
    if not session_data:
        return
    if isinstance(session_data, Promise):
        session_data.then(finish_session_delete)
        return
    finish_session_delete(session_data)


def finish_session_delete(session_data):
    try:
        session = Session.objects.get(id=session_data["id"])
        deactivate_session(session)
    except Session.DoesNotExist:
        pass


def handle_new_data(data):
    log_data(data)
    data = preproc.data_create(data)
    if not data:
        return
    update_or_create_data_segment(data)

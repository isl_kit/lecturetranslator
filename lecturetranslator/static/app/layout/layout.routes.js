angular
    .module("app.layout")
    .config(layoutRoutes);

/*@ngInject*/
function layoutRoutes($stateProvider, authProvider) {

    $stateProvider.state("main", {
        url: "/",
        abstract: true,
        views: {
            "main-body": {
                controller: "MainLayoutController",
                controllerAs: "vm",
                templateUrl: "layout/templates/main" + (window.IE === 11 ? "_legacy" : window.IE > 0 ? "_unsupported" : "" ) + ".tpl.html"
            }
        }
    });

    $stateProvider.state("main.welcome", {
        url: "",
        views: {
            "main-content": {
                controller: "WelcomeController",
                controllerAs: "vm",
                templateUrl: "layout/templates/welcome.tpl.html"
            }
        }
    });

    $stateProvider.state("main.login", {
        url: "login",
        views: {
            "main-content": {
                controller: "LoginController",
                controllerAs: "vm",
                templateUrl: "layout/templates/login.tpl.html"
            }
        },
        data: {
        },
        resolve: {
            authenticated: authProvider.setRules({
                login: false
            })
        }
    });

    $stateProvider.state("main.contact", {
        url: "contact",
        data: {
            title: "Lecture Translator - Contact"
        },
        views: {
            "main-content": {
                controller: "ContactController",
                controllerAs: "vm",
                templateUrl: "layout/templates/contact.tpl.html"
            }
        }
    });

    $stateProvider.state("main.legals", {
        url: "legals",
        data: {
            title: "Lecture Translator - Legals"
        },
        views: {
            "main-content": {
                controller: "LegalsController",
                controllerAs: "vm",
                templateUrl: "layout/templates/legals.tpl.html"
            }
        }
    });
}

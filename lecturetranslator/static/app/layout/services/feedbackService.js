angular
    .module("app.layout")
    .factory("feedbackService", feedbackService);

/*@ngInject*/
function feedbackService($http) {

    var service = {
        sendFeedback: sendFeedback,
        verifyFeedback: verifyFeedback
    };

    return service;

    //////////

    function sendFeedback(data) {
        return $http.post("/contact/sendFeedback/", data).then(function (response) {
            return response.data.tid;
        });
    }

    function verifyFeedback(tid) {
        return $http.get("/contact/verifyFeedback/?tid=" + tid);
    }
}

angular
    .module('app.layout')
    .directive("selectNgFiles", function() {
  return {
    require: "ngModel",
    link: function postLink(scope,elem,attrs,ngModel) {
      elem.on("change", function(e) {
        console.log("scope", scope, ngModel, elem);
        var files = elem[0].files;
          console.log(files);
        ngModel.$setViewValue(files);
    });
    }
};
});

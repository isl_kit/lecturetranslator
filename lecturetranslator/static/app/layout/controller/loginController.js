angular
    .module("app.layout")
    .controller("LoginController", LoginController);

/*@ngInject*/
function LoginController($scope, $state, auth) {
    var vm = this;

    vm.formData = {
        name: "",
        password: ""
    };
    vm.login = login;

    //////////

    function login(valid) {
        auth.login(vm.formData).then(function () {
            $state.go("main.welcome");
        }, function () {
            $scope.loginForm.password.$setValidity("authenticated", false);
        });
    }
}

angular
    .module("app.layout")
    .controller("ContactController", ContactController);

/*@ngInject*/
function ContactController($scope, $state, $mdDialog, feedbackService) {
    var vm = this;

    vm.topicOptions = [
        { id: 0, label: "Feedback"},
        { id: 1, label: "Questions"},
        { id: 2, label: "Problems"},
        { id: 3, label: "Improvements & Ideas"},
        { id: 4, label: "Other"}
    ];
    vm.formData = {
        name: "",
        email: "",
        message: "",
        topic: 0
    };
    vm.send = send;

    //////////

    function send(valid) {
        if (!valid) {
            return;
        }

        feedbackService.sendFeedback({
            name: vm.formData.name,
            email: vm.formData.email,
            message: vm.formData.message,
            topic: vm.topicOptions[vm.formData.topic].label
        }).then(function (tid) {
            $mdDialog.show($mdDialog.alert()
                .parent(angular.element(document.body))
                .clickOutsideToClose(true)
                .title("Thank you!")
                .textContent("We have received your feedback and will get back to you as soon as possible.")
                .ok("Close")
            ).then(function () {
                clearForm();
            });
        });
    }

    function clearForm() {
        vm.formData.name = "";
        vm.formData.email = "";
        vm.formData.message = "";
        vm.formData.topic = 0;
        $scope.contactForm.$setPristine();
        $scope.contactForm.$setUntouched();
        console.log($scope.contactForm);
    }
}

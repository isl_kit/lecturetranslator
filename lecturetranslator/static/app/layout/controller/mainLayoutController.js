angular
    .module("app.layout")
    .controller("MainLayoutController", MainLayoutController);

/*@ngInject*/
function MainLayoutController($scope, $state, $transitions, $location, sessionService, Session, auth, recordingService) {
    var vm = this;

    var sessions = {}, sessionId = null;

    vm.auth = auth;

    vm.recording = false;
    vm.paused = false;
    vm.meter = null;
    vm.initRecording = false;
    vm.recSessionReady = false;

    vm.logout = logout;
    vm.startSession = startSession;
    vm.goToRecording = goToRecording;
    vm.togglePause = togglePause;
    vm.stop = stop;

    vm.searchExpanded = false;
    vm.startExpanded = false;
    vm.goToEdit = goToEdit;
    vm.canActivateEdit = canActivateEdit;

    activate();

    //////////

    function activate() {
        $scope.$on("rec.stateChange", recordingStateChangeHandler);
        //$scope.$on("$stateChangeSuccess", stateChangeSuccessHandler);
        $transitions.onSuccess({}, stateChangeSuccessHandler);
        $scope.$on("sessions.add", sessionsAddHandler);
    }

    function logout() {
        auth.logout().then(function () {
            $state.go($state.current, {}, {reload: true});
        });
    }

    function stateChangeSuccessHandler() {
        vm.searchExpanded = false;
        vm.startExpanded = false;
    }

    function recordingStateChangeHandler(evt, type, param) {
        if (type === "start-session") {
            vm.meter = recordingService.getMeter();
            vm.recording = true;
            vm.paused = false;
        } else if (type === "stop-session") {
            vm.recording = false;
            vm.meter = null;
            sessionId = null;
            vm.recSessionReady = false;
        } else if (type === "set-pause") {
            vm.paused = param;
        }
    }

    function startSession(fingerprint) {
        if (vm.recording) {
            return;
        }
        recordingService.startSession("DemoSession", {}, "", fingerprint, "", false).then(function (id) {
            sessionId = id;
            checkSession();
        });
        vm.initRecording = true;
    }

    function checkSession() {
        var recSessionId = recordingService.getSessionId();
        if (recSessionId && _.has(sessionService.sessions, recSessionId)) {
            if (sessionId === recSessionId) {
                $state.go("main.session.stream", {s:sessionId});
                sessionId = null;
                vm.initRecording = false;
            }
            vm.recSessionReady = true;
        }
    }

    function goToRecording() {
        $state.go("main.record");
    }

    function togglePause() {
        recordingService.setPause(!recordingService.isPaused());
    }

    function stop() {
        recordingService.stopSession();
    }

    function sessionsAddHandler(event, sessions) {
        checkSession();
    }

    function goToEdit() {
        $state.go('^.edit');
    }

    function canActivateEdit() {
        return !!$state.get('^.edit');
    }
}

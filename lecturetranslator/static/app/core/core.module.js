angular.module("app.core", [
    /* Angular modules */
    "ngAnimate",
    "ngCookies",
    "ngSanitize",
    "ngMaterial",
    "ngMessages",

    /* Application configuration */
    "app.config",

    /* Data Access */
    "app.data",

    /* Authentication */
    "app.auth",

    /* common: essentials */
    "co.exception",
    "co.logging",
    "co.storage",
    "co.helper",

    /* common: behaviour */
    "co.recording",
    "co.emoji",
    "co.socketio",

    /* common: widgets */


    /* 3rd party modules */
    "ui.router",
    //"luegg.directives",
    "infinite-scroll",
    "vjs.video"
]);

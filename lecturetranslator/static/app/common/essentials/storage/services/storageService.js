angular
    .module("co.storage")
    .factory("storageService", storageService);

/*@ngInject*/
function storageService($cookies) {

    var isActive = {
        localStorage: canUseLocalStorage()
    };

    var service = {
        get: getGet(),
        set: getSet(),
        unset: getUnset()
    };
    return service;

    //////////

    function canUseLocalStorage() {
        var storageTestKey = 'sTest',
            storage = window.localStorage;

        try {
          storage.setItem(storageTestKey, 'test');
          if (storage.getItem(storageTestKey) !== 'test') {
            return false;
          }
          storage.removeItem(storageTestKey);
        } catch (e) {
            return false;
        }
        return true;
    }

    function getGet() {
        if (isActive.localStorage) {
            return function (key, json) {
                var val = localStorage.getItem(key);
                if (json) {
                    return JSON.parse(val);
                } else {
                    return val;
                }
            };
        } else {
            return function (key, json) {
                var val = $cookies.getObject(key);
                if (json) {
                    return JSON.parse(val);
                } else {
                    return val;
                }
            };
        }
    }

    function getSet() {
        if (isActive.localStorage) {
            return function (key, value, json) {
                if (json) {
                    value = JSON.stringify(value);
                }
                return localStorage.setItem(key, value);
            };
        } else {
            return function (key, value, json) {
                if (json) {
                    value = JSON.stringify(value);
                }
                return $cookies.putObject(key, value);
            };
        }
    }

    function getUnset() {
        if (isActive.localStorage) {
            return function (key) {
                return localStorage.removeItem(key);
            };
        } else {
            return function (key) {
                return $cookies.remove(key);
            };
        }
    }
}

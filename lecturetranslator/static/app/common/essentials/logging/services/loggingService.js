angular
    .module("co.logging")
    .factory("loggingService", loggingService);

/* @ngInject */
function loggingService($http, $q) {

    var deferred = $q.defer(),
        tabId = guid(),
        joinId = 0,
        promise = deferred.promise,
        fingerprint;

    var logTypes = {
        "open": 0,
        "close": 1,
        "join": 2,
        "leave": 3
    };

    var service = {
        log: log
    };

    activate();

    return service;

    //////////

    function activate() {
        new Fingerprint2().get(function(result, components){
            fingerprint = {
                hash: result,
                browser: getComponentValue(components, "user_agent"),
                language: getComponentValue(components, "language")
            };
            deferred.resolve(fingerprint);
        });
    }

    function getComponentValue(components, key) {
        var component = _.find(components, function (component) {
            return component.key === key;
        });
        if (component) {
            return component.value;
        } else {
            return null;
        }
    }

    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        }
        return s4() + s4() + s4() + s4() + s4() + s4() + s4() + s4();
    }

    function req(type, sessionId) {
        if (type === "join") {
            joinId = guid();
        }
        var data = {
            tab_id: tabId,
            type: logTypes[type],
            hash: fingerprint.hash,
            browser: fingerprint.browser,
            language: fingerprint.language,
            session_id: sessionId || null,
            join_id: joinId
        };
        var r = $.ajax({
            type: "POST",
            async: false,
            url: "/logging/log/",
            data: data
        });
    }

    function log(type, sessionId) {
        if (fingerprint) {
            req(type, sessionId);
        } else {
            promise.then(function (fingerprint) {
                req(type, sessionId);
            });
        }
    }
}

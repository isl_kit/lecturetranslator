angular
    .module("co.helper")
    .factory("helperService", helperService);

/*@ngInject*/
function helperService($location, $timeout) {

    var service = {
        locationSearch: locationSearch,
        getCurrentTerm: getCurrentTerm,
        getTermByDate: getTermByDate,
        getTermLimits: getTermLimits,
        getAvailableTerms: getAvailableTerms,
        binaryIndexOf: binaryIndexOf,
        getTimeFromDate: getTimeFromDate,
        queryParamsFormat: queryParamsFormat,
        parseDateString: parseDateString
    };

    return service;

    //////////

    function locationSearch(state, name, value) {
        state.current.reloadOnSearch = false;
        $location.search(name, value);
        $timeout(function () {
            state.current.reloadOnSearch = undefined;
        });
    }

    function getAvailableTerms() {
        return ['WS2015', 'SS2016', 'WS2016', 'SS2017'];
    }

    function getCurrentTerm() {
        var now = new Date();
        return getTermByDate(now.getMonth(), now.getFullYear());
    }

    function getTermByDate(month, year) {
        if (month < 3) {
            return "WS" + (year - 1);
        } else if (month < 9) {
            return "SS" + year;
        } else {
            return "WS" + year;
        }
    }

    function getTermLimits(name) {
        var type = name.substring(0, 2),
            year = parseInt(name.substring(2, 6), 10),
            minDate, maxDate;
        if (type === "SS") {
            minDate = new Date(year, 3, 1);
            maxDate = new Date(year, 8, 30);
        } else {
            minDate = new Date(year, 9, 1);
            maxDate = new Date(year + 1, 2, 31);
        }
        return {
            minDate: minDate,
            maxDate: maxDate
        };
    }

    function binaryIndexOf(arr, searchValue, res) {
    	var minIndex = 0;
    	var maxIndex = arr.length - 1;
    	var currentIndex;
    	var currentElement;
    	var resultIndex;
        if (arr.length === 0) {
            return [];
        }
    	while (minIndex <= maxIndex) {
    		resultIndex = currentIndex = (minIndex + maxIndex) / 2 | 0;
    		currentElement = res(arr[currentIndex]);
    		if (currentElement < searchValue) {
    			minIndex = currentIndex + 1;
    		}
    		else if (currentElement > searchValue) {
    			maxIndex = currentIndex - 1;
    		}
    		else {
                while (currentIndex && currentElement === searchValue) {
                    currentIndex -= 1;
                    currentElement = res(arr[currentIndex]);
                }
    			return [currentElement === searchValue ? currentIndex : currentIndex + 1];
    		}
    	}
        return [maxIndex, minIndex];
    }

    function getTimeFromDate(d) {
        return zeroPad(d.getHours()) + ":" + zeroPad(d.getMinutes());
    }

    function zeroPad(n) {
        if (n < 10) {
            return "0" + n;
        }
        return "" + n;
    }

    function queryParamsFormat(params) {
        var qs = [];
        _.each(params, function (val, key) {
            if (val === undefined || val === null || (_.isArray(val) && val.length === 0)) {
                return;
            }
            if (_.isArray(val)) {
                _.each(val, function (aval) {
                    if (aval === undefined || aval === null) {
                        return;
                    }
                    qs.push(key + '=' + aval);
                });
            } else {
                qs.push(key + '=' + val);
            }
        });
        return qs.join('&');
    }

    function parseDateString(dateString) {
        return new Date(dateString.replace(/-/g, "/").replace("T", " ").replace(/\.[0-9]*$/, ""));
    }
}

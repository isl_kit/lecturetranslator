angular
    .module("co.recording")
    .factory("LinearResampler", LinearResamplerModel);

/*@ngInject*/
function LinearResamplerModel() {

    LinearResampler.prototype.reset = reset;
    LinearResampler.prototype.process = process;

    return LinearResampler;

    //////////

    function LinearResampler(fromSampleRate, toSampleRate, outputBufferSize, callback) {
        this.fromSampleRate = fromSampleRate;
        this.toSampleRate = toSampleRate;
        this.outputBufferSize = outputBufferSize;
        this.callback = callback;
        this.dist = fromSampleRate / toSampleRate;
        this.reset();
    }

    function reset() {
        this.outputBuffer = new Float32Array(this.outputBufferSize);
        this.i_out = 0;
        this.pos = 0;
        this.i = 1;
        this.lastValue = null;
    }

    function process(data) {
        var max_i = data.length, newValue, rem;

        if (!this.lastValue) {
            this.lastValue = data[this.i - 1];
        }

        while (this.i < max_i) {
            newValue = data[this.i];
            this.outputBuffer[this.i_out] = this.lastValue * (1 - this.pos) + newValue * this.pos;

            this.pos += this.dist;
            rem = Math.floor(this.pos);

            if (rem > 0) {
                this.pos -= rem;
                this.i += rem;
                this.lastValue = data[this.i - 1];
            }

            if (this.i_out === this.outputBufferSize - 1) {
                this.callback(this.outputBuffer);
                this.outputBuffer = new Float32Array(this.outputBufferSize);
                this.i_out = 0;
            } else {
                this.i_out += 1;
            }
        }

        if (this.i == max_i) {
            this.i = 0;
        } else if (this.i > max_i) {
            this.i = this.i - max_i;
            this.lastValue = null;
        }

    }
}

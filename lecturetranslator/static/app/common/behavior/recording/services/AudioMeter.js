angular
    .module("co.recording")
    .factory("AudioMeter", AudioMeter);

/*@ngInject*/
function AudioMeter($rootScope) {

    var service = {
        create: createAudioMeter
    };
    return service;

    //////////

    function createAudioMeter(audioContext, clipLevel, averaging, clipLag) {
        var processor = audioContext.createScriptProcessor(512);

        processor.onaudioprocess = volumeAudioProcess;
        processor.clipping = false;
        processor.lastClip = 0;
        processor.volume = 0;
        processor.clipLevel = clipLevel || 0.80;
        processor.averaging = averaging || 0.95;
        processor.clipLag = clipLag || 750;

        // this will have no effect, since we don't copy the input to the output,
        // but works around a current Chrome bug.
        processor.connect(audioContext.destination);

        processor.checkClipping = function () {
            if (!this.clipping) {
                return false;
            }
            if ((this.lastClip + this.clipLag) < window.performance.now()) {
                this.clipping = false;
            }
            return this.clipping;
        };

        processor.shutdown = function () {
            this.disconnect();
            this.onaudioprocess = null;
        };

        return processor;
    }

    function volumeAudioProcess( event ) {
        var buf = event.inputBuffer.getChannelData(0),
            bufLength = buf.length,
            sum = 0, x, rms, that = this;

        // Do a root-mean-square on the samples: sum up the squares...
        for (var i = 0; i < bufLength; i++) {
            x = buf[i];
            if (Math.abs(x) >= this.clipLevel) {
                this.clipping = true;
                this.lastClip = window.performance.now();
            }
            sum += x * x;
        }

        // ... then take the square root of the sum.
        rms = Math.sqrt(sum / bufLength);

        // Now smooth this out with the averaging factor applied
        // to the previous sample - take the max here because we
        // want "fast attack, slow release."
        $rootScope.$apply(function () {
            that.volume = Math.max(rms, that.volume * that.averaging);
            that.checkClipping();
        });
    }
}
angular
    .module("co.recording")
    .factory("ConstantBandwidthResampler", ConstantBandwidthResamplerModel);

/*@ngInject*/
function ConstantBandwidthResamplerModel() {

    ConstantBandwidthResampler.prototype.setup = setup;
    ConstantBandwidthResampler.prototype.reset = reset;
    ConstantBandwidthResampler.prototype.process = process;

    Buffer.prototype.push = push;
    Buffer.prototype.get = get;

    return ConstantBandwidthResampler;

    //////////

    function ConstantBandwidthResampler() {
        this.reset();
    }

    function setup(inSR, outSR, hlen, inSize, outSize, callback) {
        var g, ratio, B, T, t, j, i, c = 0, frel;

        if (hlen < 16 || hlen > 96) {
            throw "hlen must be a value between 16 and 96";
        }

        this.reset();
        frel = 1 - 2.6 / hlen;
        ratio = outSR / inSR;
        g = gcd(outSR, inSR);
        this.outSRmin = outSR / g;
        this.inSRmin = inSR / g;
        if (16 * ratio >= 1 && this.outSRmin <= 1000) {
            if (ratio < 1) {
                frel *= ratio;
                hlen = Math.ceil(hlen / ratio);
            }
            this.coeff = new Float32Array(hlen * (this.outSRmin + 1));
        } else {
            throw "ratio not in range";
        }


        for (j = 0; j <= this.outSRmin; j++) {
            t = j / this.outSRmin;
            for (i = 0; i < hlen; i++) {
                this.coeff[c + hlen - i - 1] = frel * sinc(t * frel) * wind(t / hlen);
                t += 1;
            }
            c += hlen;
        }

        this.inSize = inSize;
        this.outSize = outSize || Math.floor(inSize * this.outSRmin / this.inSRmin);
        this.hlen = hlen;
        this.nread = 2 * hlen;
        this.buffer = new Buffer(4096);
        this.callback = callback;
        this.outdata = new Float32Array(this.outSize);
        this.outIndex = 0;
    }

    function reset() {
        this.hlen = 0;
        this.outSRmin = 0;
        this.inSRmin = 0;
        this.nread = 0;
        this.phase = 0;
        this.outIndex = 0;
        this.coeff = null;
        this.outdata = null;
        this.outSize = 0;
        this.idx1 = 0;
        this.idx2 = 0;
        this.callback = null;
        this.buffer = null;
    }

    function process(inp_data) {
        var hlen, phase, outSRmin, inSRmin, nread, i, idx1, idx2, outIndex, outdata, outSize,
            buff, coeff,
            c1_idx, c2_idx, q1_idx, q2_idx, s;

        hlen = this.hlen;
        outSRmin = this.outSRmin;
        inSRmin = this.inSRmin;
        nread = this.nread;
        phase = this.phase;
        outIndex = this.outIndex;
        coeff = this.coeff;
        outdata = this.outdata;
        outSize = this.outSize;
        buff = this.buffer;

        idx1 = this.idx1;
        idx2 = this.idx2;

        buff.push(inp_data);

        while (true) {
            if (idx2 + nread > buff.index + buff.delta) {
                break;
            }
            idx2 += nread;
            nread = 0;
            c1_idx = hlen * phase;
            c2_idx = hlen * (outSRmin - phase);
            q1_idx = idx1;
            q2_idx = idx2;
            s = 0;
            for (i = 0; i < hlen; i++) {
                q2_idx--;
                s += buff.get(q1_idx) * coeff[c1_idx + i] + buff.get(q2_idx) * coeff[c2_idx + i];
                q1_idx++;
            }

            outdata[outIndex++] = s;

            phase += inSRmin;
            if (phase >= outSRmin) {
                nread = Math.floor(phase / outSRmin);
                phase -= nread * outSRmin;
                idx1 += nread;
            }
            //console.log("YHAHA");
            if (outIndex >= outSize) {
                //console.log("OKOKO");
                this.callback(outdata);
                outdata = new Float32Array(outSize);
                outIndex = 0;
            }

        }
        this.outdata = outdata;
        this.outIndex = outIndex;
        this.idx1 = idx1;
        this.idx2 = idx2;
        this.nread = nread;
        this.phase = phase;
    }

    function Buffer(size) {
        this.size = size;
        this.data = new Float32Array(size);
        this.index = 0;
        this.delta = 0;
        this.padding = 1000;
    }

    function push(arr) {
        var i, diff, data = this.data, max_i;
        if (arr.length + this.index > this.size) {
            diff = this.index - this.padding;
            max_i = this.padding;
            for (i = 0; i < max_i; i++) {
                data[i] = data[i + diff];
            }
            this.index -= diff;
            this.delta += diff;
        }
        for (i = 0; i < arr.length; i++) {
            data[this.index++] = arr[i];
        }
    }

    function get(i) {
        return this.data[i - this.delta];
    }

    function gcd(a, b) {
        if (a === 0) {
            return b;
        }
        if (b === 0) {
            return a;
        }
        while (true) {
            if (a > b) {
                a = a % b;
                if (a === 0) {
                    return b;
                }
                if (a === 1) {
                    return 1;
                }
            } else {
                b = b % a;
                if (b === 0) {
                    return a;
                }
                if (b === 1) {
                    return 1;
                }
            }
        }
        return 1;
    }

    function sinc(x) {
        x = Math.abs(x);
        if (x < 1e-6) {
            return 1;
        }
        x *= Math.PI;
        return Math.sin(x) / x;
    }

    function wind(x) {
        x = Math.abs(x);
        if (x >= 1) {
            return 0;
        }
        x *= Math.PI;
        return 0.384 + 0.500 * Math.cos(x) + 0.116 * Math.cos(2 * x);
    }
}

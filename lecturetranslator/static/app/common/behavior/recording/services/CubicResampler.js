angular
    .module("co.recording")
    .factory("CubicResampler", CubicResamplerModel);

/*@ngInject*/
function CubicResamplerModel() {

    CubicResampler.prototype.reset = reset;
    CubicResampler.prototype.process = process;

    return CubicResampler;

    //////////

    function CubicResampler(fromSampleRate, toSampleRate, outputBufferSize, callback) {
        this.fromSampleRate = fromSampleRate;
        this.toSampleRate = toSampleRate;
        this.outputBufferSize = outputBufferSize;
        this.callback = callback;
        this.dist = fromSampleRate / toSampleRate;
        this.reset();
    }

    function reset() {
        this.outputBuffer = new Float32Array(this.outputBufferSize);
        this.i_out = 0;
        this.pos = 0;
        this.i = 3;
        this.val0 = null;
        this.val1 = null;
        this.val2 = null;
    }

    function process(data) {
        var max_i = data.length,
            val0, val1, val2, val3, rem,
            i, i_out, pos, dist,
            outputBufferSize;

        i = this.i;
        val0 = this.val0 !== undefined ? this.val0 : data[i - 3];
        val1 = this.val1 !== undefined ? this.val1 : data[i - 2];
        val2 = this.val2 !== undefined ? this.val2 : data[i - 1];
        i_out = this.i_out;
        pos = this.pos;
        dist = this.dist;
        outputBufferSize = this.outputBufferSize;

        while (i < max_i) {
            val3 = data[i];

            this.outputBuffer[i_out] = getValue(val0, val1, val2, val3, pos);

            pos += dist;
            rem = Math.floor(pos);

            if (rem > 0) {
                pos -= rem;
                i += rem;
                if (rem === 1) {
                    val0 = val1;
                    val1 = val2;
                    val2 = val3;
                } else if (rem === 2) {
                    val0 = val2;
                    val1 = val3;
                    val2 = data[i - 1];
                } else if (rem === 3) {
                    val0 = val3;
                    val1 = data[i - 2];
                    val2 = data[i - 1];
                } else {
                    val0 = data[i - 3];
                    val1 = data[i - 2];
                    val2 = data[i - 1];
                }
            }

            if (i_out === outputBufferSize - 1) {
                this.callback(this.outputBuffer);
                this.outputBuffer = new Float32Array(outputBufferSize);
                i_out = 0;
            } else {
                i_out += 1;
            }
        }

        this.i = i - max_i;
        this.i_out = i_out;
        this.val0 = val0;
        this.val1 = val1;
        this.val2 = val2;
        this.pos = pos;
    }

    function getValue(val0, val1, val2, val3, pos) {
        var a0 = -1/6 * val0 + 0.5 * val1 - 0.5 * val2 + 1/6 * val3,
            a1 = 0.5 * val0 - val1 + 0.5 * val2,
            a2 = -1/3 * val0 - 0.5 * val1 + val2 - 1/6 * val3,
            a3 = val1;

        /*var a0 = val3 - val2 - val0 + val1,
            a1 = val0 - val1 - a0,
            a2 = val2 - val0,
            a3 = val1;*/

        return a0 * Math.pow(pos, 3) + a1 * Math.pow(pos, 2) + a2 * pos + a3;
    }
}

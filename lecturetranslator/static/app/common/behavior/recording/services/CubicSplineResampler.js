angular
    .module("co.recording")
    .factory("CubicSplineResampler", CubicSplineResamplerModel);

/*@ngInject*/
function CubicSplineResamplerModel(CubicResampler) {

    CubicSplineResampler.prototype = Object.create(CubicResampler.prototype);
    CubicSplineResampler.prototype.constructor = CubicSplineResampler;
    CubicSplineResampler.prototype.getValue = getValue;

    return CubicSplineResampler;

    //////////

    function CubicSplineResampler(fromSampleRate, toSampleRate, outputBufferSize, callback) {
        CubicResampler.call(this, fromSampleRate, toSampleRate, outputBufferSize, callback);
    }

    function getValue(val0, val1, val2, val3, pos) {
        var a0 = -0.5 * val0 + 1.5 * val1 - 1.5 * val2 + 0.5 * val3,
            a1 = val0 - 2.5 * val1 + 2 * val2 - 0.5 * val3,
            a2 = -0.5 * val0 + val1 + 0.5 * val2,
            a3 = val1;

        return a0 * Math.pow(pos, 3) + a1 * Math.pow(pos, 2) + a2 * pos + a3;
    }
}

angular
    .module("co.recording")
    .factory("recordingService", recordingService);

/*@ngInject*/
function recordingService($q, $interval, $rootScope, $timeout, ConstantBandwidthResampler, LinearResampler, CubicResampler, CubicSplineResampler, AudioMeter) {

    var resampler, wsClient, startDeferred, connDeferred,
        //getUserMedia,
        audioContext, meter, mediaStream,
        gainNode, playbackGainNode, recordNode,
        recording = false, paused = false, sessionData,
        sessionId,
        gain = 1,
        c = 0;

    var service = {
        startSession: startSession,
        stopSession: stopSession,
        setPause: setPause,
        setGain: setGain,
        getGain: getGain,
        getMeter: getMeter,
        isRecording: isRecording,
        isPaused: isPaused,
        getSessionData: getSessionData,
        getSessionId: getSessionId
    };

    activate();

    return service;

    //////////

    function activate() {
        window.AudioContext = window.AudioContext || window.webkitAudioContext;
        //getUserMedia = selectGetUserMedia();
        window.URL = window.URL || window.webkitURL;
        $(window).on("beforeunload", beforeUnloadHandler);
        $(window).on("unload", unloadHandler);
    }

    function beforeUnloadHandler() {
        if (recording) {
            return "You are currently recording a session. Closing the Tab will terminate this session.";
        }
    }

    function unloadHandler() {
        stopSession();
    }

    function selectGetUserMedia() {
        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            return navigator.mediaDevices.getUserMedia.bind(navigator.mediaDevices);
        } else if (navigator.getUserMedia) {
            return navigator.getUserMedia.bind(navigator);
        } else if (navigator.webkitGetUserMedia) {
            return navigator.webkitGetUserMedia.bind(navigator);
        } else if (navigator.mozGetUserMedia) {
            return navigator.mozGetUserMedia.bind(navigator);
        }
        return null;
    }

    function startWebSocketConnection() {
        if (wsClient) {
            return $q.when(true);
        }
        connDeferred = $q.defer();
        console.log(WEBSOCKET_URL);
        wsClient = new WebSocket(WEBSOCKET_URL);
        wsClient.onopen = openHandler;
        wsClient.onmessage = messageHandler;
        wsClient.onclose = closeHandler;
        return connDeferred.promise;
    }

    function stopWebSocketConnection() {
        if (!wsClient) {
            return;
        }
        var tmpClient = wsClient;
        wsClient = null;
        tmpClient.close();
    }

    function openHandler() {
        console.log("OPEN");
        if (sessionId) {
            send({type: "reconnect", data: {sessionid: sessionId}});
        }
        connDeferred.resolve(true);
    }

    function messageHandler(event) {
        var message = JSON.parse(event.data);
        if (message.ready === false) {
            if (message.closed) {
                stopSession(true);
            } else {
                $timeout(getStatus, 1000);
            }
        } else if (message.ready === true) {
            sessionId = message.session_id;
            console.log("sessionID: " + message.session_id);
            initUserMedia().then(function () {
                startDeferred.resolve(message.session_id);
            }, function () {
                startDeferred.reject();
            });
        }
    }

    function closeHandler() {
        if (!wsClient) {
            return;
        }
        console.log("reconnect", arguments);
        wsClient = null;
        startWebSocketConnection();
    }

    function send(obj, binary) {
        if (!wsClient || wsClient.readyState !== 1) {
            return;
        }
        if (binary) {
            if (!paused && wsClient.bufferedAmount === 0) {
                if (c > 10) {
                    wsClient.send(obj);
                }
                c += 1;
            }
        } else {
            wsClient.send(JSON.stringify(obj));
        }
    }

    function createAudioContext() {
        try {
            audioContext = new AudioContext();
            console.log(audioContext);
            //meter = AudioMeter.create(audioContext);
        } catch (e) {
            console.error("No web audio support in this browser!");
        }
    }

    function initResampler() {
        resampler = new CubicResampler(audioContext.sampleRate || 44100, 16000, 8192, function (data) {
            data = convertFloat32ToInt16(data);
            send(data, true);
        });
    }

    function initUserMedia() {
        var deferred = $q.defer();
        AdapterJS.webRTCReady(function(isUsingPlugin) {
            getUserMedia({audio: { optional: [{
                echoCancellation: false
            }]}}, function (stream) {
                startUserMedia(stream);
                mediaStream = stream;
                deferred.resolve();
            }, function (e) {
                console.error("No live audio input: " + e);
                deferred.reject();
            });
        });
        return deferred.promise;
    }

    function startUserMedia(stream) {
        var input = audioContext.createMediaStreamSource(stream);

        gainNode = audioContext.createGain();
        gainNode.gain.value = gain;
        playbackGainNode = audioContext.createGain();
        playbackGainNode.gain.value = 0;
        recordNode = audioContext.createScriptProcessor(2048);
        recordNode.onaudioprocess = recorderProcess;

        recordNode.connect(audioContext.destination);
        //input.connect(recordNode);
        input.connect(gainNode);
        //gainNode.connect(meter);
        gainNode.connect(recordNode);
        gainNode.connect(playbackGainNode);
        playbackGainNode.connect(audioContext.destination);
        console.log("Recorder initialised.");
    }

    function recorderProcess(e) {
        if (!recording) {
            return;
        }
        var buffer = e.inputBuffer.getChannelData(0);
        resampler.process(buffer);
    }

    function getStatus() {
        send({ type: "status" });
    }

    function startSession(title, params, desc, fingerprint, password, logging) {
        if (recording) {
            return;
        }
        startDeferred = $q.defer();
        recording = true;
        paused = false;
        sessionData = {
            title: title,
            params: params,
            desc: desc,
            fingerprint: fingerprint,
            password: password,
            logging: logging
        };
        c = 0;
        createAudioContext();
        initResampler();
        startWebSocketConnection().then(function () {
            send({ type: "start", data: sessionData });
            getStatus();
        });
        triggerStateChange("start-session");
        return startDeferred.promise;
    }

    function stopSession(error) {
        if (!recording) {
            return;
        }
        recording = false;
        startDeferred = null;
        sessionId = null;
        if (mediaStream) {
            mediaStream.getTracks()[0].stop();
        }
        //meter.shutdown();
        if (recordNode) {
            recordNode.disconnect();
            recordNode.onaudioprocess = null;
        }

        send({ type: "stop" });
        sessionData = null;
        stopWebSocketConnection();

        audioContext.close();
        audioContext = null;
        meter = null;
        resampler = null;
        mediaStream = null;

        gainNode = null;
        playbackGainNode = null;
        recordNode = null;
        triggerStateChange("stop-session", error);
    }

    function setGain(value) {
        if (recording) {
            gainNode.gain.value = value;
        }
        gain = value;
        triggerStateChange("set-gain", value);
    }

    function getGain() {
        return gain;
    }

    function getMeter() {
        return meter;
    }

    function isRecording() {
        return recording;
    }

    function setPause(p) {
        if (!recording) {
            return;
        }
        paused = p;
        triggerStateChange("set-pause", p);
    }

    function isPaused() {
        return paused;
    }

    function triggerStateChange(type, param) {
        $rootScope.$broadcast("rec.stateChange", type, param);
    }

    function getSessionData() {
        return angular.copy(sessionData);
    }

    function getSessionId() {
        return sessionId;
    }

    function convertFloat32ToInt16(buffer) {
        l = buffer.length;
        buf = new Int16Array(l);
        while (l--) {
            buf[l] = Math.min(1, buffer[l])*0x7FFF;
        }
        return buf.buffer;
    }
}

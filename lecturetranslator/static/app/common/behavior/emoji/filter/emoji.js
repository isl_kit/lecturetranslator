angular
    .module("co.emoji")
    .filter("emoji", emojiFilter);

/*@ngInject*/
function emojiFilter(emojiService) {

    var emojis = emojiService.getAllEmojiNames(),
        rEmojis = new RegExp(":(" + emojis.join("|") + "):", "g");

    return function (input) {
        if (input === undefined) return;
        if (typeof input === "object") return input;
        return input.replace(rEmojis, function (match, text) {
            return "<i class='emoji emoji_" + text + "' title=':" + text + ":'></i>";
        });
    };
}

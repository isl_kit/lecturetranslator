angular
    .module("app.data")
    .factory("mediaEventService", mediaEventService);

/*@ngInject*/
function mediaEventService($http, MediaEvent, helperService) {

    var service = {
        list: list
    };

    return service;

    //////////


    function list(queryParams) {
        var qs = helperService.queryParamsFormat(queryParams);
        return $http.get('/session-media/events/?' + qs).then(function (response) {
            return _.map(response.data, function (data) {
                return new MediaEvent(data);
            });
        });
    }
}

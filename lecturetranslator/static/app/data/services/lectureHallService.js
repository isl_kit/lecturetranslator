angular
    .module("app.data")
    .factory("lectureHallService", lectureHallService);

/*@ngInject*/
function lectureHallService($http, LectureHall, helperService) {

    var service = {
        get: get,
        list: list,
        create: create,
        update: update
    };

    return service;

    //////////

    function get(id) {
        return $http.get('/halls/' + id + '/').then(function (response) {
            return new LectureHall(response.data);
        });
    }

    function list(queryParams) {
        var qs = helperService.queryParamsFormat(queryParams);
        return $http.get('/halls/?' + qs).then(function (response) {
            return _.map(response.data, function (hallData) {
                return new LectureHall(hallData);
            });
        });
    }

    function create(data) {
        return $http.post('/halls/', data).then(function (response) {
            return new LectureHall(response.data);
        });
    }

    function update(id, data) {
        return $http.patch('/halls/' + id + '/', data).then(function (response) {
            return new LectureHall(response.data);
        });
    }
}

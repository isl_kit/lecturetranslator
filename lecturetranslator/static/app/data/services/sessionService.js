angular
    .module('app.data')
    .factory('sessionService', sessionService);

/*@ngInject*/
function sessionService($http, $rootScope, Session, SessionNodes, Data, io, authEntityService, helperService) {

    var repo = {},
        cache = [],
        initialized = false;

    var service = {
        get: get,
        list: list,
        data: data,
        nodes: nodes,
        sessions: repo
    };

    activate();

    return service;

    //////////

    function activate() {
        io.join('sessions');
        io.on('message_sessions', ioMessageHandler);
        list({active: true}).then(initSessionsHandler);
    }

    function get(id) {
        return $http.get('/sessions/' + id + '/').then(function (response) {
            return new Session(response.data);
        });
    }

    function list(queryParams) {
        var qs = helperService.queryParamsFormat(queryParams);
        return $http.get('/sessions/?' + qs).then(function (response) {
            return _.map(response.data, function (data) {
                return new Session(data);
            });
        });
    }

    function data(id, streamIdent) {
        return $http.get('/sessions/' + id + '/' + streamIdent + '/data/').then(function (response) {
            return _.map(response.data, function (data) {
                return new Data(data);
            });
        });
    }

    function nodes(id) {
        return $http.get('/sessions/' + id + '/nodes/').then(function (response) {
            return new SessionNodes(response.data);
        });
    }

    function ioMessageHandler(message) {
        if (!initialized) {
            cache.push(message);
            return;
        }
        $rootScope.$apply(function () {
            messageHandler(message);
        });
    }

    function messageHandler(message) {
        var session = new Session(message.data),
            event;
        if (message.type === 'sessions_add') {
            event = repo[session.id] ? 'update' : 'add';
            repo[session.id] = session;
        } else if (message.type === 'sessions_end') {
            event = 'remove';
            if (_.has(repo, session.id)) {
                delete repo[session.id];
            }
        }
        if (event === 'add' && session.auth && !session.auth.authenticated) {
            authEntityService.get(session.auth.id).then(function (authEntity) {
                session.auth = authEntity;
            });
        }
        $rootScope.$broadcast('sessions.' + event, session);
    }

    function initSessionsHandler(sessions) {
        _.each(sessions, function (session) {
            repo[session.id] = session;
            $rootScope.$broadcast('sessions.add', session);
        });
        clearCache();
        initialized = true;
    }

    function clearCache() {
        _.each(cache, messageHandler);
        cache.length = 0;
    }
}

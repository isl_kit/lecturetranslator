angular
    .module("app.data")
    .factory("lecturerService", lecturerService);

/*@ngInject*/
function lecturerService($http, Lecturer) {

    var service = {
        get: get,
        list: list,
        create: create,
        update: update,
        getUser: getUser,
        me: me
    };

    return service;

    //////////

    function get(id) {
        return $http.get('/lecturers/' + id + '/').then(function (response) {
            return new Lecturer(response.data);
        });
    }

    function list(search) {
        var qs = '';
        if (search) {
            qs = '?search=' + search;
        }
        return $http.get('/lecturers/' + qs).then(function (response) {
            return _.map(response.data, function (hallData) {
                return new Lecturer(hallData);
            });
        });
    }

    function create(data) {
        return $http.post('/lecturers/', data).then(function (response) {
            return new Lecturer(response.data);
        });
    }

    function update(id, data) {
        return $http.patch('/lecturers/' + id + '/', data).then(function (response) {
            return new Lecturer(response.data);
        });
    }

    function getUser(id) {
        return $http.get('/lecturers/' + id + '/user/').then(function (response) {
            return response.data.user;
        });
    }

    function me() {
        return $http.get('/lecturers/me/').then(function (response) {
            if (response.data.id) {
                return new Lecturer(response.data);
            }
            return null;
        });
    }
}

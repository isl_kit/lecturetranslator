angular
    .module("app.data")
    .factory("eventService", eventService);

/*@ngInject*/
function eventService($http, Event, helperService) {

    var service = {
        get: get,
        list: list,
        create: create,
        update: update,
        remove: remove
    };

    return service;

    //////////

    function get(id) {
        return $http.get('/events/' + id + '/').then(function (response) {
            return new Event(response.data);
        });
    }

    function list(queryParams) {
        var qs = helperService.queryParamsFormat(queryParams);
        return $http.get('/events/?' + qs).then(function (response) {
            return _.map(response.data, function (data) {
                return new Event(data);
            });
        });
    }

    function create(data) {
        return $http.post('/events/', data).then(function (response) {
            return new Event(response.data);
        });
    }

    function update(id, data) {
        return $http.patch('/events/' + id + '/', data).then(function (response) {
            return new Event(response.data);
        });
    }

    function remove(id) {
        return $http.delete('/events/' + id + '/');
    }
}

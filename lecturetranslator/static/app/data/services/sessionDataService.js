angular
    .module("app.data")
    .factory("sessionDataService", sessionDataService);

/*@ngInject*/
function sessionDataService($http, $rootScope, $mdDialog, $q, Session, Lecture, Lecturer, LectureHall, LectureSemester, Semester, Event, io, AuthEntity, helperService) {

    var sessions = {};

    var service = {
        getSession: getSession,
        sessions: sessions,
        searchLectureTitles: searchLectureTitles,
        searchLectureHalls: searchLectureHalls,
        searchLecturer: searchLecturer,
        slugExists: slugExists,
        scheduleLecture: scheduleLecture,
        getLecture: getLecture,
        //getSimpleLecture: getSimpleLecture,
        getLectureSemester: getLectureSemester,
        getAllHalls: getAllHalls,
        getAllLecturers: getAllLecturers,
        getAllSemesters: getAllSemesters,
        getAllHallsForList: getAllHallsForList,
        getAllLecturersForList: getAllLecturersForList,
        getLectures: getLectures,
        getUpcomingEvents: getUpcomingEvents,
        //getAllEvents: getAllEvents,
        getEventsByUser: getEventsByUser,
        getHall: getHall,
        updateLectureHall: updateLectureHall,
        getLecturer: getLecturer,
        getLecturerUser: getLecturerUser,
        createLecturer: createLecturer,
        updateLecturer: updateLecturer,
        updateLecture: updateLecture,
        updateLectureSemester: updateLectureSemester,
        updateEvent: updateEvent,
        addEvent: addEvent,
        removeEvent: removeEvent
    };

    activate();

    return service;

    //////////

    function activate() {
        io.join("sessions");
        io.join("sessions_hidden");
        io.on("message_sessions", messageHandler);
        io.on("message_sessions_hidden", messageHandler);
    }

    function messageHandler(message) {
        var res = [], resUpdate = [];
        if (message.type === "sessions_add") {
            $rootScope.$apply(function () {
                _.each(message.data, function (sessionData) {
                    var session = new Session(sessionData);
                    if (sessions[sessionData.id]) {
                        resUpdate.push(session);
                    } else {
                        res.push(session);
                    }
                    sessions[sessionData.id] = session;
                    if (!session.auth) {
                        getAuth(session);
                    }
                });
                if (res.length > 0) {
                    $rootScope.$broadcast("sessions.add", res);
                }
                if (resUpdate.length > 0) {
                    $rootScope.$broadcast("sessions.update", resUpdate);
                }
            });
        } else if (message.type === "sessions_end") {
            $rootScope.$apply(function () {
                _.each(message.data, function (session) {
                    if (_.has(sessions, session.id)) {
                        res.push(sessions[session.id]);
                        delete sessions[session.id];
                    }
                });
                $rootScope.$broadcast("sessions.remove", res);
            });
        }
    }

    function getAuth(session) {
        $http.get("/mediator/getAuth/?session_id=" + session.id).then(function (response) {
            session.auth = new AuthEntity(response.data.auth);
        });
    }

    function getSession(sessionId) {
        return $http.get("/mediator/getSession/?session_id=" + sessionId).then(function (response) {
            return new Session(response.data.session);
        }, function (response) {
            if (response.data.auth) {
                return new AuthEntity(response.data.auth);
            } else {
                return null;
            }
        });
    }

    function searchLectureTitles(searchString) {
        return $http.get("/mediator/searchLectureTitles/?title=" + searchString).then(function (response) {
            return response.data.candidates;
        });
    }

    function searchLectureHalls(searchString) {
        return $http.get("/mediator/searchLectureHalls/?name=" + searchString).then(function (response) {
            return response.data.candidates;
        });
    }

    function searchLecturer(searchString) {
        return $http.get("/mediator/searchLecturer/?name=" + searchString).then(function (response) {
            return response.data.candidates;
        });
    }

    function slugExists(slug) {
        return $http.get("/mediator/slugExists/?slug=" + slug).then(function (response) {
            return response.data.exists;
        });
    }

    function scheduleLecture(title, description, slug, lecturerIds, semester, startDate, endDate, events, password, archive) {
        var data = {
            title: title,
            desc: description,
            slug: slug,
            lecturer_ids: lecturerIds,
            semester: semester,
            start: getDateString(startDate),
            end: getDateString(endDate),
            password: password,
            events: events,
            archive: archive
        };
        return $http.post("/mediator/addLecture/", data).then(function (response) {
            return response;
        });
    }

    function getLectureSemester(slug, semester, include_events) {
        var params = "?slug=" + slug + "&semester=" + semester;
        if (include_events) {
            params += "&incl_events=1";
        }
        return $http.get("/mediator/getLectureSemester/" + params).then(function (response) {
            var lectureSemester = new LectureSemester(response.data.lecture_semester),
                semesters = _.map(response.data.semesters, function (semesterData) {
                    return new Semester(semesterData);
                }),
                events = _.map(response.data.lecture_semester.events, function (eventData) {
                    return new Event(eventData);
                });
            events.sort(function (a, b) {
                return a.start - b.start;
            });
            semesters.sort(semesterSort);
            lectureSemester.events = events;
            return {
                lectureSemester: lectureSemester,
                semesters: semesters
            };
        });
    }

    function getSimpleLecture(slug) {
        return $http.get("/mediator/getSimpleLecture/?slug=" + slug).then(function (response) {
            return new Lecture(response.data.lecture);
        });
    }

    function getLecture(slug) {
        return $http.get("/mediator/getLecture/?slug=" + slug).then(function (response) {
            var lecture = new Lecture(response.data.lecture),
                lectureSemesters = _.map(response.data.lecture.lecture_semesters, function (lectureSemesterData) {
                    return new LectureSemester(lectureSemesterData);
                });
            lectureSemesters.sort(function (a, b) {
                var semA = a.semester.name,
                    semB = b.semester.name,
                    yearA = parseInt(semA.substr(2,4)),
                    yearB = parseInt(semB.substr(2,4)),
                    typeA = semA.substr(0,2),
                    typeB = semB.substr(0,2);

                if (yearA === yearB) {
                    if (typeA === "SS" && typeB === "WS") {
                        return -1;
                    } else if (typeA === "WS" && typeB === "SS") {
                        return 1;
                    } else {
                        return 0;
                    }
                } else {
                    return yearA - yearB;
                }
            });
            lecture.lectureSemesters = lectureSemesters;
            return lecture;
        });
    }

    function getAllLecturers() {
        return $http.get("/mediator/getAllLecturers/").then(function (response) {
            var lecturers = _.map(response.data.lecturers, function (lecturerData) {
                return new Lecturer(lecturerData);
            });
            lecturers.sort(function (a, b) {
                a = a.name.toLowerCase();
                b = b.name.toLowerCase();
                if(a === b) return 0;
                if(a > b) return 1;
                return -1;
            });
            return lecturers;
        });
    }

    function getAllHalls() {
        return $http.get("/mediator/getAllHalls/").then(function (response) {
            var halls = _.map(response.data.halls, function (hallData) {
                return new LectureHall(hallData);
            });
            halls.sort(function (a, b) {
                a = a.name.toLowerCase();
                b = b.name.toLowerCase();
                if(a === b) return 0;
                if(a > b) return 1;
                return -1;
            });
            return halls;
        });
    }

    function getAllHallsForList() {
        return $http.get("/mediator/getAllHallsForList/").then(function (response) {
            var halls = _.map(response.data.halls, function (hallData) {
                return new LectureHall(hallData);
            });
            halls.sort(function (a, b) {
                a = a.name.toLowerCase();
                b = b.name.toLowerCase();
                if(a === b) return 0;
                if(a > b) return 1;
                return -1;
            });
            return halls;
        });
    }

    function getAllLecturersForList() {
        return $http.get("/mediator/getAllLecturersForList/").then(function (response) {
            var lecturers = _.map(response.data.lecturers, function (lecturerData) {
                return new Lecturer(lecturerData);
            });
            lecturers.sort(function (a, b) {
                a = a.name.toLowerCase();
                b = b.name.toLowerCase();
                if(a === b) return 0;
                if(a > b) return 1;
                return -1;
            });
            return lecturers;
        });
    }

    function getAllSemesters() {
        return $http.get("/mediator/getAllSemesters/").then(function (response) {
            var semesters = _.map(response.data.semesters, function (semesterData) {
                return new Semester(semesterData);
            });
            semesters.sort(semesterSort);
            return semesters;
        });
    }

    function getLectures(start, stop, title, hall_ids, lecturer_ids, semester_ids) {
        return $http.get("/mediator/getLectures/?start=" + start + "&stop=" + stop + "&title=" + title + "&hall_ids=" + JSON.stringify(hall_ids) + "&semester_ids=" + JSON.stringify(semester_ids) + "&lecturer_ids=" + JSON.stringify(lecturer_ids)).then(function (response) {
            return _.map(response.data.lectures, function (lectureData) {
                var lecture = new Lecture(lectureData),
                    lectureSemesters = _.map(lectureData.lecture_semesters, function (lectureSemesterData) {
                        return new LectureSemester(lectureSemesterData);
                    });
                lectureSemesters.sort(function (a, b) {
                    var semA = a.semester.name,
                        semB = b.semester.name,
                        yearA = parseInt(semA.substr(2,4)),
                        yearB = parseInt(semB.substr(2,4)),
                        typeA = semA.substr(0,2),
                        typeB = semB.substr(0,2);

                    if (yearA === yearB) {
                        if (typeA === "SS" && typeB === "WS") {
                            return -1;
                        } else if (typeA === "WS" && typeB === "SS") {
                            return 1;
                        } else {
                            return 0;
                        }
                    } else {
                        return yearA - yearB;
                    }
                });
                lecture.lectureSemesters = lectureSemesters;
                return lecture;
            });
        });
    }

    function getHall(id) {
        return $http.get("/mediator/getHall/?id=" + id).then(function (response) {
            return new LectureHall(response.data.hall);
        });
    }

    function getLecturer(id) {
        return $http.get("/mediator/getLecturer/?id=" + id).then(function (response) {
            return new Lecturer(response.data.lecturer);
        });
    }

    function getLecturerUser(id) {
        return $http.get("/mediator/getLecturerUser/?id=" + id).then(function (response) {
            return response.data.username;
        });
    }

    function createLecturer(title, name, user) {
        var data = {
            title: title,
            name: name,
            user: user
        };
        return $http.post("/mediator/createLecturer/", data).then(function (response) {
            return new Lecturer(response.data.lecturer);
        });
    }

    function updateLecturer(id, title, name, user) {
        var data = {
            id: id,
            title: title,
            name: name,
            user: user
        };
        return $http.post("/mediator/updateLecturer/", data).then(function (response) {
            return new Lecturer(response.data.lecturer);
        });
    }

    function updateLectureHall(id, name) {
        var data = {
            id: id,
            name: name
        };
        return $http.post("/mediator/updateLectureHall/", data).then(function (response) {
            return new LectureHall(response.data.hall);
        });
    }

    function updateLecture(slug, title) {
        var data = {
            slug: slug,
            title: title
        };
        return $http.post("/mediator/updateLecture/", data).then(function (response) {
            return new Lecture(response.data.lecture);
        });
    }

    function updateLectureSemester(id, description, lecturerIds, archive, tags) {
        var data = {
            id: id,
            desc: description,
            archive: archive,
            lecturer_ids: lecturerIds,
            tags: _.map(tags, function (tag) {
                return tag.name;
            })
        };
        return $http.post("/mediator/updateLectureSemester/", data).then(function (response) {
            return new LectureSemester(response.data.lecture_semester);
        });
    }

    function removeEvent(id) {
        var deferred = $q.defer();
        var confirm = $mdDialog.confirm()
            .title("Delete Event")
            .textContent("Are you sure, you want to delete this event?")
            .ok("Yes, delete it")
            .cancel("Cancel");
        console.log("show dialog");
        $mdDialog.show(confirm).then(function () {
            $http.post("/mediator/removeEvent/", {id: id}).then(function (response) {
                deferred.resolve();
            }, function () {
                deferred.reject();
            });
        }, function () {
            deferred.reject();
        });
        return deferred.promise;
    }

    function addEvent(lectureSemesterId, start, stop, lectureHallName) {
        var data = {
            lecture_semester_id: lectureSemesterId,
            start: start,
            stop: stop,
            hall: lectureHallName
        };
        return $http.post("/mediator/addEvent/", data).then(function (response) {
            return new Event(response.data.event);
        });
    }

    function updateEvent(id, start, stop, lectureHallName) {
        var data = {
            id: id,
            start: start,
            stop: stop,
            hall: lectureHallName
        };
        return $http.post("/mediator/updateEvent/", data).then(function (response) {
            return new Event(response.data.event);
        });
    }


    function getUpcomingEvents() {
        return $http.get("/mediator/getUpcomingEvents/").then(function (response) {
            var events = _.map(response.data.events, function (eventData) {
                return new Event(eventData);
            });
            events.sort(function (a, b) {
                return a.start - b.start;
            });
            return events;
        });
    }

    function getAllEvents(lectureSemesterId) {
        return $http.get("/mediator/getAllEvents/?id=" + lectureSemesterId).then(function (response) {
            var events = _.map(response.data.events, function (eventData) {
                return new Event(eventData);
            });
            events.sort(function (a, b) {
                return a.start - b.start;
            });
            return events;
        });
    }

    function getEventsByUser(semesterName) {
        semesterName = semesterName || helperService.getCurrentTerm();
        return $http.get("/mediator/getEventsByUser/?semester=" + semesterName).then(function (response) {
            var events = _.map(response.data.events, function (eventData) {
                return new Event(eventData);
            });
            events.sort(function (a, b) {
                return a.start - b.start;
            });
            return events;
        });
    }

    function getDateString(d) {
        return d.getFullYear() + "-" + zeroPad(d.getMonth() + 1, 2) + "-" + zeroPad(d.getDate(), 2);
    }

    function zeroPad(num, padding) {
        num = num.toString();
        while (num.length < padding) {
            num = "0" + num;
        }
        return num;
    }

    function semesterSort(a, b) {
        var semA = a.name,
            semB = b.name,
            yearA = parseInt(semA.substr(2,4)),
            yearB = parseInt(semB.substr(2,4)),
            typeA = semA.substr(0,2),
            typeB = semB.substr(0,2);

        if (yearA === yearB) {
            if (typeA === "SS" && typeB === "WS") {
                return -1;
            } else if (typeA === "WS" && typeB === "SS") {
                return 1;
            } else {
                return 0;
            }
        } else {
            return yearA - yearB;
        }
    }
}

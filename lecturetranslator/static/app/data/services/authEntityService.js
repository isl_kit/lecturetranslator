angular
    .module('app.data')
    .factory('authEntityService', authEntityService);

/*@ngInject*/
function authEntityService($http, AuthEntity) {

    var service = {
        get: get,
        authenticate: authenticate
    };

    return service;

    //////////

    function get(id) {
        return $http.get('/auth/authItem/' + id + '/').then(function (response) {
            return new AuthEntity(response.data);
        });
    }

    function authenticate(id, data) {
        return $http.post("/auth/authItem/" + id + '/auth/', data).then(function (response) {
            return response.data.authenticated;
        });
    }
}

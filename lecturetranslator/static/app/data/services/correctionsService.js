angular
    .module('app.data')
    .factory('correctionsService', correctionsService);

/*@ngInject*/
function correctionsService($http, Commit, CommitResult) {

    var service = {
        data: data,
        update: update,
        transfer: transfer
    };

    return service;

    //////////

    function data(sessionId, fingerprint) {
        return $http.get('/streamdata/public/' + sessionId + '/' + fingerprint + '/').then(function (response) {
            return new Commit(response.data);
        });
    }

    function update(sessionId, fingerprint, data) {
        return $http.patch('/streamdata/corrections/' + sessionId + '/' + fingerprint + '/', data).then(function (response) {
            return new Commit(response.data);
        });
    }

    function transfer(sessionId, fingerprint) {
        return $http.patch('/streamdata/public/' + sessionId + '/' + fingerprint + '/').then(function(response) {
            return;
        });
    }
}

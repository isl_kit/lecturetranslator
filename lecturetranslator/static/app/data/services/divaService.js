angular
    .module("app.data")
    .factory("divaService", divaService);

/*@ngInject*/
function divaService($http, DivaCollection, helperService) {

    var service = {
        get: get,
        list: list,
        create: create,
        update: update,
        remove: remove
    };

    return service;

    //////////

    function get(id) {
        return $http.get('/diva/' + id + '/').then(function (response) {
            return new DivaCollection(response.data);
        });
    }

    function list(queryParams) {
        var qs = helperService.queryParamsFormat(queryParams);
        return $http.get('/diva/?' + qs).then(function (response) {
            return _.map(response.data, function (data) {
                return new DivaCollection(data);
            });
        });
    }

    function create(data) {
        return $http.post('/diva/', data).then(function (response) {
            return new DivaCollection(response.data);
        });
    }

    function update(id, data) {
        return $http.patch('/diva/' + id + '/', data).then(function (response) {
            return new DivaCollection(response.data);
        });
    }

    function remove(id) {
        return $http.delete('/diva/' + id + '/');
    }
}

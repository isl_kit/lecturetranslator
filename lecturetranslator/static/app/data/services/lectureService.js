angular
    .module("app.data")
    .factory("lectureService", lectureService);

/*@ngInject*/
function lectureService($http, Lecture, LectureTerm, helperService) {

    var service = {
        get: get,
        create: create,
        list: list,
        update: update
    };

    return service;

    //////////

    function get(slug) {
        return $http.get('/lectures/' + slug + '/').then(function (response) {
            return new Lecture(response.data);
        });
    }

    function create(data) {
        return $http.post('/lectures/', data).then(function (response) {
            return new Lecture(response.data);
        });
    }

    function list(queryParams) {
        var qs = helperService.queryParamsFormat(queryParams);
        return $http.get('/lectures/?' + qs).then(function (response) {
            return _.map(response.data, function (data) {
                return new Lecture(data);
            });
        });
    }

    function update(slug, data) {
        return $http.patch('/lectures/' + slug + '/', data).then(function (response) {
            return new Lecture(response.data);
        });
    }
}

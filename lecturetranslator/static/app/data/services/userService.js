angular
    .module('app.data')
    .factory('userService', userService);

/*@ngInject*/
function userService($http, User) {

    var service = {
        getOrCreate: getOrCreate
    };

    return service;

    //////////

    function getOrCreate(data) {
        return $http.post('/auth/user/', data).then(function (response) {
            return new User(response.data);
        });
    }
}

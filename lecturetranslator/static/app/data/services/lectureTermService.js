angular
    .module("app.data")
    .factory("lectureTermService", lectureTermService);

/*@ngInject*/
function lectureTermService($http, Lecture, LectureTerm, helperService) {

    var service = {
        get: get,
        list: list,
        create: create,
        update: update
    };

    return service;

    //////////

    function get(term, slug) {
        return $http.get('/lectures/' + slug + '/' + term + '/').then(function (response) {
            return new LectureTerm(response.data);
        });
    }

    function list(queryParams) {
        var qs = helperService.queryParamsFormat(queryParams);
        return $http.get('/lectures/terms/?' + qs).then(function (response) {
            return _.map(response.data, function (data) {
                return new LectureTerm(data);
            });
        });
    }

    function create(data) {
        return $http.post('/lectures/terms/', data).then(function (response) {
            return new LectureTerm(response.data);
        });
    }

    function update(term, slug, data) {
        return $http.patch('/lectures/' + slug + '/' + term + '/', data).then(function (response) {
            return new LectureTerm(response.data);
        });
    }
}

angular
    .module('app.data')
    .factory('sessionMediaService', sessionMediaService);

/*@ngInject*/
function sessionMediaService($http, VTT, Slide, SlideContent, MediaCollection) {

    var service = {
        media: media,
        slides: slides,
        vtt: vtt
    };

    return service;

    //////////

    function media(id) {
        return $http.get('/session-media/' + id + '/media/').then(function (response) {
            return new MediaCollection(id, response.data.paths);
        });
    }

    function vtt(id) {
        return $http.get('/session-media/' + id + '/vtt/').then(function (response) {
            return _.map(response.data.vtt, function (data) {
                return new VTT(data.file, id, data.lang);
            });
        });
    }

    function slides(id) {
        return $http.get('/session-media/' + id + '/slides/').then(function (response) {
            var content = {};
            _.each(response.data.content, function (data) {
                content[data.id] = new SlideContent(data.id, data.text);
            });
            var oldSlide = null;
            return _.filter(_.map(response.data.slides, function (data) {
                var slide = new Slide(data.id, data.file, id, data.seconds, content[data.id] ? content[data.id].text : '');
                if (oldSlide) {
                    oldSlide.stop = slide.start;
                    oldSlide.duration = oldSlide.stop - oldSlide.start;
                }
                oldSlide = slide;
                return slide;
            }), function (slide) {
                return slide.duration > 2;
            });
        });
    }
}

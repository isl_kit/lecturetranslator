angular
    .module("app.data")
    .factory("DocumentTag", DocumentTagModel);

/*@ngInject*/
function DocumentTagModel() {

    function DocumentTag(data) {
        this.name = data.name;
        this.description = data.description || '';
    }

    return DocumentTag;

    //////////

}

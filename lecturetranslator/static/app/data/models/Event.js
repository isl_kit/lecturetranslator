angular
    .module("app.data")
    .factory("Event", EventModel);

/*@ngInject*/
function EventModel(LectureHall, DocumentTag, LectureTerm, helperService) {

    function Event(data) {
        this.id = data.id;
        this.start = helperService.parseDateString(data.start);
        this.stop = data.stop ? helperService.parseDateString(data.stop) : null;
        this.fingerprint = data.fingerprint || '';
        this.title = data.title;
        this.lectureHallId = data.lecture_hall_id;
        this.lectureHall = data.lecture_hall ? new LectureHall(data.lecture_hall) : null;
        this.lectureTerm = data.lecture_term ? new LectureTerm(data.lecture_term): null;
        this.sessionId = data.session;
        this.archive = data.archive;
        this.canAccess = data.can_access || false;
        this.canUpload = data.can_upload || false;
        this.tags = data.tags ? _.map(data.tags, function (tagData) {
            if (typeof tagData === 'string') {
                tagData = { name: tagData };
            }
            return new DocumentTag(tagData);
        }) : [];
    }

    return Event;
}

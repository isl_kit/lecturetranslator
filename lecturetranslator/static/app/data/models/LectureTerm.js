angular
    .module("app.data")
    .factory("LectureTerm", LectureTermModel);

/*@ngInject*/
function LectureTermModel(Lecture, LectureHall, Lecturer, AuthEntity) {

    function LectureTerm(data) {
        this.id = data.id;
        this.description = data.description;
        this.term = data.term;
        this.lecture = data.lecture ? new Lecture(data.lecture) : null;
        this.lectureHalls = data.lecture_halls ? _.map(data.lecture_halls, function (hallData) {
            return new LectureHall(hallData);
        }) : [];
        this.lecturers = data.lecturers ? _.map(data.lecturers, function (lecturerData) {
            return new Lecturer(lecturerData);
        }) : [];
        this.auth = data.auth ? new AuthEntity(data.auth) : null;
        this.events = [];
        this.archive = data.archive;
        this.mediaAccess = data.media_access;
    }

    return LectureTerm;

    //////////
}

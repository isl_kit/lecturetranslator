angular
    .module("app.data")
    .factory("LectureHall", LectureHallModel);

/*@ngInject*/
function LectureHallModel() {

    var repository = {};

    function LectureHall(data) {
        var that = repository[data.id] || {};
        that.id = data.id;
        that.name = data.name;
        repository[data.id] = that;
        return that;
    }

    LectureHall.getById = getById;

    return LectureHall;

    //////////

    function getById(id) {
        return repository[id];
    }
}

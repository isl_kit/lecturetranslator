angular
    .module("app.data")
    .factory("DivaCollection", DivaCollectionModel);

/*@ngInject*/
function DivaCollectionModel() {

    function DivaCollection(data) {
        this.key = data.key;
        this.id = data.id;
    }

    return DivaCollection;

    //////////
}

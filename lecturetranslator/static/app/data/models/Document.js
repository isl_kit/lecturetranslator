angular
    .module("app.data")
    .factory("Document", DocumentModel);

/*@ngInject*/
function DocumentModel(Resource) {

    function Document(data) {
        this.resource = new Resource(data.resource);
    }

    return Document;

    //////////
}

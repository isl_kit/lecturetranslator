angular
    .module("app.data")
    .factory("Lecturer", LecturerModel);

/*@ngInject*/
function LecturerModel() {

    var repository = {};

    function Lecturer(data) {
        var that = repository[data.id] || {};
        that.id = data.id;
        that.name = data.name;
        that.title = data.title;
        that.fullName = (data.title ? data.title + " " : "") + data.name;
        repository[data.id] = that;
        return that;
    }

    Lecturer.getById = getById;

    return Lecturer;

    //////////

    function getById(id) {
        return repository[id];
    }
}

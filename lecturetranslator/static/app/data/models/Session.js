angular
    .module("app.data")
    .factory("Session", SessionModel);

/*@ngInject*/
function SessionModel(Stream, AuthEntity, Event, SessionMedia, helperService) {

    function Session(data) {
        var that = this;
        this.id = data.id;
        this.active = data.active;
        this.start = data.start;
        this.created = helperService.parseDateString(data.created);
        //this.source = data.source;
        this.inputFingerprint = data.input_fingerprint;
        this.title = data.title;
        this.desc = data.desc;
        //this.params = data.params;
        //this.extras = data.extras;
        this.streams = data.streams ? data.streams.filter(function (stream) {
            if (LIMIT_STREAMS.length === 0 || stream.ident === data.transcript_stream) {
                return true;
            }
            return _.contains(LIMIT_STREAMS, stream.fingerprint.split('-')[0].substring(0, 2));
        }).map(function (stream) {
            return new Stream(stream);
        }) : [];
        this.defaultStreamIdents = data.default_streams || [];
        this.auth = data.auth ? new AuthEntity(data.auth) : null;
        this.canAccess = data.can_access || false;
        this.event = data.event ? new Event(data.event) : null;
        this.transcriptStream = this.getStream(data.transcript_stream);
        this.backchannelStream = this.getBackchannelStream();
        this.metaStream = this.getMetaStream();
        this.classifyStream = this.getClassifyStream();
        this.media = new SessionMedia();
        this.printTitle = this.event ? this.event.lectureTerm.lecture.title : this.title;
    }

    Session.prototype.getStream = getStream;
    Session.prototype.getBackchannelStream = getBackchannelStream;
    Session.prototype.getMetaStream = getMetaStream;
    Session.prototype.getClassifyStream = getClassifyStream;

    return Session;

    //////////

    function getStream(streamIdent) {
        return _.find(this.streams, function (stream) {
            return stream.ident === streamIdent;
        });
    }

    function getBackchannelStream() {
        return _.find(this.streams, function (stream) {
            return stream.fingerprint === "bc";
        });
    }

    function getMetaStream() {
        var that = this;
        return _.find(that.streams, function (stream) {
            return stream.fingerprint === that.transcriptStream.fingerprint && stream.type === "unseg-text";
        });
    }

    function getClassifyStream() {
        var that = this;
        return _.find(that.streams, function (stream) {
            return stream.fingerprint === "mu" && stream.type === "text";
        });
    }
}

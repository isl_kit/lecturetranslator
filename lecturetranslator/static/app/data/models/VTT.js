angular
    .module("app.data")
    .factory("VTT", VTTModel);

/*@ngInject*/
function VTTModel() {

    var langdict = {
        en: "English",
        de: "German",
        fr: "French",
        it: "Italian",
        xx: "Nothing",
        lt: "Lithuanian",
        zh: "Chinese",
        lv: "Latvian",
        et: "Estonian",
        vi: "Vietnamese",
        es: "Spanish",
        ro: "Romanian",
        mk: "Macedonian",
        hi: "Hindi",
        id: "Indonese",
        hu: "Hungarian",
        sl: "Slovenian",
        el: "Greek",
        sk: "Slovak",
        tr: "Turkish",
        cs: "Czech",
        ar: "Arabic",
        uk: "Ukrainian",
        ru: "Russian",
        fa: "Persian",
        hy: "Armenian",
        ca: "Catalan",
        da: "Danish",
        he: "Hebrew",
        fi: "Finnish",
        eo: "Esperanto",
        sv: "Swedish",
        pt: "Portuguese",
        sq: "Albanian",
        pl: "Polish",
        bg: "Bulgarian",
        nl: "Dutch",
        nb: "Norwegian",
        am: "English2",
        ja: "Japanese",
        sb: "Schwäbisch"
    };

    function VTT(file, sessionId, lang) {
        this.path = "/media/streams/" + sessionId + "/vtt/" + file;
        this.label = langdict[lang] || lang;
        this.lang = lang;
    }

    return VTT;

    //////////
}

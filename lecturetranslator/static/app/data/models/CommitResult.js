angular
    .module("app.data")
    .factory("CommitResult", CommitResultModel);

/*@ngInject*/
function CommitResultModel(Data) {

    function CommitResult(data) {
        this.commit = data.commit;
        this.success = data.success;
    }

    return CommitResult;
}

angular
    .module("app.data")
    .factory("SlideContent", SlideContentModel);

/*@ngInject*/
function SlideContentModel() {

    function SlideContent(id, text) {
        this.id = id;
        this.text = text.toLowerCase();
    }

    return SlideContent;

    //////////
}

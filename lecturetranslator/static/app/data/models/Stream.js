angular
    .module("app.data")
    .factory("Stream", StreamModel);

/*@ngInject*/
function StreamModel() {

    function Stream(data) {
        this.ident = data.ident;
        this.id = data.id;
        this.label = data.label;
        this.sid = data.sid;
        this.type = data.type;
        this.fingerprint = data.fingerprint;
        this.displayname = data.displayname;
        this.control = data.control;
    }

    return Stream;
}

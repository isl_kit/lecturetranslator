angular
    .module("app.data")
    .factory("Commit", CommitModel);

/*@ngInject*/
function CommitModel(Data) {

    function Commit(data) {
        this.id = data.id;
        this.data = data.data.map(function (data) {
            return new Data(data, true);
        });
    }

    return Commit;
}

angular
    .module("app.data")
    .factory("Resource", ResourceModel);

/*@ngInject*/
function ResourceModel() {

    function Resource(data) {
        this.hash = data.hash;
        this.size = data.size;
        this.textExists = data.text_exists;
    }

    return Resource;

    //////////
}

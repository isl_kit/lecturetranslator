angular
    .module("app.data")
    .factory("MediaEvent", MediaEventModel);

/*@ngInject*/
function MediaEventModel(Session, Event, helperService) {

    function MediaEvent(data) {
        this.id = data.id;
        this.start = helperService.parseDateString(data.start);
        this.stop = helperService.parseDateString(data.stop);
        this.audioPath = data.audio_path;
        this.path = data.path;
        this.mediaType = data.media_type;
        this.event = data.event ? new Event(data.event) : null;
        this.sessionId = data.session;
    }

    return MediaEvent;
}

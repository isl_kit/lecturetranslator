angular
    .module("app.data")
    .factory("AuthEntity", AuthEntityModel);

/*@ngInject*/
function AuthEntityModel() {

    var repository = {};

    function AuthEntity(data) {
        if (typeof data === 'number') {
            if (repository[data]) {
                return repository[data];
            }
            this.id = data;
            this.authenticated = false;
            return this;
        }
        var that = this;
        if (repository[data.id]) {
            that = repository[data.id];
        }
        that.id = data.id;
        that.authenticated = data.authenticated;
        repository[data.id] = that;
        return that;
    }
    return AuthEntity;

    //////////
}

angular
    .module("app.data")
    .factory("Lecture", LectureModel);

/*@ngInject*/
function LectureModel(LectureHall, Lecturer, AuthEntity) {

    function Lecture(data) {
        this.slug = data.slug;
        this.title = data.title;
        this.terms = data.lecture_terms || [];
        this.lectureTerms = [];
    }

    return Lecture;

    //////////
}

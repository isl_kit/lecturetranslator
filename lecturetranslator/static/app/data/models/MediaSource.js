angular
    .module("app.data")
    .factory("MediaSource", MediaSourceModel);

/*@ngInject*/
function MediaSourceModel() {

    var typePreferences = {
        mp4: 3,
        ogg: 2,
        webm: 1
    };

    function MediaSource(file, session_id) {
        this.file = file;
        if (file.indexOf("http://") === 0 || file.indexOf("https://") === 0 ) {
            this.path = file;
        } else {
            this.path = "/media/streams/" + session_id + "/" + file;
        }
        this.type = getMediaType(file);
    }

    MediaSource.sort = function (a, b) {
        a = typePreferences[a.type.split("/")[1]] || 0;
        b = typePreferences[b.type.split("/")[1]] || 0;
        return b - a;
    };

    return MediaSource;

    //////////

    function getMediaType(file) {
        var parts = file.split("."),
            ext = parts[parts.length - 1];
        if (file.indexOf("http://") === 0 || file.indexOf("https://") === 0 ) {
            return "video/" + ext;
        }
        if (parts[0] === "video") {
            if (ext === "ogv") {
                return "video/ogg";
            } else if (ext === "mp4") {
                return "video/mp4";
            } else if (ext === "webm") {
                return "video/webm";
            } else {
                return "video/" + ext;
            }
        } else {
            if (ext === "ogg") {
                return "audio/ogg";
            } else if (ext === "mp4") {
                return "audio/mp4";
            } else if (ext === "webm") {
                return "audio/webm";
            } else {
                return "audio/" + ext;
            }
        }
    }
}

angular
    .module("app.data")
    .factory("MediaCollection", MediaCollectionModel);

/*@ngInject*/
function MediaCollectionModel(MediaSource) {

    function MediaCollection(sessionId, data) {
        this.videos = _.map(data.video, function (path) {
            return new MediaSource(path, sessionId);
        });
        this.audios = _.map(data.audio, function (path) {
            return new MediaSource(path, sessionId);
        });
        this.urls = _.map(data.url, function (path) {
            return new MediaSource(path, sessionId);
        });
        this.sources = this.urls.length > 0 ? this.urls : this.videos.length > 0 ? this.videos : this.audios;
        this.hasVideo = this.urls.length > 0 || this.videos.length > 0;
        this.hasMedia = this.hasVideo || this.audios.length > 0;
    }

    return MediaCollection;

    //////////
}

angular
    .module("app.data")
    .factory("SessionNodes", SessionNodesModel);

/*@ngInject*/
function SessionNodesModel() {

    function SessionNodes(data) {
        this.children = {};
        this.parent = null;
        this.length = 0;
        this.totalLength = 0;

        this.buildTree(data);
    }

    SessionNodes.prototype.buildTree = buildTree;

    return SessionNodes;

    //////////

    function buildTree(data) {
        var main, that = this;

        _.each(data.streams, function (stream) {
            main = that;
            _.each(stream.compute_steps, function (step, i) {
                if (step.node) {
                    main = createNodeItem(main, step.node);
                }
                if (!step.node) {
                    main = createInputItem(main, step);
                } else if (i === stream.compute_steps.length - 1) {
                    main = createStepItem(main, step, stream, i === stream.compute_steps.length - 1, stream.ident === data.transcript_stream);
                }
            });
        });
        getLength(that);
    }

    function getLength(obj) {
        var res;
        if (obj.length === 0) {
            res = 1;
        } else {
            res = _.reduce(obj.children, function (memo, sub) {
                return memo + getLength(sub);
            }, 0);
        }
        obj.totalLength = res;
        return res;
    }

    function createStepItem(home, step, stream, last, source) {
        var stepId = getStepId(step);
        if (!_.has(home.children, stepId)) {
            home.children[stepId] = {
                itemType: "stream",
                main: last && source,
                last: last,
                fingerprint: step.fingerprint,
                type: step.type,
                name: step.name,
                control: stream.control,
                displayname: stream.displayname,
                sid: stream.sid,
                ident: stream.ident,
                children: {},
                parent: home,
                length: 0
            };
            home.length += 1;
        }
        return home.children[stepId];
    }

    function createInputItem(home, step) {
        var stepId = getStepId(step);
        if (!_.has(home.children, stepId)) {
            home.children[stepId] = {
                itemType: "stream",
                fingerprint: step.fingerprint,
                type: step.type,
                name: step.name,
                children: {},
                parent: home,
                length: 0
            };
            home.length += 1;
        }
        return home.children[stepId];
    }

    function createNodeItem(home, node) {
        var nodeId = getNodeId(node);
        if (!_.has(home.children, nodeId)) {
            home.children[nodeId] = {
                itemType: "node",
                name: node.name,
                creator: node.creator,
                input: {
                    fingerprint: node.input_fingerprint,
                    type: node.input_type
                },
                output: {
                    fingerprint: node.output_fingerprint,
                    type: node.output_type
                },
                children: {},
                parent: home,
                length: 0
            };
            home.length += 1;
        }
        return home.children[nodeId];
    }

    function getStepId(step) {
        return [
            step.fingerprint,
            step.type,
            step.name
        ].join("|");
    }

    function getNodeId(node) {
        return [
            node.name,
            node.creator,
            node.input_fingerprint,
            node.input_type,
            node.output_fingerprint,
            node.output_type
        ].join("|");
    }
}

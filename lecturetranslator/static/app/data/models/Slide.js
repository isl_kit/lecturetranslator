angular
    .module("app.data")
    .factory("Slide", SlideModel);

/*@ngInject*/
function SlideModel() {

    function Slide(id, file, sessionId, seconds, content) {
        this.id = id;
        this.uid = id + "_" + Math.floor(seconds);
        this.path = "/media/streams/" + sessionId + "/slides_360/" + file;
        this.start = seconds;
        this.stop = Infinity;
        this.duration = Infinity;
        this.time = formatDuration(seconds);
        this.content = content;
    }

    return Slide;

    //////////

    function formatDuration(seconds) {
        var minutes = Math.floor(seconds / 60);
        seconds = seconds % 60;
        var hours = Math.floor(minutes / 60);
        minutes = minutes % 60;
        return zeroPad(hours) + ":" + zeroPad(minutes) + ":" + zeroPad(Math.round(seconds));
    }

    function zeroPad(num) {
        if (num < 10) {
            return "0" + num;
        }
        return num;
    }
}

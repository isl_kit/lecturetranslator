angular
    .module("app.data")
    .factory("Data", DataModel);

/*@ngInject*/
function DataModel(AudioData, LinkData, helperService) {

    function Data(data, noSpace) {
        this.id = data.id || Math.floor(Math.random() * 1000000000);
        this.index = data.index;
        this.start = data.start;
        this.stop = data.stop;
        this.mediaStart = data.start / 1000;
        this.mediaStop = data.stop / 1000;
        this.creator = data.creator;
        this.created = data.created;
        this.paragraph = data.p;
        this.version = data.version;
        this.class = data.x;
        this.last = data.end_of_paragraph;
        this.orig = data.creator.toLowerCase().indexOf("asr") > -1 || data.creator.toLowerCase().indexOf("seg") > -1;
        this.text = data.text + (noSpace ? '' : ' ');
        this.htmlText = this.text;
        this.links = data.links ? _.map(data.links, function (link) {
            return new LinkData(link);
        }) : [];
        this.addLinks(this.links);
    }

    Data.prototype.update = update;
    Data.prototype.addLinks = addLinks;

    return Data;

    //////////

    function update(data) {
        if (data.v <= this.version) {
            return;
        }
        this.text = data.text + " ";
        this.htmlText = this.text;
        this.start = data.start;
        this.stop = data.stop;
        this.mediaStart = data.start / 1000;
        this.mediaStop = data.stop / 1000;
        this.version = data.version;
        this.class = data.x;
        this.last = data.end_of_paragraph;
    }

    function addLinks(linkDatas) {
        var text = this.text;
        _.each(linkDatas, function (linkData) {
            text = text.replace(new RegExp(linkData.name, "i"), "<a target='_blank' href='" + linkData.link + "'>$&</a>");
        });
        this.htmlText = text;
    }
}

angular
    .module("app.data")
    .factory("SessionMedia", SessionMediaModel);

/*@ngInject*/
function SessionMediaModel() {

    function SessionMedia(data) {
        this.mediaCollection = null;
        this.slides = [];
        this.VTTs = [];
    }

    SessionMedia.prototype.hasVideo = hasVideo;
    SessionMedia.prototype.hasMedia = hasMedia;

    return SessionMedia;

    //////////

    function hasVideo() {
        return this.mediaCollection && this.mediaCollection.hasVideo;
    }

    function hasMedia() {
        return this.mediaCollection && this.mediaCollection.hasMedia;
    }
}

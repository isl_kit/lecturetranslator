angular
    .module("app.data")
    .factory("FileNode", FileNodeModel);

/*@ngInject*/
function FileNodeModel(DocumentTag, Document, Directory, helperService) {

    function FileNode(data) {
        this.id = data.id;
        this.name = data.name;
        this.created = helperService.parseDateString(data.created);
        this.nodeType = data.node_type;
        this.numberOfChildren = data.number_of_children;
        this.parentId = data.parent || null;
        this.tags = _.map(data.tags, function (tagData) {
            if (typeof tagData === 'string') {
                tagData = { name: tagData };
            }
            return new DocumentTag(tagData);
        });
        this.document = data.document ? new Document(data.document) : null;
        this.directory = data.directory ? new Directory(data.directory) : null;
        this.ancestors = data.ancestors ? _.map(data.ancestors, function (nodeData) {
            return new FileNode(nodeData);
        }) : [];
        this.size = this.nodeType === 'DOC' ? this.document.resource.size : this.numberOfChildren;
    }

    return FileNode;
}

angular
    .module("app.data")
    .factory("LinkData", LinkDataModel);

/*@ngInject*/
function LinkDataModel() {

    function LinkData(data) {
        this.id = data.id;
        this.dataId = data.data_id;
        this.name = data.name;
        this.link = data.link;
    }

    return LinkData;

    //////////
}

angular
    .module("app.data")
    .factory("AudioData", AudioDataModel);

/*@ngInject*/
function AudioDataModel() {

    function AudioData(data) {
        this.id = data.id;
        this.index = data.index;
        this.duration = data.duration / 1000000;
        this.startTimestamp = data.start_timestamp;
        this.stopTimestamp = data.stop_timestamp;
    }

    return AudioData;

    //////////
}

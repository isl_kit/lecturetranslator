angular
    .module("app.sessions")
    .controller("CopyMoveModalController", CopyMoveModalController);

/*@ngInject*/
function CopyMoveModalController($mdDialog, nodeId) {
    var vm = this;

    vm.rootNodeId = nodeId || undefined;
    vm.selectedDir = undefined;
    vm.selectHandler = selectHandler;
    vm.cancel = cancel;
    vm.submit = submit;

    //////////

    function selectHandler(event) {
        vm.selectedDir = event.node;
    }

    function cancel() {
        $mdDialog.cancel();
    }

    function submit(action) {
        if (vm.selectedDir === undefined) {
            return;
        }
        $mdDialog.hide({
            action: action,
            node: vm.selectedDir
        });
    }
}

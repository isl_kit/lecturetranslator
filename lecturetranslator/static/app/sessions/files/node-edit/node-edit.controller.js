angular
    .module("app.sessions")
    .controller("NodeEditController", NodeEditController);

/*@ngInject*/
function NodeEditController($scope, $timeout, $state, $filter, node, fileService) {
    var vm = this;

    vm.node = node;
    vm.formData = {};
    vm.fileSaved = false;
    vm.submitFile = submitFile;
    vm.remove = remove;
    vm.goBack = goBack;
    vm.changeHandler = function () {
        console.log($scope);
    };

    activate();

    //////////

    function activate() {
        resetFormData();
    }

    function submitFile(form) {
        if (!form.$valid) {
            return;
        }
        var tags = _.map(vm.formData.tags, function (tag) {
            return tag.name;
        });
        tags = [];
        fileService.update(node.id, tags, vm.formData.name).then(function (node) {
            vm.node = node;
            resetFormData();
            vm.fileSaved = true;
            form.$setPristine();
            $timeout(function () {
                vm.fileSaved = false;
            }, 3000);
        });
    }

    function resetFormData() {
        vm.formData = {
            name: vm.node.name,
            created: $filter('date')(vm.node.created, 'yyyy-MM-dd HH:mm'),
            size: vm.node.nodeType === 'DOC' ? $filter('fileSize')(vm.node.size) : vm.node.size,
            tags: vm.node.tags.slice(0)
        };
    }

    function remove() {
        fileService.remove(node).then(goBack);
    }

    function goBack() {
        $state.go('main.files', {node: vm.node.parentId});
    }
}

angular
    .module("app.sessions")
    .component("eventSelect", {
        bindings: {
            eventId: "<",
            onEventSelect: "&"
        },
        controller: "EventSelectController",
        controllerAs: "vm",
        templateUrl: "sessions/files/event-select/event-select.tpl.html"
});

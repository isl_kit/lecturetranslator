angular
    .module("app.sessions")
    .controller("EventSelectController", EventSelectController);

/*@ngInject*/
function EventSelectController($state, lecturerService, eventService) {
    var vm = this;

    vm.events = [];
    vm.event = null;

    vm.select = select;

    vm.$onInit = activate;
    vm.$onChanges = onChanges;

    //////////

    function activate() {
        lecturerService.me().then(function (lecturer) {
            if (!lecturer) {
                return;
            }
            eventService.list({ lecturer: lecturer.id }).then(function (events) {
                vm.events = events;
                doSelect();
            });
        });
    }

    function onChanges(changes) {
        if (changes.eventId.currentValue !== changes.eventId.previousValue) {
            doSelect();
        }
    }

    function doSelect() {
        vm.event = _.find(vm.events, function (event) {
            return event.id === vm.eventId;
        }) || null;
    }

    function select(event) {
        if (event === vm.event) {
            $state.go('main.files', { event: undefined }, { inherit: true });
        } else {
            $state.go('main.files', { event: event }, { inherit: true });
        }
        vm.onEventSelect({
            $event: {
                event: vm.event
            }
        });
    }
}

angular
    .module("app.sessions")
    .controller("FileUploadModalController", FileUploadModalController);

/*@ngInject*/
function FileUploadModalController($state, $mdDialog, FileNode, parentId, eventId) {
    var vm = this;
    var counter = 0;
    var nodes = [];

    vm.uploadFiles = [];
    vm.progressEnabled = !!window.FormData;

    vm.cancel = cancel;
    vm.remove = remove;
    vm.add = add;
    vm.done = done;
    vm.fail = fail;
    vm.progress = progress;
    vm.startUpload = startUpload;

    //////////

    function cancel() {
        $mdDialog.cancel();
    }

    function remove(uploadFileObj) {
        vm.uploadFiles = _.without(vm.uploadFiles, uploadFileObj);
    }

    function add(e, data) {
        var uploadFileObj, key;

        uploadFileObj = {
            data: data,
            isUploading: false,
            name: data.files[0].name,
            progress: 0,
            tags: []
        };
        data.formData = {
            tags: []
        };
        if (parentId !== undefined) {
            data.formData.parent = parentId;
        }
        if (eventId !== undefined) {
            data.formData.event = eventId;
        }
        data.fileObjKey = "c" + (counter++);
        vm.uploadFiles.push(uploadFileObj);

    }

    function done(e, data) {
        var node = new FileNode(data.result);
        nodes.push(node);
        vm.uploadFiles = _.filter(vm.uploadFiles, function (uploadFileObj) {
            return uploadFileObj.data.fileObjKey !== data.fileObjKey;
        });
        if (vm.uploadFiles.length === 0) {
            $mdDialog.hide(nodes);
        }
    }

    function fail(e, data) {
        $mdDialog.cancel();
    }

    function progress(e, data) {
        var uploadFileObj = _.find(vm.uploadFiles, function (uploadFileObj) {
            return uploadFileObj.data.fileObjKey === data.fileObjKey;
        });
        if (vm.progressEnabled && uploadFileObj) {
            uploadFileObj.progress = parseInt(100.0 * data.loaded / data.total, 10);
        }
    }

    function startUpload() {
        _.each(vm.uploadFiles, function (uploadFileObj) {
            uploadFileObj.isUploading = true;
            /*
            uploadFileObj.data.formData.tags = JSON.stringify(_.map(uploadFileObj.tags, function (tag) {
                return tag.name;
            }));*/
            uploadFileObj.data.formData.tags = '[]';
            uploadFileObj.data.submit();
        });
    }
}

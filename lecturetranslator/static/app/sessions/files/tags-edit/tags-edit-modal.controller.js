angular
    .module("app.sessions")
    .controller("TagsEditModalController", TagsEditModalController);

/*@ngInject*/
function TagsEditModalController($mdDialog, node) {
    var vm = this;

    vm.tags = node.tags.slice(0);
    vm.cancel = cancel;
    vm.submit = submit;

    //////////

    function cancel() {
        $mdDialog.cancel();
    }

    function submit(form) {
        if (!form.$valid) {
            return;
        }
        var tags = _.map(vm.tags, function (tag) {
            return tag.name;
        });
        $mdDialog.hide(tags);
    }
}

angular
    .module("app.sessions")
    .component("eventNodes", {
        bindings: {
            event: "<",
            readOnly: "<"
        },
        controller: "EventNodesController",
        controllerAs: "vm",
        templateUrl: "sessions/files/event-nodes/event-nodes.tpl.html"
});

angular
    .module("app.sessions")
    .controller("EventNodesController", EventNodesController);

/*@ngInject*/
function EventNodesController($scope, $state, fileService) {
    var vm = this;

    var parentId = null;

    var sortFunctions = {
        name: [nodeSortNameAsc, nodeSortNameDesc],
        size: [nodeSortSizeAsc, nodeSortSizeDesc],
        date: [nodeSortDateAsc, nodeSortDateDesc]
    };

    var selectedBaseNode = null;

    vm.nodes = [];
    vm.selectedNodes = [];

    vm.currentOrder = {
        type: 'date',
        isAscending: false
    };
    vm.sort = sort;

    vm.select = select;
    vm.deleteNode = deleteNode;
    vm.deleteSelected = deleteSelected;
    vm.rename = rename;
    vm.startUpload = startUpload;
    vm.download = download;
    vm.downloadSelected = downloadSelected;

    vm.$onInit = activate;

    //////////

    function activate() {
        getNodes();
    }

    function getNodes() {
        fileService.getEventNodes(vm.event.id).then(function (nodes) {
            vm.nodes = nodes;
            vm.sort('date', false);
        });
    }

    function select(event, node) {
        if (vm.readOnly) {
            return;
        }
        if (event.shiftKey) {
            selectRangeTo(node);
        } else if (event.ctrlKey) {
            toggleSelectSingle(node);
        } else {
            clearSelection();
            toggleSelectSingle(node);
        }
    }

    function deleteNode(node) {
        fileService.remove([node]).then(function () {
            removeNodesFromList([node]);
        });
    }

    function deleteSelected() {
        fileService.remove(vm.selectedNodes).then(function () {
            removeNodesFromList(vm.selectedNodes);
        });
    }

    function rename(node) {
        fileService.rename(node);
    }

    function startUpload() {
        fileService.upload(undefined, vm.event.id).then(addNodesToList);
    }

    function download(node) {
        window.open('/media/files/' + node.document.resource.hash + '/orig');
    }

    function downloadSelected() {
        _.each(vm.selectedNodes, download);
    }

    //////////

    function removeNodesFromList(nodes) {
        var ids = _.map(nodes, function (node) {
            return node.id;
        });
        vm.nodes = _.filter(vm.nodes, function (node) {
            return !_.contains(ids, node.id);
        });
        clearSelection();
    }

    function addNodesToList(nodes) {
        var changed = false;
        _.each(nodes, addOrReplaceNode);
        applyCurrentSort();
        clearSelection();
    }

    function getNodeById(nodeId) {
        return _.find(vm.nodes, function (n) {
            return n.id === nodeId;
        });
    }

    function toggleSelectSingle(node) {
        if (node.selected) {
            vm.selectedNodes = _.without(vm.selectedNodes, node);
            if (node === selectedBaseNode) {
                selectedBaseNode = vm.selectedNodes[0] || null;
            }
        } else {
            vm.selectedNodes.push(node);
            selectedBaseNode = node;
        }
        node.selected = !node.selected;
    }

    function selectRangeTo(node) {
        var baseNode = selectedBaseNode;
        if (!baseNode) {
            toggleSelectSingle(node);
        } else {
            var nodeIdx = _.indexOf(vm.nodes, node),
                baseNodeIdx = _.indexOf(vm.nodes, baseNode);
            clearSelection();
            selectRange(baseNodeIdx, nodeIdx);
            selectedBaseNode = vm.nodes[baseNodeIdx];
        }
    }

    function selectRange(idx1, idx2) {
        if (idx2 < idx1) {
            var s = idx2;
            idx2 = idx1;
            idx1 = s;
        }
        for (; idx1 <= idx2; idx1++) {
            vm.nodes[idx1].selected = true;
            vm.selectedNodes.push(vm.nodes[idx1]);
        }
    }

    function clearSelection() {
        _.each(vm.selectedNodes, function (node) {
            node.selected = false;
        });
        vm.selectedNodes = [];
        selectedBaseNode = null;
    }

    function clearSelectionAndBroadcast() {
        clearSelection();
    }

    function addOrReplaceNode(node) {
        var idx = _.findIndex(vm.nodes, function (n) {
            return n.id === node.id;
        });
        if (idx > -1) {
            vm.nodes.splice(idx, 1, node);
        } else {
            vm.nodes.push(node);
        }
    }

    function applyCurrentSort() {
        var idx = vm.currentOrder.isAscending ? 0 : 1;
        vm.nodes.sort(sortFunctions[vm.currentOrder.type][idx]);
    }

    function sort(sortType, isAscending) {
        if (vm.currentOrder.type === sortType) {
            vm.currentOrder.isAscending = isAscending === undefined ? !vm.currentOrder.isAscending : isAscending;
        } else {
            vm.currentOrder = {
                type: sortType,
                isAscending: isAscending === undefined ? true : isAscending
            };
        }
        applyCurrentSort();
    }

    function nodeSortName(a, b) {
        var aLower = a.name.toLowerCase(),
            bLower = b.name.toLowerCase();
        if (aLower < bLower) {
            return -1;
        }
        if (bLower < aLower) {
            return 1;
        }
        return 0;
    }

    function nodeSortNameAsc(a, b) {
        var typeRes = nodeSortType(a, b);
        if (typeRes !== 0) {
            return typeRes;
        }
        return nodeSortName(a, b);
    }

    function nodeSortNameDesc(a, b) {
        var typeRes = nodeSortType(a, b);
        if (typeRes !== 0) {
            return typeRes;
        }
        return nodeSortName(a, b) * -1;
    }

    function nodeSortType(a, b) {
        if (a.nodeType < b.nodeType) {
            return -1;
        } else if (b.nodeType < a.nodeType) {
            return 1;
        }
        return 0;
    }

    function nodeSortDate(a ,b) {
        return a.created - b.created;
    }

    function nodeSortDateAsc(a ,b) {
        var typeRes = nodeSortType(a, b);
        if (typeRes !== 0) {
            return typeRes;
        }
        return nodeSortDate(a ,b);
    }

    function nodeSortDateDesc(a ,b) {
        var typeRes = nodeSortType(a, b);
        if (typeRes !== 0) {
            return typeRes;
        }
        return nodeSortDate(a ,b) * -1;
    }


    function nodeSortSize(a ,b) {
        return a.size - b.size;
    }


    function nodeSortSizeAsc(a ,b) {
        var typeRes = nodeSortType(a, b);
        if (typeRes !== 0) {
            return typeRes;
        }
        return nodeSortSize(a ,b);
    }

    function nodeSortSizeDesc(a ,b) {
        var typeRes = nodeSortType(a, b);
        if (typeRes !== 0) {
            return typeRes;
        }
        return nodeSortSize(a ,b) * -1;
    }
}

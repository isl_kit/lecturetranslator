angular
    .module("app.sessions")
    .controller("DirCreateModalController", DirCreateModalController);

/*@ngInject*/
function DirCreateModalController($mdDialog) {
    var vm = this;

    vm.dirName = '';
    vm.cancel = cancel;
    vm.submit = submit;

    //////////

    function cancel() {
        $mdDialog.cancel();
    }

    function submit(form) {
        if (!form.$valid) {
            return;
        }
        console.log(form);
        $mdDialog.hide(vm.dirName);
    }
}

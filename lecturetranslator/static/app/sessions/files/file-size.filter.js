angular
    .module("app.sessions")
    .filter("fileSize", fileSizeFilter);

/*@ngInject*/
function fileSizeFilter() {
    var sizes = [
        "Bytes", "KB", "MB", "GB", "TB", "PB"
    ];
    return function (size) {
        var i = 0;
        while (size > 1024) {
            size /= 1024;
            i += 1;
        }
        return (Math.round(size * 100) / 100) + " " + sizes[i];
    };
}

angular
    .module("app.sessions")
    .controller("NodeRenameModalController", NodeRenameModalController);

/*@ngInject*/
function NodeRenameModalController($mdDialog, node) {
    var vm = this;

    vm.nodeName = node.name;
    vm.cancel = cancel;
    vm.submit = submit;

    //////////

    function cancel() {
        $mdDialog.cancel();
    }

    function submit(form) {
        if (!form.$valid) {
            return;
        }
        $mdDialog.hide(vm.nodeName);
    }
}

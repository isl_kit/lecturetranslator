angular
    .module("app.sessions")
    .component("nodeList", {
        bindings: {
            parent: "<",
            onSelectChange: "&"
        },
        controller: "NodeListController",
        controllerAs: "vm",
        templateUrl: "sessions/files/node-list/node-list.tpl.html"
});

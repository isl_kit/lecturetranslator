angular
    .module("app.sessions")
    .controller("NodeListController", NodeListController);

/*@ngInject*/
function NodeListController($scope, $state, fileService) {
    var vm = this;

    var parentId = null;

    var sortFunctions = {
        name: [nodeSortNameAsc, nodeSortNameDesc],
        size: [nodeSortSizeAsc, nodeSortSizeDesc],
        date: [nodeSortDateAsc, nodeSortDateDesc]
    };

    var selectedNodes = [],
        selectedBaseNode = null;

    vm.nodes = [];

    vm.currentOrder = {
        type: 'date',
        isAscending: false
    };
    vm.sort = sort;

    vm.select = select;
    vm.deleteNode = deleteNode;
    vm.rename = rename;
    vm.editTags = editTags;
    vm.copyMove = copyMove;
    vm.showText = showText;

    vm.open = function (node) {
        if (node.nodeType === 'DIR') {
            $state.go("main.files", { node: node.id }, { inherit: true });
        } else {
            //$state.go("main.file",  { id: node.id });
        }
    };

    vm.$onInit = activate;
    vm.$onChanges = onChanges;

    //////////

    function activate() {
        console.log("activate");
        $scope.$on('nodes.create', nodesCreateHandler);
        $scope.$on('nodes.delete', nodeDeleteHandler);
        $scope.$on('nodes.update', nodeUpdateHandler);
    }

    function onChanges(changes) {
        if (changes.parent.currentValue !== changes.parent.previousValue) {
            console.log("changes");
            parentId = vm.parent ? vm.parent.id : null;
            getNodes();
        }
    }

    function getNodes() {
        fileService.getNodes(parentId || undefined).then(function (nodes) {
            vm.nodes = nodes;
            vm.sort('date', false);
        });
    }

    function select(event, node) {
        if (event.shiftKey) {
            selectRangeTo(node);
        } else if (event.ctrlKey) {
            toggleSelectSingle(node);
        } else {
            clearSelection();
            toggleSelectSingle(node);
        }
        vm.onSelectChange({
            $event: {
                nodes: selectedNodes.slice(0)
            }
        });
    }

    function deleteNode(node) {
        fileService.remove([node]);
    }

    function rename(node) {
        fileService.rename(node);
    }

    function editTags(node) {
        fileService.editTags(node);
    }

    function copyMove(node) {
        fileService.copyMove([node], parentId).then(function (result) {
            var destinationParentId = result.parent ? result.parent.id : null;
            if (parentId !== destinationParentId) {
                $state.go('main.files', { node: destinationParentId }, { inherit: true });
            }
        });
    }

    function showText(node) {
        window.open('/media/files/' + node.document.resource.hash + '/content.txt');
    }

    //////////

    function nodeDeleteHandler(event, nodes) {
        var ids = _.map(nodes, function (node) {
            return node.id;
        });
        vm.nodes = _.filter(vm.nodes, function (node) {
            return !_.contains(ids, node.id);
        });
        clearSelectionAndBroadcast();
    }

    function nodeUpdateHandler(event, node) {
        var matchNode = getNodeById(node.id);
        if (matchNode) {
            matchNode.name = node.name;
            matchNode.tags = node.tags.slice(0);
            applyCurrentSort();
        }
        clearSelectionAndBroadcast();
    }

    function nodesCreateHandler(event, nodes) {
        var changed = false;
        _.each(nodes, function (node) {
            if (node.parentId === parentId) {
                addOrReplaceNode(node);
                changed = true;
            }
        });
        if (changed) {
            applyCurrentSort();
        }
        clearSelectionAndBroadcast();
    }

    function getNodeById(nodeId) {
        return _.find(vm.nodes, function (n) {
            return n.id === nodeId;
        });
    }

    function toggleSelectSingle(node) {
        if (node.selected) {
            selectedNodes = _.without(selectedNodes, node);
            if (node === selectedBaseNode) {
                selectedBaseNode = selectedNodes[0] || null;
            }
        } else {
            selectedNodes.push(node);
            selectedBaseNode = node;
        }
        node.selected = !node.selected;
    }

    function selectRangeTo(node) {
        var baseNode = selectedBaseNode;
        if (!baseNode) {
            toggleSelectSingle(node);
        } else {
            var nodeIdx = _.indexOf(vm.nodes, node),
                baseNodeIdx = _.indexOf(vm.nodes, baseNode);
            clearSelection();
            selectRange(baseNodeIdx, nodeIdx);
            selectedBaseNode = vm.nodes[baseNodeIdx];
        }
    }

    function selectRange(idx1, idx2) {
        if (idx2 < idx1) {
            var s = idx2;
            idx2 = idx1;
            idx1 = s;
        }
        for (; idx1 <= idx2; idx1++) {
            vm.nodes[idx1].selected = true;
            selectedNodes.push(vm.nodes[idx1]);
        }
    }

    function clearSelection() {
        _.each(selectedNodes, function (node) {
            node.selected = false;
        });
        selectedNodes = [];
        selectedBaseNode = null;
    }

    function clearSelectionAndBroadcast() {
        clearSelection();
        vm.onSelectChange({
            $event: {
                nodes: []
            }
        });
    }

    function addOrReplaceNode(node) {
        console.log('add or replace', node, vm.nodes);
        var idx = _.findIndex(vm.nodes, function (n) {
            return n.id === node.id;
        });
        if (idx > -1) {
            vm.nodes.splice(idx, 1, node);
        } else {
            vm.nodes.push(node);
        }
    }

    function applyCurrentSort() {
        var idx = vm.currentOrder.isAscending ? 0 : 1;
        vm.nodes.sort(sortFunctions[vm.currentOrder.type][idx]);
    }

    function sort(sortType, isAscending) {
        if (vm.currentOrder.type === sortType) {
            vm.currentOrder.isAscending = isAscending === undefined ? !vm.currentOrder.isAscending : isAscending;
        } else {
            vm.currentOrder = {
                type: sortType,
                isAscending: isAscending === undefined ? true : isAscending
            };
        }
        applyCurrentSort();
    }

    function nodeSortName(a, b) {
        var aLower = a.name.toLowerCase(),
            bLower = b.name.toLowerCase();
        if (aLower < bLower) {
            return -1;
        }
        if (bLower < aLower) {
            return 1;
        }
        return 0;
    }

    function nodeSortNameAsc(a, b) {
        var typeRes = nodeSortType(a, b);
        if (typeRes !== 0) {
            return typeRes;
        }
        return nodeSortName(a, b);
    }

    function nodeSortNameDesc(a, b) {
        var typeRes = nodeSortType(a, b);
        if (typeRes !== 0) {
            return typeRes;
        }
        return nodeSortName(a, b) * -1;
    }

    function nodeSortType(a, b) {
        if (a.nodeType < b.nodeType) {
            return -1;
        } else if (b.nodeType < a.nodeType) {
            return 1;
        }
        return 0;
    }

    function nodeSortDate(a ,b) {
        return a.created - b.created;
    }

    function nodeSortDateAsc(a ,b) {
        var typeRes = nodeSortType(a, b);
        if (typeRes !== 0) {
            return typeRes;
        }
        return nodeSortDate(a ,b);
    }

    function nodeSortDateDesc(a ,b) {
        var typeRes = nodeSortType(a, b);
        if (typeRes !== 0) {
            return typeRes;
        }
        return nodeSortDate(a ,b) * -1;
    }


    function nodeSortSize(a ,b) {
        return a.size - b.size;
    }


    function nodeSortSizeAsc(a ,b) {
        var typeRes = nodeSortType(a, b);
        if (typeRes !== 0) {
            return typeRes;
        }
        return nodeSortSize(a ,b);
    }

    function nodeSortSizeDesc(a ,b) {
        var typeRes = nodeSortType(a, b);
        if (typeRes !== 0) {
            return typeRes;
        }
        return nodeSortSize(a ,b) * -1;
    }
}

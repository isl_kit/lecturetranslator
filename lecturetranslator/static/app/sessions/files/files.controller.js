angular
    .module("app.sessions")
    .controller("FilesController", FilesController);

/*@ngInject*/
function FilesController($scope, $stateParams, $state, fileService) {
    var vm = this,
        parentId = null,
        selectedNodes = [];

    vm.parent = null;
    vm.initialized = false;

    vm.startUpload = startUpload;
    vm.addDirectory = addDirectory;
    vm.selectChangeHandler = selectChangeHandler;
    vm.selectionSize = 0;

    vm.deleteNodes = deleteNodes;
    vm.rename = rename;
    vm.editTags = editTags;
    vm.copyMove = copyMove;

    vm.eventSelectHandler = function ($event) {
        console.log($event.event);
    };

    vm.uiOnParamsChanged = changeHandler;

    activate();

    //////////

    function activate() {
        changeHandler($stateParams);
    }

    function changeHandler(newParams) {
        if (newParams.hasOwnProperty('node')) {
            updateNode(+newParams.node || null);
        }
    }

    function updateNode(nodeId) {
        clearSelection();
        parentId = nodeId;
        if (parentId) {
            fileService.getNode(parentId, 'DIR').then(function (node) {
                vm.parent = node;
                vm.initialized = true;
            });
        } else {
            vm.parent = null;
            vm.initialized = true;
        }
    }

    function startUpload() {
        fileService.upload(+$stateParams.node || undefined);
    }

    function addDirectory() {
        fileService.addDirectory(parentId);
    }

    function selectChangeHandler(event) {
        vm.selectionSize = event.nodes.length;
        selectedNodes = event.nodes;
    }

    function clearSelection() {
        vm.selectionSize = 0;
        selectedNodes = [];
    }

    function deleteNodes() {
        fileService.remove(selectedNodes);
    }

    function rename() {
        fileService.rename(selectedNodes[0]);
    }

    function editTags() {
        fileService.editTags(selectedNodes[0]);
    }

    function copyMove() {
        fileService.copyMove(selectedNodes, parentId).then(function (result) {
            var destinationParentId = result.parent ? result.parent.id : null;
            if (parentId !== destinationParentId) {
                $state.go('main.files', { node: destinationParentId });
            }
        });
    }
}

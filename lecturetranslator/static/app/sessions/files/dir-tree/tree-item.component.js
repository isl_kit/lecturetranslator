angular
    .module("app.sessions")
    .component("treeItem", {
        bindings: {
            item: '<',
            level: '<',
            tree: '<'
        },
        controller: "TreeItemController",
        controllerAs: "vm",
        templateUrl: "sessions/files/dir-tree/tree-item.tpl.html"
});

angular
    .module("app.sessions")
    .controller("DirTreeController", DirTreeController);

/*@ngInject*/
function DirTreeController(TreeItem, fileService) {
    var vm = this;
    var itemHash = {};
    var selectedItem = null;

    vm.homeItem = null;
    vm.select = select;

    vm.$onInit = activate;

    //////////

    function activate() {
        vm.homeItem = new TreeItem({
            name: 'HOME',
            numberOfChildren: 1
        }, null);
        fileService.getNodesTree(vm.nodeId || undefined).then(function (nodes) {
            var matchItem;
            _.each(nodes, function (node) {
                var treeItem = new TreeItem(node),
                    parentItem = node.parentId === null ? vm.homeItem : itemHash[node.parentId];
                if (node.id === vm.nodeId) {
                    matchItem = treeItem;
                }
                parentItem.isResolved = true;
                parentItem.isOpen = true;
                parentItem.children.push(treeItem);
                itemHash[node.id] = treeItem;
            });
            if (matchItem) {
                select(matchItem);
            }
        });
    }

    function select(treeItem) {
        if (treeItem.selected) {
            return;
        }
        if (selectedItem) {
            selectedItem.selected = false;
        }
        treeItem.selected = true;
        selectedItem = treeItem;
        vm.onSelect({
            $event: {
                node: selectedItem === vm.homeItem ? null : selectedItem.node
            }
        });
    }
}

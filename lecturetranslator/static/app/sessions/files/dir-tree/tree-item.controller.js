angular
    .module("app.sessions")
    .controller("TreeItemController", TreeItemController);

/*@ngInject*/
function TreeItemController() {
    var vm = this;

    vm.style = {};
    vm.toggleOpen = toggleOpen;

    vm.$onInit = activate;

    //////////

    function activate() {
        vm.style = {
            'padding-left': (vm.level * 20 + 10) + 'px'
        };
    }

    function toggleOpen() {
        if (vm.item.isOpen) {
            vm.item.isOpen = false;
        } else {
            vm.item.getChildren().then(function () {
                vm.item.isOpen = true;
            });
        }
        vm.tree.select(vm.item);
    }
}

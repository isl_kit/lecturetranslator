angular
    .module("app.data")
    .factory("TreeItem", TreeItemModel);

/*@ngInject*/
function TreeItemModel($q, fileService) {

    function TreeItem(node) {
        this.node = node;
        this.isResolved = node.numberOfChildren === 0;
        this.isOpen = false;
        this.selected = false;
        this.children = [];

    }

    TreeItem.prototype.getChildren = getChildren;

    return TreeItem;

    //////////

    function getChildren() {
        var deferred = $q.defer(),
            that = this;
        if (that.isResolved) {
            deferred.resolve();
        } else {
            fileService.getNodes(that.node.id, 'DIR').then(function (nodes) {
                that.children = _.map(nodes, function (node) {
                    return new TreeItem(node);
                });
                that.isResolved = true;
                deferred.resolve();
            }, function () {
                deferred.reject();
            });
        }
        return deferred.promise;
    }
}

angular
    .module("app.sessions")
    .component("dirTree", {
        bindings: {
            nodeId: '<',
            onSelect: '&'
        },
        controller: "DirTreeController",
        controllerAs: "vm",
        templateUrl: "sessions/files/dir-tree/dir-tree.tpl.html"
});

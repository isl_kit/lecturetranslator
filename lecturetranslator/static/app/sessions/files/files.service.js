angular
    .module("app.sessions")
    .factory("fileService", fileService);

/*@ngInject*/
function fileService($rootScope, $http, $q, $mdDialog, FileNode, DocumentTag) {

    var service = {
        getNodes: getNodes,
        getEventNodes: getEventNodes,
        getNodesTree: getNodesTree,
        getNode: getNode,
        update: update,
        getTags: getTags,
        remove: remove,
        upload: upload,
        addDirectory: addDirectory,
        rename: rename,
        editTags: editTags,
        copyMove: copyMove
    };
    return service;

    //////////


    function getNodes(parentId, type) {
        var url = "/resources/nodes/?";
        if (parentId === undefined) {
            url += 'parent__isnull=true';
        } else {
            url += 'parent=' + parentId;
        }
        if (type !== undefined) {
            url += '&type=' + type;
        }
        return $http.get(url).then(function (response) {
            return _.map(response.data, function (nodeData) {
                return new FileNode(nodeData);
            });
        });
    }

    function getEventNodes(eventId) {
        var url = "/resources/nodes/?parent__isnull=true&type=DOC&event=" + eventId;
        return $http.get(url).then(function (response) {
            return _.map(response.data, function (nodeData) {
                return new FileNode(nodeData);
            });
        });
    }

    function getNodesTree(nodeId) {
        var url = "/resources/nodes/tree/";
        if (nodeId !== undefined) {
            url += '?id=' + nodeId;
        }
        return $http.get(url).then(function (response) {
            return _.map(response.data, function (nodeData) {
                return new FileNode(nodeData);
            });
        });
    }

    function getNode(id, type) {
        var url = "/resources/nodes/" + id + "/";
        if (type !== undefined) {
            url += '?type=' + type;
        }
        return $http.get(url).then(function (response) {
            return new FileNode(response.data);
        });
    }

    function getTags(search) {
        var url = "/resources/tags/";
        if (search) {
            url += "?search=" + search;
        }
        return $http.get(url).then(function (response) {
            return _.map(response.data, function (tagData) {
                return new DocumentTag(tagData);
            });
        });
    }

    function update(id, tags, name) {
        var url = "/resources/nodes/" + id + "/";
        return $http.patch(url, {
            tags: tags,
            name: name
        }).then(function (response) {
            var node = new FileNode(response.data);
            $rootScope.$broadcast('nodes.update', node);
            return node;
        });
    }

    function remove(nodes) {
        var deferred = $q.defer();
        var desc = nodes.length === 1 ? nodes[0].nodeType === 'DIR' ? 'directory' : 'document' : 'items';
        var names = _.map(nodes, function (node) { return node.name; }).join(', ');
        var confirm = $mdDialog.confirm()
            .title('Would you like to delete this ' + desc + '?')
            .textContent(names)
            .ariaLabel('Delete ' + desc)
            .targetEvent(null)
            .ok('Yes')
            .cancel('Cancel');
        $mdDialog.show(confirm).then(function () {
            var ids = _.map(nodes, function (node) { return node.id; });
            var queryParams = '?ids=' + ids.join('&ids=');
            $http.delete("/resources/nodes/set/" + queryParams).then(function () {
                $rootScope.$broadcast('nodes.delete', nodes);
                deferred.resolve(true);
            }, function () {
                deferred.reject();
            });
        }, function() {
            deferred.reject();
        });
        return deferred.promise;
    }

    function upload(parentId, eventId) {
        return $mdDialog.show({
            controller: 'FileUploadModalController',
            controllerAs: 'vm',
            templateUrl: 'sessions/files/file-upload/file-upload-modal.tpl.html',
            locals: { parentId: parentId, eventId: eventId },
            parent: angular.element(document.body),
            targetEvent: null,
            clickOutsideToClose: false,
            fullscreen: false
        }).then(function (nodes) {
            $rootScope.$broadcast('nodes.create', nodes);
            return nodes;
        });
    }

    function addDirectory(parentId) {
        var deferred = $q.defer();
        $mdDialog.show({
            controller: 'DirCreateModalController',
            controllerAs: 'vm',
            templateUrl: 'sessions/files/dir-create/dir-create-modal.tpl.html',
            parent: angular.element(document.body),
            targetEvent: null,
            clickOutsideToClose: false,
            fullscreen: false
        }).then(function (dirName) {
            var data = {
                name: dirName,
                parent: parentId || null,
                tags: []
            };
            $http.post("/resources/nodes/", data).then(function (response) {
                var node = new FileNode(response.data);
                $rootScope.$broadcast('nodes.create', [node]);
                deferred.resolve(node);
            }, function () {
                deferred.reject();
            });
        }, function () {
            deferred.reject();
        });
        return deferred.promise;
    }

    function rename(node) {
        var deferred = $q.defer();
        $mdDialog.show({
            controller: 'NodeRenameModalController',
            controllerAs: 'vm',
            templateUrl: 'sessions/files/node-rename/node-rename-modal.tpl.html',
            locals: { node: node },
            parent: angular.element(document.body),
            targetEvent: null,
            clickOutsideToClose: false,
            fullscreen: false
        }).then(function (nodeName) {
            $http.patch('/resources/nodes/' + node.id + '/', {
                name: nodeName
            }).then(function (response) {
                var newNode = new FileNode(response.data);
                node.name = newNode.name;
                console.log("broadcast");
                $rootScope.$broadcast('nodes.update', newNode);
                deferred.resolve(newNode);
            }, function () {
                deferred.reject();
            });
        }, function () {
            deferred.reject();
        });
        return deferred.promise;
    }

    function editTags(node) {
        var deferred = $q.defer();
        $mdDialog.show({
            controller: 'TagsEditModalController',
            controllerAs: 'vm',
            templateUrl: 'sessions/files/tags-edit/tags-edit-modal.tpl.html',
            locals: { node: node },
            parent: angular.element(document.body),
            targetEvent: null,
            clickOutsideToClose: false,
            fullscreen: false
        }).then(function (tags) {
            $http.patch('/resources/nodes/' + node.id + '/', {
                tags: tags
            }).then(function (response) {
                var newNode = new FileNode(response.data);
                node.tags = newNode.tags.slice(0);
                $rootScope.$broadcast('nodes.update', newNode);
                deferred.resolve(newNode);
            }, function () {
                deferred.reject();
            });
        }, function () {
            deferred.reject();
        });
        return deferred.promise;
    }

    function copyMove(nodes, parentId) {
        var deferred = $q.defer();
        $mdDialog.show({
            controller: 'CopyMoveModalController',
            controllerAs: 'vm',
            templateUrl: 'sessions/files/copy-move/copy-move-modal.tpl.html',
            locals: { nodeId: parentId },
            parent: angular.element(document.body),
            targetEvent: null,
            clickOutsideToClose: false,
            fullscreen: false
        }).then(function (result) {
            var url = '/resources/nodes/' + result.action + '/',
                data = {
                    destination: result.node ? result.node.id : null,
                    nodes: _.map(nodes, function (node) {
                        return node.id;
                    })
                };
            $http.post(url, data).then(function (response) {
                var newNodes = _.map(response.data, function (nodeData) {
                    return new FileNode(nodeData);
                });
                $rootScope.$broadcast('nodes.create', newNodes);
                deferred.resolve({
                    action: result.action,
                    nodes: newNodes,
                    parent: result.node
                });
            }, function () {
                deferred.reject();
            });
        }, function () {
            deferred.reject();
        });
        return deferred.promise;
    }
}

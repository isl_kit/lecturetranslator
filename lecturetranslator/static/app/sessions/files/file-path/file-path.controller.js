angular
    .module("app.sessions")
    .controller("FilePathController", FilePathController);

/*@ngInject*/
function FilePathController() {
    var vm = this;

    vm.path = [
        { label: 'root', id: null }
    ];

    vm.$onChanges = onChanges;

    //////////

    function onChanges(changes) {
        if (changes.node.currentValue !== changes.node.previousValue) {
            vm.path = [
                { label: 'root', id: null }
            ];
            if (vm.node) {
                _.each(vm.node.ancestors.reverse(), function (node) {
                    vm.path.push({
                        label: node.name, id: node.id
                    });
                });
                vm.path.push({
                    label: vm.node.name, id: vm.node.id
                });
            }
        }
    }
}

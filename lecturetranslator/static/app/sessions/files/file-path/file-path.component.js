angular
    .module("app.sessions")
    .component("filePath", {
        bindings: {
            node: "<"
        },
        controller: "FilePathController",
        controllerAs: "vm",
        templateUrl: "sessions/files/file-path/file-path.tpl.html"
});

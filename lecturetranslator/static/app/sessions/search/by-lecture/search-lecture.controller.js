angular
    .module("app.sessions")
    .controller("SearchLectureController", SearchLectureController);

/*@ngInject*/
function SearchLectureController($state, $stateParams, lecturerService, lectureHallService, lectureService, helperService) {
    var vm = this;

    vm.title = "";

    vm.halls = undefined;
    vm.lecturers = undefined;

    vm.filterLimit = 5;
    vm.showAllHalls = false;
    vm.showAllLecturers = false;

    vm.selectedHalls = [];
    vm.selectedLecturers = [];

    vm.lectures = [];
    vm.refresh = refresh;

    vm.filterOpen = false;
    vm.toggleFilter = toggleFilter;

    activate();

    //////////

    function activate() {
        loadLectures();
        lecturerService.list().then(function (lecturers) {
            vm.lecturers = _.map(lecturers, function (lecturer) {
                return makeSelectable(lecturer);
            });
        });
        lectureHallService.list().then(function (lectureHalls) {
            vm.halls = _.map(lectureHalls, function (lectureHall) {
                return makeSelectable(lectureHall);
            });
        });
    }

    function makeSelectable(obj) {
        return _.extend({
            selected: false
        }, obj);
    }

    function loadLectures() {
        var halls = _.map(vm.selectedHalls, function (hall) {
            return hall.id;
        });
        var lecturers = _.map(vm.selectedLecturers, function (lecturer) {
            return lecturer.id;
        });
        lectureService.list({
            search: vm.title,
            hall: halls,
            lecturer: lecturers
        }).then(function (lectures) {
            vm.lectures = lectures;
        });
    }


    function refresh() {
        vm.lectureTerms = [];
        vm.selectedHalls = _.filter(vm.halls, function (hall) {
            return hall.selected;
        });
        vm.selectedLecturers = _.filter(vm.lecturers, function (lecturer) {
            return lecturer.selected;
        });
        loadLectures();
    }

    function toggleFilter() {
        vm.filterOpen = !vm.filterOpen;
    }
}

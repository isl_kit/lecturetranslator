angular
    .module("app.sessions")
    .controller("SearchLectureHallController", SearchLectureHallController);

/*@ngInject*/
function SearchLectureHallController(lectureHalls) {
    var vm = this;

    vm.lectureHalls = lectureHalls;

    activate();

    //////////

    function activate() {

    }
}

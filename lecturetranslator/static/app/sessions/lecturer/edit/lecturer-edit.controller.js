angular
    .module("app.sessions")
    .controller("LecturerEditController", LecturerEditController);

/*@ngInject*/
function LecturerEditController($state, lecturer, username, lecturerService) {
    var vm = this;

    vm.formData = {
        title: lecturer.title,
        name: lecturer.name,
        username: username
    };
    vm.submit = submit;
    vm.cancel = cancel;

    //////////

    function submit(valid) {
        if (!valid) {
            return;
        }
        lecturerService.update(lecturer.id, {
            title: vm.formData.title,
            name: vm.formData.name,
            user: vm.formData.username
        }).then(returnToView);
    }

    function cancel() {
        returnToView();
    }

    function returnToView() {
        $state.go("main.lecturer.view", {id: lecturer.id});
    }
}

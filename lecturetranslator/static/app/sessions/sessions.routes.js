angular
    .module("app.sessions")
    .config(sessionRoutes);

/*@ngInject*/
function sessionRoutes($stateProvider, authProvider) {

    $stateProvider.state("main.search", {
        url: "search",
        abstract: true,
        views: {
            "main-content": {
                controller: "SearchController",
                controllerAs: "vm",
                template: "<div ui-view=\"search-content\"></div>"
            }
        }
    });

    $stateProvider.state("main.files", {
        url: "files?node",
        params: {
            node: { dynamic: true }
        },
        views: {
            "main-content": {
                controller: "FilesController",
                controllerAs: "vm",
                templateUrl: "sessions/files/files.tpl.html"
            }
        },
        data: {
            redirectOnError: 'main.welcome'
        },
        resolve: {
            "authenticated": authProvider.setRules({
                permissions: ["can_upload"]
            })
        }
    });

    $stateProvider.state("main.file", {
        url: "files/:id",
        views: {
            "main-content": {
                controller: "NodeEditController",
                controllerAs: "vm",
                templateUrl: "sessions/files/node-edit/node-edit.tpl.html"
            }
        },
        data: {
            redirectOnError: 'main.welcome'
        },
        resolve: {
            "authenticated": authProvider.setRules({
                permissions: ["can_upload"]
            }),
            "node": /*@ngInject*/ function ($stateParams, fileService) {
                return fileService.getNode($stateParams.id);
            }
        }
    });

    $stateProvider.state("main.search.lecture", {
        url: "/lecture",
        views: {
            "search-content": {
                controller: "SearchLectureController",
                controllerAs: "vm",
                templateUrl: "sessions/search/by-lecture/search-lecture.tpl.html"
            }
        }
    });

    $stateProvider.state("main.search.lecturer", {
        url: "/lecturer",
        views: {
            "search-content": {
                controller: "SearchLecturerController",
                controllerAs: "vm",
                templateUrl: "sessions/search/by-lecturer/search-lecturer.tpl.html"
            }
        },
        resolve: {
            "lecturers": /*@ngInject*/ function (lecturerService) {
                return lecturerService.list();
            }
        }
    });

    $stateProvider.state("main.search.hall", {
        url: "/hall",
        views: {
            "search-content": {
                controller: "SearchLectureHallController",
                controllerAs: "vm",
                templateUrl: "sessions/search/by-lecture-hall/search-lecture-hall.tpl.html"
            }
        },
        resolve: {
            "lectureHalls": /*@ngInject*/ function (lectureHallService) {
                return lectureHallService.list();
            }
        }
    });

    $stateProvider.state("main.recent", {
        url: "recent",
        views: {
            "main-content": {
                controller: "RecentSessionsController",
                controllerAs: "vm",
                templateUrl: "sessions/recent-sessions/recent-sessions.tpl.html"
            }
        },
        resolve: {
            "upcomingEvents": /*@ngInject*/ function (eventService) {
                return eventService.list({
                    offset_post: 7 * 24 * 60,
                    offset_pre: 0
                });
            },
            subHeaderTitle: function() {
                return 'Sessions';
            }
        }
    });

    $stateProvider.state("main.session", {
        url: "session/:s",
        abstract: true,
        views: {
            "main-content": {
                //controller: "SessionController",
                //controllerAs: "vm",
                template: "<div ui-view=\"session-content\"></div>"
            }
        },
        resolve: {
            "session": /*@ngInject*/ function ($q, $stateParams, sessionService, sessionAuthService) {
                var deferred = $q.defer();
                sessionService.get($stateParams.s).then(function (session) {
                    if (!session.canAccess) {
                        deferred.reject();
                    } else if (session.auth === null || session.auth.authenticated) {
                        deferred.resolve(session);
                    } else {
                        sessionAuthService.dialog(session.auth).then(function () {
                            deferred.resolve(session);
                        }, function () {
                            deferred.reject();
                        });
                    }
                });
                return deferred.promise;
            }
        }
    });

    $stateProvider.state("main.session.stream", {
        url: "?lang",
        params: {
            lang: { dynamic: true }
        },
        views: {
            "session-content": {
                controller: "SessionRootController",
                controllerAs: "vm",
                templateUrl: "sessions/session-content/session-root.tpl.html"
            }
        },
        resolve: {
            subHeaderTitle: /*@ngInject*/ function (session) {
                return session.event && session.event.title ? session.event.title : session.title;
            }
        }
    });

    $stateProvider.state("main.session.details", {
        url: "/details",
        views: {
            "session-content": {
                controller: "SessionDetailsController",
                controllerAs: "vm",
                templateUrl: "sessions/session-details/session-details.tpl.html"
            }
        },
        resolve: {
            "sessionNodes": /*@ngInject*/ function (session, sessionService) {
                return sessionService.nodes(session.id);
            }
        }
    });

    $stateProvider.state("main.record", {
        url: "sessions/start?f&t&d&p&l&o&tr",
        views: {
            "main-content": {
                controller: "RecorderController",
                controllerAs: "vm",
                templateUrl: "sessions/recorder/recorder.tpl.html"
            }
        },
        data: {
            redirectOnError: 'main.welcome'
        },
        resolve: {
            "authenticated": authProvider.setRules({
                permissions: ["can_record"]
            }),
            "webRTCReady": function ($q, $rootScope) {
                var deferred = $q.defer();
                if ($rootScope.webRTCReady) {
                    deferred.resolve(true);
                } else {
                    deferred.reject();
                }
                return deferred.promise;
            }
        }
    });

    $stateProvider.state("main.scheduler", {
        url: "lectures/schedule",
        views: {
            "main-content": {
                controller: "SchedulerController",
                controllerAs: "vm",
                templateUrl: "sessions/scheduler/scheduler.tpl.html"
            }
        },
        data: {
            redirectOnError: 'main.welcome'
        },
        resolve: {
            "lecturers": /*@ngInject*/ function (lecturerService) {
                return lecturerService.list();
            },
            "authenticated": authProvider.setRules({
                isStaff: true
            })
        }
    });

    $stateProvider.state("main.lecture", {
        url: "lecture/:slug",
        abstract: true,
        views: {
            "main-content": {
                controller: "LectureController",
                controllerAs: "vm",
                templateUrl: "sessions/lecture/lecture.tpl.html"
            }
        },
        resolve: {
            "lecture": /*@ngInject*/ function ($stateParams, lectureService) {
                return lectureService.get($stateParams.slug);
            }
        }
    });

    $stateProvider.state("main.lecture.terms", {
        url: "",
        views: {
            "lecture-content": {
                controller: "LectureTermsController",
                controllerAs: "vm",
                templateUrl: "sessions/lecture/terms/lecture-terms.tpl.html"
            }
        }
    });

    $stateProvider.state("main.lecture.term", {
        url: "/:term",
        abstract: true,
        views: {
            "lecture-content": {
                template: "<div ui-view=\"lecture-term-content\"></div>"
            }
        },
        resolve: {
            "lectureTerm": /*@ngInject*/ function ($stateParams, lectureTermService) {
                return lectureTermService.get($stateParams.term, $stateParams.slug);
            }
        }
    });

    $stateProvider.state("main.lecture.term.view", {
        url: "",
        views: {
            "lecture-term-content": {
                controller: "LectureViewController",
                controllerAs: "vm",
                templateUrl: "sessions/lecture/view/lecture-view.tpl.html"
            }
        }
    });


    $stateProvider.state("main.lecture.term.edit", {
        url: "/edit",
        views: {
            "lecture-term-content": {
                controller: "LectureEditController",
                controllerAs: "vm",
                templateUrl: "sessions/lecture/edit/lecture-edit.tpl.html"
            }
        },
        data: {
            redirectOnError: 'main.welcome'
        },
        resolve: {
            "authenticated": authProvider.setRules({
                isStaff: true
            }),
            "lecturers": /*@ngInject*/ function (lecturerService) {
                return lecturerService.list();
            }
        }
    });


    $stateProvider.state("main.hall", {
        url: "hall/:id",
        abstract: true,
        views: {
            "main-content": {
                controller: "LectureHallController",
                controllerAs: "vm",
                templateUrl: "sessions/lecture-hall/lecture-hall.tpl.html"
            }
        },
        resolve: {
            "lectureHall": /*@ngInject*/ function ($stateParams, lectureHallService) {
                return lectureHallService.get($stateParams.id);
            }
        }
    });


    $stateProvider.state("main.hall.view", {
        url: "?date",
        params: {
            date: { dynamic: true }
        },
        views: {
            "lecture-hall-content": {
                controller: "LectureHallViewController",
                controllerAs: "vm",
                templateUrl: "sessions/lecture-hall/view/lecture-hall-view.tpl.html"
            }
        }
    });

    $stateProvider.state("main.hall.edit", {
        url: "/edit",
        views: {
            "lecture-hall-content": {
                controller: "LectureHallEditController",
                controllerAs: "vm",
                templateUrl: "sessions/lecture-hall/edit/lecture-hall-edit.tpl.html"
            }
        },
        data: {
            redirectOnError: 'main.welcome'
        },
        resolve: {
            "authenticated": authProvider.setRules({
                isStaff: true
            })
        }
    });

    $stateProvider.state("main.lecturer", {
        url: "lecturer/:id",
        abstract: true,
        views: {
            "main-content": {
                controller: "LecturerController",
                controllerAs: "vm",
                templateUrl: "sessions/lecturer/lecturer.tpl.html"
            }
        },
        resolve: {
            "lecturer": /*@ngInject*/ function ($stateParams, lecturerService) {
                return lecturerService.get($stateParams.id);
            }
        }
    });

    $stateProvider.state("main.lecturer.view", {
        url: "?date",
        params: {
            date: { dynamic: true }
        },
        views: {
            "lecturer-content": {
                controller: "LecturerViewController",
                controllerAs: "vm",
                templateUrl: "sessions/lecturer/view/lecturer-view.tpl.html"
            }
        }
    });

    $stateProvider.state("main.lecturer.edit", {
        url: "/edit",
        views: {
            "lecturer-content": {
                controller: "LecturerEditController",
                controllerAs: "vm",
                templateUrl: "sessions/lecturer/edit/lecturer-edit.tpl.html"
            }
        },
        data: {
            redirectOnError: 'main.welcome'
        },
        resolve: {
            "username": /*@ngInject*/ function (lecturerService, lecturer) {
                return lecturerService.getUser(lecturer.id);
            },
            "authenticated": authProvider.setRules({
                isStaff: true
            })
        }
    });

    $stateProvider.state("main.event", {
        url: "event/:id",
        abstract: true,
        views: {
            "main-content": {
                template: "<div ui-view=\"event-content\"></div>"
            }
        },
        resolve: {
            event: /*@ngInject*/ function ($stateParams, eventService) {
                return eventService.get($stateParams.id);
            }
        }
    });

    $stateProvider.state("main.event.content", {
        url: "?lang",
        params: {
            lang: { dynamic: true }
        },
        views: {
            "event-content": {
                controller: "EventRootController",
                controllerAs: "vm",
                templateUrl: "sessions/event/event-root.tpl.html"
            }
        },
        resolve: {
            session: /*@ngInject*/ function ($stateParams, $state, $q, event, sessionService, sessionAuthService) {
                var deferred = $q.defer();
                if (event.sessionId) {
                    sessionService.get(event.sessionId).then(function (session) {
                        if (!session.canAccess) {
                            alert("You need to log in to view this event. You will get redirected to the login page");
                            $state.go('main.login');
                            deferred.reject();
                        } else if (session.auth === null || session.auth.authenticated) {
                            deferred.resolve(session);
                        } else {
                            sessionAuthService.dialog(session.auth).then(function () {
                                deferred.resolve(session);
                            }, function () {
                                deferred.reject();
                            });
                        }
                    });
                } else {
                    if (event.lectureTerm.auth === null || event.lectureTerm.auth.authenticated) {
                        deferred.resolve(null);
                    } else {
                        sessionAuthService.dialog(event.lectureTerm.auth).then(function () {
                            deferred.resolve(null);
                        }, function () {
                            deferred.reject();
                        });
                    }
                }
                return deferred.promise;
            },
            subHeaderTitle: /*@ngInject*/ function (event) {
                return event.title || event.lectureTerm.lecture.title;
            }
        }
    });

    $stateProvider.state("main.event.sessions", {
        url: "/sessions",
        views: {
            "event-content": {
                controller: "EventSessionsController",
                controllerAs: "vm",
                templateUrl: "sessions/event/sessions/event-sessions.tpl.html"
            }
        },
        resolve: {
            authenticated: authProvider.setRules({
                isStaff: true
            }),
            mediaEvents: /*@ngInject*/ function ($stateParams, mediaEventService) {
                return mediaEventService.list({
                    event: $stateParams.id
                });
            },
            sessions: /*@ngInject*/ function ($stateParams, sessionService) {
                return sessionService.list({
                    event: $stateParams.id
                });
            }
        }
    });
}

angular
    .module("app.sessions")
    .directive("liveIndicator", liveIndicatorDirective);

/*@ngInject*/
function liveIndicatorDirective() {

    var directive = {
        restrict: "E",
        scope: {},
        link: link,
        templateUrl: "sessions/share/live-indicator/live-indicator.tpl.html"
    };
    return directive;

    //////////

    function link(scope, element, attributes) {
    }
}

angular
    .module("app.sessions")
    .directive("sessionAuth", sessionAuthDirective);

/*@ngInject*/
function sessionAuthDirective(sessionAuthService) {

    var directive = {
        restrict: "E",
        scope: {
            authEntity: "=",
            small: "=?",
            noAction: "=?"
        },
        link: link,
        replace: true,
        templateUrl: "sessions/share/session-auth/session-auth.tpl.html"
    };
    return directive;

    //////////

    function link(scope, element, attributes) {
        if (!scope.authEntity) {
            scope.authEntity = {
                authenticated: true
            };
        }
        scope.small = !!scope.small;
        scope.noAction = !!scope.noAction;
        scope.startAuthenticate = function ($event) {
            sessionAuthService.dialog(scope.authEntity);
            $event.stopPropagation();
            $event.preventDefault();
            return false;
        };
    }
}

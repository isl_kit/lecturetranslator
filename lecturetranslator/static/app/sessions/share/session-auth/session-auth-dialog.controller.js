angular
    .module("app.sessions")
    .controller("SessionAuthDialogController", SessionAuthDialogController);

/*@ngInject*/
function SessionAuthDialogController($scope, $mdDialog, authEntityService, authEntity) {
    var vm = this;

    vm.password = "";
    vm.login = login;
    vm.cancel = cancel;
    vm.busy = false;

    //////////

    function login() {
        if (vm.busy) {
            return;
        }
        vm.busy = true;
        authEntityService.authenticate(authEntity.id, {
            password: vm.password
        }).then(function (authenticated) {
            vm.busy = false;
            authEntity.authenticated = true;
            $mdDialog.hide(true);
        }, function () {
            vm.busy = false;
            vm.password = "";
            $scope.unlockForm.password.$setValidity("pwvalid", false);
        });

    }

    function cancel() {
        $mdDialog.cancel();
    }
}

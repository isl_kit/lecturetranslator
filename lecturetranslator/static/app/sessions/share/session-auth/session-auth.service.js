angular
    .module("app.sessions")
    .factory("sessionAuthService", sessionAuthService);

/*@ngInject*/
function sessionAuthService($http, $mdDialog, AuthEntity) {

    var service = {
        dialog: dialog
    };
    return service;

    //////////

    function dialog(authEntity) {
        return $mdDialog.show({
            controller: "SessionAuthDialogController",
            controllerAs: "vm",
            templateUrl: "sessions/share/session-auth/session-auth-dialog.tpl.html",
            parent: angular.element(document.body),
            targetEvent: null,
            clickOutsideToClose: true,
            fullscreen: false,
            locals: {
                authEntity: authEntity
            }
        });
    }
}

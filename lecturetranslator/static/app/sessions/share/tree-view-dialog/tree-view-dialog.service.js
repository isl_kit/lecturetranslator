angular
    .module("app.sessions")
    .factory("treeViewDialogService", treeViewDialogService);

/* @ngInject */
function treeViewDialogService($mdDialog) {

    var service = {
        show: show
    };

    return service;

    //////////

    function show(session) {
        return $mdDialog.show({
            controller: "TreeViewDialogController",
            controllerAs: "vm",
            templateUrl: "sessions/share/tree-view-dialog/tree-view-dialog.tpl.html",
            locals: {
                session: session
            },
            parent: angular.element(document.body),
            targetEvent: null,
            clickOutsideToClose: true,
            fullscreen: true
        });
    }
}

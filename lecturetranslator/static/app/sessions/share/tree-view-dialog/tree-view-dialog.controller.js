angular
    .module("app.sessions")
    .controller("TreeViewDialogController", TreeViewDialogController);

/*@ngInject*/
function TreeViewDialogController($mdDialog, session) {
    var vm = this;

    vm.session = session;
    vm.close = close;

    //////////

    function close() {
        $mdDialog.cancel();
    }
}

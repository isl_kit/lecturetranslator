angular
    .module("app.sessions")
    .filter("formatDate", formatDate);

/*@ngInject*/
function formatDate() {

    return function(date) {
        var month = addZero(date.getMonth() + 1),
        day = addZero(date.getDate()),
        hour = addZero(date.getHours()),
        minutes = addZero(date.getMinutes());

        var now = new Date();

        if (now.getFullYear() === date.getFullYear() &&
            now.getMonth() === date.getMonth() &&
            now.getDate() === date.getDate()) {
            return hour + ":" + minutes;
        }
        return day + "." + month + ". " + hour + ":" + minutes;
    };

    //////////

    function addZero(num) {
        if (num < 10) {
            return "0" + num;
        } else {
            return num.toString();
        }
    }
}

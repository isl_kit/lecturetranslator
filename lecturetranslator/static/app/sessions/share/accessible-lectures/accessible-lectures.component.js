angular
    .module("app.sessions")
    .component("accessibleLectures", {
        bindings: {
        },
        controller: "AccessibleLecturesController",
        controllerAs: "vm",
        templateUrl: "sessions/share/accessible-lectures/accessible-lectures.tpl.html"
});

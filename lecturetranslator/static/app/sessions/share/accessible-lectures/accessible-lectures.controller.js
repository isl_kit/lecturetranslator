angular
    .module("app.sessions")
    .controller("AccessibleLecturesController", AccessibleLecturesController);

/*@ngInject*/
function AccessibleLecturesController($scope, $state, lectureTermService) {
    var vm = this;

    vm.lectureTerms = [];

    vm.goToLecture = goToLecture;

    vm.$onInit = activate;

    //////////

    function activate() {
        lectureTermService.list({accessible: true}).then(function (lectureTerms) {
           vm.lectureTerms = _.map(_.groupBy(lectureTerms, function (lectureTerm) {
                return lectureTerm.lecture.slug;
            }), function (lectureTermSet) {
                return {
                    slug: lectureTermSet[0].lecture.slug,
                    terms: lectureTermSet.map(function (lectureTerm) {
                        return lectureTerm.term;
                    }),
                    title: lectureTermSet[0].lecture.title
                };
            });
        });
    }

    function goToLecture(lectureTerm) {
        if (lectureTerm.terms.length === 1) {
            $state.go('main.lecture.term.view', { slug: lectureTerm.slug, term: lectureTerm.terms[0] });
        } else {
            $state.go('main.lecture.terms', { slug: lectureTerm.slug });
        }
    }
}

angular
    .module("app.sessions")
    .factory("lecturerCreateDialogService", lecturerCreateDialogService);

/* @ngInject */
function lecturerCreateDialogService($mdDialog) {

    var service = {
        show: show
    };

    return service;

    //////////

    function show() {
        return $mdDialog.show({
            controller: "LecturerCreateDialogController",
            controllerAs: "vm",
            templateUrl: "sessions/share/lecturer-create-dialog/lecturer-create-dialog.tpl.html",
            parent: angular.element(document.body),
            targetEvent: null,
            clickOutsideToClose: true,
            fullscreen: false
        });
    }
}

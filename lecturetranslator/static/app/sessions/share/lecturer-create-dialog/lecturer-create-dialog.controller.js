angular
    .module("app.sessions")
    .controller("LecturerCreateDialogController", LecturerCreateDialogController);

/*@ngInject*/
function LecturerCreateDialogController($mdDialog, $q, lecturerService, userService) {
    var vm = this;
    vm.title = "";
    vm.name = "";
    vm.user = "";
    vm.close = close;
    vm.create = create;

    //////////

    function create(valid) {
        if (!valid) {
            return;
        }
        getOrCreateUser(vm.user).then(function (username) {
            lecturerService.create({
                title: vm.title,
                name: vm.name,
                user: username
            }).then(function (lecturer) {
                $mdDialog.hide(lecturer);
            });
        });
    }

    function getOrCreateUser(username) {
        var deferred = $q.defer();
        if (!username) {
            deferred.resolve(username);
        } else {
            userService.getOrCreate({
                name: username
            }).then(function () {
                deferred.resolve(username);
            }, function () {
                deferred.reject();
            });
        }
        return deferred.promise;
    }

    function close() {
        $mdDialog.cancel();
    }
}

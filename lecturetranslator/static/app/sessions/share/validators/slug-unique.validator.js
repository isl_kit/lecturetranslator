angular
    .module("app.sessions")
    .directive("slugUnique", slugUniqueDirective);

/*@ngInject*/
function slugUniqueDirective($q, lectureService) {

    var directive = {
        restrict: "A",
        require: "ngModel",
        link: link
    };
    return directive;

    //////////

    function link($scope, $attr, $elem, ctrl) {
        ctrl.$asyncValidators.slugUnique = function (value) {
            var deferred = $q.defer();
            lectureService.get(value).then(function () {
                deferred.reject();
            }, function () {
                deferred.resolve();
            });
            return deferred.promise;
        };
    }
}

angular
    .module("app.sessions")
    .directive("termUnique", termUniqueDirective);

/*@ngInject*/
function termUniqueDirective($q, lectureTermService) {

    var directive = {
        restrict: "A",
        require: "ngModel",
		scope: {
			termUnique: '@',
		},
        link: link
    };
    return directive;

    //////////

    function link($scope, $attr, $elem, ctrl) {
        ctrl.$asyncValidators.termUnique = function (value) {
            var deferred = $q.defer();
            lectureTermService.get(value, $scope.termUnique).then(function () {
                deferred.reject();
            }, function () {
                deferred.resolve();
            });
            return deferred.promise;
        };
    }
}

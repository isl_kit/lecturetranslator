angular
    .module("app.data")
    .factory("sessionHandleService", sessionHandleService);

/*@ngInject*/
function sessionHandleService() {

    var service = {
        getSelectedStreams: getSelectedStreams
    };

    return service;

    //////////

    function getSelectedStreams(session, langParam) {
        var selectedStreamIdents = [];
        if (!langParam) {
            selectedStreamIdents = session.defaultStreamIdents.slice(0);
        } else {
            selectedStreamIdents = _.filter(_.map(langParam.split(","), getIdentByFingerprint.bind(null, session)), function (ident) {
                return !!ident;
            });
        }
        selectedStreamIdents.sort(identSortFunction.bind(null, session));
        return selectedStreamIdents;
    }

    function getIdentByFingerprint(session, fingerprint) {
        var stream = _.find(session.streams, function (stream) {
            return stream.type === 'text' && stream.fingerprint === fingerprint;
        });
        return stream ? stream.ident : null;
    }

    function identSortFunction(session, a, b) {
        if (a === session.transcriptStream.ident) {
            return -1;
        }
        if (b === session.transcriptStream.ident) {
            return 1;
        }
        if (a < b) {
            return -1;
        }
        if (b < a) {
            return 1;
        }
        return 0;
    }
}

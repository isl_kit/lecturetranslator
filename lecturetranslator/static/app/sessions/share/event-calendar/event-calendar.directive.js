angular
    .module("app.sessions")
    .directive("eventCalendar", eventCalendarDirective);

/*@ngInject*/
function eventCalendarDirective() {

    var directive = {
        restrict: "E",
        scope: {},
        controller: "EventCalendarController",
        controllerAs: "vm",
        bindToController: {
            start: "=?",
            lectureHall: "=?",
            lecturer: "=?",
            lecture: "=?",
            onWeekChange: "&"
        },
        templateUrl: "sessions/share/event-calendar/event-calendar.tpl.html",
        replace: true
    };
    return directive;
}

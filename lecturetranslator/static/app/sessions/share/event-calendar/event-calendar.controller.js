angular
    .module("app.sessions")
    .controller("EventCalendarController", EventCalendarController);

/*@ngInject*/
function EventCalendarController($scope, $http, $filter, $state, helperService, eventService) {
    var vm = this,
        timeStart, timeStop;

    vm.days = [];
    vm.hours = [];
    vm.events = [
        [],
        [],
        [],
        [],
        [],
        [],
        []
    ];
    vm.busy = true;
    vm.givenLectureHall = false;
    vm.givenLecturer = false;
    vm.givenLecture = false;

    vm.forward = forward;
    vm.backward = backward;

    vm.$onInit = activate;

    //////////

    function activate() {
        vm.givenLectureHall = !!vm.lectureHall;
        vm.givenLecturer = !!vm.lecturer;
        vm.givenLecture = !!vm.lecture;
        $scope.$watch("vm.start", update);
    }

    function setDays() {
        var start = vm.start || new Date();
        var currentDay = start.getDay();
        var offset = currentDay === 0 ? -6 : currentDay > 1 ? 1 - currentDay : 0;
        start.setHours(0,0,0,0);
        start = createOffsetDate(start, offset);
        for (i = 0; i < 7; i++) {
            vm.days[i] = start;
            start = createOffsetDate(start, 1);
        }
    }

    function createOffsetDate(date, offset) {
        var newDate = new Date(date);
        newDate.setDate(newDate.getDate() + offset);
        return newDate;
    }

    function forward() {
        if (vm.busy) {
            return;
        }
        vm.onWeekChange({date: createOffsetDate(vm.days[0], 7)});
    }

    function backward() {
        if (vm.busy) {
            return;
        }
        vm.onWeekChange({date: createOffsetDate(vm.days[0], -7)});
    }

    function update() {
        setDays();
        var params = {
            start__gte: getDateString(vm.days[0]),
            start__lte:  getDateString(createOffsetDate(vm.days[0], 7))
        };
        if (vm.givenLecturer) {
            params.lecturer = vm.lecturer.id;
        }
        if (vm.givenLecture) {
            params.slug = vm.lecture.slug;
        }
        if (vm.givenLectureHall) {
            params.hall = vm.lectureHall.id;
        }
        vm.busy = true;
        eventService.list(params).then(function (events) {
            var i;
            setTimeStart(events);
            setTimeStop(events);
            updateHours();
            events = _.map(events, getEventStyle);
            events = _.groupBy(events, function (event) {
                var day = event.start.getDay();
                return day === 0 ? 6 : day - 1;
            });
            for (i = 0; i < 7; i++) {
                vm.events[i] = events[i] || [];
            }
            vm.busy = false;
        });
    }

    function updateHours() {
        vm.hours = _.map(new Array(timeStop - timeStart), function (item, index) {
            return getTime(index + timeStart);
        });
    }

    function setTimeStart(events) {
        var i, max_i, event, hour;
        timeStart = 7;
        for (i = 0, max_i = events.length; i < max_i; i++) {
            event = events[i];
            hour = Math.max(event.start.getHours() - 1 ,0);
            if (hour < timeStart) {
                timeStart = hour;
            }
        }
    }

    function setTimeStop(events) {
        var i, max_i, event, hour, duration;
        timeStop = 21;
        for (i = 0, max_i = events.length; i < max_i; i++) {
            event = events[i];
            duration = Math.round((event.stop - event.start) / 1000 / 60 / 60);
            hour = Math.min(event.start.getHours() + duration + 1, 24);
            if (hour > timeStop) {
                timeStop = hour;
            }
        }
    }

    function getEventStyle(event) {
        var duration = Math.round((event.stop - event.start) / 1000 / 60),
            height = duration - 1,
            top = event.start.getMinutes() + 60 * event.start.getHours() - timeStart * 60 - 1;

        event.style = {
            top: top + "px",
            height: height + "px"
        };
        return event;
    }

    function getDateString(date) {
        var d = date.getFullYear() + "-" + zeroPad(date.getMonth() + 1) + "-" + zeroPad(date.getDate());
        return d + 'T00:00';
    }

    function zeroPad(num) {
        if (num < 10) {
            return "0" + num;
        }
        return num;
    }

    function getTime(num) {
        if (num < 10) {
            num = "0" + num;
        }
        return num + ":00";
    }
}

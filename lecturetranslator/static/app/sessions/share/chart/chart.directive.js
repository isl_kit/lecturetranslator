angular
    .module("app.sessions")
    .directive("chart", chartDirective);

/*@ngInject*/
function chartDirective() {

    var directive = {
        restrict: "E",
        scope: {
            tree: "="
        },
        link: linkFunction,
        templateUrl: "sessions/share/chart/chart.tpl.html",
        replace: true
    };
    return directive;

    //////////

    function linkFunction(scope, element, attributes) {
        var tree = scope.tree,
            length = tree.totalLength,
            perc = 100 / length;

        scope.length = length;
        scope.cols = new Array(length);
        scope.parts = [];
        flatten(tree.children, 0, scope.parts, 0);
    }

    function flatten(parts, level, res, pos) {
        if (!res[level]) {
            res[level] = [];
            res[level].pos = 0;
        }
        var skip = 0;
        if (pos > res[level].pos) {
            skip = pos - res[level].pos;
        }
        res[level].pos = pos;
        var groupedParts = _.groupBy(parts, function (part) {
            return part.itemType;
        }), done = false;
        _.each(groupedParts.stream, function (part) {
            if (!done) {
                done = true;
                if (skip > 0) {
                    res[level].push({
                        itemType: "skip",
                        totalLength: skip
                    });
                }
            }
            res[level].push(part);
            if (part.length > 0) {
                flatten(part.children, level + 1, res, res[level].pos);
            }
            res[level].pos += part.totalLength;
        });
        _.each(groupedParts.node, function (part) {
            res[level].push(part);
            if (!done) {
                done = true;
                if (skip > 0) {
                    res[level].push({
                        itemType: "skip",
                        totalLength: skip
                    });
                }
            }
            if (part.length > 0) {
                flatten(part.children, level + 1, res, res[level].pos);
            }
            res[level].pos += part.totalLength;
        });
    }
}

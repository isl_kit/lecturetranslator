angular
    .module("app.sessions")
    .component("sessionList", {
        bindings: {
        },
        controller: "SessionListController",
        controllerAs: "vm",
        templateUrl: "sessions/share/session-list/session-list.tpl.html"
});

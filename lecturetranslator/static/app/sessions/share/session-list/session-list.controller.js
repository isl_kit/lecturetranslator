angular
    .module("app.sessions")
    .controller("SessionListController", SessionListController);

/*@ngInject*/
function SessionListController($scope, $state, sessionService) {
    var vm = this;

    vm.sessions = [];
    vm.goToSession = goToSession;

    vm.$onInit = activate;

    //////////

    function activate() {
        _.each(sessionService.sessions, sessionAddHandler.bind(null, null));
        $scope.$on("sessions.add", sessionAddHandler);
        $scope.$on("sessions.update", sessionAddHandler);
        $scope.$on("sessions.remove", sessionRemoveHandler);
    }

    function sessionsSortFunction(a, b) {
        if (a.title > b.title) {
            return 1;
        }
        if (b.title > a.title) {
            return -1;
        }
        return 0;
    }

    function sortSessions() {
        vm.sessions.sort(sessionsSortFunction);
    }

    function getSessionById(id) {
        return _.find(vm.sessions, function (session) {
            return session.id === id;
        });
    }

    function removeSession(sessionId) {
        var presentSession = getSessionById(sessionId);
        if (!presentSession) {
            return;
        }
        vm.sessions = _.without(vm.sessions, presentSession);
    }

    function sessionAddHandler(event, session) {
        removeSession(session.id);
        vm.sessions.push(session);
        sortSessions();
    }

    function sessionRemoveHandler(event, session) {
        removeSession(session.id);
    }

    function goToSession(session) {
        if (session.event) {
            $state.go('main.event.content', {id: session.event.id});
        } else {
            $state.go('main.session.stream', {s: session.id});
        }
    }
}

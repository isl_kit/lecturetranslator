angular
    .module("app.sessions")
    .component("tagSelect", {
        require: {
            model: "ngModel"
        },
        controller: "TagSelectController",
        controllerAs: "vm",
        templateUrl: "sessions/share/tag-select/tag-select.tpl.html"
});

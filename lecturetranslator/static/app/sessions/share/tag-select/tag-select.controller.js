angular
    .module("app.sessions")
    .controller("TagSelectController", TagSelectController);

/*@ngInject*/
function TagSelectController(fileService, DocumentTag) {
    var vm = this,
        currentTags = [];

    vm.searchText = '';
    vm.selectedItem = null;

    vm.transformChip = transformChip;
    vm.tagSearch = tagSearch;
    vm.changeHandler = changeHandler;
    vm.itemSelectHandler = itemSelectHandler;
    vm.$onInit = activate;

    //////////

    function activate() {
        vm.model.$render = function () {
            vm.tags = vm.model.$viewValue;
        };
    }

    function transformChip(chip) {
        console.log(chip);
        if (chip instanceof DocumentTag) {
            return chip;
        }
        var tag = _.find(currentTags, function (tag) {
            return tag.name === chip;
        });
        if (!/^[0-9a-zA-Z\-_]+$/.test(chip) || chip.length > 64) {
            return null;
        }
        return tag || {
            name: chip
        };
    }

    function tagSearch(searchText) {
        return fileService.getTags(searchText).then(function (tags) {
            currentTags = tags;
            return tags;
        });
    }

    function changeHandler() {
        vm.model.$setViewValue(vm.tags);
    }

    function itemSelectHandler() {
        console.log("select");
    }
}

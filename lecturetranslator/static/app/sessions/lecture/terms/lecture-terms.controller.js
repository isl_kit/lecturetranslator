angular
    .module("app.sessions")
    .controller("LectureTermsController", LectureTermsController);

/*@ngInject*/
function LectureTermsController($state, lecture) {
    var vm = this;

    vm.lecture = lecture;

    activate();

    //////////

    function activate() {
        console.log(lecture);
        if (lecture.terms.length === 1) {
            // location: 'replace' to be able to get back to the lecture list instead of looping
            $state.go('main.lecture.term.view', {slug: lecture.slug, term: lecture.terms[0]}, { location: 'replace' });
        }
    }

}

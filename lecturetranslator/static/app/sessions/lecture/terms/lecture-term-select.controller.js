angular
    .module("app.sessions")
    .controller("LectureTermSelectController", LectureTermSelectController);

/*@ngInject*/
function LectureTermSelectController($state) {
    var vm = this;

    vm.goToTerm = goToTerm;

    //////////

    function goToTerm(term) {
        console.log(vm.baseState, term);
        $state.go(vm.baseState, { slug: vm.lecture.slug, term: term });
    }
}

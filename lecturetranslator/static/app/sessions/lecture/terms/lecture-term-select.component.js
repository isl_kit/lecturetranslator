angular
    .module("app.sessions")
    .component("lectureTermSelect", {
        bindings: {
            lecture: "<",
            term: "<",
            baseState: "@"
        },
        controller: "LectureTermSelectController",
        controllerAs: "vm",
        templateUrl: "sessions/lecture/terms/lecture-term-select.tpl.html"
});

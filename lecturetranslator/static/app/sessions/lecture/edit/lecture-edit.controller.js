angular
    .module("app.sessions")
    .controller("LectureEditController", LectureEditController);

/*@ngInject*/
function LectureEditController($filter, $timeout, $state, lecture, lectureTerm, helperService, lecturers, eventService, divaService, lectureHallService, lectureTermService, lecturerCreateDialogService, lectureService) {
    var vm = this;

    vm.lectureFormData = {
        slug: lecture.slug,
        title: lecture.title
    };
    vm.lectureSaved = false;
    vm.submitLecture = submitLecture;

    vm.lecture = lecture;
    vm.lectureTerm = lectureTerm;
    vm.events = [];
    vm.divaCollection = null;
    vm.divaFormData = {
        key: ''
    };
    vm.divaSaved = false;
    vm.submitDiva = submitDiva;
    vm.termLimits = helperService.getTermLimits(lectureTerm.term);
    vm.terms = lecture.terms;
    vm.term = lectureTerm.term;
    vm.termFormData = {
        lecturers: {
            all: lecturers,
            selected: _.map(lectureTerm.lecturers, function (lecturer) {
                return lecturer.id;
            })
        },
        description: lectureTerm.description,
        archive: lectureTerm.archive,
        mediaAccess: lectureTerm.mediaAccess
    };
    vm.newEventFormData = {};
    vm.showNewEventForm = false;

    vm.lectureTermSaved = false;

    vm.openLecturerCreateDialog = openLecturerCreateDialog;
    vm.submitLectureTerm = submitLectureTerm;
    vm.submitEvent = submitEvent;
    vm.deleteEvent = deleteEvent;
    vm.createEvent = createEvent;
    vm.resetNewEventForm = resetNewEventForm;

    activate();

    //////////

    function activate() {
        resetNewEventForm();
        getEvents();
        getDivaCollection();
    }

    function submitLecture(form) {
        if (!form.$valid) {
            return;
        }
        lectureService.update(lecture.slug, {
            slug: vm.lectureFormData.slug,
            title: vm.lectureFormData.title
        }).then(function (newLecture) {
            lecture.slug = newLecture.slug;
            lecture.title = newLecture.title;
            vm.lectureSaved = true;
            form.$setPristine();
            $timeout(function () {
                vm.lectureSaved = false;
            }, 3000);
        });
    }

    function getEvents() {
        eventService.list({
            term: lectureTerm.term,
            slug: lecture.slug
        }).then(function (events) {
            vm.events = _.map(events, mapEvent);
        });
    }

    function getDivaCollection() {
        divaService.list({
            lecture_term: lectureTerm.id
        }).then(function (divaCollections) {
            vm.divaCollection = divaCollections[0] || null;
            vm.divaFormData.key = vm.divaCollection ? vm.divaCollection.key : '';
        });
    }

    function submitDiva(form) {
        if (!form.$valid) {
            return;
        }
        if (vm.divaCollection && vm.divaFormData.key) {
            divaService.update(vm.divaCollection.id, {
                key: vm.divaFormData.key
            }).then(function (divaCollection) {
                vm.divaCollection = divaCollection;
                vm.divaSaved = true;
                form.$setPristine();
                $timeout(function () {
                    vm.divaSaved = false;
                }, 3000);
            });
        } else if (vm.divaCollection && !vm.divaFormData.key) {
            divaService.remove(vm.divaCollection.id).then(function () {
                vm.divaCollection = null;
                vm.divaSaved = true;
                form.$setPristine();
                $timeout(function () {
                    vm.divaSaved = false;
                }, 3000);
            });
        } else if (vm.divaFormData.key) {
            divaService.create({
                key: vm.divaFormData.key,
                lecture_term: lectureTerm.id
            }).then(function (divaCollection) {
                vm.divaCollection = divaCollection;
                vm.divaSaved = true;
                form.$setPristine();
                $timeout(function () {
                    vm.divaSaved = false;
                }, 3000);
            });
        }
    }

    function mapEvent(event) {
        return {
            id: event.id,
            saved: false,
            fingerprint: event.fingerprint,
            start: event.start,
            startDate: event.start,
            startTime: helperService.getTimeFromDate(event.start),
            stopDate: event.stop,
            stopTime: helperService.getTimeFromDate(event.stop),
            hall: {
                selected: event.lectureHall ? event.lectureHall.name : null,
                searchText: event.lectureHall ? event.lectureHall.name : "",
                query: hallSearch
            }
        };
    }

    function resetNewEventForm() {
        vm.showNewEventForm = false;
        vm.newEvent = {
            startDate: vm.termLimits.minDate,
            startTime: "08:00",
            stopDate: vm.termLimits.minDate,
            stopTime: "09:45",
            fingerprint: "de-DE-lecture_KIT",
            hall: {
                selected: null,
                searchText: "",
                query: hallSearch
            }
        };
    }

    function hallSearch(search) {
        return lectureHallService.list({ search: search });
    }

    function submitLectureTerm(form) {
        if (!form.$valid) {
            return;
        }
        lectureTermService.update(vm.term, lecture.slug, {
            description: vm.termFormData.description,
            archive: vm.termFormData.archive,
            media_access: vm.termFormData.mediaAccess,
            lecturers: vm.termFormData.lecturers.selected
        }).then(function (updatedLectureTerm) {
            lectureTerm.archive = updatedLectureTerm.archive;
            lectureTerm.description = updatedLectureTerm.description;
            lectureTerm.lecturers = updatedLectureTerm.lecturers;
            vm.lectureTermSaved = true;
            form.$setPristine();
            $timeout(function () {
                vm.lectureTermSaved = false;
            }, 3000);
        });
    }

    function createEvent(valid) {
        if (!valid) {
            return;
        }
        var start = $filter("date")(vm.newEvent.startDate, "yyyy-MM-dd") + " " + vm.newEvent.startTime,
            stop = $filter("date")(vm.newEvent.stopDate, "yyyy-MM-dd") + " " + vm.newEvent.stopTime,
            lectureHallName = vm.newEvent.hall.searchText;
        eventService.create({
            lecture_term: lectureTerm.id,
            lecture_hall: lectureHallName,
            fingerprint: vm.newEvent.fingerprint,
            start: start,
            stop: stop
        }).then(function (newEvent) {
            var newEventObj = mapEvent(newEvent);
            vm.events.unshift(newEventObj);
            resetNewEventForm();
            $timeout(function () {
                newEventObj.saved = true;
            });
            $timeout(function () {
                newEventObj.saved = false;
            }, 3000);
        });
    }

    function submitEvent(event) {
        var start = $filter("date")(event.startDate, "yyyy-MM-dd") + " " + event.startTime,
            stop = $filter("date")(event.stopDate, "yyyy-MM-dd") + " " + event.stopTime,
            lectureHallName = event.hall.searchText;

        eventService.update(event.id, {
            lecture_hall: lectureHallName,
            start: start,
            stop: stop,
            fingerprint: event.fingerprint
        }).then(function (newEvent) {
            var newEventObj = mapEvent(newEvent),
                index = _.indexOf(vm.events, event);
            vm.events.splice(index, 1, newEventObj);
            $timeout(function () {
                newEventObj.saved = true;
            });
            $timeout(function () {
                newEventObj.saved = false;
            }, 3000);
        });
    }

    function deleteEvent(event) {
        eventService.remove(event.id).then(function () {
            vm.events = _.without(vm.events, event);
        });
    }

    function openLecturerCreateDialog() {
        lecturerCreateDialogService.show().then(function (lecturer) {
            vm.termFormData.lecturers.all.push(lecturer);
            vm.termFormData.lecturers.selected.push(lecturer.id);
            vm.termFormData.lecturers.all.sort(lecturerSorting);
        });
    }
}

angular
    .module("app.sessions")
    .controller("LectureEditTermController", LectureEditTermController);

/*@ngInject*/
function LectureEditTermController($filter, $timeout, $state, lecture, lectureTerm, helperService, lecturers, eventService, lectureHallService, lectureTermService, lecturerCreateDialogService) {
    var vm = this;

    vm.lectureTerm = lectureTerm;
    vm.events = [];
    vm.termLimits = helperService.getTermLimits(lectureTerm.term);
    vm.terms = lecture.terms;
    vm.term = lectureTerm.term;
    vm.formData = {
        lecturers: {
            all: lecturers,
            selected: _.map(lectureTerm.lecturers, function (lecturer) {
                return lecturer.id;
            })
        },
        description: lectureTerm.description,
        archive: lectureTerm.archive,
        mediaAccess: lectureTerm.mediaAccess
    };
    vm.newEventFormData = {};
    vm.showNewEventForm = false;

    vm.lectureTermSaved = false;

    vm.openLecturerCreateDialog = openLecturerCreateDialog;
    vm.submitLectureTerm = submitLectureTerm;
    vm.submitEvent = submitEvent;
    vm.deleteEvent = deleteEvent;
    vm.createEvent = createEvent;
    vm.resetNewEventForm = resetNewEventForm;

    activate();

    //////////

    function activate() {
        resetNewEventForm();
        getEvents();
    }

    function getEvents() {
        eventService.list({
            term: lectureTerm.term,
            slug: lecture.slug
        }).then(function (events) {
            vm.events = _.map(events, mapEvent);
        });
    }

    function mapEvent(event) {
        return {
            id: event.id,
            saved: false,
            start: event.start,
            startDate: event.start,
            startTime: helperService.getTimeFromDate(event.start),
            stopDate: event.stop,
            stopTime: helperService.getTimeFromDate(event.stop),
            hall: {
                selected: event.lectureHall ? event.lectureHall.name : null,
                searchText: event.lectureHall ? event.lectureHall.name : "",
                query: hallSearch
            }
        };
    }

    function resetNewEventForm() {
        vm.showNewEventForm = false;
        vm.newEvent = {
            startDate: vm.termLimits.minDate,
            startTime: "08:00",
            stopDate: vm.termLimits.minDate,
            stopTime: "09:45",
            hall: {
                selected: null,
                searchText: "",
                query: hallSearch
            }
        };
    }

    function hallSearch(search) {
        return lectureHallService.list({ search: search });
    }

    function submitLectureTerm(form) {
        if (!form.$valid) {
            return;
        }
        lectureTermService.update(vm.term, lecture.slug, {
            description: vm.formData.description,
            archive: vm.formData.archive,
            lecturers: vm.formData.lecturers.selected
        }).then(function (updatedLectureTerm) {
            lectureTerm.archive = updatedLectureTerm.archive;
            lectureTerm.description = updatedLectureTerm.description;
            lectureTerm.lecturers = updatedLectureTerm.lecturers;
            vm.lectureTermSaved = true;
            form.$setPristine();
            $timeout(function () {
                vm.lectureTermSaved = false;
            }, 3000);
        });
    }

    function createEvent(valid) {
        if (!valid) {
            return;
        }
        var start = $filter("date")(vm.newEvent.startDate, "yyyy-MM-dd") + " " + vm.newEvent.startTime,
            stop = $filter("date")(vm.newEvent.stopDate, "yyyy-MM-dd") + " " + vm.newEvent.stopTime,
            lectureHallName = vm.newEvent.hall.searchText;
        eventService.create({
            lecture_term: lectureTerm.id,
            lecture_hall: lectureHallName,
            start: start,
            stop: stop
        }).then(function (newEvent) {
            var newEventObj = mapEvent(newEvent);
            vm.events.unshift(newEventObj);
            resetNewEventForm();
            $timeout(function () {
                newEventObj.saved = true;
            });
            $timeout(function () {
                newEventObj.saved = false;
            }, 3000);
        });
    }

    function submitEvent(event) {
        var start = $filter("date")(event.startDate, "yyyy-MM-dd") + " " + event.startTime,
            stop = $filter("date")(event.stopDate, "yyyy-MM-dd") + " " + event.stopTime,
            lectureHallName = event.hall.searchText;

        eventService.update(event.id, {
            lecture_hall: lectureHallName,
            start: start,
            stop: stop
        }).then(function (newEvent) {
            var newEventObj = mapEvent(newEvent),
                index = _.indexOf(vm.events, event);
            vm.events.splice(index, 1, newEventObj);
            $timeout(function () {
                newEventObj.saved = true;
            });
            $timeout(function () {
                newEventObj.saved = false;
            }, 3000);
        });
    }

    function deleteEvent(event) {
        eventService.remove(event.id).then(function () {
            vm.events = _.without(vm.events, event);
        });
    }

    function openLecturerCreateDialog() {
        lecturerCreateDialogService.show().then(function (lecturer) {
            vm.formData.lecturers.all.push(lecturer);
            vm.formData.lecturers.selected.push(lecturer.id);
            vm.formData.lecturers.all.sort(lecturerSorting);
        });
    }
}

angular
    .module("app.sessions")
    .controller("LectureViewController", LectureViewController);

/*@ngInject*/
function LectureViewController($http, $scope, $filter, $state, lecture, lectureTerm, eventService, auth) {
    var vm = this;

    vm.lecture = lecture;
    vm.lectureTerm = lectureTerm;

    vm.show_list = show_list;
    vm.hide_list = hide_list;
    vm.modeUploadList = "not_upload";

    vm.event_upload = event_upload;

    vm.auth = auth;

    vm.upl_data = {
        url: 'session-media/event-integrate/',
        data: { title: ""},
        files: undefined,
        generator: function(){
            var vm = this;
            var fd = new FormData();
            console.log("generator", vm);
            fd.append("title", ("" + vm.data.title));
            fd.append("start", vm.data.startDate);
            fd.append("stop", vm.data.stopDate);
            fd.append("lecture_term", lectureTerm.id);
            fd.append("fingerprint_in", vm.data.fingerPrint);
            fd.append("video", vm.files[0], vm.files[0].name);
            fd.append("pw", vm.data.pw);
            return fd;
        }
    };

    vm.events = [];

    activate();

    //////////


    function activate() {
        fetchEvents();
        console.log("vm", vm);
    }

    function event_upload() {
        var config = {
            headers: {'Content-Type': undefined},
            transformRequest: angular.identity
        };
        return $http.post(vm.upl_data.url, vm.upl_data.generator(), config).then(
            function(result) { alert(result.status + "\n" + result.statusText);},
            function(result) { alert("Something went wrong:\n" + result.status + "\n\"" + result.statusText + "\"\nDetails:\n" + JSON.stringify(result.data)); console.log(result);}
        );
    }

    function fetchEvents() {
        eventService.list({
            term: lectureTerm.term,
            slug: lecture.slug
        }).then(function (events) {
            vm.events = groupEvents(events);
        });
    }

    function groupEvents(events) {
        var result = [],
            hash = {};
        _.each(events, function (event) {
            var month = getMonthString(event.start);
            if (!_.has(hash, month)) {
                hash[month] = {
                    label: month,
                    events: []
                };
                result.push(hash[month]);
            }
            hash[month].events.push(event);
        });
        return result;
    }

    function getMonthString(date) {
        return $filter('date')(date, 'MMMM yyyy');
    }

    function show_list(){
        vm.modeUploadList = 'upload';
    }

    function hide_list(){
        vm.modeUploadList = 'not_upload';
    }

}

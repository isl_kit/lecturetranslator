angular
    .module("app.sessions")
    .controller("SessionRootController", SessionRootController);

/*@ngInject*/
function SessionRootController($stateParams, session, sessionHandleService) {
    var vm = this;

    vm.session = session;
    vm.selectedStreams = [];
    vm.uiOnParamsChanged = uiOnParamsChanged;

    activate();

    //////////

    function activate() {
        vm.selectedStreams = sessionHandleService.getSelectedStreams(session, $stateParams.lang);
    }

    function uiOnParamsChanged(params) {
        vm.selectedStreams = sessionHandleService.getSelectedStreams(session, params.lang);
    }
}

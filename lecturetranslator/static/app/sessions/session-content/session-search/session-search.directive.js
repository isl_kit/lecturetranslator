angular
    .module("app.sessions")
    .directive("sessionSearch", sessionSearchDirective);

/*@ngInject*/
function sessionSearchDirective() {

    var directive = {
        restrict: "E",
        scope: {},
        controller: "SessionSearchController",
        controllerAs: "vm",
        bindToController: {
            text: "="
        },
        templateUrl: "sessions/session-content/session-search/session-search.tpl.html"
    };
    return directive;
}

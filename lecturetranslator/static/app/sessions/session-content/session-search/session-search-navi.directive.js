angular
    .module("app.sessions")
    .directive("sessionSearchNavi", sessionSearchNaviDirective);

/*@ngInject*/
function sessionSearchNaviDirective() {

    var directive = {
        restrict: "E",
        scope: {},
        controller: "SessionSearchNaviController",
        controllerAs: "vm",
        bindToController: {
            text: "=",
            datas: "=",
            filteredDatas: "=",
            startAttr: "@",
            pos: "=",
            textAttr: "@"
        },
        templateUrl: "sessions/session-content/session-search/session-search-navi.tpl.html"
    };
    return directive;
}

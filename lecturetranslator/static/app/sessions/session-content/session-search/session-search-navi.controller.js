angular
    .module("app.sessions")
    .controller("SessionSearchNaviController", SessionSearchNaviController);

/*@ngInject*/
function SessionSearchNaviController($scope) {
    var vm = this;

    vm.index = -1;
    vm.goToNext = goToNext;
    vm.goToPrev = goToPrev;
    vm.filteredDatas = [];

    activate();

    //////////

    function activate() {
        $scope.$watch("vm.text", textChangeHandler);
        $scope.$on("$destroy", deactivate);
    }

    function deactivate() {
        clearSearch();
    }

    function textChangeHandler(text, oldVal) {
        if (text.length < 3) {
            clearSearch();
            return;
        }
        search(text);
    }

    function goToNext() {
        if (vm.index >= vm.filteredDatas.length - 1) {
            vm.index = 0;
        } else {
            vm.index += 1;
        }
        updatePos();
    }

    function goToPrev() {
        if (vm.index <= 0) {
            vm.index = vm.filteredDatas.length - 1;
        } else {
            vm.index -= 1;
        }
        updatePos();
    }

    function search(text) {
        text = text.toLowerCase();
        vm.filteredDatas = _.filter(vm.datas, function (data) {
            var index = data[vm.textAttr].toLowerCase().indexOf(text);
            if (index > -1) {
                data.selected = true;
            } else {
                data.selected = false;
            }
            return data.selected;
        });
        if (vm.filteredDatas.length > 0) {
            vm.index = 0;
        } else {
            vm.index = -1;
        }
        updatePos();
    }

    function clearSearch() {
        _.each(vm.filteredDatas, function (data) {
            data.selected = false;
        });
        vm.filteredDatas = [];
        vm.index = -1;
    }

    function updatePos() {
        if (vm.index >= 0) {
            vm.pos = vm.filteredDatas[vm.index][vm.startAttr];
        }
    }
}

angular
    .module("app.sessions")
    .directive("scrollCopy", scrollCopyDirective);

/*@ngInject*/
function scrollCopyDirective($timeout, $window, helperService) {

    var directive = {
        restrict: "A",
        scope: {
        },
        link: link
    };
    return directive;

    //////////

    function link(scope, element, attributes) {
        var el = element[0],
            id = attributes.scrollCopy,
            refElement = $('#' + id),
            refEl = refElement[0],
            scrollTop, refScrollTop;

        activate();

        //////////

        function activate() {
            refElement.on("scroll", refScrollHandler);
            element.on("scroll", scrollHandler);
        }

        function refScrollHandler() {
            if (refEl.scrollTop === refScrollTop) {
                return;
            }
            refScrollTop = refEl.scrollTop;
            scrollTop = Math.round(refScrollTop / (refEl.scrollHeight - refEl.clientHeight) * (el.scrollHeight - el.clientHeight));
            element.scrollTop(scrollTop);
        }

        function scrollHandler() {
            if (el.scrollTop === scrollTop) {
                return;
            }
            scrollTop = el.scrollTop;
            refScrollTop = Math.round(scrollTop * (refEl.scrollHeight - refEl.clientHeight) / (el.scrollHeight - el.clientHeight));
            refElement.scrollTop(refScrollTop);
        }
    }
}

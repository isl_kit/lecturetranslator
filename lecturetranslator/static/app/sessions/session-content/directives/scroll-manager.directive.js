angular
    .module("app.sessions")
    .directive("scrollManager", scrollManagerDirective);

/*@ngInject*/
function scrollManagerDirective($timeout, $window, helperService) {

    var alignHeight = 0,
        SID = 0,
        minID = 0,
        ids = [],
        // Relative amount of space to clear
        clearSizeRelative = 0.5,
        // Animation time
        clearAnimationTime = 1000,
        // Animation style
        clearAnimationStyle = "swing",
        clearAnimationLock = {};

    var directive = {
        restrict: "A",
        scope: {
            live: "<",
            datas: "<",
            pos: "=",
            glued: "=",
            anchor: "<",
            idPrefix: "@",
            idAttr: "@",
            startAttr: "@",
            stopAttr: "@"
        },
        link: link
    };
    return directive;

    //////////

    function link(scope, element, attributes) {
        var el = element[0],
            blocked = false,
            cheight = el.clientHeight,
            height = el.scrollHeight,
            width = element.width(),
            currentPos = scope.pos,
            currentTop = 0,
            searchResults = [],
            lastValues = [],
            id = SID++,
            // Amount of space to clear upon a full text container
            // Initialize with null to set padding on first digestHandler call
            clearSize = null,
            enableClearAnimations = el.animate !== undefined;
        // Animation lock for scrollManager with id, prevents multiple calls to animate
        clearAnimationLock[id] = false;

        ids.push(id);
        minID = Math.min.apply(Math, ids);

        $timeout(activate);

        //////////

        function activate() {
            updateAlignHeight(0);
            scope.$watch("glued", gluedChangeHandler);
            scope.$watch("pos", posChangeHandler);
            scope.$watch("anchor", anchorChangeHandler);
            element.on("scroll", scrollHandler);
            scope.$watch("datas.length", datasLengthChangeHandler);
            $window.addEventListener("resize", fixToAnchor, false);
            if(scope.live) {
                scope.$watch(digestHandler);
            }
            element.on("$destroy", function() {
                ids = _.without(ids, id);
                minID = Math.min.apply(Math, ids);
            });
        }

        function log() {
            var prefix = element.parent().find(".language-show").text();
            prefix = prefix.substr(prefix.length - 3, 2);
            var args = Array.prototype.slice.call(arguments, 0);
            args.unshift(prefix + "::");
            if (prefix === 'en') {
                console.log.apply(console, args);
            }
        }

        // Handle text scroll position every time text is modified
        function digestHandler() {
            if (height !== el.scrollHeight || cheight !== el.clientHeight || width !== element.width()) {
                if (cheight !== el.clientHeight || width !== element.width()) {
                    unlockHeight();
                }

                // Update clear size at bottom of container if height changed
                if (cheight !== el.clientHeight || clearSize == null) {
                    cheight = el.clientHeight;
                    clearSize = Math.floor(cheight * clearSizeRelative);

                    // Set bottom space of text container
                    element.find("div#data-live-text-container")
                        .css("padding-bottom", clearSize);
                }
                height = el.scrollHeight;
                width = element.width();

                if (height - cheight - el.scrollTop >= clearSize) {
                    if (!enableClearAnimations) {
                        fixToAnchor();
                    } else if (!clearAnimationLock[id] && scope.glued) {

                        // Set animation lock
                        clearAnimationLock[id] = true;
                        scrollToSmooth(height - cheight);

                        // Reset lock after animation has finished
                        setTimeout((function() {
                            clearAnimationLock[id] = false;
                            // Glue to scope
                            scope.glued = true;
                        }).bind(this), clearAnimationTime + 100);
                    }
                }
            }
        }

        function datasLengthChangeHandler(val, oldVal) {
            if (val === oldVal) {
                return;
            }
            lockHeight(el.scrollHeight - 34);
        }

        function lockHeight(height) {
            element.find('.anti-jitter-container').css('min-height', height);
        }

        function unlockHeight() {
            element.find('.anti-jitter-container').css('min-height', 'auto');
        }

        function gluedChangeHandler(value, oldVal) {
            if (value === oldVal) {
                return;
            }
            fixToAnchor();
        }

        function posChangeHandler(pos) {
            if (pos === currentPos) {
                return;
            }
            var scrollTop = mediaStartToScrollTop(pos);
            if (scrollTop !== null) {
                scrollTo(scrollTop);
                currentPos = pos;
                if (pos !== scope.anchor) {
                    scope.glued = false;
                }
            }
        }

        function anchorChangeHandler(value, oldVal) {
            if (value === oldVal) {
                return;
            }
            fixToAnchor();
        }

        function fixToAnchor() {
            var nearest, data, top;
            nearest = getNearestDatas(scope.anchor, resolveStart);
            data = nearest.datas[0] || nearest.datas[1];
            if (!scope.glued) {
                return;
            }
            if (scope.anchor === 0) {
                top = 0;
            } else if (scope.anchor === -1) {
                top = el.scrollHeight - el.clientHeight;
            } else {
                if (!data) {
                    return;
                }
                top = Math.floor(Math.max(0, Math.min(el.scrollHeight - el.clientHeight, resolveScrollTopAndUpdateAlignHeight(data))));
            }
            scrollTo(top);
        }

        function scrollTo(value) {
            if (currentTop === value) {
                return;
            }
            currentTop = value;
            //console.log("SET TO [" + id + "] " + value + " (" + currentPos + ", " + currentTop + ", " + alignHeight + ")");
            element.stop();
            element.scrollTop(value);
        }

        // Scroll the text container smoothly
        function scrollToSmooth(value) {
            if (currentTop === value) {
                return;
            }
            currentTop = value;
            element.stop();
            element.animate({
                scrollTop: value
            }, clearAnimationTime, clearAnimationStyle);
        }

        function isLastValue(value) {
            return _.contains(lastValues, value);
        }

        function setLastValue(value) {
            lastValues[1] = value;
            lastValues.reverse();
        }

        function scrollHandler() {
            var scrollTop = Math.round(el.scrollTop),
                mediaStart = scrollTopToMediaStart(scrollTop),
                glued;

            // Skip if during animation
            if (clearAnimationLock[id]) {
                return;
            }

            if (mediaStart === null || currentPos === mediaStart || currentTop === scrollTop) {
                return;
            }
            if (scope.anchor === -1) {
                glued = scrollTop >= el.scrollHeight - el.clientHeight - 1;
            } else if (scope.anchor === 0) {
                glued = mediaStart === 0;
            } else {
                glued = false;
            }
            if (glued && scope.glued) {
                return;
            }
            currentPos = mediaStart;
            currentTop = scrollTop;
            //console.log("SCROLL TO [" + id + "] " + scrollTop + " (" + currentPos + ", " + currentTop + ", " + scope.anchor + ")");
            scope.$apply(function () {
                scope.glued = glued;
                if (glued) {
                    fixToAnchor();
                } else {
                    scope.pos = mediaStart;
                }
            });
        }

        function mediaStartToScrollTop(mediaStart) {
            var nearest = getNearestDatas(mediaStart, resolveStart),
                data1 = nearest.datas[0],
                data2 = nearest.datas[1],
                result;
            if (mediaStart === 0) {
                return 0;
            } else if (mediaStart === Infinity) {
                return el.scrollHeight - el.clientHeight;
            }
            if (nearest.datas.length === 2) {
                if (!data1) {
                    result = 0;
                } else if (!data2) {
                    result = el.scrollHeight - el.clientHeight;
                } else {
                    var y1 = resolveScrollTop(data1),
                        y2 = resolveScrollTop(data2);
                    result = y1 + nearest.factor * (y2 - y1);
                }
            } else if (nearest.datas.length === 1) {
                result = resolveScrollTop(data1);
            } else {
                return null;
            }

            return Math.floor(Math.max(0, Math.min(el.scrollHeight - el.clientHeight, result)));
        }

        function scrollTopToMediaStart(scrollTop) {
            var nearest, data1, data2;

            updateAlignHeight(scrollTop);
            nearest = getNearestDatas(scrollTop, resolveScrollTop);
            //console.log("UPDATE", nearest, ttt, scrollTop);
            data1 = nearest.datas[0];
            data2 = nearest.datas[1];
            /*if (scrollTop === 0) {
                if (scope.anchor === 0) {
                    return 0;
                }
            } else if (scrollTop >= el.scrollHeight - el.clientHeight) {
                if (scope.anchor === Infinity) {
                    return Infinity;
                }
            }*/
            if (nearest.datas.length === 2) {
                if (!data1) {
                    return resolveStart(data2);
                } else if (!data2) {
                    return resolveStop(data1);
                } else {
                    var m1 = resolveStart(data1),
                        m2 = resolveStart(data2);
                    return m1 + nearest.factor * (m2 - m1);
                }
            } else if (nearest.datas.length === 1) {
                return resolveStart(data1);
            } else {
                return null;
            }
        }

        function updateAlignHeight(scrollTop) {
            var firstDataEl;
            if (scrollTop === 0) {
                if (scope.datas.length > 0) {
                    firstDataEl = $("#" + scope.idPrefix + scope.datas[0][scope.idAttr]);
                    alignHeight = getDataPos(firstDataEl) + (firstDataEl.outerHeight() - firstDataEl.height()) / 2;
                } else {
                    alignHeight = 50;
                }
            } else if (scrollTop >= el.scrollHeight - el.clientHeight) {
                alignHeight = el.clientHeight;
            } else {
                alignHeight = el.clientHeight * 0.3;
            }
        }

        function getDataPos(dataEl) {
            return dataEl.offset().top - element.offset().top + el.scrollTop + (dataEl.outerHeight() - dataEl.height()) / 2;
        }

        function resolveStart(data) {
            return data[scope.startAttr];
        }

        function resolveStop(data) {
            return data[scope.stopAttr];
        }

        function resolveScrollTop(data) {
            //console.log("#" + scope.idPrefix + data[scope.idAttr]);
            var dataEl = $("#" + scope.idPrefix + data[scope.idAttr]),
                pos = getDataPos(dataEl);
            return pos - alignHeight;
        }

        function resolveScrollTopAndUpdateAlignHeight(data) {
            var dataEl = $("#" + scope.idPrefix + data[scope.idAttr]),
                pos = getDataPos(dataEl),
                lastPos = getDataPos($("#" + scope.idPrefix + scope.datas[scope.datas.length - 1][scope.idAttr])),
                result = pos - alignHeight;
            if (result <= 0) {
                result = 0;
                updateAlignHeight(0);
            } else if (pos >= lastPos) {
                updateAlignHeight(el.scrollHeight - el.clientHeight);
                result = el.scrollHeight - el.clientHeight;
            } else {
                updateAlignHeight();
                result = pos - alignHeight;
            }
            return result;
        }

        function getNearestDatas(value, resolveFunc) {
            var index = helperService.binaryIndexOf(scope.datas, value, resolveFunc),
                factor = 1;
            if (index.length === 2) {
                var firstData = scope.datas[index[0]],
                    secondData = scope.datas[index[1]],
                    firstRes, secondRes;
                if (firstData && secondData) {
                    firstRes = resolveFunc(firstData);
                    secondRes = resolveFunc(secondData);
                    factor = (value - firstRes) / (secondRes - firstRes);
                }
                return {
                    factor: factor,
                    datas: [firstData, secondData]
                };
            } else if (index.length === 1) {
                return {
                    factor: 1,
                    datas: [scope.datas[index[0]]]
                };
            } else {
                return {
                    factor: 0,
                    datas: []
                };
            }
        }
    }
}

angular
    .module("app.sessions")
    .controller("SessionSidebarController", SessionSidebarController);

/*@ngInject*/
function SessionSidebarController($scope, $state, treeViewDialogService, auth, layoutService, linkService, emoticonService) {
    var vm = this;

    vm.auth = auth;

    vm.linksEnabled = linkService.isEnabled();
    vm.emojiEnabled = emoticonService.isEnabled();
    vm.ls = layoutService;
    vm.streamSelect = [];

    vm.toggleSelectStream = toggleSelectStream;
    vm.toggleWorkMode = toggleWorkMode;
    vm.toggleOrientation = toggleOrientation;
    vm.toggleFullscreen = toggleFullscreen;
    vm.openTreeView = openTreeView;
    vm.toggleLinks = toggleLinks;
    vm.toggleEmoji = toggleEmoji;

    vm.$onInit = activate;

    //////////

    function activate() {
        $scope.$watch("vm.streams", streamsUpdateHandler);
        $scope.$watch("vm.selectedStreamIdents", selectedStreamIdentsUpdateHandler);
    }

    function selectedStreamIdentsUpdateHandler() {
        _.each(vm.streamSelect, function (stream) {
            stream.selected = _.contains(vm.selectedStreamIdents, stream.ident);
        });
    }

    function streamsUpdateHandler() {
        vm.streamSelect = _.map(vm.streams, function (stream) {
            return {
                selected: _.contains(vm.selectedStreamIdents, stream.ident),
                label: stream.label,
                fingerprint: stream.fingerprint,
                ident: stream.ident
            };
        });
        updateselectedStreamIdents();
    }

    function updateselectedStreamIdents() {
        var selectedStreamIdents = _.map(_.filter(vm.streamSelect, function (stream) {
            return stream.selected;
        }), function (stream) {
            return stream.ident;
        });
        if (vm.selectedStreamIdents.length !== selectedStreamIdents.length || _.intersection(selectedStreamIdents, vm.selectedStreamIdents).length !== selectedStreamIdents.length) {
            updateRoute(selectedStreamIdents);
        }
    }

    function updateRoute(selectedStreamIdents) {
        var selectedFingerprints = _.map(selectedStreamIdents, getFingerprintByIdent);
        $state.go($state.current, {lang: selectedFingerprints.join(",")});
    }

    function getFingerprintByIdent(ident) {
        return _.find(vm.streams, function (stream) {
            return stream.ident === ident;
        }).fingerprint;
    }

    function toggleSelectStream(stream) {
        stream.selected = !stream.selected;
        updateselectedStreamIdents();
    }

    function toggleWorkMode(mode) {
        if (vm.workMode === mode) {
            vm.workMode = 0;
        } else {
            vm.workMode = mode;
            if (mode === 3 && vm.ls.layout.dir === 1) {
                toggleOrientation();
            }
        }
    }

    function toggleOrientation() {
        layoutService.toggleOrientation();
    }

    function toggleFullscreen() {
        layoutService.toggleFullscreen();
    }

    function openTreeView() {
        treeViewDialogService.show(vm.session);
    }

    function toggleLinks() {
        vm.linksEnabled = !vm.linksEnabled;
        if (vm.linksEnabled) {
            linkService.enable();
        } else {
            linkService.disable();
        }
    }

    function toggleEmoji() {
        vm.emojiEnabled = !vm.emojiEnabled;
        if (vm.emojiEnabled) {
            emoticonService.enable();
        } else {
            emoticonService.disable();
        }
    }
}

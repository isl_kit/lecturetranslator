angular
    .module("app.sessions")
    .directive("sessionSidebar", sessionSidebarDirective);

/*@ngInject*/
function sessionSidebarDirective() {

    var directive = {
        restrict: "E",
        scope: {},
        controller: "SessionSidebarController",
        controllerAs: "vm",
        bindToController: {
            session: "=",
            streams: "=",
            workMode: "=",
            selectedStreamIdents: "="
        },
        templateUrl: "sessions/session-content/session-sidebar/session-sidebar.tpl.html"
    };
    return directive;
}

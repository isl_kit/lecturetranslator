angular
    .module("app.sessions")
    .controller("SessionDataController", SessionDataController);

/*@ngInject*/
function SessionDataController($scope, $window, $q, $mdDialog, auth, layoutService, correctionsService) {
    var vm = this,
        rtlLangs = ["ar"],
        dataHash = {},
        streamHash = {};

    vm.auth = auth;

    vm.streams = [];
    vm.streamDatas = [];

    vm.ls = layoutService;
    vm.glue = glue;
    vm.glued = true;
    vm.pos = 0;
    vm.workMode = 0;
    vm.scrollButtonStyle = {
        right: (getScrollbarWidth() + 10) + "px"
    };

    vm.$onInit = activate;
    vm.onSpanSelect = onSpanSelect;

    vm.commitAllowed = EDITOR_ALLOW_COMMIT;  // Global variable

    vm.editDone = editDone;
    vm.saveEdit = saveEdit;
    vm.toggleEdit = toggleEdit;
    vm.toggleHistory = toggleHistory;
    vm.saveEdits = saveEdits;
    vm.transferEdits = transferEdits;

    vm.timeSetter = 0;

    //////////

    function activate() {
        vm.streams = _.without(vm.session.streams, vm.session.backchannelStream, vm.session.metaStream).filter(function (stream) {
            return stream.type !== "unseg-text";
        });
        vm.streamDatas = _.map(vm.streams, streamDataFromStream);
        $scope.$watch("vm.selectedStreamIdents", selectedStreamIdentsUpdateHandler);
    }

    function selectedStreamIdentsUpdateHandler() {
        vm.streamDatas = _.filter(_.map(vm.selectedStreamIdents, function (ident) {
            var stream = streamHash[ident];
            if (!stream) {
                return null;
            }
            return selectStream(stream);
        }), function (streamData) {
            return !!streamData;
        });
        layoutService.setStreamLength(vm.streamDatas.length);
    }

    function selectStream(stream) {
        var streamData = getStreamData(stream.ident);
        if (!streamData) {
            return null;
        }
        getCommit(streamData);
        return streamData;
    }

    function streamDataFromStream(stream) {
        var dataStream = {
            stream: stream,
            dir: _.contains(rtlLangs, stream.fingerprint) ? "rtl": "ltr",
            commit: null,
            reducedData: [],
            filteredData: [],
            edit: false,
            commitEdit: false,
            history: false
        };
        dataHash[stream.ident] = dataStream;
        streamHash[stream.ident] = stream;
        return dataStream;
    }

    function getStreamData(ident) {
        return dataHash[ident];
    }

    function getCommit(streamData) {
        if (streamData.commit) {
            return $q.when(streamData);
        }
        correctionsService.data(vm.session.id, streamData.stream.fingerprint).then(function (commit) {
            streamData.commit = commit;
            streamData.reducedData = _.filter(commit.data, function (data) {
                return data.text.replace('<hr>', '').trim().length > 0;
            });
        });
    }

    function refresh(streamData) {
        streamData.commit = null;
        getCommit(streamData);
    }

    function glue() {
        vm.glued = true;
    }

    function onSpanSelect(data) {
        if (vm.workMode === 2 || vm.workMode === 3) {
            vm.onSelectData({data: data});
            vm.glued = true;
        }
    }

    function toggleEdit(streamData) {
        if (streamData.edit) {
            streamData.commitEdit = false;
            streamData.edit = false;
        } else {
            streamData.edit = true;
        }
    }

    function toggleHistory(streamData) {
        streamData.history = !streamData.history;
    }

    function saveEdits(streamData) {
        streamData.triggerSave = true;
    }

    function transferEdits(streamData) {
        correctionsService.transfer(vm.session.id, streamData.stream.fingerprint);
    }

    function editDone(streamData, text) {
        refresh(streamData);
    }

    function saveEdit(streamData) {
        if (!streamData.edit) {
            return;
        }
        streamData.commitEdit = true;
        streamData.edit = false;
    }

    function showMergeConflictDialog() {
        var confirm = $mdDialog.confirm()
            .title('Merge Conflicts!')
            .textContent('Your edit cannot be submitted due to merge conflicts. This means that there is a new version of the text available. You have load the new version first, before you can submit your edit. Would you like to load the newest version now? Since this will override your changes, you can also choose to keep your edited version, so that you can backup your changes and when you are done, you can simply reload the page. What would you like to do?')
            .ariaLabel('Merge conflicts')
            .targetEvent(null)
            .ok('Load new version and lose all changes')
            .cancel('Keep changes and reload page later');

        return $mdDialog.show(confirm);
    }
}

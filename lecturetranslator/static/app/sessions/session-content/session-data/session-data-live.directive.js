angular
    .module("app.sessions")
    .directive("sessionDataLive", sessionDataLiveDirective);

/*@ngInject*/
function sessionDataLiveDirective() {

    var directive = {
        restrict: "E",
        scope: {
            session: "=",
            selectedStreamIdents: "=",
        },
        controller: "SessionDataLiveController",
        controllerAs: "vm",
        bindToController: true,
        templateUrl: "sessions/session-content/session-data/session-data-live.tpl.html"
    };
    return directive;
}

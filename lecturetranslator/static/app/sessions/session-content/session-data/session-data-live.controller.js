angular
    .module("app.sessions")
    .controller("SessionDataLiveController", SessionDataLiveController);

/*@ngInject*/
function SessionDataLiveController($scope, EmojiHandler, BackchannelHandler, layoutService, streamService) {
    var vm = this,
        rtlLangs = ["ar"];

    vm.emojiHandler = new EmojiHandler();
    vm.backchannelHandler = new BackchannelHandler();

    vm.streams = [];
    vm.dataStreams = [];

    vm.ls = layoutService;
    vm.glue = glue;
    vm.glued = true;
    vm.pos = 0;

    // Timeout for stream elements to be changed (in ms)
    vm.streamFinalizeTimeout = 10000;
    vm.streamFinalizeTimer = {};

    vm.scrollButtonStyle = {
        right: (getScrollbarWidth() + 10) + "px"
    };

    vm.$onInit = activate;

    //////////

    function activate() {
        vm.streams = _.without(vm.session.streams, vm.session.backchannelStream, vm.session.metaStream).filter(function (stream) {
            return stream.type !== "unseg-text";
        });
        if (vm.session.metaStream) {
            vm.emojiHandler.start(vm.session.metaStream.id);
        }
        if (vm.session.backchannelStream) {
            vm.backchannelHandler.start(vm.session.backchannelStream.id);
        }
        $scope.$watch("vm.selectedStreamIdents", selectedStreamIdentsUpdateHandler);
        $scope.$on("sessions.update", sessionUpdateHandler);
        $scope.$on("$destroy", deactivate);
    }

    function selectedStreamIdentsUpdateHandler() {
        var streamsToDeselect = _.filter(vm.dataStreams, function (dataStream) {
            return vm.selectedStreamIdents.indexOf(dataStream.id) === -1;
        });
        _.each(streamsToDeselect, function (dataStream) {
            disconnect(dataStream);
        });
        vm.dataStreams = _.filter(_.map(vm.selectedStreamIdents, function (ident) {
            var dataStream = getDataStreamByIdent(ident);
            if (dataStream) {
                return dataStream;
            }
            var stream = getStreamByIdent(ident);
            if (stream) {
                return connect(stream);
            }
            return null;
        }), function (dataStream) {
            return dataStream;
        });
        layoutService.setStreamLength(vm.dataStreams.length);
    }

    function deactivate() {
        _.each(vm.dataStreams, disconnect);
        vm.emojiHandler.stop();
        vm.backchannelHandler.stop();
    }

    function updateUI(dataStream, eventType) {
        if (eventType === "update") {
            // Mark stream elements as final or temporary
            markFinal(dataStream);
            // Update UI
            $scope.$apply();
        } else {
            dataStream.initialized = true;
        }
    }

    // Set temporary flag on data, if not final
    function markFinal(dataStream) {
        // Clear timer
        if (vm.streamFinalizeTimer[dataStream.id] != undefined) {
            clearTimeout(vm.streamFinalizeTimer[dataStream.id]);
        }

        // Mark last element as not final
        var stream = dataStream.data[1];
        stream[stream.length - 1].temporary = true;

        // Mark second to last element as final
        if (stream.length > 1) {
            stream[stream.length - 2].temporary = false;
        }

        // Set timeout for last element to become final
        vm.streamFinalizeTimer[dataStream.id] = setTimeout(function() {
            stream[stream.length - 1].temporary = false;
        }, vm.streamFinalizeTimeout);
    }

    function connect(stream) {
        var dataStream = {
            id: stream.ident,
            dir: _.contains(rtlLangs, stream.fingerprint) ? "rtl": "ltr",
            fingerprint: stream.fingerprint,
            initialized: false,
            data: [[],[],[]],
            filteredData: [],
            label: stream.label
        };
        dataStream.data = streamService.connect(vm.session.id, stream.ident, updateUI.bind(null, dataStream));
        return dataStream;
    }

    function disconnect(dataStream) {
        vm.dataStreams = _.without(vm.dataStreams, dataStream);
        streamService.disconnect(vm.session.id, dataStream.id);
    }

    function getStreamByIdent(ident) {
        return _.find(vm.streams, function (stream) {
            return stream.ident === ident;
        });
    }

    function getDataStreamByIdent(ident) {
        return _.find(vm.dataStreams, function (dataStream) {
            return dataStream.id === ident;
        });
    }

    function glue() {
        vm.glued = true;
    }

    function sessionUpdateHandler(event, session) {
        if (session.id === vm.session.id) {
            updateSession(session);
        }
    }

    function updateSession(session) {
        var newStreams = _.without(session.streams, session.backchannelStream, session.metaStream).filter(function (stream) {
            return stream.type !== "unseg-text";
        });
        if (vm.session.backchannelStream && !session.backchannelStream) {
            vm.backchannelHandler.stop();
        } else if (!vm.session.backchannelStream && session.backchannelStream) {
            vm.backchannelHandler.start(session.backchannelStream.id);
        }
        if (vm.session.metaStream && !session.metaStream) {
            vm.emojiHandler.stop();
        } else if (!vm.session.metaStream && session.metaStream) {
            vm.emojiHandler.start(session.metaStream.id);
        }
        vm.session = session;
        vm.streams = newStreams;
    }
}

angular
    .module("app.sessions")
    .factory("BackchannelHandler", BackchannelHandlerModel);

/* @ngInject */
function BackchannelHandlerModel(liveStreamService) {

    function BackchannelHandler() {
        this.streamId = 0;
    }

    BackchannelHandler.prototype.start = start;
    BackchannelHandler.prototype.stop = stop;

    return BackchannelHandler;

    //////////

    function start(streamId) {
        if (BACKCHANNEL && !this.streamId) {
            this.streamId = streamId;
            liveStreamService.join(this.streamId, "s", 3, _messageHandler.bind(this));
        }
    }

    function stop() {
        if (BACKCHANNEL && this.streamId) {
            liveStreamService.leave(this.streamId, "s", 3);
            this.streamId = 0;
        }
    }

    function _messageHandler(message) {
        var sound, file, num;
        num = Math.floor(Math.random() * 17);
        num = num < 10 ? "0" + num : "" + num;
        file = "bc-" + num + ".ogg";
        sound = new Audio("/static/assets/wav/" + file);
        sound.play();
    }
}

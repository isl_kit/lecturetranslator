angular
    .module("app.sessions")
    .factory("DataCompositer", DataCompositerModel);

/* @ngInject */
function DataCompositerModel(Data, helperService) {

    function DataCompositer(stream) {
        this.stream = stream;
        this.paragraphs = [];
        this.flatDatas = [];
        this.filteredDatas = [];
        this._hash = {};
        this._cache = [];
        this._initialized = false;
        this._handlers = [];
    }

    DataCompositer.prototype.init = init;
    DataCompositer.prototype.addData = addData;
    DataCompositer.prototype.updateData = updateData;
    DataCompositer.prototype.addLinks = addLinks;
    DataCompositer.prototype.isInitialized = isInitialized;
    DataCompositer.prototype.addEventHandler = addEventHandler;
    DataCompositer.prototype._addData = _addData;
    DataCompositer.prototype._addToCache = _addToCache;
    DataCompositer.prototype._clearCache = _clearCache;
    DataCompositer.prototype._trigger = _trigger;

    return DataCompositer;

    //////////

    function init(datas) {
        var firstNonZeroData = null;
        datas = _.map(datas, function (data) {
            var d = new Data(data);
            if (firstNonZeroData === null && d.mediaStart > 0) {
                firstNonZeroData = d;
            }
            return d;
        });
        var lastData = datas[datas.length - 1],
            that = this;

        if (firstNonZeroData) {
            _.each(datas, function (data) {
                if (data.mediaStart > 0) {
                    data.compStart = data.mediaStart;
                } else {
                    data.compStart = (data.startTimestamp - firstNonZeroData.startTimestamp) / 1000;
                }
            });
        }
        if (lastData) {
            this.paragraphs = _.map(new Array(lastData.paragraph + 1), function () {
                return [];
            });
            _.each(datas, function (data) {
                that._hash[data.id] = data;
                that.paragraphs[data.paragraph].push(data);
                if (data.text.trim().length > 0) {
                    that.flatDatas.push(data);
                }
            });
        }
        this._trigger("init");
        this._initialized = true;
        this._clearCache();
    }

    function addData(data) {
        var oldData = this._hash[data.id];
        if (!this._initialized || (oldData && oldData.stopTimestamp > data.e)) {
            this._addToCache(data);
            return;
        }
        this._clearCache();
        this._addData(data);
    }

    function updateData(datas) {
        _.each(datas, function (data) {
            var oldData = this._hash[data.id];
            if (oldData) {
                oldData.update(data);
                this._trigger("update", oldData.id);
            }
        });
    }

    function addLinks(linkDatas) {
        var data = this._hash[linkDatas[0].dataId];
        data.addLinks(linkDatas);
    }

    function addEventHandler(handler) {
        this._handlers.push(handler);
    }

    function isInitialized() {
        return this._initialized;
    }

    function _trigger(type, arg) {
        _.each(this._handlers, function (handler) {
            handler(type, arg || null);
        });
    }

    function _addData(data) {
        var oldData = this._hash[data.id];
        if (oldData) {
            oldData.update(data);
            this._trigger("update", oldData.id);
        } else {
            data = new Data(data);
            this._hash[data.id] = data;
            if (!this.paragraphs[data.paragraph]) {
                this.paragraphs[data.paragraph] = [];
            }
            this.paragraphs[data.paragraph].push(data);
            if (data.text.trim().length > 0) {
                this.flatDatas.push(data);
            }
            this._trigger("add", data);
        }
    }

    function _addToCache(data) {
        this._cache.push(data);
    }

    function _clearCache() {
        var that = this;
        _.each(this._cache, function (data) {
            that._addData(data);
        });
        this._cache = [];
    }
}

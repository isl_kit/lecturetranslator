angular
    .module("app.sessions")
    .factory("liveStreamService", liveStreamService);

/* @ngInject */
function liveStreamService(io) {

    var service = {
        join: join,
        leave: leave,
        _handlers: {}
    };

    return service;

    //////////

    function join(streamId, prefix, componentId, callback) {
        var sid = prefix + streamId,
            handler = function (message) {
                callback(message);
            };
        if (!service._handlers[sid]) {
            service._handlers[sid] = {};
            io.join(sid);
        }
        service._handlers[sid][componentId] = handler;
        io.on("message_" + sid, callback);
    }

    function leave(streamId, prefix, componentId) {
        var sid = prefix + streamId,
            handler = service._handlers[sid] ? service._handlers[sid][componentId] : null;
        if (!handler) {
            return;
        }
        io.off("message_" + sid, handler);
        delete service._handlers[sid][componentId];
        if (_.size(service._handlers[sid]) === 0) {
            io.leave(sid);
            delete service._handlers[sid];
        }
    }
}

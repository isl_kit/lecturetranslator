angular
    .module("app.sessions")
    .factory("linkService", linkService);

/* @ngInject */
function linkService($rootScope) {

    var rawlinks = {
        "http://ahclab.naist.jp/Prof.Nakamura/index_e.html": [
            "satoshi nakamura",
            "satoshi",
            "nakamura"
        ],
        "http://ailab.ijs.si/marko_grobelnik/": [
            "marco grobelnik",
            "marco",
            "grobelnik"
        ],
        "http://daviddao.de/": [
            "david dao",
            "david",
            "dao"
        ],
        "http://finance.fbv.kit.edu/14_668.php": [
            "jörg franke",
            "jörg",
            "franke"
        ],
        "http://h2t.anthropomatik.kit.edu/english/21_66.php/": [
            "tamim asfour",
            "tamim",
            "asfour"
        ],
        "http://isl.anthropomatik.kit.edu/english/21_74.php": [
            "alex waibel",
            "alex",
            "waibel"
        ],
        "http://mi.eng.cam.ac.uk/~sjy/": [
            "steve young",
            "steve"
        ],
        "http://people.tcd.ie/Profile?Username=nick": [
            "nick campbell",
            "nick",
            "campbell"
        ],
        "http://sdq.ipd.kit.edu/people/anne_koziolek/": [
            "anne koziolek",
            "anne",
            "koziolek"
        ],
        "http://videolectures.net/jaap_van_der_meer/": [
            "jaap van der meer",
            "jaap",
            "van der meer"
        ],
        "http://videolectures.net/jimmy_siegfried_kunzmann/": [
            "jimmy kunzmann",
            "jimmy",
            "kunzmann"
        ],
        "http://www-i6.informatik.rwth-aachen.de/web/Staff/ney/": [
            "hermann ney",
            "hermann",
            "ney"
        ],
        "http://www-prima.inrialpes.fr/jlc/jlc.html": [
            "james crowley",
            "james",
            "crowley"
        ],
        "http://www.accipio-consulting.com/dr-volker-steinbiss/": [
            "volker steinbiss",
            "volker",
            "steinbiss"
        ],
        "http://www.bccn-goettingen.de/People/person.2006-10-04.1932142124": [
            "florentin wörgötter",
            "florentin",
            "wörgötter"
        ],
        "http://www.cs.cmu.edu/~sburger/": [
            "susanne burger",
            "burger"
        ],
        "http://www.cs.cmu.edu/~vogel/": [
            "stephan vogel",
            "stephan"
        ],
        "http://www.cs.ust.hk/~dekai/": [
            "dekai wu",
            "dekai",
            "wu"
        ],
        "http://www.dcs.shef.ac.uk/~roger/": [
            "roger moore",
            "roger",
            "moore"
        ],
        "http://www.dfg.de/dfg_profil/geschaeftsstelle/struktur/personen/index.jsp?id=484849495356": [
            "andreas engelke",
            "andreas",
            "engelke"
        ],
        "http://www.dfki.de/~hansu/": [
            "hans uszkoreit",
            "hans",
            "uszkoreit"
        ],
        "http://www.ias.et.tu-dresden.de/ias/index.php?id=435": [
            "rüdiger hoffmann",
            "hoffmann"
        ],
        "http://www.itas.kit.edu/english/staff_decker_michael.php": [
            "michael decker",
            "michael",
            "decker"
        ],
        "http://www.kit.edu/ps/vp-innovation.php": [
            "thomas hirth",
            "thomas",
            "hirth"
        ],
        "http://www.scc.kit.edu/personen/hannes.hartenstein.php": [
            "hannes hartenstein",
            "hartenstein"
        ],
        "http://www.sp.m.is.nagoya-u.ac.jp/~takeda/": [
            "kazuya takeda",
            "kazuya",
            "takeda"
        ],
        "http://www.takanishi.mech.waseda.ac.jp/top/takanishi/": [
            "atsuo takanishi",
            "atsuo",
            "takanishi"
        ],
        "http://www.uni-ulm.de/index.php?id=nt-minker": [
            "wolfgang minker",
            "wolfgang",
            "minker"
        ],
        "http://www.waseda.jp/sem-sagisaka/en/fcl_info.html": [
            "yoshinori sagisaka",
            "yoshinori",
            "sagisaka"
        ],
        "http://www.waseda.jp/wias/eng/aboutus/organization/kobayashi.html": [
            "tetsunori kobayashi",
            "tetsunori",
            "kobayashi"
        ],
        "http://www.zak.kit.edu/Direktorin.php": [
            "caroline y. robertson-von trotha",
            "caroline y.",
            "robertson-von trotha"
        ],
        "http://www.zkonsult.de/%C3%BCber-mich/": [
            "elisabeth zuber-knost",
            "elisabeth",
            "zuber-knost"
        ],
        "http://www2.informatik.uni-freiburg.de/~burgard/": [
            "wolfram burgard",
            "wolfram",
            "burgard"
        ],
        "https://ces.itec.kit.edu/21_500.php": [
            "winfried görke",
            "winfried",
            "görke"
        ],
        "https://euractory.eucommunity.eu/susanne-altenberg": [
            "susanne altenberg",
            "altenberg"
        ],
        "https://his.anthropomatik.kit.edu/english/21_321.php": [
            "rüdiger dillmann",
            "dillmann"
        ],
        "https://perso.limsi.fr/gadda/": [
            "gilles adda",
            "gilles",
            "adda"
        ],
        "https://perso.limsi.fr/mariani/": [
            "joseph mariani",
            "joseph",
            "mariani"
        ],
        "https://ps.ipd.kit.edu/176_376.php": [
            "walter tichy",
            "walter",
            "tichy"
        ],
        "https://raue.com/en/anwalt/ulrich-amelung/": [
            "ulrich amelung",
            "ulrich",
            "amelung"
        ],
        "https://sites.google.com/site/marcellofedericohome/": [
            "marcello federico",
            "marcello",
            "federico"
        ],
        "https://www.b-tu.de/fg-kommunikationstechnik/team/ehemalige/prof-dr-ing-klaus-ruediger-fellbaum": [
            "klaus fellbaum",
            "klaus",
            "fellbaum"
        ],
        "https://www.cs.cmu.edu/~jgc/": [
            "jaime carbonell",
            "jaime",
            "carbonell"
        ],
        "https://www.hrk.de/hrk-at-a-glance/executive-board/horst-hippler/": [
            "horst hippler",
            "horst",
            "hippler"
        ],
        "https://www.hs-karlsruhe.de/fakultaeten/wirtschaftswissenschaften/bachelorstudiengaenge/international-management/leitung-und-gremien.html": [
            "ivica rogina",
            "ivica",
            "rogina"
        ],
        "https://www.klinikum.uni-heidelberg.de/Dr-med-Hannes-Goetz-Kenngott.100842.0.html": [
            "hannes kenngott",
            "kenngott"
        ],
        "https://www.klinikum.uni-heidelberg.de/Prof-Dr-med-Markus-W-Buechler.104383.0.html": [
            "markus büchler",
            "markus",
            "büchler"
        ],
        "https://www.microsoft.com/en-us/research/people/christw/": [
            "chris wendt",
            "chris",
            "wendt"
        ],
        "https://www.pkm.kit.edu/english/press_office_team_landgraf.php": [
            "monika landgraf",
            "monika",
            "landgraf"
        ],
        "https://www.szs.kit.edu/english/287_1072.php": [
            "rainer stiefelhagen",
            "rainer",
            "stiefelhagen"
        ],
        "https://www.uni-bielefeld.de/Universitaet/Ueberblick/Organisation/Rektorat/Rektor/": [
            "gerhard sagerer",
            "gerhard",
            "sagerer"
        ],
        "http://ec.europa.eu/index_en.htm": [
            "european commission"
        ],
        "http://en.hs-furtwangen.de/": [
            "hochschule furtwangen"
        ],
        "http://en.nagoya-u.ac.jp/": [
            "nagoya university"
        ],
        "http://english.cas.cn/": [
            "chinese academy of science",
            "cas"
        ],
        "http://equityplus.net/": [
            "equity plus"
        ],
        "http://www.baden-baden.de/": [
            "baden-baden"
        ],
        "http://www.bccn-goettingen.de/": [
            "bccn"
        ],
        "http://www.bosch.com/en/com/home/index.php": [
            "bosch"
        ],
        "http://www.cmu.edu/": [
            "carnegie mellon university",
            "cmu"
        ],
        "http://www.cnrs.fr/": [
            "centre national de la recherche scientifique",
            "cnrs"
        ],
        "http://www.dfg.de/en/": [
            "deutsche forschungsgesellschaft",
            "dfg"
        ],
        "http://www.ebay.com/": [
            "ebay"
        ],
        "http://www.elra.info/en/": [
            "european language resource association",
            "elra"
        ],
        "http://www.eml.org/english/": [
            "european media laboratory",
            "eml"
        ],
        "http://www.europarl.europa.eu/portal/en": [
            "european parliament"
        ],
        "http://www.fbk.eu/": [
            "fondazione bruno kessler",
            "fbk"
        ],
        "http://www.flintec.de/": [
            "flintec"
        ],
        "http://www.hs-karlsruhe.de/home.html": [
            "hochschule karlsruhe"
        ],
        "http://www.inria.fr/en/": [
            "inventeurs du monde numérique",
            "inria"
        ],
        "http://www.kit.edu/index.php": [
            "karlsruhe institute of technology",
            "kit"
        ],
        "http://www.mhp.com/de/home/": [
            "mhp"
        ],
        "http://www.naist.jp/en/": [
            "nara institute of science and technology",
            "naist"
        ],
        "http://www.nuance.com/index.htm": [
            "nuance communications"
        ],
        "http://www.pitt.edu/": [
            "university of pittsburgh"
        ],
        "http://www.pja.edu.pl/en/": [
            "polnish-japanese academy of information technology",
            "pja"
        ],
        "http://www.rwth-aachen.de/cms/~a/root/lidx/1/": [
            "rwth aachen",
            "rwth"
        ],
        "http://www.sheffield.ac.uk/": [
            "university of shefield"
        ],
        "http://www.sony.com/": [
            "sony"
        ],
        "http://www.televic.com/en/": [
            "televic"
        ],
        "http://www.uni-bielefeld.de/(en)/": [
            "bielefeld university"
        ],
        "http://www.uni-heidelberg.de/index_e.html": [
            "universität heidelberg"
        ],
        "http://www.uni-saarland.de/en/home.html": [
            "saarland university"
        ],
        "http://www.ust.hk/": [
            "hong kong university of science and technology",
            "hkust"
        ],
        "https://consulting.vector.com/": [
            "vector consulting"
        ],
        "https://raue.com/en/": [
            "raue llp"
        ],
        "https://tu-dresden.de/": [
            "technische universität dresden",
            "tu dresden"
        ],
        "https://www.b-tu.de/en/": [
            "technische universität cottbus",
            "tu cottbus"
        ],
        "https://www.broadinstitute.org/": [
            "broad institute"
        ],
        "https://www.cam.ac.uk/": [
            "university of cambridge"
        ],
        "https://www.citrix.com/": [
            "citrix"
        ],
        "https://www.dfki.de/web": [
            "künstliche intelligenz\"",
            "dfki"
        ],
        "https://www.dkfz.de/de/index.html": [
            "deutsches krebsforschungszentrum",
            "dkfz"
        ],
        "https://www.facebook.com/": [
            "facebook"
        ],
        "https://www.google.com/": [
            "google"
        ],
        "https://www.hrk.de/home/": [
            "german rectors conference",
            "hrk"
        ],
        "https://www.ieee.org/index.html": [
            "institute of electrical and electronic engineers",
            "ieee"
        ],
        "https://www.ijs.si/ijsw/JSI": [
            "jozef stefan institute",
            "jsi"
        ],
        "https://www.kuleuven.be/kuleuven/": [
            "kuleuven"
        ],
        "https://www.limsi.fr/en/": [
            "et les sciences de l'ingénieur\"",
            "limsi"
        ],
        "https://www.microsoft.com/": [
            "microsoft"
        ],
        "https://www.taus.net/": [
            "taus"
        ],
        "https://www.tcd.ie/": [
            "trinity college dublin"
        ],
        "https://www.uni-freiburg.de/start-en.html?set_language=en": [
            "freiburg university"
        ],
        "https://www.uni-ulm.de/en/homepage.html": [
            "ulm university"
        ],
        "https://www.waseda.jp/top/en": [
            "waseda university"
        ]
    }, wordHash = {}, re, enabled = false;

    var service = {
        resolve: resolve,
        enable: enable,
        disable: disable,
        isEnabled: isEnabled
    };

    activate();

    return service;

    //////////

    function activate() {
        var words = [];
        _.each(rawlinks, function (wordArr, link) {
            _.each(wordArr, function (word) {
                words.push(word);
                wordHash[word] = link;
            });
        });
        re = new RegExp("\\b(" + words.join("|") + ")\\b", "gi");
    }

    function enable() {
        if (!LINKS_ENABLED) {
            return;
        }
        enabled = true;
        $rootScope.$broadcast("links.statechange", true);
    }

    function disable() {
        if (!LINKS_ENABLED) {
            return;
        }
        enabled = false;
        $rootScope.$broadcast("links.statechange", false);
    }

    function isEnabled() {
        return enabled;
    }

    function resolve(text) {
        if (!LINKS_ENABLED) {
            return text;
        }
        return text.replace(re, function(matched){
            return "<a target='_blank' href='" + wordHash[matched.toLowerCase()] + "'>" + matched + "</a>";
        });
    }
}

angular
    .module("app.sessions")
    .factory("streamMockService", streamMockService);

/* @ngInject */
function streamMockService($timeout, $interval, Data) {

    var streams = {};

    return {
        connect: connect,
        disconnect: disconnect
    };

    //////////

    function initStream(streamId, callback) {
        streams[streamId] = {
            raw: [],
            rawIndex: 0,
            intKey: null,
            static: [],
            dynamic: [],
            cache: [],
            all: [],
            hash: {},
            initialized: false,
            callback: callback
        };
        return streams[streamId];
    }

    function connect(streamId, callback) {
        if (_.has(streams, streamId)) {
            return;
        }
        var stream = initStream(streamId, callback);
        stream.raw = getDatasFromText(getText());
        console.log(stream.raw);
        $timeout(function () {
            var initDatas = stream.raw.slice(0, 25);
            stream.rawIndex = 25;
            initData(stream, initDatas);
        }, 300);
        stream.intKey = $interval(messageHandler.bind(null, streamId), 200);
        streams[streamId] = stream;
        return [stream.static, stream.dynamic, stream.all];
    }

    function disconnect(streamId) {
        if (!_.has(streams, streamId)) {
            return;
        }
        $interval.cancel(streams[streamId].intKey);
        delete streams[streamId];
    }

    function messageHandler(streamId) {
        var stream = streams[streamId], rawData;
        if (!stream) {
            return;
        }
        rawData = stream.raw[stream.rawIndex++];
        if (!rawData) {
            $interval.cancel(stream.intKey);
            return;
        }
        var oldData = stream.hash[rawData.id];
        if (!stream.initialized || (oldData && oldData.stopTimestamp > rawData.e)) {
            addToCache(stream, rawData);
            return;
        }
        clearCache(stream);
        handleData(stream, rawData);
        stream.callback("put");
    }

    function initData(stream, rawDatas) {
        _.each(rawDatas, function (rawData, idx) {
            var data = new Data(rawData);
            if (idx < rawDatas.length - 1) {
                stream.static.push(data);
            } else {
                stream.dynamic.push(data);
            }
            stream.all.push(data);
            stream.hash[data.id] = data;
        });
        stream.initialized = true;
        clearCache(stream);
        stream.callback("init");
    }

    function handleData(stream, rawData) {
        var oldData = stream.hash[rawData.id],
            lastData,
            data;
        if (oldData) {
            Data.call(oldData, rawData);
        } else {
            lastData = stream.dynamic[stream.dynamic.length - 1];
            data = new Data(rawData);
            if (lastData && lastData.paragraph != data.paragraph) {
                lastData.last = true;
            }
            stream.hash[data.id] = data;
            stream.dynamic.push(data);
            stream.all.push(data);
        }
    }

    function addToCache(stream, rawData) {
        stream.cache.push(rawData);
    }

    function clearCache(stream) {
        _.each(stream.cache, function (rawData) {
            handleData(stream, rawData);
        });
        stream.cache = [];
    }

    function getDatasFromText(text) {
        var i = 0, substr, datas = [];
        while (text) {
            substr = text.substr(0, 50);
            text = text.substr(50);
            i += 1;
            var id = getRandomId();
            datas.push({
                id: id,
                s: i * 1000,
                e: i * 1000,
                ms: i,
                me: i,
                c: 'asr',
                p: Math.floor(i / 12),
                v: 0,
                x: 0,
                l: false,
                t: substr.substr(0, 25)
            });
            datas.push({
                id: id,
                s: i * 1000 + 200,
                e: i * 1000 + 200,
                ms: i + 0.2,
                me: i + 0.2,
                c: 'asr',
                p: Math.floor(i / 12),
                v: 0,
                x: 0,
                l: false,
                t: substr
            });
            datas.push({
                id: id,
                s: i * 1000 + 400,
                e: i * 1000 + 400,
                ms: i + 0.4,
                me: i + 0.4,
                c: 'asr',
                p: Math.floor(i / 12),
                v: 0,
                x: 0,
                l: false,
                t: substr.substr(0, 10)
            });
            datas.push({
                id: getRandomId(),
                s: i * 1000 + 500,
                e: i * 1000 + 500,
                ms: i + 0.6,
                me: i + 0.6,
                c: 'asr',
                p: Math.floor(i / 12),
                v: 0,
                x: 0,
                l: (i + 1) % 12 === 0,
                t: substr.substr(10)
            });
        }
        return datas;
    }

    function getRandomId() {
        return Math.floor(Math.random() * 1000000000);
    }

    //////////

    function getText() {
        return "Welcome back to week one unit to you. Where we would like to give you a brief summary of excess advance and hdi or higher deployment infrastructure now as we saw at first applications being built one hina we found that much of the processing. Was taking place in the database, so we felt would be ashamed to add to the infrastructure footprint by requiring requiring additional web apps or for providing minimal functionality so we saw an opportunity to you in very small footprint ed what happened web server directly in hanoi itself SAP hina extend applications services or access for short. Was first first introduced in seb hines sbs zero five this enables our customers to develop full-blown applications that ran completely from within the single honey instance, which reduces the architectural layers and layers that ucl having this app web server so closely intertwined with the database. Allowed us to benefit from the memory and massive parallel processing capabilities of hot simplify the architecture which meant we had the single process for installing patching and illustrating both the database and the application server layers before we go into the details, it's important to understand that, we do not remove anything customers can upgrade with coffins SAP will not remove anything until we see mass of customers moving from africa moving their applications from excess classic to access advanced excessive gas represents an evolution of the application server burning on its on the strengths and expanding the scope so there was a desire to unify the architecture of solutions built in the cloud, and on premise now now you see p already was looking to adopt cloud foundry as the base of its next generation offering in-car foundry a provide scalability options and flexibility flexible run times needed for the cloud environment, which makes perfect sense. For for hcp and today excesses offer on a both on pram and one each gp but access in SAP is rather separated from the rest of the cp the goal of excess excess aia was to unify these two delivery channel's into a single based architecture and this is excess aia is essentially based on cloud foundry and in the near future hanukkah platform while ryan at one o'clock founder for one pram we felt like the a the complete cloud foundry technical stack was a bit much so we created our own implementation of the cloud foundry apis as excess fat this means that the core concepts of our foundry will be present when both on pram and hicp in cloud foundry standard bill packs will be utilized for the language support in both environments and moving to cloud foundry of the obvious impacts are that we have a foundation in excess to support multiple languages and run times and today exists eight is the liver with the forward and fully supports apache time iii java as well as google. V eight javascript and no j s the overall architecture. Being open creates the possibility of having a true. Bring your own language and bring your own runtime scenario so customers and partners can utilize other cloud foundry a billion packs as well, such as ruby go in each a php with cloud foundry it also brings the concept of micro services and excess classic. We had any single access engine process with the poor of javascript be apps there were no options. To have control over which version of java script. Your app runs against kahle founder eight nx with car foundry in excess aid. This works differently so during deployment deployment of an app or service a copy of the entire java or no gis runtime this copy with it he services. Isolated and matched up to their, to their version so even when upgrading an entire hina access system a deployed services continue to run with their existing run-time it is. Until you do the redeploy and then pick up the new version of the runtime this allows is single service to target multiple run-time versions in parallel now. At the lowest level we, we no longer have one. Process hosting hosting of the run times instead each runtime he's runtime insists has its own. O s o s process so if one service crashes. Does not take out the entire server and only that services affected now the default installation option and we're still place excess aid directly on the higher server for an all in one box. Experience but now with excess hair, you can install on a separate hosts, which means you can scale. Access independently from the database and use cheaper hardware to do so. So we also make it easier to choose between on pram and ink cloud deployment because of this stain or architecture described here will soon be easy and to develop the single application, can be deployed on premise and in the cloud or both. Without changing mat make without making code changes over the next to say. We no longer use source code repository within harnett within a hundred database. And say we designed we we we design the tools and processes around the external get repository so this brings in standard features of get including merging and branching so on and as of sbs, well we do. Ship get in garrett garrett for.";
    }

}

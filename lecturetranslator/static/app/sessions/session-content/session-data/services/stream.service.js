angular
    .module("app.sessions")
    .factory("streamService", streamService);

/* @ngInject */
function streamService(liveStreamService, sessionService, Data) {

    var streams = {};

    return {
        connect: connect,
        disconnect: disconnect,
        update: update
    };

    //////////

    function initStream(streamId, callback) {
        streams[streamId] = {
            static: [],
            dynamic: [],
            cache: [],
            all: [],
            hash: {},
            initialized: false,
            callback: callback
        };
        return streams[streamId];
    }

    function connect(sessionId, streamIdent, callback) {
        var streamId = getStreamId(sessionId, streamIdent);
        if (_.has(streams, streamId)) {
            return;
        }
        var stream = initStream(streamId, callback);
        liveStreamService.join(streamId, "s", 1, messageHandler.bind(null, streamId));
        sessionService.data(sessionId, streamIdent).then(function (datas) {
            initData(stream, datas);
        });
        streams[streamId] = stream;
        return [stream.static, stream.dynamic, stream.all];
    }

    function disconnect(sessionId, streamIdent) {
        var streamId = getStreamId(sessionId, streamIdent);
        if (!_.has(streams, streamId)) {
            return;
        }
        liveStreamService.leave(streamId, "s", 1);
        delete streams[streamId];
    }

    function update(sessionId, streamIdent, datas) {
        var streamId = getStreamId(sessionId, streamIdent);
        if (!_.has(streams, streamId)) {
            return;
        }
        var stream = streams[streamId];
        stream = initStream(streamId, stream.callback);
        initData(stream, datas);
        return [stream.static, stream.dynamic, stream.all];
    }

    function messageHandler(streamId, message) {
        var stream = streams[streamId],
            rawData = message.data;
        if (!stream) {
            return;
        }
        var oldData = stream.hash[rawData.start];
        if (!stream.initialized || (oldData && oldData.stop > rawData.stop && isLast(stream, oldData))) {
            addToCache(stream, rawData);
            return;
        }
        clearCache(stream);
        handleData(stream, rawData);
        stream.callback("update");
    }

    function initData(stream, datas) {
        _.each(datas, function (data, idx) {
            if (idx < datas.length - 1) {
                stream.static.push(data);
            } else {
                stream.dynamic.push(data);
            }
            stream.all.push(data);
            stream.hash[data.start] = data;
        });
        stream.initialized = true;
        clearCache(stream);
        stream.callback("init");
    }

    function clearData(stream) {
        stream.static = [];
        stream.dynamic = [];
        stream.all = [];
        stream.hash = {};
        stream.initialized = false;
    }

    function isLast(stream, data) {
        return stream.all.length > 0 && stream.all[stream.all.length - 1] === data;
    }

    function handleData(stream, rawData) {
        var oldData = stream.hash[rawData.start],
            data;
        if (oldData) {
            if (rawData.created > oldData.created) {
                Data.call(oldData, rawData);
            }
        } else {
            data = new Data(rawData);
            stream.hash[data.start] = data;
            addAtTimestamp(stream.dynamic, data);
            addAtTimestamp(stream.all, data);
        }
    }

    function addAtTimestamp(arr, data) {
        var idx = arr.length;
        while (idx > 0 && arr[idx - 1].start > data.start) {
            idx--;
        }
        arr.splice(idx, 0, data);
    }

    function addToCache(stream, rawData) {
        stream.cache.push(rawData);
    }

    function clearCache(stream) {
        _.each(stream.cache, function (rawData) {
            handleData(stream, rawData);
        });
        stream.cache = [];
    }

    function getStreamId(sessionId, streamIdent) {
        return sessionId + ':' + streamIdent;
    }
}

angular
    .module("app.sessions")
    .factory("emoticonService", emoticonService);

/* @ngInject */
function emoticonService($rootScope) {

    var rawlinks = {
        "star": ["star", "stern"],
        "heart": ["love", "heart", "liebe", "herz"],
        "boom": ["explosion"],
        "thumbsup": ["good", "great", "nice", "gut", "super"],
        "thumbsdown": ["bad", "awful", "schlecht", "mies"],
        "v": ["peace", "friede"],
        "ok_hand": ["ok"],
        "clap": ["clapping", "clap", "klatschen"],
        "family": ["family", "humans", "people", "familie", "menschen", "leute", "personen"],
        "man": ["man", "men", "mann", "männer", "gentlemen", "herren"],
        "woman": ["woman", "ladies", "frau", "frauen", "damen"],
        "boy": ["boy", "junge"],
        "girl": ["girl", "mädchen"],
        "baby": ["baby"],
        "zzz": ["sleep", "snore", "schlafen", "schnarchen"],
        "musical_notes": ["music", "musik"],
        "fire": ["fire", "hot", "feuer", "heiß"],
        "runner": ["running", "rennen"],
        "cop": ["cop", "police", "polizei"],
        "ear": ["ear", "ohr"],
        "eyes": ["eyes", "see", "look", "augen", "sehen"],
        "nose": ["nose", "nase"],
        "tongue": ["tongue", "zunge"],
        "sunny": ["sunny", "sun", "sonne", "sonnig"],
        "umbrella": ["rain", "rainy", "regen", "regnerisch"],
        "cloud": ["cloudy", "cloud", "wolke", "bewölkt"],
        "snowflake": ["snow", "snowing", "schnee", "schneien"],
        "cat": ["cat", "katze"],
        "dog": ["dog", "hund"],
        "mouse": ["mouse", "maus"],
        "earth_africa": ["world", "earth", "welt", "erde"],
        "sound": ["sound"],
        "car": ["car", "auto"],
        "helicopter": ["helicopter", "hubschrauber"]
    }, wordHash = {}, re, enabled = false;

    var service = {
        resolve: resolve,
        enable: enable,
        disable: disable,
        isEnabled: isEnabled
    };

    activate();

    return service;

    //////////

    function activate() {
        var words = [];
        _.each(rawlinks, function (wordArr, link) {
            _.each(wordArr, function (word) {
                words.push(word);
                wordHash[word] = link;
            });
        });
        re = new RegExp("\\b(" + words.join("|") + ")\\b", "gi");
    }

    function enable() {
        enabled = true;
        $rootScope.$broadcast("emoji.statechange", true);
    }

    function disable() {
        enabled = false;
        $rootScope.$broadcast("emoji.statechange", false);
    }

    function isEnabled() {
        return enabled;
    }

    function resolve(text) {
        return text.replace(re, function(matched){
            var text = wordHash[matched.toLowerCase()];
            return "<i class='emoji emoji_" + text + "' title=':" + text + ":'></i>";
        });
    }
}

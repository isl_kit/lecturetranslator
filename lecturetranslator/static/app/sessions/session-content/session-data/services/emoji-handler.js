angular
    .module("app.sessions")
    .factory("EmojiHandler", EmojiHandlerModel);

/* @ngInject */
function EmojiHandlerModel($timeout, Data, liveStreamService) {

    function EmojiHandler() {
        this.streamId = 0;
        this.showEmoji = false;
        this.currentEmoji = ":smile:";
        this._laughStarts = {};
        this._timeoutKey = null;
    }

    EmojiHandler.prototype.start = start;
    EmojiHandler.prototype.stop = stop;
    EmojiHandler.prototype._showEmoticon = _showEmoticon;
    EmojiHandler.prototype._hideEmoticon = _hideEmoticon;

    return EmojiHandler;

    //////////

    function start(streamId) {
        if (EMOJI_ENABLED && !this.streamId) {
            this.streamId = streamId;
            liveStreamService.join(this.streamId, "s", 2, _messageHandler.bind(this));
        }
    }

    function stop() {
        if (EMOJI_ENABLED && this.streamId) {
            liveStreamService.leave(this.streamId, "s", 2);
            this.streamId = 0;
        }
    }

    function _messageHandler(message) {
        var data = new Data(message.data);
        if (data.text.indexOf("<LAUGH>") > -1 && !this._laughStarts[data.id]) {
            this._laughStarts[data.id] = true;
            this._showEmoticon();
        }
    }

    function _showEmoticon() {
        if (this._timeoutKey) {
            $timeout.cancel(this._timeoutKey);
        }
        this.showEmoji = true;
        this._timeoutKey = $timeout(_hideEmoticon.bind(this), 3000);
    }

    function _hideEmoticon() {
        this._timeoutKey = null;
        this.showEmoji = false;
    }
}

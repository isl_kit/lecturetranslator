angular
    .module("app.sessions")
    .factory("layoutService", layoutService);

/* @ngInject */
function layoutService($mdMedia, storageService) {

    var layout = {},
        streamLength = 0,
        // Switch for enabling fullscreen animations
        animateFullscreen = false,
        // Variables for saving state in non-fullscreen mode
        windowedStyle = {},
        windowedHeight;

    var service = {
        layout: {},
        update: update,
        toggleOrientation: toggleOrientation,
        toggleFullscreen: toggleFullscreen,
        setStreamLength: setStreamLength,
        setHeight: setHeight,
        getRawLayout: getRawLayout
    };

    activate();

    return service;

    //////////

    function activate() {
        layout = storageService.get("LT-Layout", true) || {
            dir: 0,
            h: 300,
            //fullscreen: false
        };
        layout.fullscreen = false;
    }

    function update() {
        compute();
        storageService.set("LT-Layout", layout, true);
    }

    function toggleOrientation() {
        layout.dir = +!layout.dir;
        update();
    }

    // Toggle fullscreen
    function toggleFullscreen() {
        layout.fullscreen = !layout.fullscreen;
        if (layout.fullscreen) {
            windowedHeight = layout.h;
            layout.h = $(window).height();
        } else {
            layout.h = windowedHeight;
        }
        update();
    }

    function setHeight(height) {
        if (height === layout.h) {
            return;
        }
        layout.h = height;
        update();
    }

    function setStreamLength(sl) {
        if (sl === streamLength) {
            return;
        }
        streamLength = sl;
        update();
    }

    function getRawLayout() {
        return layout;
    }

    function compute() {
        var ht, htLast, wt, wtLast;

        service.layout.dir = $mdMedia("xs") ? 1 : layout.dir;
        if (service.layout.dir === 0) {
            service.layout.textDir = "row";
            service.layout.flex = "none";
            ht = layout.h;
            htLast = layout.h;
            if (streamLength > 0) {
                wt = Math.round(100 / streamLength);
                wtLast = 100 - wt * (streamLength - 1);
            } else {
                wt = 100;
                wtLast = 100;
            }
        } else {
            service.layout.textDir = "column";
            service.layout.flex = "noshrink";
            if (streamLength > 0) {
                ht = Math.round(layout.h / streamLength);
                htLast = layout.h - ht * (streamLength - 1);
            } else {
                ht = layout.h;
                htLast = layout.h;
            }
            wt = 100;
            wtLast = 100;
        }
        service.layout.styles = {};

        // Apply fullscreen animation if toggled
        var textContainerStyle = {};
        var fullscreenToggle = service.layout.fullscreen !== undefined && service.layout.fullscreen !== layout.fullscreen;
        if (animateFullscreen && fullscreenToggle) {
            if (layout.fullscreen) {
                // Set top, left, width and height explicitly so it is animatable
                element = $("div.fullscreen-base");
                windowedStyle = {
                    "position": "fixed",
                    "top": (element.offset().top - $(window).scrollTop()) + "px",
                    "left": (element.offset().left - $(window).scrollLeft()) + "px",
                    "width": element.width() + "px",
                    "height": element.height() + "px"
                };
                textContainerStyle = windowedStyle;

                // Enable fullscreen after that
                setTimeout(function() {
                    service.layout.fullscreen = true;
                }, 10);
            } else {
                // Disable fullscreen
                service.layout.fullscreen = false;
                // Reset previous (non-fullscreen) style
                textContainerStyle = windowedStyle;
            }

            // Remove explicit top, left, width and height afterwards
            setTimeout(function() {
                service.layout.styles.textContainer = {};
            }, 1000);
        } else {
            service.layout.fullscreen = layout.fullscreen;
        }

        // Set styles via ngStyle
        service.layout.styles = {
            textArea: {
                "height": ht + "px",
                "max-height": ht + "px",
                "width": wt + "%",
                "max-width": wt + "%"
            },
            textAreaLast: {
                "height": htLast + "px",
                "max-height": htLast + "px",
                "width": wtLast + "%",
                "max-width": wtLast + "%"
            },
            textAreaEdit: {
                "height": (layout.h - 36) + "px",
                "max-height": (layout.h - 36) + "px"
            },
            textContainer: textContainerStyle
        };
    }
}

angular
    .module("app.sessions")
    .directive("sessionData", sessionDataDirective);

/*@ngInject*/
function sessionDataDirective() {

    var directive = {
        restrict: "E",
        scope: {
            session: "=",
            time: "=",
            text: "=",
            selectedStreamIdents: "=",
            onSelectData: "&",
            sliderStart: "=",
            sliderEnd: "=",
            timeSetter: "<",
            playCallback: "<",
            pauseCallback: "<",
            isPlayingGetter: "<"
        },
        controller: "SessionDataController",
        controllerAs: "vm",
        bindToController: true,
        templateUrl: "sessions/session-content/session-data/session-data.tpl.html"
    };
    return directive;
}

angular
    .module('app.sessions')
    .directive('streamText', streamTextDirective);

/*@ngInject*/
function streamTextDirective($sanitize) {

    var directive = {
        restrict: 'A',
        scope: {
            datas: '<',
            editable: '<',
            time: '<',
            filteredData: '<',
            click: '&',
            editDone: '&'
        },
        link: link
    };
    return directive;

    //////////

    function link(scope, element, attributes) {

        var dataHash = {},
            elements = [];
        createHtml(element, scope.datas, elements);
        createHash(scope.datas);
        element.click(clickHandler);
        scope.$watch('editable', editableChangeHandler);
        scope.$watch('datas', datasChangeHandler);
        scope.$watch('time', timeChangeHandler);
        scope.$watch('filteredData', searchChangeHandler);

        //////////

        function clickHandler(ev) {
            if (event.target.nodeName === 'SPAN') {
                scope.$apply(function () {
                    scope.click({data: dataHash[$(event.target).data('id')]});
                });
            }
        }

        function createHash(datas) {
            _.each(datas, function (data) {
                dataHash[data.id] = data;
            });
        }

        function editableChangeHandler(value, oldVal) {
            if (value === oldVal) {
                return;
            }
            if (value === false) {
                scope.editDone({text: fullText()});
                restore();
            }
        }

        function datasChangeHandler(value, oldVal) {
            if (value === oldVal) {
                return;
            }
            createHash(scope.datas);
            restore();
        }

        function timeChangeHandler(time, oldVal) {
            if (time === oldVal) {
                return;
            }
            _.each(elements, function (el) {
                if (el.data.mediaStart <= time && el.data.mediaStop > time && time > 0) {
                    el.obj.addClass('playing');
                } else {
                    el.obj.removeClass('playing');
                }
            });
        }

        function searchChangeHandler(value, oldVal) {
            if (value === oldVal) {
                return;
            }
            _.each(elements, function (el) {
                if (el.data.selected) {
                    el.obj.addClass('selected');
                } else {
                    el.obj.removeClass('selected');
                }
            });
        }

        function restore() {
            elements = [];
            element.empty();
            createHtml(element, scope.datas, elements);
        }

        function fullText() {
            return element.find('div').map(function () {
                return $(this).text().trim();
            }).get().join('\n\n');
        }
    }

    function createHtml(rootEl, datas, elements) {
        var div = $('<div></div>');
        _.each(datas, function (data) {
            var parts = data.text.split('<hr>');
            _.each(parts, function (part, i) {
                if (i > 0) {
                    rootEl.append(div);
                    div = $('<div></div>');
                }
                if (part.length === 0) {
                    return;
                }
                var content = $sanitize(part);
                var spanText = '<span data-id="' + data.id + '"';
                if (data.class) {
                    spanText += ' data-cl="' + data.class + '"';
                }
                if (i === 0) {
                    spanText += ' id="' + data.id + '"';
                }
                spanText += '></span>';
                var span = $(spanText);
                span.html(content);
                elements.push({
                    obj: span,
                    data: data
                });
                div.append(span);
            });
        });
        if (div.children.length > 0) {
            rootEl.append(div);
        }
    }
}

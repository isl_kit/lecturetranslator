
import { html, render } from 'lit-html'
import * as Y from 'yjs'
import { EditorView } from 'prosemirror-view' // eslint-disable-line
import { ySyncPluginKey } from 'y-prosemirror'
import * as dom from 'lib0/dom.js'
import * as pair from 'lib0/pair.js'

function computeYChange(permanentUserData) {
  return (type, id) => {
    const user = type === 'added' ? permanentUserData.getUserByClientId(id.client) : permanentUserData.getUserByDeletedId(id);
    return {
      user,
      id,
      type
    };
  };
}

/**
 * A Quill-style document delta operation.
 * @typedef {Object} Operation
 * @property {string} insert - The text this operation is based on. In the case of a delete operation, this is the text that was deleted.
 * @property {(ChangeDescription|null)} change - A description of the change relative to the previous document state, if present.
 */

/**
 * A description of the change an {@link Operation} represents.
 * @typedef {Object} ChangeDescription
 * @property {string} changeType - The type of change. This is either "added" or "removed".
 * @property {string} user - The user who made the change.
 */

/**
 * Get the operations representing the current document state and the changes relative to the previous snapshot.
 * @param {Y.Doc} yDoc
 * @param {EditorView} editorview
 * @param {Version} prevSnapshot
 * @param {Version} curSnapshot
 *
 * @return {Operation[]}
 */
function getDocOperations(yDoc, editorview, prevSnapshot, curSnapshot) {
  let type = yDoc.get('prosemirror', Y.XmlFragment);
  const snapshotDeltaTypes = Y.typeListToArraySnapshot(type, new Y.Snapshot(prevSnapshot.ds, curSnapshot.sv));
  const pud = ySyncPluginKey.getState(editorview.state).permanentUserData;
  let operations = [];

  snapshotDeltaTypes.forEach(function (node, _, __) {
    if (node.nodeName == "segment") {
      const children = Y.typeListToArraySnapshot(node, new Y.Snapshot(prevSnapshot.ds, curSnapshot.sv));

      for (const child of children) {
        // The children of a segment are all text nodes for which we compute the deltas.`
        const deltas = child.toDelta(curSnapshot, prevSnapshot, computeYChange(pud));
        for (const delta of deltas) {
          let op = {
            insert: delta.insert
          };
          const attributes = delta.attributes;
          if (attributes && attributes.ychange) {
            op.change = {
              changeType: attributes.ychange.type,
              user: attributes.ychange.user
            };
          }
          operations.push(op);
        }
      }
    }
    else if (node.nodeName == 'horizontal_rule') {
      operations.push({
        insert: "<hr>",
      });
    }
  });
  return operations;
}


/**
 * This callback is passed the operations representing the document and its changes relative to the previous version.
 *
 * @callback operationsCallback
 * @param {Operation[]} operations
 */

/**
 * @param {Y.Doc} yDoc
 * @param {EditorView} editorview
 * @param {operationsCallback} correctionsFunc
 */
export const addVersion = (yDoc, editorview, correctionsFunc) => {
  const versions = yDoc.getArray('versions')
  const prevVersion = versions.length === 0 ? null : versions.get(versions.length - 1)
  const prevSnapshot = prevVersion === null ? Y.emptySnapshot : Y.decodeSnapshot(prevVersion.snapshot)
  const snapshot = Y.snapshot(yDoc)
  if (prevVersion != null) {
    // account for the action of adding a version to ydoc
    prevSnapshot.sv.set(prevVersion.clientID, /** @type {number} */(prevSnapshot.sv.get(prevVersion.clientID)) + 1)
  }
  if (!Y.equalSnapshots(prevSnapshot, snapshot)) {
    const ops = getDocOperations(yDoc, editorview, prevSnapshot, snapshot);
    correctionsFunc(ops);

    versions.push([{
      date: new Date().getTime(),
      snapshot: Y.encodeSnapshot(snapshot),
      clientID: yDoc.clientID
    }]);
  }
}

const liveTracking = /** @type {HTMLInputElement} */ (dom.element('input', [
  pair.create('type', 'checkbox'),
  pair.create('name', 'yjs-live-tracking'),
  pair.create('value', 'Live Tracking ')
]))

const updateLiveTrackingState = editorstate => {
  setTimeout(() => {
    const syncState = ySyncPluginKey.getState(editorstate.state)
    liveTracking.checked = syncState.prevSnapshot != null && syncState.snapshot == null
  }, 500)
}

const renderVersion = (editorview, version, prevSnapshot) => {
  editorview.dispatch(editorview.state.tr.setMeta(ySyncPluginKey, { snapshot: Y.decodeSnapshot(version.snapshot), prevSnapshot: prevSnapshot == null ? Y.emptySnapshot : Y.decodeSnapshot(prevSnapshot) }))
  updateLiveTrackingState(editorview)
}

const unrenderVersion = editorview => {
  const binding = ySyncPluginKey.getState(editorview.state).binding
  if (binding != null) {
    binding.unrenderSnapshot()
  }
  updateLiveTrackingState(editorview)
}

function clickVersion(editorview, version, prevSnapshot, pauseCallback) {
  renderVersion(editorview, version, prevSnapshot, pauseCallback);
  pauseCallback();
}

/**
 * @param {EditorView} editorview
 * @param {Version} version
 * @param {Version|null} prevSnapshot
 */
const versionTemplate = (editorview, version, prevSnapshot, pauseCallback) => html`<div class="version-list" @click=${e => clickVersion(editorview, version, prevSnapshot, pauseCallback)}>${new Date(version.date).toLocaleString()}</div>`

const versionList = (editorview, doc, pauseCallback) => {
  const versions = doc.getArray('versions')
  return html`<div>${versions.length > 0 ? versions.map((version, i) => versionTemplate(editorview, version, i > 0 ? versions.get(i - 1).snapshot : null, pauseCallback)) : html`<div>No versions...</div>`}</div>`
}

function clickSnapshotButton(doc, editorview, correctionsFunc, pauseCallback, closeCallback) {
  pauseCallback();
  closeCallback();
  addVersion(doc, editorview, correctionsFunc);
}
const snapshotButton = (doc, editorview, correctionsFunc, pauseCallback, closeCallback) => {
  return html`<button @click=${(e) => clickSnapshotButton(doc, editorview, correctionsFunc, pauseCallback, closeCallback)}>Save</button>`
}

function closeOnClickOutside(element, anchor, closeCallback) {
  const outsideClickListener = event => {
    let notClicked = (el) => !el.contains(event.target) && isVisible(el);
    if (notClicked(element) && notClicked(anchor)) { // or use: event.target.closest(selector) === null
      closeCallback();
      removeClickListener();
    }
  };

  const removeClickListener = () => {
    document.removeEventListener('click', outsideClickListener);
  };

  document.addEventListener('click', outsideClickListener);
}

const isVisible = elem => !!elem && !!(elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length);

/**
 * @param {HTMLElement} parent
 * @param {Y.Doc} doc
 * @param {EditorView} editorview
 */
export const attachVersion = (parent, doc, editorview, correctionsFunc, pauseCallback, setHistory, historyAnchor, historyAnchorParent, uniqueString) => {
  let open = false;

  let closeCallback = () => { if (setHistory) { setHistory(false); } };

  const rerender = () => {
    let id = "modal-" + uniqueString;
    render(html`<div class="version-modal" id=${id} ?hidden=${!open}>${snapshotButton(doc, editorview, correctionsFunc, pauseCallback, closeCallback)}${versionList(editorview, doc, pauseCallback)}</div>`, historyAnchor)
    if (open) {
      closeOnClickOutside(document.getElementById(id), historyAnchorParent, closeCallback);
    }
  }
  updateLiveTrackingState(editorview)
  liveTracking.addEventListener('click', e => {
    if (liveTracking.checked) {
      const versions = doc.getArray('versions')
      const lastVersion = versions.length > 0 ? Y.decodeSnapshot(versions.get(versions.length - 1).snapshot) : Y.emptySnapshot
      editorview.dispatch(editorview.state.tr.setMeta(ySyncPluginKey, { snapshot: null, prevSnapshot: lastVersion }))
    } else {
      unrenderVersion(editorview)
    }
  })
  /*
  parent.insertBefore(liveTracking, null)
  parent.insertBefore(dom.element('label', [
    pair.create('for', 'yjs-live-tracking')
  ], [
    dom.text('Live Tracking ')
  ]), null)
  const btn = document.createElement('button')
  btn.setAttribute('type', 'button')
  btn.textContent = 'Versions'
  btn.addEventListener('click', () => {
    open = !open
    unrenderVersion(editorview)
    rerender()
  })
  const vContainer = document.createElement('div')
  parent.insertBefore(btn, null)
  parent.insertBefore(vContainer, null)
  */
  doc.getArray('versions').observe(rerender)
  rerender()

  const setOpenFun = (newOpenVal) => {
    open = newOpenVal;
    unrenderVersion(editorview);
    rerender();
  };
  return setOpenFun;
}

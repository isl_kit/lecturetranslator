## What is this subdirectory for? ##
This subdirectory contains the collaborative editor which allows users to edit the lecture transcript to correct mistakes etc.

## How to build the javascript?  ##
First, you need to run 'npm install' to install all dependencies.
The javascript used here is ES6 and uses imports, so we need webpack and babel to convert it to 'old' javascript.
This is now a part of the gulp pipeline, so running "gulp build" in the root directory will take care of everything.


## How to start the websocket? ##
PORT=1234 YPERSISTENCE=./db node server/server.js

A different port can be used, but make sure to also make that change in the editor javascript (and rebuild afterwards).

## How does this work? ##
The editor is based on Prosemirror. Yjs is a shared editing framework that essentially implements the 'Google Docs' style collaborative editing for us. To do that, it needs to communicate with other client via a websocket. 
Behind the scenes, a local LevelDB database stores the state of the document to enable persistent storage.


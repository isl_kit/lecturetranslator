import { Plugin } from 'prosemirror-state'
import { ySyncPluginKey } from 'y-prosemirror'

/**
 * Prosemirror editor plugin that lets you highlight a particular node in the document (specified by its position).
 */
export let segmentPlayPlugin = (getTime, setTime, play, pause, isPlaying) => new Plugin({
    state: {
        init(_, { doc }) { return null; },
        apply(tr, old) {
            if(tr.docChanged && !tr.getMeta(ySyncPluginKey)) {
                // The document was changed by the user (and not by other users via yjs)
                // => pause the video
                pause();
            }
        }
    },
    props: {
        handleClick(view, pos, event) {
            let node = view.state.doc.resolve(pos).parent;
            if(node.type.name != "segment") {
                return false;
            }
            let curTime = getTime() * 1000;
            let nodeStartTime = node.attrs["startTime"];
            let nodeStopTime = node.attrs["stopTime"];
            if(nodeStartTime <= curTime && curTime < nodeStopTime) {
                // Node is currently active
                // But is the audio playing?
                if(isPlaying()) {
                    pause();
                }
                else {
                    setTime(nodeStartTime / 1000);
                    play();
                }
            }
            else {
                setTime(nodeStartTime / 1000);
                if(nodeStartTime == this.lastPlayed) {
                    pause();
                }
                else {
                    play();
                }
            }
            this.lastPlayed = nodeStartTime;
            return true;
        }
    },
    lastPlayed: 0
})
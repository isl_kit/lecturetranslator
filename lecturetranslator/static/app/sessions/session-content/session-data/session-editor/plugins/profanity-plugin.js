import { Plugin, TextSelection } from "prosemirror-state"
import { Decoration, DecorationSet } from "prosemirror-view"

// Helper for iterating through the nodes in a document that changed
// compared to the given previous document. Useful for avoiding
// duplicate work on each transaction.
function changedDescendants(old, cur, offset, f) {
    let oldSize = old.childCount, curSize = cur.childCount
    outer: for (let i = 0, j = 0; i < curSize; i++) {
        let child = cur.child(i)
        for (let scan = j, e = Math.min(oldSize, i + 3); scan < e; scan++) {
            if (old.child(scan) == child) {
                j = scan + 1
                offset += child.nodeSize
                continue outer
            }
        }
        f(child, offset)
        if (j < oldSize && old.child(j).sameMarkup(child))
            changedDescendants(old.child(j), child, offset + 1, f)
        else
            child.nodesBetween(0, child.content.size, f, offset + 1)
        offset += child.nodeSize
    }
}

/**
 * Lint the document to find and record all profanities
 * @param {Node} doc The ProseMirror document node
 * @param {RegExp} profanityRegex The regex that matches all profanities
 */
function lint(doc, candidates, profanityRegex) {
    let result = []

    function record(msg, from, to, fix) {
        result.push({ msg, from, to, fix })
    }


    // For each node in the document
    for (let i = 0; i < candidates.length; i++) {
        let node = candidates[i].child;
        let pos = candidates[i].offset;
        if (node.isText) {
            // Scan text nodes for profanities
            let m;
            while (m = profanityRegex.exec(node.text))
                record(`Profanity detected: '${m[0]}'`,
                    pos + m.index, pos + m.index + m[0].length, fixProfanity(m[0]));
        }
    }
    return result
}

/**
 * Provide a fix for a profanity (by censoring it)
 * @param {string} profanity The profanity string
 */
function fixProfanity(profanity) {
    let replacement = profanity.replace(/./g, '*');  // Replace all letters with asterisks. hello => *****
    return function ({ state, dispatch }) {
        dispatch(state.tr.replaceWith(this.from, this.to,
            state.schema.text(replacement)))
    }
}

/**
 * Add decorations for all profanities in the document
 * @param {Node} doc The ProseMirror document node
 * @param {RegExp} profanityRegex The regex that matches all profanities
 */
function lintDeco(doc, candidates, profanityRegex) {
    let decos = []
    lint(doc, candidates, profanityRegex).forEach(prob => {
        decos.push(Decoration.inline(prob.from, prob.to, { class: "problem", title: prob.msg }),
            Decoration.widget(prob.from, lintIcon(prob)))
    })
    return decos
}

/**
 * Create an HTML element representing an icon for the specified problem
 * @param {Object} prob The problem for which an icon will be created
 */
function lintIcon(prob) {
    let icon = document.createElement("div")
    icon.className = "lint-icon"
    icon.title = prob.msg
    icon.problem = prob
    return icon
}

/**
 * Build a regex that globally and case insensitively matches any of the input words.
 * @param {Array<string>} words The string array of words to build the regex from
 */
function buildRegexFromWords(words) {
    let regexString = "\\b(";
    if (words.length > 0) {
        regexString += words[0];
    }
    for (let i = 1; i < words.length; i++) {
        regexString += "|" + words[i];
    }
    regexString += ")\\b";
    return new RegExp(regexString, "ig");
}

/**
 * A plugin that detects and highlights all profanities in the document and lets the user censor them automatically.
 * @param {string} languageCode Specifies the language of the document (used to get the right profanities)
 */
export let profanityPlugin = (languageCode) => {
    // Import profanity list for the specified language and build a regex from it
    let profanities = require("naughty-words/" + languageCode + ".json");
    let profanityRegex = buildRegexFromWords(profanities);

    return new Plugin({
        state: {
            init(_, { doc }) { return null },
            apply(tr, old) {
                if (!tr.docChanged) {
                    return old;
                }
                let previousSet = (old == null) ? null : old.map(tr.mapping, tr.doc);
                let candidates = [];
                changedDescendants(tr.before, tr.doc, 0, (child, offset) => candidates.push({ child: child, offset: offset }));
                if (previousSet != null) {
                    for (let i = 0; i < candidates.length; i++) {
                        let child = candidates[i].child;
                        let offset = candidates[i].offset;
                        let intersectingDecos = previousSet.find(offset, offset + child.nodeSize);
                        previousSet = previousSet.remove(intersectingDecos);
                    }
                }
                let newDecos = lintDeco(tr.doc, candidates, profanityRegex);
                let decoSet = previousSet;
                if(decoSet == null) {
                    decoSet = DecorationSet.create(tr.doc, newDecos)
                }
                else {
                    decoSet = decoSet.add(tr.doc, newDecos);
                }
                return decoSet
            }
        },
        props: {
            decorations(state) { return this.getState(state) },
            /**
             * Fix the problem if the user clicks the icon (in this case by censoring the profanity)
             */
            handleClick(view, _, event) {
                if (/lint-icon/.test(event.target.className)) {
                    let prob = event.target.problem
                    if (prob.fix) {
                        prob.fix(view)
                        view.focus()
                        return true
                    }
                }
            }
        }
    });
}
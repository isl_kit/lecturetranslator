import { Plugin } from 'prosemirror-state'
import { Decoration, DecorationSet } from 'prosemirror-view'

/**
 * Find the positions (from and to) of the node that is currently playing (according to the time parameter)
 * @param {Node} doc 
 * @param {number} time 
 */
function getPlayingNodePositions(doc, time) {
    // Default value (0, 0): If we didn't find a segment to play, reset a potential previous highlight 
    // by highlighting from 0 to 0 (which highlights nothing)
    let positions = { from: 0, to: 0 };

    // Iterate over all nodes in the document to find the segment that should be playing right now
    doc.descendants(function (node, pos, parent, index) {
        const startTime = node.attrs["startTime"];
        const stopTime = node.attrs["stopTime"];
        if (startTime !== undefined && stopTime !== undefined && startTime <= time && time < stopTime) {
            positions = { from: pos, to: pos + node.nodeSize };
            return;
        }
    });
    return positions;
}

/**
 * Find the node that is currently playing (according to the time parameter) and create a decoration for it
 * @param {Node} doc The document node
 * @param {number} time The current time
 */
function calculateDecorations(doc, time) {
    let pos = getPlayingNodePositions(doc, time);

    return DecorationSet.create(doc, [
        Decoration.node(pos.from, pos.to, { class: "playing" })  // Add 'playing' class to currently playing node
    ]);
}

/**
 * Prosemirror editor plugin that lets you highlight a particular node in the document (specified by its position).
 */
export let highlightPlugin = new Plugin({
    // Our state is an object {decoSet: ..., time: ...} that saves
    // the current decorations as well as the current time.
    state: {
        init() {
            // No decorations set by default
        },
        apply(tr, value) {
            if (tr.getMeta("highlightPlugin")) {
                // This branch is reached whenever the editor passes us a new 'time' value
                const { time } = tr.getMeta("highlightPlugin");
                // Calculate decoration based on new time value
                return {
                    decoSet: calculateDecorations(tr.doc, time),
                    time: time
                };
            }
            else {
                if (value == undefined) {
                    return null;
                }
                let gotRemoved = false;

                // Map "other" changes so our decoration "stays put" 
                // (e.g. user is typing so decoration's pos must change)
                let newDecoSet = value.decoSet.map(tr.mapping, tr.doc, {
                    onRemove: function (_) {
                        gotRemoved = true;
                    }
                });
                if (gotRemoved) {
                    // Old decoration was removed => calculate new decoration
                    let time = value.time;  // Get the time from the old state
                    return {
                        decoSet: calculateDecorations(tr.doc, time),
                        time: time
                    };
                }
                // Old decoration wasn't removed => Return remapped old decoration
                return { decoSet: newDecoSet, time: value.time };
            }
        }
    },
    props: {
        decorations(state) {
            return highlightPlugin.getState(state) ? highlightPlugin.getState(state).decoSet : null;
        }
    }
})

const offsetHeightFactor = 0.45;  // Offset scrollTop by roughly half of the visible part of the editor (45% feels better)
const scrollThresholdFactor = 0.85;  // Start scrolling if the highlight is beyond 85% of the viewport

/**
* Scroll smoothly to the highlighted node (if necessary) and return the new scrollTop coordinate
* @param {EditorView} view The view of the ProseMirror editor
* @param {number} lastScrollTop The current scrollTop coordinate (how far is the editor scrolled vertically?)
*/
export function scrollToHighlight(view, lastScrollTop) {
    const pluginState = highlightPlugin.getState(view.state);
    if (pluginState != null) {
        const decorations = pluginState.decoSet.find(0, view.state.doc.nodeSize);
        if (decorations.length > 0) {
            const highlightPos = decorations[0].from;  // Document position of the currently highlighted node
            const domNode = view.nodeDOM(highlightPos);  // DOM Node at that position

            // 'Global' position (within the editor, i.e. not just the visible part) of the node
            const domNodePos = domNode.offsetTop;
            // 'Local' position (with respect to the visible part of the editor) of the node
            const domNodeClientPos = domNode.getBoundingClientRect().bottom - view.dom.getBoundingClientRect().top;

            // Make sure we're not already scrolling
            const isScrolling = (lastScrollTop != view.dom.scrollTop);

            // Vertical threshold beyond which a scrolling takes place
            const scrollThreshold = view.dom.clientHeight * scrollThresholdFactor;
            // If the highlighted node is not within the visible part of the editor 
            // (minus some threshold at the bottom), scroll to center it
            if ((!isScrolling) && (domNodeClientPos > scrollThreshold || domNodeClientPos < 0)) {
                const offset = view.dom.clientHeight * offsetHeightFactor;
                const scrollPos = Math.max(domNodePos - offset, 0);
                view.dom.scrollTo({
                    top: scrollPos,
                    left: 0,
                    behavior: 'smooth',
                });
            }
            lastScrollTop = view.dom.scrollTop;  // Update scrollTop
        }
    }
    return lastScrollTop;
}

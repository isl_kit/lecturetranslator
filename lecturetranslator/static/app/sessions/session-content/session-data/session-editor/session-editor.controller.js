import * as Y from 'yjs'
import { WebsocketProvider } from 'y-websocket'
import { ySyncPlugin, yCursorPlugin, yUndoPlugin, undo, redo } from 'y-prosemirror'
import { highlightPlugin, scrollToHighlight } from './plugins/highlight-plugin.js'
import { segmentPlayPlugin } from './plugins/segment-play-plugin.js'
import { profanityPlugin } from './plugins/profanity-plugin.js'
import { EditorState } from 'prosemirror-state'
import { EditorView } from 'prosemirror-view'
import { schema } from './schema.js'
import { exampleSetup } from 'prosemirror-example-setup'
import { keymap } from 'prosemirror-keymap'
import { addVersion, attachVersion } from './version.js'

var stringToColor = function (str) {
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    var colour = '#';
    for (var i = 0; i < 3; i++) {
        var value = (hash >> (i * 8)) & 0xFF;
        colour += ('00' + value.toString(16)).substr(-2);
    }
    return colour;
}

angular
    .module("app.sessions")
    .controller("SessionEditorController", ['$scope', 'auth', 'correctionsService', function ($scope, auth, correctionsService) {
        var vm = this;

        vm.auth = auth;

        vm.timeSetter = 0;

        vm.connected = false;

        vm.$onInit = function () { angular.element(document).ready(activate); };

        //////////

        function correctionsFunc(operations) {
            const stream = JSON.parse(vm.stream);
            // Get the current commit
            correctionsService.data(vm.session.id, stream.fingerprint).then(function (commit) {
                // Update transcript based on current commit
                correctionsService.update(vm.session.id, stream.fingerprint, { commit_id: commit.id, operations: operations });
            });
        }

        function activate() {
            if(!vm.auth.isTranscriptCorrector() && !vm.auth.isStaff()) {
                // Don't activate the editor for non-correctors and non-staff users
                return;
            }

            const id = $scope.$id;  // Unique identifier to separate between two editors that are active simultaneously

            const userName = vm.auth.user.name;
            const userColor = stringToColor(userName);
            const user = { username: userName, color: userColor, lightColor: userColor + "33" };
            const ydoc = new Y.Doc()
            const permanentUserData = new Y.PermanentUserData(ydoc);
            permanentUserData.setUserMapping(ydoc, ydoc.clientID, user.username);
            const colorMapping = new Map();
            colorMapping.set(user.username, {light: user.lightColor, dark: user.color});
            ydoc.gc = false
            const stream = JSON.parse(vm.stream);

            const room_id = vm.session.id + "-" + stream.ident + "-" + stream.fingerprint;
            const ws_url = EDITOR_WEBSOCKET_URL;  // Provided by .env

            const provider = new WebsocketProvider(ws_url, room_id, ydoc)

            provider.on('status', event => {
                vm.connected = event.status == "connected";
                $scope.$apply();
            })

            // Tell the provider our user information
            const state = { "user": { name: userName, color: userColor } }
            provider.awareness.setLocalState(state)

            const type = ydoc.get('prosemirror', Y.XmlFragment)

            let getTime = () => vm.time;
            let setTime = (time) => {
                vm.timeSetter(time);
            };
            angular.element(document).ready(function () {
                const editor = document.getElementById('editor' + "_" + id)
                const prosemirrorView = new EditorView(editor, {
                    state: EditorState.create({
                        schema,
                        plugins: [
                            ySyncPlugin(type, { permanentUserData, colorMapping}),
                            yCursorPlugin(provider.awareness),
                            yUndoPlugin(),
                            keymap({
                                'Mod-z': undo,
                                'Mod-y': redo,
                                'Mod-Shift-z': redo
                            }),
                            highlightPlugin,
                            segmentPlayPlugin(getTime, setTime, () => { if (vm.playCallback) { vm.playCallback(); } }, () => { if (vm.pauseCallback) { vm.pauseCallback(); } }, () => vm.isPlayingGetter()),
                            profanityPlugin(stream.fingerprint)
                        ].concat(exampleSetup({ schema }))
                    })
                })

                let setHistoryFunction = (newVal) => {
                    $scope.$apply(function () {
                        vm.history = newVal;
                    });
                };

                let setVersionDropdownOpen = attachVersion(document.getElementById('y-version' + "_" + id), ydoc, prosemirrorView, correctionsFunc, () => { if (vm.pauseCallback) { vm.pauseCallback(); } }, setHistoryFunction, document.getElementById("history-container-" + stream.fingerprint), document.getElementById("history-container-parent-" + stream.fingerprint), stream.fingerprint);

                // Fires whenever the history is toggled (through the external history button)
                $scope.$watch("history", function (newVal, oldVal, scope) {
                    setVersionDropdownOpen(newVal);
                });

                $scope.$watch("triggerSave", function(newVal, oldVal, scope) {
                    if(newVal == true) {
                        addVersion(ydoc, prosemirrorView, correctionsFunc);
                    }
                    vm.triggerSave = false;
                });

                let lastScrollTop = 0;
                // Fires whenever time changes (e.g. when the video is playing)
                $scope.$watch("time", function (newTime, oldTime, scope) {
                    if (newTime == oldTime || newTime < 0) { return; }  // Don't do anything if time hasn't changed or is invalid
                    const time = newTime * 1000;  // seconds => milliseconds
                    // Pass the highlight plugin the new time
                    let transaction = prosemirrorView.state.tr.setMeta("highlightPlugin", { time: time });
                    // Make sure the highlight is always visible by scrolling to it (if necessary)
                    lastScrollTop = scrollToHighlight(prosemirrorView, lastScrollTop);
                    prosemirrorView.dispatch(transaction);
                });

            });
        }

    }]);

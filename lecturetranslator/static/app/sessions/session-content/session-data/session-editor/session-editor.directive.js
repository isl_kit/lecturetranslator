angular
    .module("app.sessions")
    .directive("sessionEditor", sessionEditorDirective);

/*@ngInject*/
function sessionEditorDirective() {
    var directive = {
        restrict: "E",
        scope: {
            session: "@",
            stream: "@",
            commit: "@",
            history: "=",
            triggerSave: "=",
            time: "=",
            timeSetter: "<",
            playCallback: "<",
            pauseCallback: "<",
            isPlayingGetter: "<"
        },
        controller: "SessionEditorController",
        controllerAs: "vm",
        bindToController: {
            session: "=",
            stream: "@",
            commit: "@",
            history: "=",
            triggerSave: "=",
            time: "=",
            timeSetter: "<",
            playCallback: "<",
            pauseCallback: "<",
            isPlayingGetter: "<"
        },
        template: "<div class=\"connecting-area\"><div class=\"connecting-area-content\"><md-progress-circular ng-show=\"!vm.connected\" \"md-mode=\"indeterminate\" class=\"md-hue-2\" md-diameter=\"50px\"></md-progress-circular><p>Connecting...</p><p>If your internet connection is fine, there might be an issue on our end.</p></div></div><div ng-show=\"vm.connected\" class=\"y-functions\" id=\"y-functions_{{$id}}\"><div class=\"y-version\" id=\"y-version_{{$id}}\"></div><div class=\"editor\" id=\"editor_{{$id}}\"></div>"
    };
    return directive;
}

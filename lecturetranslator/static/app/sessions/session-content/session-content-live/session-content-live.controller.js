angular
    .module("app.sessions")
    .controller("SessionContentLiveController", SessionContentLiveController);

/*@ngInject*/
function SessionContentLiveController($scope, $mdMedia, auth, layoutService, loggingService) {
    var vm = this;

    vm.auth = auth;

    vm.$onInit = activate;
    vm.$onDestroy = deactivate;

    //////////

    function activate() {
        $scope.$watch(function() { return $mdMedia("xs"); }, function(xs, oldVal) {
            if (xs === oldVal) {
                return;
            }
            layoutService.update();
        });
        loggingService.log('join', vm.session.id);
    }

    function deactivate() {
        loggingService.log('leave', vm.session.id);
    }
}

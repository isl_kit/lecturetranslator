angular
    .module("app.sessions")
    .component("sessionContentLive", {
        bindings: {
            session: '<',
            selectedStreamIdents: '<'
        },
        controller: "SessionContentLiveController",
        controllerAs: "vm",
        templateUrl: "sessions/session-content/session-content-live/session-content-live.tpl.html"
});

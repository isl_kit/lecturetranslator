angular
    .module("app.sessions")
    .controller("SessionContentController", SessionContentController);

/*@ngInject*/
function SessionContentController($scope, $state, $mdMedia, auth, sessionMediaService, layoutService, loggingService) {
    var vm = this;

    vm.auth = auth;

    vm.searchText = "";
    vm.selectDataHandler = selectDataHandler;
    vm.mediaInitialized = false;

    vm.setTime = 0;
    vm.timeSetterCallback = undefined;
    vm.setTimeSetterCallback = function(timeSetterCallback) {
        vm.timeSetterCallback = timeSetterCallback;
        $scope.$apply();
    };
    vm.playCallback = undefined;
    vm.setPlayCallback = function(playCallback) {
        vm.playCallback = playCallback;
        $scope.$apply();
    };
    vm.pauseCallback = undefined;
    vm.setPauseCallback = function(pauseCallback) {
        vm.pauseCallback = pauseCallback;
        $scope.$apply();
    };
    vm.isPlayingGetter = undefined;
    vm.setPlayingGetter = function(isPlayingGetter) {
        vm.isPlayingGetter = isPlayingGetter;
        $scope.$apply();
    };
    vm.time = -1;
    vm.$onInit = activate;
    vm.$onDestroy = deactivate;

    //////////

    function activate() {
        vm.time = vm.session.active ? -1 : 0;
        $scope.$watch(function() { return $mdMedia("xs"); }, function(xs, oldVal) {
            if (xs === oldVal) {
                return;
            }
            layoutService.update();
        });
        loggingService.log('join', vm.session.id);
        getMedia();
    }

    function deactivate() {
        loggingService.log('leave', vm.session.id);
    }

    function getMedia() {
        var c = 0;
        sessionMediaService.media(vm.session.id).then(function (mediaCollection) {
            vm.session.media.mediaCollection = mediaCollection;
            setMediaInit();
        }, setMediaInit);
        sessionMediaService.vtt(vm.session.id).then(function (VTTs) {
            vm.session.media.VTTs = VTTs;
            setMediaInit();
        }, setMediaInit);
        sessionMediaService.slides(vm.session.id).then(function (slides) {
            vm.session.media.slides = slides;
            setMediaInit();
        }, setMediaInit);

        function setMediaInit() {
            c = c + 1;
            if (c === 3) {
                vm.mediaInitialized = true;
            }
        }
    }

    function selectDataHandler(data) {
        vm.setTime = data.mediaStart;
    }
}

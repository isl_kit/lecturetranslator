angular
    .module("app.sessions")
    .component("sessionContent", {
        bindings: {
            session: '<',
            selectedStreamIdents: '<'
        },
        controller: "SessionContentController",
        controllerAs: "vm",
        templateUrl: "sessions/session-content/session-content/session-content.tpl.html"
});

angular
    .module("app.sessions")
    .directive("resizeHandle", resizeHandleDirective);

/*@ngInject*/
function resizeHandleDirective(layoutService) {

    var directive = {
        restrict: "E",
        scope: {
        },
        link: link,
        replace: true,
        templateUrl: "sessions/session-content/resize-handle/resize-handle.tpl.html"
    };
    return directive;

    //////////

    function link(scope, element, attributes) {

        var origin = 0,
            origHeight = 0,
            parent = $("#" + attributes.parentId),
            lastPos = null;

        scope.pressed = false;

        activate();

        //////////

        function activate() {
            element.mousedown(down);
            $(document).mousemove(move);
            $(document).mouseup(up);
            element[0].addEventListener("touchstart", downTouch, false);
            document.addEventListener("touchmove", moveTouch, false);
            document.addEventListener("touchend", upTouch, false);
            element.on("$destroy", deactivate);
        }

        function deactivate() {
            element.off("mousedown", down);
            $(document).off("mousemove", move);
            $(document).off("mouseup", up);
            element[0].removeEventListener("touchstart", downTouch);
            document.removeEventListener("touchmove", moveTouch);
            document.removeEventListener("touchend", upTouch);
        }

        function downTouch(ev) {
            var pos = ev.changedTouches[0].pageY;
            return startResize(pos);
        }

        function moveTouch(ev) {
            if (!scope.pressed) {
                return;
            }
            var pos = ev.changedTouches[0].pageY;
            ev.preventDefault();
            return resize(pos);
        }

        function upTouch(ev) {
            return endResize();
        }

        function down(ev) {
            var pos = ev.pageY;
            return startResize(pos);
        }

        function up(ev) {
            return endResize();
        }

        function move(ev) {
            if (!scope.pressed) {
                return;
            }
            var pos = ev.pageY;
            return resize(pos);
        }

        function startResize(pos) {
            origHeight = parent.height();
            setPressed(true);
            origin = pos;
            return false;
        }

        function resize(pos) {
            if (pos !== lastPos || lastPos === null) {
                scope.$apply(function () {
                    var h = Math.max(origHeight + (pos - origin), 300);
                    parent.height(h);
                    layoutService.setHeight(h);
                });
                lastPos = pos;
            }
        }

        function endResize() {
            origHeight = 0;
            setPressed(false);
            lastPos = null;
        }

        function setPressed(value) {
            scope.$apply(function () {
                scope.pressed = value;
            });
        }
    }
}

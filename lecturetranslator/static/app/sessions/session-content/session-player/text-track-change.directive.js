angular
    .module("app.sessions")
    .directive("textTrackChange", textTrackChangeDirective);

/*@ngInject*/
function textTrackChangeDirective($window, $parse, $timeout) {

    var directive = {
        restrict: "A",
        link: link
    };
    return directive;

    //////////

    function link(scope, element, attributes) {
        var callback = $parse(attributes.textTrackChange),
            currentId;

        $timeout(init);

        //////////

        function init() {
//          See git blame and commit message of explanation of the following commented out code
//          scope.$watch(update);
//          $window.addEventListener("resize", update, false);
            videojs(element[0].id).on("texttrackchange", textTrackChangeHandler);
            element.on("$destroy", function () {
                videojs(element[0].id).off("texttrackchange", textTrackChangeHandler);
//                $window.removeEventListener("resize", update);
            });
        }

        function textTrackChangeHandler(ev) {
            var textTracks = videojs(element[0].id).textTracks();
            var selectedTrack = _.find(textTracks, function (track) {
                return track.mode === "showing";
            });
            var id = selectedTrack ? selectedTrack.id : null;
            if (id !== currentId) {
                currentId = id;
                $timeout(function () {
                    callback(scope, {track: selectedTrack || null});
                });
            }
        }
/*
        function update() {
            //videojs(element[0].id).handleTechTextTrackChange_();
        }
*/
    }
}

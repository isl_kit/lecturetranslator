angular
    .module("app.sessions")
    .directive("sessionPlayer", sessionPlayerDirective);

/*@ngInject*/
function sessionPlayerDirective() {

    var directive = {
        restrict: "E",
        scope: {},
        controller: "SessionPlayerController",
        controllerAs: "vm",
        bindToController: {
            session: "=",
            text: "=",
            timeSetter: "=",
            timeGetter: "=",
            sliderStart: "=",
            sliderEnd: "=",
            setPlayCallback: "<",
            setPauseCallback: "<",
            setPlayingGetter: "<",
            setTimeSetterCallback: "<"
        },
        templateUrl: "sessions/session-content/session-player/session-player.tpl.html"
    };
    return directive;
}

angular
    .module("app.sessions")
    .controller("SessionPlayerController", SessionPlayerController);

/*@ngInject*/
function SessionPlayerController($scope, $timeout) {
    var vm = this;

    vm.captionsEnabled = true;
    vm.slideSelectHandler = slideSelectHandler;
    vm.textTrackChangeHandler = textTrackChangeHandler;
    vm.mediaToggle = {
        sources: [],
        tracks: []
    };

    vm.$onInit = activate;

    //////////

    function activate() {
        vm.mediaToggle = {
            sources: _.map(vm.session.media.mediaCollection.sources, function (mediaSource) {
                return {
                    src: mediaSource.path,
                    type: mediaSource.type
                };
            }),
            tracks: _.map(vm.session.media.VTTs, function (vtt) {
                return {
                    kind: "captions",
                    label: vtt.label,
                    src: vtt.path,
                    srclang: vtt.lang,
                    default: vtt.lang === vm.session.transcriptStream.fingerprint
                };
            })
        };

        $scope.$watch("vm.timeSetter", timeSetterChangeHandler);
        $timeout(function () {
            videojs("session_video").ready(function () {
                var player = this;
                player.on("timeupdate", function () {
                    timeUpdateHandler(this.currentTime());
                });

                var isPlayingGetter = function () { return !player.paused(); };
                vm.setPlayingGetter(isPlayingGetter);
                var playCallback = function () { return player.play(); };
                var pauseCallback = function() { return player.pause(); };
                vm.setPlayCallback(playCallback);
                vm.setPauseCallback(pauseCallback);
                var timeSetterCallback = function(time) { player.currentTime(time); };
                vm.setTimeSetterCallback(timeSetterCallback);
                // TODO: Only enable range slider when user is authenticated with cutting permissions
                // Add range slider to let user select segments of the video that he wants to cut out
                /** TODO: Re-add this later
                var options = {};
                player.rangeslider(options);
                player.showSlider();

                player.on("sliderchange", function () {
                    var values = player.getValueSlider();
                    vm.sliderStart = values.start;
                    vm.sliderEnd = values.end;
                });
                */
            });
        });
    }

    function slideSelectHandler(slide) {
        playFromPosition(slide.start);
    }

    function timeUpdateHandler(time) {
        $scope.$apply(function () {
            vm.timeGetter = time;
        });
    }

    function timeSetterChangeHandler(time, oldTime) {
        if (time === oldTime) {
            return;
        }
        var player = videojs("session_video");
        player.currentTime(time);
    }

    function textTrackChangeHandler(ev, track) {
        vm.captionsEnabled = track !== null;
    }

    function playFromPosition(seconds) {
        var player = videojs("session_video");
        player.play();
        player.currentTime(seconds);
    }

    function cut() {
        var player = videojs("session_video");

        console.log(vm.session);
        // Get the current session media and the slider values and pack them in one object
        data = vm.session.media;
        data.interval = player.getValueSlider();

        vm.waitingForAJAX = true;
        // Send the data to the server via an AJAX call and wait for response
        $http.post("session-media/media_corrections/", data).then(function (response) {
            player.pause();  // Pause the player to prevent unwanted audio playing in the background

            newSrc = breakCache(player.currentSrc());
            player.src({ type: player.currentType(), src: player.currentSrc() + "?t=" + timestamp });

            player.load();

            player.setValueSlider(0, player.duration());
            vm.waitingForAJAX = false;
        }
        );
    }

    function breakCache(srcUrl) {
        // Add a timestamp to the video url to break the cache. Otherwise the browser will just display the old, unedited version from the cache.
        timestamp = + new Date();   // Intentional unary '+' operator to trigger the valueOf method in Date!
        return srcUrl + "?t=" + timestamp;
    }
}

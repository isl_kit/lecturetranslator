angular
    .module("app.sessions")
    .controller("SessionSlidesController", SessionSlidesController);

/*@ngInject*/
function SessionSlidesController($scope) {
    var vm = this;

    vm.selectSlide = selectSlide;
    vm.filteredSlides = [];
    vm.showSlides = true;
    vm.glued = true;
    vm.pos = 0;

    activate();

    //////////

    function activate() {
        $scope.$watch("vm.showSlides", showSlidesChangeHandler);
    }

    function selectSlide(slide) {
        vm.onSelect({slide: slide});
        vm.glued = true;
    }

    function showSlidesChangeHandler(val, oldVal) {
        if (!val || val === oldVal) {
            return;
        }
        vm.glued = true;
    }
}

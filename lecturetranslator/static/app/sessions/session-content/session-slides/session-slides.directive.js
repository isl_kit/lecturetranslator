angular
    .module("app.sessions")
    .directive("sessionSlides", sessionSlidesDirective);

/*@ngInject*/
function sessionSlidesDirective() {

    var directive = {
        restrict: "E",
        scope: {},
        controller: "SessionSlidesController",
        controllerAs: "vm",
        bindToController: {
            slides: "=",
            time: "=",
            text: "=",
            onSelect: "&"
        },
        templateUrl: "sessions/session-content/session-slides/session-slides.tpl.html"
    };
    return directive;
}

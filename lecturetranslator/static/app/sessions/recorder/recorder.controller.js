angular
    .module("app.sessions")
    .controller("RecorderController", RecorderController);

/*@ngInject*/
function RecorderController($scope, $state, $stateParams, sessionService, recordingService, authEntityService, Session) {
    var vm = this,
        tempGain = 0;

    vm.meter = null;
    vm.gain = recordingService.getGain() * 100;
    vm.recState = 0;
    vm.formData = {
        title: $stateParams.t || "DemoSession",
        desc: $stateParams.d || "",
        fingerprint: $stateParams.f || "de-DE-lecture_KIT",
        password: $stateParams.p || "",
        logging: $stateParams.l === "on",
        defaultTranslation: $stateParams.tr || "",
        openDirectly: $stateParams.o === "yes"
    };
    vm.sessionError = {};
    vm.session = null;
    vm.authenticated = false;

    vm.record = record;
    vm.pause = pause;
    vm.stop = stop;
    vm.openSession = openSession;
    vm.toggleMute = toggleMute;
    vm.mute = false;

    activate();

    //////////

    function activate() {
        initRecordingState();
        $scope.$watch("vm.gain", gainWatcher);
        $scope.$on("sessions.add", sessionsAddHandler);
        $scope.$on("rec.stateChange", recordingStateChangeHandler);
    }

    function initRecordingState() {
        var session, sessionData;
        if (recordingService.isRecording()) {
            sessionData = recordingService.getSessionData();
            vm.meter = recordingService.getMeter();
            vm.recState = recordingService.isPaused() ? 2 : 1;
            vm.formData.title = sessionData.title;
            vm.formData.desc = sessionData.desc;
            vm.formData.fingerprint = sessionData.fingerprint;
            vm.formData.password = sessionData.password;
            vm.formData.logging = sessionData.logging;
            vm.formData.defaultTranslation = sessionData.params.T || "";
        }
    }

    function recordingStateChangeHandler(evt, type, param) {
        if (type === "start-session") {
            initRecordingState();
        } else if (type === "stop-session") {
            if (param) {
                $scope.$apply(function () {
                    vm.meter = null;
                    vm.recState = 0;
                    vm.session = null;
                    vm.sessionError.backenderror = true;
                });
            }
        }
    }

    function record(valid) {
        if (vm.recState > 0 || !valid) {
            return;
        }
        var password = vm.formData.password,
            title = vm.formData.title,
            params = {};
        vm.recState = 1;
        vm.sessionError = {};
        params.T = vm.formData.defaultTranslation || -1;
        recordingService.startSession(title, params, vm.formData.desc, vm.formData.fingerprint, password, vm.formData.logging).then(function (sessionId) {
            vm.meter = recordingService.getMeter();
            checkSession();
        });
    }

    function pause() {
        if (vm.recState === 0) {
            return;
        }
        var pausing = vm.recState === 1;
        recordingService.setPause(pausing);
        vm.recState = pausing ? 2 : 1;
    }

    function stop() {
        if (vm.recState === 0) {
            return;
        }
        recordingService.stopSession();
        vm.meter = null;
        vm.recState = 0;
        vm.session = null;
    }

    function toggleMute() {
        vm.mute = !vm.mute;
        if (vm.mute) {
            tempGain = vm.gain;
            vm.gain = 0;
        } else {
            vm.gain = tempGain;
        }
    }

    function openSession(sessionId) {
        var session = sessionService.sessions[sessionId];
        $state.go("main.session.stream", {s: sessionId});
    }

    function checkSession() {
        var sessionId = recordingService.getSessionId();
        if (sessionId && _.has(sessionService.sessions, sessionId)) {
            vm.session = sessionService.sessions[sessionId];
            if (!vm.session.needsAuthentication) {
                vm.authenticated = true;
                directOpen();
            } else if (!vm.authenticated) {
                authEntityService.authenticate(vm.session.auth.id, {
                    password: vm.formData.password
                }).then(function (authenticated) {
                    vm.authenticated = authenticated;
                    directOpen();
                });
            }
        } else {
            vm.session = null;
            vm.authenticated = false;
        }
    }

    function directOpen() {
        if (vm.session && vm.authenticated && vm.formData.openDirectly) {
            $state.go("main.session.stream", {s: vm.session.id});
        }
    }

    function gainWatcher(newVal, oldVal) {
        if (newVal === oldVal) {
            return;
        }
        recordingService.setGain(newVal / 100);
    }

    function sessionsAddHandler(event, sessions) {
        checkSession();
    }
}

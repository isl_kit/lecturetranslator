angular
    .module("app.sessions")
    .component("eventContent", {
        bindings: {
            event: '<'
        },
        controller: "EventContentController",
        controllerAs: "vm",
        templateUrl: "sessions/event/content/event-content.tpl.html"
});

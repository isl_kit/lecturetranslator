angular
    .module("app.sessions")
    .controller("EventRootController", EventRootController);

/*@ngInject*/
function EventRootController($scope, $stateParams, event, session, sessionHandleService) {
    var vm = this;

    vm.session = session;
    vm.selectedStreams = [];
    vm.event = event;

    vm.uiOnParamsChanged = uiOnParamsChanged;

    activate();

    //////////

    function activate() {
        $scope.$on("sessions.add", sessionAddHandler);
        updateSelectedStreams();
    }

    function uiOnParamsChanged(params) {
        updateSelectedStreams();
    }

    function sessionAddHandler(evt, session) {
        if (session.event && session.event.id === vm.event.id) {
            vm.session = session;
            updateSelectedStreams();
        }
    }

    function updateSelectedStreams() {
        if (vm.session) {
            vm.selectedStreams = sessionHandleService.getSelectedStreams(vm.session, $stateParams.lang);
        } else {
            vm.selectedStreams = [];
        }
    }
}

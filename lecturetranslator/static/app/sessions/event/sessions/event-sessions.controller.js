angular
    .module("app.sessions")
    .controller("EventSessionsController", EventSessionsController);

/*@ngInject*/
function EventSessionsController(event, mediaEvents, sessions, eventService) {
    var vm = this;

    vm.event = event;
    vm.mediaEvents = mediaEvents;
    vm.sessions = sessions;

    vm.assign = assign;

    //////////

    function assign(sessionId) {
        eventService.update(vm.event.id, {
            session: sessionId
        }).then(function (event) {
            vm.event = event;
        });
    }
}

angular
    .module("app.sessions")
    .controller("LectureHallEditController", LectureHallEditController);

/*@ngInject*/
function LectureHallEditController($state, lectureHall, lectureHallService) {
    var vm = this;

    vm.formData = {
        name: lectureHall.name
    };
    vm.submit = submit;
    vm.cancel = cancel;

    //////////

    function submit(valid) {
        if (!valid) {
            return;
        }
        lectureHallService.update(lectureHall.id, {
            name: vm.formData.name
        }).then(returnToView);
    }

    function cancel() {
        returnToView();
    }

    function returnToView() {
        $state.go("main.hall.view", {id: lectureHall.id});
    }
}

angular
    .module("app.sessions")
    .controller("LectureHallViewController", LectureHallViewController);

/*@ngInject*/
function LectureHallViewController($state, $filter, $stateParams, lectureHall) {
    var vm = this;

    vm.lectureHall = lectureHall;
    vm.start = null;
    vm.weekChangeHandler = weekChangeHandler;

    vm.uiOnParamsChanged = uiOnParamsChanged;

    activate();

    //////////

    function activate() {
        update($stateParams.date);
    }

    function update(dateString) {
        var date;
        if (dateString) {
            date = new Date(dateString.replace(/-/g, "/") + ' 00:00:00');
            if (!isNaN(date.getTime())) {
                vm.start = date;
            }
        }
        if (!vm.start) {
            vm.start = new Date();
        }
    }

    function weekChangeHandler(date) {
        $state.go($state.current, {date: $filter("date")(date, "yyyy-MM-dd")});
    }

    function uiOnParamsChanged(params) {
        update(params.date);
    }
}

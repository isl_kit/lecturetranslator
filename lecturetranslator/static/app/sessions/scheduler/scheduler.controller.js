angular
    .module("app.sessions")
    .controller("SchedulerController", SchedulerController);

/*@ngInject*/
function SchedulerController($scope, $state, $timeout, $q, lecturerCreateDialogService, lecturers, lectureService, lectureHallService, lectureTermService, helperService) {
    var vm = this,
        dayMapper = {
            "Monday": 1,
            "Tuesday": 2,
            "Wednesday": 3,
            "Thursday": 4,
            "Friday": 5,
            "Saturday": 6,
            "Sunday": 0
        };

    vm.title = {
        selected: null,
        searchText: null,
        query: titleSearch
    };
    vm.hall = {
        selected: null,
        searchText: null,
        query: hallSearch
    };
    vm.events = [];
    vm.description = "";
    vm.slug = "";
    vm.archive = false;
    vm.password = "";
    vm.passwordVisible = false;

    vm.daysOfWeek = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

    vm.lecture = {
        selected: null,
        title: "",
        slug: "",
        autocomp: {
            selected: null,
            searchText: "",
            query: titleSearch
        }
    };

    vm.timespan = {
        termName: "",
        fingerprint: "de-DE-lecture_KIT",
        startDate: null,
        endDate: null
    };

    vm.details = {
        lecturers: {
            all: lecturers,
            selected: [],
            create: {
                title: "",
                name: ""
            }
        },
        archive: false,
        mediaAccess: false,
        password: "",
        passwordVisible: false,
        description: ""
    };

    vm.events = [];

    vm.togglePasswordVisibility = togglePasswordVisibility;
    vm.searchLectureChange = searchLectureChange;
    vm.selectedLectureChange = selectedLectureChange;
    vm.addEvent = addEvent;
    vm.deleteEvent = deleteEvent;
    vm.openLecturerCreateDialog = openLecturerCreateDialog;

    vm.send = send;

    activate();

    //////////

    function activate() {
        addEvent();
    }

    function addEvent() {
        vm.events.push({
            day: "Monday",
            start: "08:00",
            stop: "09:30",
            hall: {
                selected: null,
                searchText: null,
                query: hallSearch
            }
        });
    }

    function deleteEvent(event) {
        if (vm.events.length <= 1) {
            return;
        }
        vm.events = _.without(vm.events, event);
    }

    function titleSearch(query) {
        return lectureService.list({ search: query });
    }

    function hallSearch(query) {
        return lectureHallService.list({ search: query });
    }

    function send(valid) {
        if (!valid) {
            return;
        }
        var events = _.union.apply(_, _.map(vm.events, getIndividualEvents)),
            term = vm.timespan.termName;

        getOrCreateLecture().then(function (slug) {
            lectureTermService.create({
                term: term,
                lecture: slug,
                auth: {
                    password: vm.details.password
                },
                lecturers: vm.details.lecturers.selected,
                description: vm.details.description,
                archive: vm.details.archive,
                media_access: vm.details.mediaAccess,
                events: events
            }).then(function () {
                $state.go("main.lecture.term.view", {slug: slug, term: term});
            });
        });
    }

    function getOrCreateLecture() {
        var deferred = $q.defer();
        if (vm.lecture.autocomp.selected) {
            deferred.resolve(vm.lecture.autocomp.selected.slug);
        } else {
            lectureService.create({
                slug: vm.lecture.slug,
                title: vm.lecture.autocomp.searchText
            }).then(function () {
                deferred.resolve(vm.lecture.slug);
            }, function () {
                deferred.reject();
            });
        }
        return deferred.promise;
    }

    function getIndividualEvents(eventDescription) {
        var events = [],
            day = dayMapper[eventDescription.day],
            startDay = vm.timespan.startDate.getDay(),
            diff = day - startDay,
            date;
        if (diff < 0) {
            diff += 7;
        }
        date = new Date(vm.timespan.startDate);
        date.setDate(date.getDate() + diff);
        while (date < vm.timespan.endDate) {
            var dateString = getDateString(date);
            events.push({
                start: dateString + 'T' + eventDescription.start,
                stop: dateString + 'T' + eventDescription.stop,
                fingerprint: vm.timespan.fingerprint,
                lecture_hall: eventDescription.hall.searchText
            });
            date.setDate(date.getDate() + 7);
        }
        return events;
    }

    function getDateString(d) {
        return d.getFullYear() + "-" + zeroPad(d.getMonth() + 1, 2) + "-" + zeroPad(d.getDate(), 2);
    }

    function zeroPad(num, padding) {
        num = num.toString();
        while (num.length < padding) {
            num = "0" + num;
        }
        return num;
    }

    function togglePasswordVisibility() {
        vm.details.passwordVisible = !vm.details.passwordVisible;
    }

    function searchLectureChange() {
        if (!vm.lecture.autocomp.selected) {
            vm.lecture.slug = "";
        }
    }

    function selectedLectureChange() {
        if (vm.lecture.autocomp.selected) {
            var slug = vm.lecture.autocomp.selected.slug;
            vm.lecture.slug = slug;
            lectureService.get(slug).then(function (lecture) {
                vm.lecture.selected = lecture;
            });
        } else {
            vm.lecture.slug = "";
        }
    }

    function openLecturerCreateDialog() {
        lecturerCreateDialogService.show().then(function (lecturer) {
            vm.details.lecturers.all.push(lecturer);
            vm.details.lecturers.selected.push(lecturer.id);
            vm.details.lecturers.all.sort(lecturerSorting);
        });
    }

    function lecturerSorting(a, b) {
        a = a.name.toLowerCase();
        b = b.name.toLowerCase();
        if(a === b) return 0;
        if(a > b) return 1;
        return -1;
    }
}

angular
    .module("app")
    .config(appConfig);

/* @ngInject */
function appConfig($urlRouterProvider, $httpProvider, $locationProvider, $qProvider, $provide, $compileProvider, $mdThemingProvider, $sceDelegateProvider, DEBUG) {

    $urlRouterProvider.otherwise(function ($injector, $location) {
        var $state = $injector.get("$state");
        $state.go("main.welcome");
    });
    $httpProvider.defaults.xsrfCookieName = "csrftoken";
    $httpProvider.defaults.xsrfHeaderName = "X-CSRFToken";

    $qProvider.errorOnUnhandledRejections(true);

    $locationProvider.hashPrefix('');

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    if (!DEBUG) {
        $compileProvider.debugInfoEnabled(false);
        $httpProvider.useApplyAsync(true);
    }

    $provide.factory("logsOutUserOn401", logsOutUserOn401);
    $httpProvider.interceptors.push("logsOutUserOn401");

    $mdThemingProvider.theme("default")
      .primaryPalette("teal", {
        'default': '500', // by default use shade 400 from the pink palette for primary intentions
        'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
        'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
        'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
      })
      // If you specify less than all of the keys, it will inherit from the
      // default shades
      .accentPalette('orange', {
        'default': '500' // use shade 200 for default, and keep all other shades the same
      });


    $sceDelegateProvider.resourceUrlWhitelist([
       "self",
       "https://media.bibliothek.kit.edu/**"
    ]);
    //////////

    /*@ngInject*/
    function logsOutUserOn401($q, $injector) {

        return {
            response: function (response) {
                return response;
            },
            responseError: function (response) {
                if(response.status === 401) {
                    var $auth = $injector.get('auth');
                    auth.setLoggedOut();
                    $injector.get("$state").go("main.welcome");
                    //window.location.reload(true);
                }
                return $q.reject(response);
            }
        };
    }
}

angular
    .module("app")
    .controller("AppController", AppController);

/* @ngInject */
function AppController ($rootScope, $state, $mdSidenav, $mdMedia, $mdDialog, $transitions, auth, recordingService, loggingService, helperService) {

    $rootScope.pageTitle = "Lecture Translator";
    $rootScope.stopPropagation = stopPropagation;
    //$rootScope.$on("$stateChangeError", stateChangeErrorHandler);
    //$rootScope.$on("$stateChangeSuccess", stateChangeSuccessHandler);
    $transitions.onSuccess({}, stateChangeSuccessHandler);
    $transitions.onError({}, stateChangeErrorHandler);
    auth.onAuthStateChange(authStateChangeHandler);
    loggingService.log("open");
    $(window).on("beforeunload", unloadHandler);
    $rootScope.toggleLeft = function() {
        $mdSidenav("left").toggle();
    };
    $rootScope.$mdMedia = $mdMedia;
    $rootScope.IE = window.IE;
    $rootScope.currentTerm = helperService.getCurrentTerm();
    $(window).on("focus", focusHandler);

    $rootScope.webRTCReady = false;
    AdapterJS.webRTCReady(function(isUsingPlugin) {
        $rootScope.webRTCReady = true;
    });

    //////////

    function unloadHandler() {
        loggingService.log("close");
    }

    function stopPropagation($event) {
        if ($event.stopPropagation) {
            $event.stopPropagation();
        }
        $event.cancelBubble = true;
    }

    function stateChangeErrorHandler(transition) {
        var toState = transition.to();
        if (toState.data && toState.data.redirectOnError) {
            $state.go(toState.data.redirectOnError);
        }
    }

    function stateChangeSuccessHandler(transition) {
        try {
            var subHeaderTitle = transition.getResolveValue('subHeaderTitle');
            if (subHeaderTitle) {
                $rootScope.subHeaderTitle = subHeaderTitle;
            } else {
                $rootScope.subHeaderTitle = "";
            }
        } catch (e) {
            $rootScope.subHeaderTitle = "";
        }
        auth.isLoggedIn();
        $mdDialog.cancel();
        if (!$mdMedia("gt-sm")) {
            $mdSidenav("left").close();
        }
    }

    function authStateChangeHandler(action) {
        if (action === "logout" && recordingService.isRecording()) {
            recordingService.stopSession();
        }
    }

    function focusHandler() {
        auth.isLoggedIn(true);
    }
}

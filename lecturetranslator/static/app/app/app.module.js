angular.module("app", [
    "app.core",

    "app.templates",

    "app.layout",
    "app.sessions"
]);
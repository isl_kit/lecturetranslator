angular
    .module("app.auth")
    .factory("User", UserModel);

/*@ngInject*/
function UserModel() {

    function User(data) {
        var that = this;
        this.id = data.id;
        this.name = data.name || "";
        this.firstName = data.firstname || "";
        this.lastName = data.lastname || "";
        this.groups = {};
        this.permissions = {};
        this.isStaff = !!data.is_staff;
        this.isTranscriptCorrector = !!data.is_transcript_corrector;
        if (data.groups) {
            _.each(data.groups, function (name) {
                that.groups[name] = true;
            });
        }
        if (data.permissions) {
            _.each(data.permissions, function (name) {
                that.permissions[name] = true;
            });
        }
    }

    return User;
}

angular
    .module("app.auth")
    .provider("auth", auth);

/*@ngInject*/
function auth() {

    var that = this, stateChangeHandler = [];
    
    that.setRules = setRules;
    that.$get = getAuthService;

    //////////

    // @ngInject
    function getAuthService($http, $q, sessionStorageService, User) {

        var service = {
            login: login,
            logout: logout,
            isLoggedIn: isLoggedIn,
            hasPermission: hasPermission,
            isStaff: isStaff,
            isTranscriptCorrector: isTranscriptCorrector,
            setLoggedIn: setLoggedIn,
            setLoggedOut: setLoggedOut,
            onAuthStateChange: onAuthStateChange,
            user: new User({id: 0}),
            authenticated: false
        };

        return service;

        //////////

        function cacheSession() {
            sessionStorageService.set("authenticated", true);
        }

        function uncacheSession() {
            sessionStorageService.unset("authenticated");
        }

        function sanitizeCredentials(credentials) {
            return {
                name: credentials.name,
                password: credentials.password
            };
        }

        function login(credentials) {
            var loginRequest = $http.post("auth/login/", sanitizeCredentials(credentials));
            loginRequest.then(function (response) {
                setLoggedIn(response.data.user);
                _.each(stateChangeHandler, function (handler) {
                    handler("login");
                });
            });
            return loginRequest;
        }

        function logout() {
            var logoutRequest = $http.post("auth/logout/");
            logoutRequest.then(function () {
                setLoggedOut();
                _.each(stateChangeHandler, function (handler) {
                    handler("logout");
                });
            });
            return logoutRequest;
        }

        function setLoggedIn(userData) {
            cacheSession();
            if (userData) {
                User.call(service.user, userData);
            }
            service.authenticated = true;
        }

        function setLoggedOut() {
            uncacheSession();
            User.call(service.user, {id: 0});
            service.authenticated = false;
        }

        function onAuthStateChange(handler) {
            stateChangeHandler.push(handler);
        }

        function isLoggedIn(forceServerCall) {
            var deferred = $q.defer(),
                authenticated = sessionStorageService.get("authenticated") === "true" ? true : false;

            if (!authenticated || forceServerCall || !service.user.name) {
                $http.get("auth/me/").then(function (response) {
                    setLoggedIn(response.data);
                    deferred.resolve(true);
                }, function(reason) {
                    if (forceServerCall) {
                        setLoggedOut();
                    }
                    deferred.resolve(false);
                });
            } else {
                setLoggedIn();
                deferred.resolve(true);
            }
            return deferred.promise;
        }

        function hasPermission(name) {
            return !!service.user.permissions[name];
        }

        function isStaff() {
            return service.user.isStaff;
        }

        function isTranscriptCorrector() {
            return service.user.isTranscriptCorrector;
        }
    }

    function setRules(options) {
        return ["$q", "auth", function ($q, auth) {
            var promises = [], permissions = [], checkedLogin = false;

            if (options.login === true || options.isStaff === true) {
                promises.push(checkLogin(true));
                checkedLogin = true;
            } else if (options.login === false) {
                promises.push(checkLogin(false));
                checkedLogin = true;
            }

            if (options.permissions && options.permissions.length > 0) {
                if (!checkedLogin) {
                    promises.push(checkLogin(true));
                }
            }

            return $q.all(promises).then(function () {
                var success = true;
                if (options.permissions) {
                    _.each(options.permissions, function (name) {
                        success = success && auth.hasPermission(name);
                    });
                }
                if (options.isStaff === true) {
                    success = success && auth.isStaff();
                } else if (options.isStaff === false) {
                    success = success && !auth.isStaff();
                }
                if (success) {
                    return true;
                }
                return $q.reject("not permitted");
            });

            //////////

            function checkLogin(expected) {
                var deferred = $q.defer();
                auth.isLoggedIn().then(function (isLoggedIn) {
                    if (isLoggedIn === expected) {
                        deferred.resolve();
                    } else {
                        deferred.reject("ĺogin requirement not satisfied " + expected);
                    }
                });
                return deferred.promise;
            }
        }];
    }
}

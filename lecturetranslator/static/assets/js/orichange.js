!(function (window, undefined) {

    var listener = [];

    if (window.onorientationchange) {
        window.onorientationchange = function () {
            _.each(listener, function (callback) {
                callback();
            });
        };
    }

    function addOrientationChangeListener(callback) {
        if (_.contains(listener, callback)) {
            return;
        }
        listener.push(callback);
    }

    window.addOrientationChangeListener = addOrientationChangeListener;

}(window));

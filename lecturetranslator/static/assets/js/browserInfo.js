function BrowserInfo() {	

	this.getBrowserName = function() {   
	  	var agent = navigator.userAgent.toLowerCase();
	  	if (agent.indexOf('msie')>-1)  
		  	return "msie";
	  	if (agent.indexOf("firefox")>-1) 
		  	return "firefox";
	  	if (agent.indexOf("opera")>-1) 
		  	return "opera";
	  	if ((agent.indexOf("safari")>-1) && (agent.indexOf("chrome") == -1)) 
		  	return "safari";
	  	if (agent.indexOf("chrome")>-1) 
		  	return "chrome";
	   return "unknown";   
	};
	
	this.getBrowserVersion = function() {
		var n = 0;
	  	var agent = navigator.userAgent.toLowerCase();
		if (/firefox[\/\s](\d+\.\d+)/.test(agent)){ 
			n = parseFloat(RegExp.$1); 
		} else if (/msie[\/\s](\d+\.\d+)/.test(agent)){ 
			n = parseFloat(RegExp.$1);
		} else if (/chrome[\/\s](\d+\.\d+)/.test(agent)){ 
			n = parseFloat(RegExp.$1); 	 		
		} else if (/opera[\/\s](\d+\.\d+)/.test(agent)){ 
			n = parseFloat(RegExp.$1); 	 
		} else if (/version[\/\s](\d+\.\d+)[\/\s]safari/.test(agent)){ 
			n = parseFloat(RegExp.$1);  	 
		}
		return n;
	};
	this.getOperatingSystem = function() {
		var os = "";
		var agent = navigator.userAgent.toLowerCase();
		var platform = navigator.platform;
		 
		if ((platform.indexOf("Win32")!=-1) || (platform.indexOf("Win64")!=-1)) { 
			if((agent.indexOf("win95")!=-1) || (agent.indexOf("windows 95")!=-1)) {
			  	os = "Windows 95";
			} else if((agent.indexOf("win98")!=-1) || (agent.indexOf("windows 98")!=-1)) {
			  	os =  "Windows 98";
			} else if(agent.indexOf("win 9x 4.90")!=-1) {
			  	os =  "Windows NT";
			} else if(agent.indexOf("windows nt 5.0")!=-1) {
			  	os =  "Windows 2000";
			} else if(agent.indexOf("windows nt 5.1")!=-1) {
			  	os =  "Windows XP";
			} else if(agent.indexOf("windows nt 6.1")!=-1) {
				os =  "Win7";
			} else if(agent.indexOf("windows nt 6")!=-1) {
			  	os =  "Windows Vista";
			} else if(agent.indexOf("windows phone")!=-1) {
			  	os =  "Windows Phone";
			}
		} else {
			os = platform; 
		}
		return os;	
	};
	this.getOsType = function() {
		var platform = navigator.platform.toLowerCase();
		var agent = navigator.userAgent.toLowerCase();
		//An identifier for the client's operating system.
		//0 - Microsoft Windows
		//1 - Apple Macintosh
		//2 - Flash (OS is unknown)
		//3 - Apple IOS (iPhone, iPad, iPod)
		//4 - Android
		//5 - RIM (Blackberry)
		//6 - Symbian
		//7 - Bada
		//8 - Palm
		//9 - Windows mobile 
		//10 - Windows Phone 7 
		//20 - Unix/Linux 
		//30 - PlayStation
		//99 - unknown
		if ((platform.indexOf("wince") != -1) || (platform.indexOf("windows mobile") != -1) ||
				(agent.indexOf("windows mobile") != -1) || (agent.indexOf("windowsmobile") != -1) ||
				(agent.indexOf("windows ce") != -1) || (agent.indexOf("windowsmobile") != -1) )	{
				return 9;
			} 
		if ((agent.indexOf("windows phone") != -1)) {
			return 10;
		}
		
		if ((platform.indexOf("win16") != -1) || (platform.indexOf("win32") != -1) || (platform.indexOf("win64") != -1)) {
			return 0;
		}		
		if ((platform.indexOf("macppc") != -1) || (platform.indexOf("mac68k") != -1) || (platform.indexOf("macintel") != -1)) {
			return 1;
		} 
		if ((agent.indexOf("iphone") != -1) || (agent.indexOf("ipad") != -1)  || (agent.indexOf("ipod") != -1)) {
			return 3;
		} 
		if ((agent.indexOf("android") != -1)) {
			return 4;
		} 
		if ((agent.indexOf("blackberry") != -1)) {
			return 5;
		} 
		if ((agent.indexOf("symbian") != -1)) {
			return 6;
		} 
		if ((agent.indexOf("bada") != -1)) {
			return 7;
		} 
		if ((agent.indexOf("palm") != -1)) {
			return 8;
		}
		if ((platform.indexOf("linux") != -1) || (platform.indexOf("sunos") != -1) ||
			(platform.indexOf("hp-ux") != -1)) {
			return 20;  //Unix
		} 
		
		if ((agent.indexOf("playStation") != -1)) {
				return 30;  //PlayStation
		} 
		return 99; //unknown
	};
	this.getOsBits = function() {
		var platform = navigator.platform.toLowerCase();
		var agent = navigator.userAgent.toLowerCase();
		if (platform.indexOf("64")!=-1) {
			return 64;;
		} else if(platform.indexOf("32")!=-1) {
			return 32;
		} else if(platform.indexOf("16")!=-1) {
			return 16;
		} else if(platform.indexOf("16")!=-1) {
			return 16;
		} else if(agent.indexOf("i686")!=-1) {
			return 32;
		} else if(agent.indexOf("x86_64")!=-1) {
			return 64;
		} else {
			return 0;
		}
	};
	
	this.getBrowserLanguage = function() {
	    var lang = navigator.language;
	    if (lang == null)
	        lang = navigator.browserLanguage;
	    return lang;
	};
	
	this.getWindowX = function() {
		if (window.screenLeft) { // IE und andere
		    return window.screenLeft;
		} else if (window.screenX) { // Firefox und andere
		    return window.screenX;
		} else {
			return 0;
		}
	}; 
	this.getWindowY = function() {
		if (window.screenTop) { // IE und andere
		    return window.screenTop;
		} else if (window.screenY) { // Firefox und andere
		    return window.screenY;
		} else {
			return 0;
		}
	}; 

	this.getViewportWidth = function() {
		if (window.innerWidth) { // Alle Browsers außer dem IE
		    return window.innerWidth; 
		} else if (document.documentElement && document.documentElement.clientWidth) {
		    // Funktionen für den IE 6, wenn es einen DOCTYPE gibt
		    return document.documentElement.clientWidth; 
		} else if (document.body.clientWidth) {
		    // Funktionen für IE4, IE5 und IE6 ohne DOCTYPE
		    return document.body.clientWidth;
		}
	}; 
	this.getViewportHeight = function() {
		if (window.innerWidth) { // Alle Browsers außer dem IE
		    return window.innerHeight; 
		} else if (document.documentElement && document.documentElement.clientWidth) {
		    // Funktionen für den IE 6, wenn es einen DOCTYPE gibt
		    return document.documentElement.clientHeight; 
		} else if (document.body.clientWidth) {
		    // Funktionen für IE4, IE5 und IE6 ohne DOCTYPE
		    return document.body.clientHeight; 
		}
	};
	
	this.getHorizontalScroll = function() {
		if (window.innerWidth) { // Alle Browsers außer dem IE
		    return window.pageXOffset; 
		} else if (document.documentElement && document.documentElement.clientWidth) {
		    // Funktionen für den IE 6, wenn es einen DOCTYPE gibt
		    return document.documentElement.scrollLeft; 
		} else if (document.body.clientWidth) {
		    // Funktionen für IE4, IE5 und IE6 ohne DOCTYPE 
		    return document.body.scrollLeft; 
		}
	}; 
	this.getVerticalScroll = function() {
		if (window.innerWidth) { // Alle Browsers außer dem IE
		    return window.pageYOffset; 
		} else if (document.documentElement && document.documentElement.clientWidth) {
		    // Funktionen für den IE 6, wenn es einen DOCTYPE gibt
		    return document.documentElement.scrollTop; 
		} else if (document.body.clientWidth) {
		    // Funktionen für IE4, IE5 und IE6 ohne DOCTYPE
		    return document.body.scrollTop; 
		}
	};
	this.getBrowserInfoTable = function() {
		var text = 	"<table>" +
					"	<caption/>Browser Info</caption>" +  
					"	<tbody>" +
					"	<tr><th>Property</th><th>Value</th></tr>" +	
					"	<tr><td>browserName</td><td>"+this.getBrowserName()+"</td></tr>" +	
					"	<tr><td>browserVersion</td><td>"+this.getBrowserVersion()+"</td></tr>" +	
					"	<tr><td>os</td><td>"+this.getOperatingSystem()+"</td></tr>" +	
					"	<tr><td>osType</td><td>"+this.getOsType()+"</td></tr>" +	
					"	<tr><td>osBits</td><td>"+this.getOsBits()+"</td></tr>" +	
					"	<tr><td>language</td><td>"+this.getBrowserLanguage()+"</td></tr>" +	
					"	<tr><td>colorDepth</td><td>"+screen.colorDepth+"</td></tr>" +	
					"	<tr><td>screenSize</td><td>"+screen.width + "," + screen.height+"</td></tr>" +	
					"	<tr><td>screenAvail</td><td>"+screen.availWidth + "," + screen.availHeight+"</td></tr>" +	
					"	<tr><td>windowPos</td><td>"+this.getWindowX() + "," + this.getWindowY()+"</td></tr>" +	
					"	<tr><td>viewPort</td><td>"+this.getViewportWidth() + "," + this.getViewportHeight()+"</td></tr>" +	
					"	<tr><td>scrollPos</td><td>"+this.getHorizontalScroll() + "," + this.getVerticalScroll()+"</td></tr>" +	
					"	<tr><td>href</td><td>"+location.href+"</td></tr>" +	
					"	<tr><td>referrer</td><td>"+document.referrer+"</td></tr>" +	
					"	<tr><td>cookieEnabled</td><td>"+navigator.cookieEnabled+"</td></tr>" +	
					"	<tr><td>javaEnabled</td><td>"+navigator.javaEnabled()+"</td></tr>" +	
					"	<tr><td>platform</td><td>"+navigator.platform+"</td></tr>" +	
					"	<tr><td>userAgent</td><td>"+navigator.userAgent+"</td></tr>" +	
					"	</tbody>" +
					"</table>";
		return text;
	};
	
	this.isDesktop = function() {
		var osType = this.getOsType();
		if ((osType == 0) || (osType == 1) || (osType == 20)) {  //Windows, Mac, Linux
			return true;
		} else {
			return false;
		}
	};
}
#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from ...tools import create_empty_video


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('source', type=str)
        parser.add_argument('target', type=str)
        parser.add_argument('dur', type=int)

    def handle(self, *args, **options):
        create_empty_video(
            options['source'],
            options['target'],
            options['dur']
        )

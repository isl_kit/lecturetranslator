#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from ...models import MediaEvent


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('mediaevent_id', type=int)

    def handle(self, *args, **options):
        media_event = MediaEvent.objects.get(id=options['mediaevent_id'])
        MediaEvent.objects.create(
            fingerprint=media_event.fingerprint,
            path=media_event.path,
            audio_path=media_event.audio_path,
            media_type=media_event.media_type,
            event=media_event.event
        )

#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from django.db.models import F

from lecturetranslator.session.models import DataSegment


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument("sessionid", type=int)
        parser.add_argument("delta", type=int)

    def handle(self, *args, **options):
        delta = options['delta']
        session_id = options['sessionid']
        DataSegment.objects.filter(stream__session__id=session_id) \
            .update(start=F('start')+delta, stop=F('stop')+delta)

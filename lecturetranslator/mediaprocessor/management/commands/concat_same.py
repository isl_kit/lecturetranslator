#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from ...tools import concat_same


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('source1', type=str)
        parser.add_argument('source2', type=str)
        parser.add_argument('target', type=str)

    def handle(self, *args, **options):
        concat_same(options['source1'], options['source2'], options['target'])

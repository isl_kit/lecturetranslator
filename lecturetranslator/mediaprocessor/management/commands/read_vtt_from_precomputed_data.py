#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import os.path

from django.core.management.base import BaseCommand

from lecturetranslator.session.models import Session

from ...helper import media_dir
from ...vtt import create_vtt_file_from_segments


class Command(BaseCommand):
    new_sentence = True

    def add_arguments(self, parser):
        parser.add_argument('sessionid', type=int)
        parser.add_argument('fingerprint', type=str)
        parser.add_argument('source', type=str)

    def handle(self, *args, **options):
        fingerprint = options['fingerprint']
        session = Session.objects.get(id=options['sessionid'])
        segments = []
        with open(options['source'], 'r') as f:
            for line in f:
                segments += self.create_vtt_segments(line.strip())
        vtt = create_vtt_file_from_segments(segments)
        vtt_dir = media_dir(session.id, parts=['vtt'], assure=True)
        with open(os.path.join(vtt_dir, '{}.vtt'.format(fingerprint)), 'w+') as f:
            f.write(vtt)

    def create_vtt_segments(self, line):
        parts = re.split(r'\s+', line, 2)
        start = float(parts[0])
        stop = float(parts[1])
        text = parts[2].strip()
        text = re.sub(r'\s+(\.|,|!|\?)', '\\1', text)
        text = re.sub(r'(¿)\s+', '\\1', text)
        text = re.sub(r'\s(%?)-(%?)\s', fix_dash, text)
        text_parts = text.split(' ')
        av_dur = (stop - start) / len(text_parts)
        result = []
        for text_part in text_parts:
            if self.new_sentence:
                text_part = re.sub(r'^(¿)?.', capitalize, text_part)
            result.append({
                'start': start,
                'stop': start + av_dur,
                'text': text_part
            })
            start += av_dur
            self.new_sentence = text_part.endswith('.')
        return result


def capitalize(match):
    return match.group(0).upper()


def fix_dash(match):
    res = '-'
    if match.group(1) == '%':
        res = ' ' + res
    if match.group(2) == '%':
        res += ' '
    return res

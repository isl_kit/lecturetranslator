#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from ...tasks import process_online_source, process_video_upload


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('sessionid', type=int)
        parser.add_argument('media_path', type=str)

    def handle(self, *args, **options):
        session_id = options['sessionid']
        path = options['media_path']
        if path.startswith('http://') or path.startswith('https://'):
            process_online_source.delay(session_id, path)
        else:
            process_video_upload.delay(session_id, path)

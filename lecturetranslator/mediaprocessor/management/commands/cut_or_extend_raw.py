#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from ...tools import cut_or_extend_raw


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('source', type=str)
        parser.add_argument('target', type=str)
        parser.add_argument('dur', type=int)

    def handle(self, *args, **options):
        cut_or_extend_raw(options['source'], options['target'], options['dur'])

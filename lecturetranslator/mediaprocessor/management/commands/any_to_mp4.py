#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from ...tools import any_to_mp4


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('source', type=str)
        parser.add_argument('target', type=str)

    def handle(self, *args, **options):
        any_to_mp4(options['source'], options['target'])

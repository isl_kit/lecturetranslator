#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from ...tools import concat_raw


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('sources', type=str)
        parser.add_argument('target', type=str)

    def handle(self, *args, **options):
        concat_raw(options['sources'].split(','), options['target'])

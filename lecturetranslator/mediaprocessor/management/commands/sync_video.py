#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from ...tasks import process_online_source, process_video_sync


class Command(BaseCommand):
    """ Sync audio from lecturetranslator to audio to from video. """

    def add_arguments(self, parser):
        """ Describe commandline parameters. """

        parser.add_argument('sessionid', type=int)
        parser.add_argument('media_path', type=str)

    def handle(self, *args, **options):
        """ Action when file is called """

        session_id = options['sessionid']
        path = options['media_path']
        process_video_sync.delay(session_id, path)

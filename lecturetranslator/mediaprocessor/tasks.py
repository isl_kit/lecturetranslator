#!/usr/bin/env python3
# -*- coding: utf-8 -*-



import re
import os
import os.path
import urllib.request, urllib.parse, urllib.error
from datetime import datetime, timedelta
from shutil import copy, copytree, rmtree

import Levenshtein

from celery import shared_task

from django.db.models import F

from lecturetranslator.session.models import Stream, Session, SessionPart, DataSegment

from .offset import align
from .helper import media_dir, timestamped_dir, fresh_dir
from .models import MediaEvent
from .tools import (
    any_to_mp4,
    any_to_wav,
    concat_raw,
    concat_same,
    create_empty_video,
    cut_or_extend_raw,
    get_video_params,
    merge_video_audio,
    raw_to_mp4,
    raw_to_wav,
    resize_image,
)

from .vtt import create_vtt_file_from_stream


@shared_task
def process_video_upload(session_id, file_path):
    cwd = generate_cwd(session_id, 'video_schedule')
    local_path = local_copy(cwd, file_path)
    wav_path = convert_to_wav(local_path)
    schedule_media_event(wav_path, file_path, session_id, MediaEvent.FILE)


@shared_task
def process_video_sync(session_id, file_path):
    cwd = generate_cwd(session_id, 'video_schedule')
    local_path = local_copy(cwd, file_path)
    wav_path = convert_to_wav(local_path)
    offset = video_offset(wav_path, session_id)
    if offset is not None:
        orig_audio = process_raw_audio(cwd, session_id)
        aligned_video = align_video(offset, local_path, orig_audio)
        if aligned_video is not None:
            published_video = publish(aligned_video, session_id)
            process_slides(published_video)

@shared_task
def process_online_source(session_id, url):
    cwd = generate_cwd(session_id, 'online_source')
    local_path = download(cwd, url)
    wav_path = convert_to_wav(local_path)
    schedule_media_event(wav_path, url, session_id, MediaEvent.URL)


@shared_task
def process_session_audio(session_id):
    cwd = generate_cwd(session_id, 'raw')
    audio_file = process_raw_audio(cwd, session_id)
    if audio_file is not None:
        publish(audio_file, session_id)


@shared_task
def process_subtitles(session_id):
    cwd = generate_cwd(session_id, 'subtitles')
    tts_dir = create_subtitles(cwd, session_id)
    publish(tts_dir, session_id)


@shared_task
def process_media_event(session_id):
    media_event = MediaEvent.objects.get(session_id=session_id)
    cwd = generate_cwd(session_id, 'apply_media_event')
    if media_event.media_type == MediaEvent.URL:
        local_path = download(cwd, media_event.path)
    else:
        local_path = local_copy(cwd, media_event.path)
    video_path = convert_to_mp4(local_path)
    published_video = publish(video_path, session_id)
    completeness_old = check_completeness_session(media_event.event.session_id, media_event.event)
    completeness_new = check_completeness_session(session_id, media_event.event)
    if completeness_old <= 0.8 and completeness_new > completeness_old:
        update_session_with_media_event(session_id, media_event)
    elif completeness_new - completeness_old > -0.2 and completeness_new - completeness_old <= 0.2:
        diff_ratio = get_diff_ratio(media_event.event.session_id, session_id)
        if diff_ratio >= 0.8:
            update_session_with_media_event(session_id, media_event)
        else:
            # ALERT
            print(('DIFF RATIO TOO LARGE::: {}: {}'.format(session_id, diff_ratio)))
            pass
    elif completeness_new - completeness_old > 0.2:
        update_session_with_media_event(session_id, media_event)
    else:
        # ALERT
        print(('COMPLETENESS DOES NOT FIT::: {}: {} / {}'.format(session_id, completeness_new, completeness_old)))
        pass
    process_slides(published_video)


@shared_task
def realign_media_events():
    now = datetime.now()
    try:
        first_media_event = MediaEvent.objects.filter(session=None).earliest('start')
    except MediaEvent.DoesNotExist:
        return
    if first_media_event.start < now - timedelta(minutes=5):
        delta = now - first_media_event.start + timedelta(minutes=15)
        MediaEvent.objects.filter(session=None).update(start=F('start')+delta, stop=F('stop')+delta)


def check_completeness_session(session_id, event):
    try:
        session = Session.objects.get(id=session_id)
    except Session.DoesNotExist:
        return 0
    dur = (event.stop - event.start).total_seconds()
    data_segments = DataSegment.objects.filter(stream=session.transcript_stream).order_by('start_abs')
    first, last = data_segments[0], data_segments.reverse()[0]
    comp_dur = (last.start_abs - first.start_abs) / 1000.0
    return comp_dur / dur


def get_diff_ratio(session_id1, session_id2):
    text1 = get_session_text(session_id1)
    text2 = get_session_text(session_id2)
    return Levenshtein.ratio(text1, text2)


def get_session_text(session_id):
    try:
        session = Session.objects.get(id=session_id)
    except Session.DoesNotExist:
        return ''
    text_segments = DataSegment.objects \
        .filter(stream=session.transcript_stream) \
        .values_list('text', flat=True) \
        .order_by('start_abs')
    return re.sub('\s+', ' ', ' '.join(text_segments))


def update_session_with_media_event(session_id, media_event):
    try:
        session = Session.objects.get(id=session_id)
    except Session.DoesNotExist:
        return
    session.hidden = False
    session.save()
    media_event.event.session = session
    media_event.event.save()


def process_slides(source):
    slides_dir = os.path.join(os.path.dirname(source))
    detect_slides(source, slides_dir)
    order_slides(slides_dir)
    create_slides_variants(slides_dir, 360)
    extract_text_from_slides(slides_dir)
    return slides_dir


def process_raw_audio(cwd, session_id):
    timetable = get_timetable(session_id)
    if len(timetable) == 0:
        return None
    if len(timetable) > 1:
        source_file = concat_timetable(cwd, timetable)
    else:
        source_file = timetable[0]['file']
    audio_final = os.path.join(cwd, 'audio.mp4')
    raw_to_mp4(source_file, audio_final)
    return audio_final


def create_subtitles(cwd, session_id):
    vtt_dir = fresh_dir(
        os.path.join(cwd, 'vtt')
    )
    text_streams = Stream.objects.filter(session_id=session_id, type='text')
    for stream in text_streams:
        vtt_file = os.path.join(vtt_dir, '{}.vtt'.format(stream.fingerprint))
        text = create_vtt_file_from_stream(stream)
        with open(vtt_file, 'w+') as f:
            f.write(text.encode('utf-8'))
    return vtt_dir


def generate_cwd(session_id, name):
    return timestamped_dir(session_id, name)


def download(cwd, url):
    dest = os.path.join(cwd, 'download.mp4')
    opener = urllib.request.URLopener()
    opener.retrieve(url, dest)
    return dest


def convert_to_wav(file_path):
    """" Convert file to .wav file

        Resulting file will be in the same directory as the source file is.
        It will be called 'video.wav'.

        Args:
            file_path (path): path to file that should be converted.

        Returns:
            path to the converted file.
    """

    dest = os.path.join(os.path.dirname(file_path), 'video.wav')
    any_to_wav(file_path, dest)
    return dest


def convert_to_mp4(file_path):
    dest = os.path.join(os.path.dirname(file_path), 'video.mp4')
    any_to_mp4(file_path, dest)
    return dest


def schedule_media_event(wav_source, path, session_id, media_type):
    session = Session.objects.select_related('event').get(id=session_id)
    if not session.event:
        return
    dur = os.path.getsize(wav_source) / 32000.0
    start = next_possible_start()
    stop = start + timedelta(seconds=dur)
    MediaEvent.objects.create(
        fingerprint=session.input_fingerprint,
        path=path,
        audio_path=wav_source,
        media_type=media_type,
        event=session.event,
        start=start,
        stop=stop
    )


def next_possible_start():
    now = datetime.now()
    try:
        last_media_event = MediaEvent.objects.latest('stop')
    except MediaEvent.DoesNotExist:
        last_media_event = None
    delta = timedelta(minutes=15)
    if last_media_event is not None and now < last_media_event.stop:
        return last_media_event.stop + delta
    return now + delta


def local_copy(cwd, source):
    """" Create copy of directory

        The resulting structure will look like this:
        *base-directory*
         - 'source'
            - content

        'cwd'
         - *base-directory*
            - content

         Args:
             cwd (path): path to copy-destination.
             source (path): absolute path to directory that should be copied.

         Returns:
             Path to copied directory
    """

    dest = os.path.join(cwd, os.path.basename(source))
    copy(source, dest)
    return dest


def publish(file_path, session_id):
    dest = os.path.join(media_dir(session_id, assure=True), os.path.basename(file_path))
    if os.path.isdir(file_path):
        if os.path.exists(dest):
            rmtree(dest)
        copytree(file_path, dest)
    else:
        if os.path.exists(dest):
            os.remove(dest)
        copy(file_path, dest)
    return dest


def align_video(offset, source, orig_audio):
    """ Align video with audio from lecturetranslator.

        Args:
            offset (int): offset between video and audio.
            source (path): path to video file.
            orig_audio (path): path to audio from lecturetranslator.

        Returns:
            Path to final and aligned video
    """

    tmp_dir = fresh_dir(
        os.path.join(os.path.dirname(source), 'cut_tmp')
    )
    video_tmp = os.path.join(tmp_dir, 'video_tmp.mp4')
    video_final = os.path.join(os.path.dirname(source), 'video.mp4')
    if offset < 0:
        merge_video_audio(source, orig_audio, video_final, video_offset=-offset)
    elif offset > 0.1:
        chunk1 = os.path.join(tmp_dir, 'video_tmp001.mp4')
        chunk2 = os.path.join(tmp_dir, 'video_tmp002.mp4')
        any_to_mp4(source, chunk2)
        create_empty_video(chunk2, chunk1, offset)
        concat_same(chunk1, chunk2, orig_audio, video_tmp)
        copy(video_tmp, video_final)
    else:
        merge_video_audio(source, orig_audio, video_final)
    return video_final


def video_offset(video_wav, session_id):
    """" Get video offset to audio recorded by lecturetanslator

        Args:
            video_wav (path): path to the audio from the video file
            session_id (int): ID of the matching session

        Returns:
            offset between recorded audio and recorded video.
            None in case there is none
    """

    timetable = get_timetable(session_id)
    if len(timetable) == 0:
        return None
    tmp_dir = fresh_dir(
        os.path.join(os.path.dirname(video_wav), 'offset_tmp')
    )
    for i, part in enumerate(timetable):
        audio_wav = os.path.join(tmp_dir, 'audio{:03}.wav'.format(i))
        raw_to_wav(part['file'], audio_wav)
        offset = align(video_wav, audio_wav)
        if offset is not None:
            return offset + part['start'] / 1000.0
    return None


def concat_timetable(cwd, timetable):
    """ Join value in timetable.

        Args:
            cwd (path): path to directory where resulting file will be stored.
            timetable (array of dictionaries): values to be joined.

        Returns:
            path to joined file.

    """
    paths = []
    parts_dir = fresh_dir(
        os.path.join(cwd, 'concat_timetable')
    )
    for i, part in enumerate(timetable):
        if i > 0:
            previous_part = timetable[i - 1]
            part_file = os.path.join(parts_dir, 'part{:03}.raw'.format(i - 1))
            paths.append(part_file)
            dur = part['start'] - previous_part['start']
            cut_or_extend_raw(previous_part['file'], part_file, dur / 1000.0)
    part_file = os.path.join(parts_dir, 'part{:03}.raw'.format(i))
    paths.append(part_file)
    copy(timetable[-1]['file'], part_file)
    concat_file = os.path.join(parts_dir, 'audio.raw')
    concat_raw(paths, concat_file)
    return concat_file


def get_timetable(session_id):
    """" Get timetable of specific session

        Args:
            session_id (int): ID of session whose timetable should be acquired

        Returns:
            Array of dictionaries.
    """
    try:
        session = Session.objects.get(id=session_id)
    except Session.DoesNotExist:
        return None
    stream = session.transcript_stream
    parts = SessionPart.objects \
        .filter(session_id=session_id) \
        .exclude(audio_file='') \
        .order_by('start_greater')
    timetable = []
    for i, part in enumerate(parts):
        data_segments = DataSegment.objects \
            .filter(stream=stream, start_abs__gt=part.start_greater) \
            .order_by('start_abs')
        if data_segments.count() == 0:
            continue
        data_segment = data_segments[0]
        timetable.append({
            'index': i,
            'start': data_segment.start - data_segment.offset,
            'file': part.audio_file
        })
    return timetable


def detect_slides(source, slides_dir):
    from slidetransitiondetector.detector import Detector
    tmp_slides_dir = fresh_dir(
        os.path.join(slides_dir, 'tmp')
    )
    detector = Detector(source, tmp_slides_dir + '/', '.jpg')
    detector.detect_slides()


def order_slides(slides_dir):
    from slidetransitiondetector.sources import ListSource
    from slidetransitiondetector.sorter import SlideSorter
    from slidetransitiondetector.slides import SlideDataHelper
    tmp_slides_dir = os.path.join(slides_dir, 'tmp')
    full_slides_dir = fresh_dir(
        os.path.join(slides_dir, 'slides')
    )
    sorter = SlideSorter(
        ListSource(SlideDataHelper(tmp_slides_dir).get_slides()),
        full_slides_dir + '/', None, '.jpg'
    )
    sorter.sort()


def create_slides_variants(slides_dir, width):
    full_slides_dir = os.path.join(slides_dir, 'slides')
    small_slides_dir = fresh_dir(
        os.path.join(slides_dir, 'slides_{}'.format(width))
    )
    for f in os.listdir(full_slides_dir):
        _, ext = os.path.splitext(f)
        source_file = os.path.join(full_slides_dir, f)
        if os.path.isfile(source_file) and ext == ".jpg":
            dest = os.path.join(small_slides_dir, f)
            resize_image(source_file, dest, width)


def extract_text_from_slides(slides_dir):
    from slidetransitiondetector.sources import ListSource
    from slidetransitiondetector.extractor import ContentExtractor
    from slidetransitiondetector.slides import SlideDataHelper
    full_slides_dir = os.path.join(slides_dir, 'slides')
    slides_text_dir = fresh_dir(
        os.path.join(slides_dir, 'slides_text')
    )
    ContentExtractor(
        ListSource(SlideDataHelper(full_slides_dir).get_slides()),
        slides_text_dir + '/'
    ).analyze()



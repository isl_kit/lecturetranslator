# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2017-06-21 17:32


from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('session', '0001_initial'),
        ('event', '0002_event_session'),
    ]

    operations = [
        migrations.CreateModel(
            name='MediaEvent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('audio_path', models.CharField(max_length=2048)),
                ('path', models.CharField(max_length=2048)),
                ('media_type', models.CharField(choices=[(b'url', b'Url'), (b'file', b'File')], default=b'file', max_length=5)),
                ('fingerprint', models.CharField(max_length=200)),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='media_events', to='event.Event')),
                ('session', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='media_events', to='session.Session')),
            ],
        ),
    ]

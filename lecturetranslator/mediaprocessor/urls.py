#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^(?P<pk>[0-9]+)/slides/$', views.SessionSlides.as_view()),
    url(r'^(?P<pk>[0-9]+)/media/$', views.SessionMedia.as_view()),
    url(r'^(?P<pk>[0-9]+)/vtt/$', views.SessionSubtitles.as_view()),
    url(r'^events/$', views.MediaEventList.as_view()),
    url(r'^event-integrate/$', views.EventIntegrate.as_view())
]

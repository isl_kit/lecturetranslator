#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import threading
import subprocess
from os.path import join, exists, isfile, splitext
from os import listdir
from datetime import timedelta

from django.conf import settings

from rest_framework import serializers

from lecturetranslator.event.serializers        import EventRelatedSerializer
from lecturetranslator.event.models             import Event
from lecturetranslator.lecture.models           import LectureTerm
from lecturetranslator.authentication.models    import AuthEntity

from .models import MediaEvent


class MediaEventSerializer(serializers.ModelSerializer):
    event = EventRelatedSerializer()
    session = serializers.SlugRelatedField(read_only=True, slug_field='id_as_string')
    class Meta:
        model = MediaEvent
        fields = (
            'id', 'start', 'stop', 'session',
            'event', 'audio_path', 'media_type', 'fingerprint'
        )


class SessionMediaSerializer(serializers.Serializer):
    paths = serializers.SerializerMethodField()

    def get_paths(self, obj):
        paths = {
            'audio': [],
            'video': [],
            'url': []
        }
        path = join(settings.STREAMS_DIR, str(obj.id))
        if not exists(path):
            return []
        for f in listdir(path):
            if not isfile(join(path, f)):
                continue
            if f.startswith('audio'):
                paths['audio'].append(f)
            elif f.startswith('video'):
                paths['video'].append(f)
        if obj.ext_video_url:
            paths['url'].append(obj.ext_video_url)
        return paths


class SessionSlidesSerializer(serializers.Serializer):
    slides = serializers.SerializerMethodField()
    content = serializers.SerializerMethodField()

    def get_slides(self, obj):
        result = []
        slides_dir = join(settings.STREAMS_DIR, str(obj.id), 'slides')
        timetablefile = join(slides_dir, 'timetable.txt')
        if not exists(timetablefile):
            return []
        with open(timetablefile, 'r') as f:
            for line in f:
                m = re.match(r'^Slide (\d+): (.+)$', line.strip())
                if not m:
                    continue
                slide_file = m.group(1) + '.jpg'
                for t_part in m.group(2).split():
                    seconds = convert_to_seconds(t_part)
                    if seconds is None:
                        continue
                    result.append({
                        'id': int(m.group(1)),
                        'file': slide_file,
                        'seconds': seconds
                    })
        result.sort(key=lambda x: x['seconds'])
        return result

    def get_content(self, obj):
        result = []
        path = join(settings.STREAMS_DIR, str(obj.id), 'slides_text')
        if not exists(path):
            return []
        for f in listdir(path):
            if not isfile(join(path, f)):
                continue
            m = re.match(r'^(\d+).txt$', f)
            if not m:
                continue
            with open(join(path, f), 'r') as f:
                text_content = f.read()
            result.append({
                'id': int(m.group(1)),
                'text': text_content
            })
        return result


class SessionSubtitlesSerializer(serializers.Serializer):
    vtt = serializers.SerializerMethodField()

    def get_vtt(self, obj):
        vtt_dir = join(settings.STREAMS_DIR, str(obj.id), 'vtt')
        if not exists(vtt_dir):
            return []
        result = []
        for f in listdir(vtt_dir):
            name, ext = splitext(f)
            if isfile(join(vtt_dir, f)) and ext == '.vtt':
                result.append({
                    'lang': name,
                    'file': f
                })
        result = sorted(result, key=lambda x: x['lang'])
        return result

class EventIntegrateSerializer(serializers.Serializer):
    title           = serializers.CharField(max_length=256)
    start           = serializers.DateTimeField()
    stop            = serializers.DateTimeField()
    lecture_term    = serializers.PrimaryKeyRelatedField(queryset=LectureTerm.objects.all())
    fingerprint_in  = serializers.CharField(max_length=64, source="fingerprint")
    video           = serializers.FileField(allow_empty_file=False, write_only=True)
    title           = serializers.CharField(max_length=200, required=False)
    pw              = serializers.CharField(max_length=64, required=False)

    def create(self, validated_data):
        print("Integrating a new event...")

        # ==== Create event in database ====
        event_data = {
            "lecture_term" : validated_data["lecture_term"],
            "fingerprint"  : validated_data["fingerprint"],
            "title"        : validated_data["title"],
            "start"        : validated_data["start"],
            "stop"         : validated_data["stop"]
        }
        event = Event(**event_data)
        event.save() # Need to save it here otherwise we won't have an id # TODO delete on error?
        print("Created event {} for lectureterm {}".format(event, event.lecture_term))

        # ==== Write file to where script expects it ====
        video = validated_data["video"]
        showname = "ltreq-{}-{}".format(event.id, video.name)
        out_file_path = join(str(settings.ROOT_DIR), "temp/DIVAKIT", showname + ".mp4") # The script always expects an mp4 file even if it isn't one
        with open(out_file_path, "wb") as f: # TODO IOError
            for ch in video.chunks():
                f.write(ch)
        print("Wrote uploaded video file to '{}'.".format(out_file_path))

        # ==== Run integration script in background process ====
#        showname = u"ltreq-{}-{}-{}".format(event.id, event.title, video.name)
        if event.fingerprint == "de": # TODO actually handle output fingerprint
            fingerprint_out = "en"
        else:
            fingerprint_out = "de"
        log_file = join(str(settings.ROOT_DIR), "log", showname + ".log")
        args = [
            join(str(settings.ROOT_DIR), "temp/DIVAKIT/IntegrationClick.sh"),
            str(showname),
            str(event.id),
            str(event.fingerprint),
            str(fingerprint_out)
        ]
        t = threading.Thread(target=self._run_async, args=(args, log_file))
        t.daemon= True
        t.start()

        return event

    def _run_async(self, args, log_file):
        """
        Calling the script via subprocess directly async from the Django thread will not work
        """
        with open(log_file, "w") as out:
            proc = subprocess.run(args, stdout=out, stderr=subprocess.STDOUT)
        if self.pw is not None and self.pw != "":
            event.session.auth = AuthEntity(password=self.pw)
            event.session.save()
        print("Event integrate done")

def convert_to_seconds(t):
    m = re.match(r'^(\d+):(\d+):(\d+).(\d+)$', t)
    if m:
        return timedelta(
            hours=int(m.group(1)),
            minutes=int(m.group(2)),
            seconds=int(m.group(3)),
            milliseconds=int(m.group(4))
        ).total_seconds()
    else:
        return None

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import MediaEvent
from guardian.admin import GuardedModelAdmin

admin.site.register(MediaEvent, GuardedModelAdmin)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.conf import settings
from django.dispatch import receiver

from lecturetranslator.mediator.signals import session_destroy

from .tasks import (
    process_media_event,
    process_session_audio,
    process_subtitles,
)


@receiver(session_destroy, dispatch_uid='mediaprocessor_session_destroy')
def session_destroy_handler(sender, session, **kwargs): #pylint: disable=W0613
    if not settings.CELERY_ENABLED:
        return
    if session.media_events.count() > 0:
        process_media_event.delay(session.id)
    else:
        process_session_audio.delay(session.id)
    process_subtitles.delay(session.id)

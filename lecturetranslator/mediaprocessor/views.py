#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from rest_framework.generics import RetrieveAPIView, ListAPIView, CreateAPIView
from rest_framework.parsers import MultiPartParser, FormParser
from django_filters.rest_framework import DjangoFilterBackend

from lecturetranslator.common.permissions import IsStaffOrReadOnly

from lecturetranslator.session.models import Session
from lecturetranslator.session.permissions import IsSessionAuthenticated, IsSessionMediaAccessible

from .filters import MediaSourceFilter
from .models import MediaEvent
from .serializers import (
    MediaEventSerializer,
    SessionMediaSerializer,
    SessionSlidesSerializer,
    SessionSubtitlesSerializer,
    EventIntegrateSerializer
)


class SessionMedia(RetrieveAPIView):
    queryset = Session.objects.all()
    serializer_class = SessionMediaSerializer
    permission_classes = [IsStaffOrReadOnly, IsSessionAuthenticated, IsSessionMediaAccessible]

    def get_serializer_context(self):
        return {
            'request': self.request
        }


class SessionSlides(RetrieveAPIView):
    queryset = Session.objects.all()
    serializer_class = SessionSlidesSerializer
    permission_classes = [IsStaffOrReadOnly, IsSessionAuthenticated, IsSessionMediaAccessible]

    def get_serializer_context(self):
        return {
            'request': self.request
        }


class SessionSubtitles(RetrieveAPIView):
    queryset = Session.objects.all()
    serializer_class = SessionSubtitlesSerializer
    permission_classes = [IsStaffOrReadOnly, IsSessionAuthenticated, IsSessionMediaAccessible]

    def get_serializer_context(self):
        return {
            'request': self.request
        }


class MediaEventList(ListAPIView):
    queryset = MediaEvent.objects \
        .select_related(
            'event',
            'event__lecture_hall',
            'event__lecture_term',
            'event__lecture_term__lecture',
        ).order_by('start').all()
    serializer_class = MediaEventSerializer
    permission_classes = [IsStaffOrReadOnly]
    filter_backends = (DjangoFilterBackend,)
    filterset_class = MediaSourceFilter

class EventIntegrate(CreateAPIView):
    """
    This will create a new event with media data integrated via a hardcoded script.
    It is a very temporary and basic solutions until the better OfflineTask framework gets merged and used.
    If you don't have the script this won't be much use to you.
    Before trying to use this please check the models for the BaseOfflineTask/OfflineTranscription etc.

    Required fields:
        title:
            String, title of the event, max length 256 characters
        start:
        stop:
            String, start/stop date and time of this event according to iso-8601:
            YYYY-MM-DDThh:mm[:ss[.uuuuuu]][+HH:MM|-HH:MM|Z]
        lecture_term:
            Integer, id of the lecture term this event should be included in
        fingerprint_in:
            String, input language fingerprint, eg "de" or "en"
        video:
            File, multimedia file that should contain at least an audio track that will be used for transcription/translation.
    """
    parser_classes      = (MultiPartParser, FormParser,)
    serializer_class    = EventIntegrateSerializer
    permission_classes  = [IsStaffOrReadOnly]

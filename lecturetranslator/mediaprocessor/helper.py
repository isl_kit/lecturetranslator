#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import string
import random
from os.path import join, exists
from os import makedirs
from datetime import datetime
from shutil import rmtree

from django.conf import settings


def random_name():
    chars = string.ascii_letters + string.digits
    return ''.join(random.choice(chars) for _ in range(8))


def media_dir(session_id, parts=None, assure=False):
    """ Get media directory for specific session

        Args:
            session_id (int): Session ID to which the directory should be returned.
            parts (path, optional): Additional directories that will be added to
                the path.
            assure (boolean, optional): true: Assure the directory exists.

        Returns:
            Path to specific media directory
    """

    if parts is None:
        parts = []
    path = join(settings.STREAMS_DIR, str(session_id), *parts)
    if assure:
        assure_dir(path)
    return path


def proc_dir(session_id, parts=None, assure=False):
    """ Get Path to proc-directory

        Args:
            session_id (int): Session ID to which the directory should be returned.
            parts (path, optional): Additional directories that will be added to
                the path.
            assure (boolean, optional): true: Assure the directory exists.

        Returns:
            Path to specific proc directory
    """

    if parts is None:
        parts = []
    path = join(settings.STREAMS_DIR, str(session_id), 'proc', *parts)
    if assure:
        assure_dir(path)
    return path


def timestamped_dir(session_id, name):
    """ Get path to timestamped proc-directory
        Args:
            session_id (int): Session ID to which the directory should be returned.
            name (string): Name of directory in Session directory.
    """

    ts = datetime.now().strftime('%Y-%m-%dT%H-%M-%S')
    dir_name = '{}_{}'.format(ts, name)
    return proc_dir(session_id, parts=[dir_name], assure=True)


def random_dir(session_id):
    dir_name = random_name()
    return proc_dir(session_id, parts=[dir_name], assure=True)


def assure_dir(path):
    """" Assure that specific directory exists.

        If it doesn't exist create the directory.

        Args:
            path (path.path): Path to specific folder.
    """

    if not exists(path):
        makedirs(path)
    return path


def fresh_dir(path):
    """" Create new directory

        If directory already exists delete it and recreate it.

        Args:
            path (path): path to directory that should be cleared
    """

    if exists(path):
        rmtree(path, ignore_errors=True)
    makedirs(path)
    return path

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

class MediaProcessorConfig(AppConfig):
    name = 'lecturetranslator.mediaprocessor'
    verbose_name = _('mediaprocessor')

    def ready(self):
        import lecturetranslator.mediaprocessor.signals  # noqa

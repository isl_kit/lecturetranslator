#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime, timedelta

from django_filters.rest_framework import FilterSet, NumberFilter

from .models import MediaEvent

class MediaSourceFilter(FilterSet):
    offset_pre = NumberFilter(
        label='offset_pre',
        method='offset_pre_filter',
        help_text='\n'.join((
            '### Filter mediaEvents by start/stop time',
            'Defines (together with `offset_post`) a time interval that centers around the current time and reaches `offset_pre` minutes into the past and `offset_post` minutes into the future. All mediaEvents are matched that overlap with this interval.'
        ))
    )
    offset_post = NumberFilter(
        label='offset_post',
        method='offset_post_filter',
        help_text='\n'.join((
            '### Filter mediaEvents by start/stop time',
            'Defines (together with `offset_pre`) a time interval that centers around the current time and reaches `offset_pre` minutes into the past and `offset_post` minutes into the future. All mediaEvents are matched that overlap with this interval.'
        ))
    )

    class Meta:
        model = MediaEvent
        fields = {
            'start': ['lt', 'gt'],
            'session': ['isnull'],
            'event': ['exact'],
            'offset_pre': ['exact'],
            'offset_post': ['exact']
        }

    def offset_pre_filter(self, queryset, name, value):
        d = datetime.now() - timedelta(minutes=int(value))
        return queryset.filter(stop__gte=d)

    def offset_post_filter(self, queryset, name, value):
        d = datetime.now() + timedelta(minutes=int(value))
        return queryset.filter(start__lte=d)

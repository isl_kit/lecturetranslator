#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import os
import shutil
import subprocess as sp


RAW_INPUT_OPTIONS = {
    'ar': 16000,
    'ac': 1,
    'f': 's16le'
}


MP4_OUTPUT_OPTIONS = {
    'c:v': 'libx264',
    'preset': 'veryfast',
    'vf': 'scale=-1:1080,format=yuv420p',
    'video_track_timescale': 600
}


AAC_OUTPUT_OPTIONS = {
    'c:a': 'aac',
    'b:a': '256k',
    'strict': 2,
    'af': 'dynaudnorm'
}


WAV_OUTPUT_OPTIONS = {
    'c:a': 'pcm_s16le',
    'ac': 1,
    'ar': 16000,
    'af': 'dynaudnorm'  #audio normalization
}


def subp(cmd, file_out=os.devnull):
    f = open(file_out, 'a+')
    f.write('#############\n')
    f.write(' '.join(cmd) + '\n')
    f.write('#############\n')
    print((' '.join(cmd)))
    p = sp.Popen(cmd, stdout=f, stderr=f)
    p.communicate()
    f.close()


def ffmpeg(sources, target, output_options=None):
    if output_options is None:
        output_options = {}
    options = []
    for source in sources:
        if isinstance(source, str):
            options += ['-i', source]
        else:
            source_name = source['name']
            input_options = source.get('options', {})
            for key, val in list(input_options.items()):
                options += ['-' + key]
                if val != '' and val is not None:
                    options += [str(val)]
            options += ['-i', source_name]

    for key, values in list(output_options.items()):
        if not isinstance(values, list):
            values = [values]
        for val in values:
            options += ['-' + key]
            if val:
                options += [str(val)]
    cmd = ['ffmpeg', '-y'] + options + ['-threads', '2', target]
    subp(cmd)


def merge_dicts(*args):
    """ Merge multiple dictionaries into one

        Args:
            *args (dictionaries) : multiple dictionaries to be merged.

        Returns:
            Merged dictionary
    """

    res = {}
    for d in args:
        if not d:
            continue
        for key, val in list(d.items()):
            res[key] = val
    return res


def raw_to_mp4(source, target, extras=None):
    ffmpeg([
        {
            'name': source,
            'options': RAW_INPUT_OPTIONS
        }
    ], target, output_options=merge_dicts(AAC_OUTPUT_OPTIONS, extras))


def raw_to_wav(source, target, extras=None):
    ffmpeg([
        {
            'name': source,
            'options': RAW_INPUT_OPTIONS
        }
    ], target, extras)


def cut_or_extend_raw(source, target, dur):
    bytes_dest = int(dur * 16000) * 2
    bytes_current = os.path.getsize(source)
    bytes_delta = bytes_dest - bytes_current
    shutil.copy(source, target)
    if bytes_delta < 0:
        with open(target, 'rb+') as f:
            f.seek(bytes_delta, os.SEEK_END)
            f.truncate()
    else:
        with open(target, 'ab') as f:
            f.write(b'\x00' * bytes_delta)


def concat_raw(sources, target):
    """ Join all files from 'sources' to 'target' file.

    Args:
        sources (path array): path to files that should be joined
        target (path): path to target-file

    """
    with open(target, 'wb+') as f:
        for infile in sources:
            shutil.copyfileobj(open(infile, 'rb'), f)


def any_to_mp4(source, target):
    ffmpeg([source], target, output_options=merge_dicts(
        MP4_OUTPUT_OPTIONS, AAC_OUTPUT_OPTIONS
    ))


def merge_video_audio(video, audio, target, video_offset=0):
    ffmpeg([{
        'name': video,
        'options': {
            'ss': video_offset
        }
    }, audio], target, output_options=merge_dicts(MP4_OUTPUT_OPTIONS, AAC_OUTPUT_OPTIONS, {
        'map': ['0:v:0', '1:a:0']
    }))

def any_to_wav(source, target, extras=None):
    """ Convert file in wav file

        Args:
            source (path): path to source file
            target (path): path to target file
            extras (
    """
    ffmpeg([source], target, output_options=merge_dicts({'vn': ''}, WAV_OUTPUT_OPTIONS, extras))


def create_empty_video(source, target, dur, extras=None):
    ffmpeg([source], target, output_options=merge_dicts({
        'vf': 'trim=0:{},geq=0:128:128'.format(dur),
        'af': 'atrim=0:{},volume=0'.format(dur),
        'video_track_timescale': 600
    }, extras))


def concat_same(video1, video2, audio, target):
    ffmpeg([video1, video2, audio], target, output_options={
        'filter_complex': '[0:v:0] [1:v:0] concat=n=2:v=1:a=0 [v]',
        'map': ['[v]', '2:a:0']
    })


def resize_image(source, dest, width, file_out=os.devnull):
    subp([
        'convert', source, '-resize', str(width), dest
    ], file_out=file_out)


def get_video_params(path):
    frmt = get_media_format(path)
    stream = None
    for st in frmt['streams']:
        if st['codec_type'] == 'video':
            stream = st
            break
    if stream is None:
        return None
    rate_parts = stream['r_frame_rate'].split('/')
    return {
        'width': stream['width'],
        'height': stream['height'],
        'fps': round(int(rate_parts[0]) * 1.0 / int(rate_parts[1])),
        'pix_fmt': stream.get('pix_fmt', None)
    }


def get_media_format(path):
    cmd = [
        'ffprobe',
        '-loglevel', 'quiet',
        '-print_format', 'json',
        '-show_streams',
        '-show_format', path
    ]
    p = sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.STDOUT)
    out, _ = p.communicate()
    return json.loads(out)


def get_media_duration(path):
    _, ext = os.path.splitext(path)
    if ext == '.raw':
        return os.path.getsize(path) / 32000.0
    else:
        frmt = get_media_format(path)
        return round(float(frmt['format']['duration']), 3)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.db import models

from lecturetranslator.event.models import Event
from lecturetranslator.session.models import Session


class MediaEvent(models.Model):
    URL = 'url'
    FILE = 'file'
    MEDIA_TYPE_CHOICES = (
        (URL, 'Url'),
        (FILE, 'File'),
    )
    start = models.DateTimeField()
    stop = models.DateTimeField()
    audio_path = models.CharField(max_length=2048)
    path = models.CharField(max_length=2048)
    media_type = models.CharField(max_length=5, choices=MEDIA_TYPE_CHOICES, default=FILE)
    fingerprint = models.CharField(max_length=200)
    event = models.ForeignKey(
        Event, models.CASCADE,
        related_name='media_events'
    )
    session = models.ForeignKey(
        Session, models.SET_NULL,
        related_name='media_events',
        null=True, blank=True
    )

    def __unicode__(self):
        return '{} {}-{} ({})'.format(
            self.media_type,
            self.start.strftime("%a %d.%m.%Y %H:%M"),
            self.stop.strftime("%H:%M"),
            self.path
        )

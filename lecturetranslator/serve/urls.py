#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^streams/(?P<session_id>[0-9]+)/', views.serve_stream),
    url(r'^files/(?P<file_hash>[0-9a-f]+)/(?P<file_name>.+)/', views.serve_file)
]

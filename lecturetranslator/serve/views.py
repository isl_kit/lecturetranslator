#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from os.path import join, sep, exists

from django_sendfile import sendfile

from django.shortcuts import get_object_or_404
from django.http import Http404
from django.views.decorators.http import require_GET
from django.conf import settings

from lecturetranslator.lecture.permissions import has_media_access
from lecturetranslator.session.auth import check_session_auth
from lecturetranslator.session.models import Session
from lecturetranslator.session.permissions import is_accessible, is_media_accessible
from lecturetranslator.resources.models import Document


@require_GET
def serve_stream(request, session_id):
    session = get_object_or_404(Session, pk=int(session_id))
    authorized = is_accessible(request, session) and \
        check_session_auth(request, session_id) and \
        is_media_accessible(request, session)
    if not authorized:
        raise Http404("not authorized")
    sub_path = sep.join(request.path.split(sep)[2:])
    return sendfile(request, join(settings.SENDFILE_ROOT, sub_path))


@require_GET
def serve_file(request, file_hash, file_name):
    documents = Document.objects \
        .select_related('event', 'event__lecture_term') \
        .prefetch_related('event__lecture_term__lecturers') \
        .filter(resource__hash=file_hash)
    if documents.count() == 0:
        raise Http404("not authorized")
    document = documents[0]
    if document.event is not None:
        if not has_media_access(request, document.event.lecture_term):
            raise Http404("not authorized")
    elif document.user != request.user:
        raise Http404("not authorized")
    file_path = join(settings.SENDFILE_ROOT, "files", file_hash, file_name)
    if not exists(file_path):
        raise Http404("file does not exist")
    if file_name == "orig":
        return sendfile(request, file_path, attachment=True, attachment_filename=document.name)
    else:
        return sendfile(request, file_path, mimetype="text/plain")

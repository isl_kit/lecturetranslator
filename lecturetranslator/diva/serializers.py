#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from rest_framework import serializers

from lecturetranslator.lecture.models import LectureTerm

from .models import DivaCollection


class DivaCollectionSerializer(serializers.ModelSerializer):
    lecture_term = serializers.PrimaryKeyRelatedField(queryset=LectureTerm.objects.all())

    class Meta:
        model = DivaCollection
        fields = ('id', 'key', 'lecture_term')
        read_only_fields = ('id',)

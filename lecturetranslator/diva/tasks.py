#!/usr/bin/env python3
# -*- coding: utf-8 -*-



import requests

from celery import shared_task

from lecturetranslator.mediaprocessor.tasks import process_online_source

from .models import DivaCollection, DivaItem

DIVA_URL = 'https://mediaservice.bibliothek.kit.edu/asset/collection.json/?collection='


@shared_task
def diva_sync_task():
    diva_sync()


def diva_sync():
    diva_collections = DivaCollection.objects \
        .select_related('lecture_term') \
        .prefetch_related('items', 'lecture_term__events') \
        .all()
    for diva_collection in diva_collections:
        ids = [i.key for i in diva_collection.items.all()]
        r = requests.get(DIVA_URL + diva_collection.key, verify=False)
        coll = r.json()
        for item in coll['result']['resultList']:
            item_id = item['id']
            if item_id in ids:
                continue
            event = find_event(diva_collection.lecture_term, item['date'])
            if not event:
                continue
            diva_item = DivaItem.objects.create(
                key=item_id,
                url=item['resourceList']['derivateList']['mp4']['url'],
                collection=diva_collection,
                event=event
            )
            process_online_source.delay(event.session_id, diva_item.url)


def find_event(lecture_term, date):
    events = [e for e in lecture_term.events.all() if e.start.strftime('%d.%m.%Y') == date]
    if len(events) != 1:
        return None
    event = events[0]
    if event.session is None:
        return None
    return event

#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from ...tasks import diva_sync

class Command(BaseCommand):

    def handle(self, *args, **options):
        diva_sync()


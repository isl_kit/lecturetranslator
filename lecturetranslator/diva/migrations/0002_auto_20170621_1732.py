# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2017-06-21 17:32


from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('diva', '0001_initial'),
        ('event', '0002_event_session'),
        ('mediaprocessor', '0001_initial'),
        ('lecture', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='divaitem',
            name='event',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to='event.Event'),
        ),
        migrations.AddField(
            model_name='divaitem',
            name='media_event',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='mediaprocessor.MediaEvent'),
        ),
        migrations.AddField(
            model_name='divacollection',
            name='lecture_term',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to='lecture.LectureTerm'),
        ),
    ]

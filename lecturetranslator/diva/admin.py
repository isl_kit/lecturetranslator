#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.contrib import admin
from guardian.admin import GuardedModelAdmin

from .models import DivaItem, DivaCollection


admin.site.register(DivaCollection, GuardedModelAdmin)
admin.site.register(DivaItem, GuardedModelAdmin)

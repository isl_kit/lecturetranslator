#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from django_filters.rest_framework import DjangoFilterBackend

from lecturetranslator.common.permissions import IsStaffOrReadOnly

from .filter import DivaCollectionFilter
from .models import DivaCollection
from .serializers import (
    DivaCollectionSerializer,
)


class DivaCollectionList(ListCreateAPIView):
    queryset = DivaCollection.objects.all()
    permission_classes = [IsStaffOrReadOnly]
    filter_backends = (DjangoFilterBackend,)
    filterset_class = DivaCollectionFilter
    serializer_class = DivaCollectionSerializer


class DivaCollectionDetail(RetrieveUpdateDestroyAPIView):
    queryset = DivaCollection.objects.all()
    permission_classes = [IsStaffOrReadOnly]
    serializer_class = DivaCollectionSerializer

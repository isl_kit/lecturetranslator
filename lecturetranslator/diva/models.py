#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.db import models

from lecturetranslator.lecture.models import LectureTerm
from lecturetranslator.event.models import Event
from lecturetranslator.mediaprocessor.models import MediaEvent


class DivaCollection(models.Model):
    key = models.CharField(max_length=36, unique=True)
    lecture_term = models.ForeignKey(
        LectureTerm, models.CASCADE,
        related_name='+'
    )


    def __unicode__(self):
        return '{} ({})'.format(
            self.lecture_term.lecture.slug,
            self.key
        )


class DivaItem(models.Model):
    key = models.CharField(max_length=36, unique=True)
    url = models.CharField(max_length=4000)
    collection = models.ForeignKey(
        DivaCollection, models.CASCADE,
        related_name='items'
    )
    event = models.ForeignKey(
        Event, models.CASCADE,
        related_name='+'
    )
    media_event = models.ForeignKey(
        MediaEvent, models.SET_NULL,
        null=True, blank=True,
        related_name='+'
    )

    def __unicode__(self):
        return '{}'.format(
            self.key
        )

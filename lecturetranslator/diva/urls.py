#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.conf.urls import url
from .views import (
    DivaCollectionList,
    DivaCollectionDetail
)

urlpatterns = [
    url(r'^$', DivaCollectionList.as_view()),
    url(r'^(?P<pk>[0-9a-f\-]+)/$', DivaCollectionDetail.as_view()),
]

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from typing import Dict, List, Any

from django_filters import filters
from django_filters.rest_framework import FilterSet

from lecturetranslator.lecture.models import LectureTerm

from .models import DivaCollection


class DivaCollectionFilter(FilterSet):
    lecture_term = filters.ModelChoiceFilter(
        queryset=LectureTerm.objects.all(),
        help_text='ID of related LectureTerm'
    )
    class Meta:
        model = DivaCollection
        fields: Dict[str, List[Any]] = {
            'lecture_term': []
        }

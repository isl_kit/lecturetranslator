#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^$', views.ResourceList.as_view()),
    url(r'^upload/$', views.DocumentUpload.as_view()),
    url(r'^nodes/$', views.NodeList.as_view()),
    url(r'^nodes/set/$', views.NodeSet.as_view()),
    url(r'^nodes/copy/$', views.NodeCopy.as_view()),
    url(r'^nodes/move/$', views.NodeMove.as_view()),
    url(r'^nodes/tree/$', views.NodeTree.as_view()),
    url(r'^nodes/(?P<pk>[0-9]+)/$', views.NodeDetail.as_view()),
    url(r'^tags/$', views.DocumentTagList.as_view()),
]

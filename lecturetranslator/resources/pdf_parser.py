#!/usr/bin/env python3
# -*- coding: utf-8 -*-



import traceback
from io import StringIO

from langdetect import detect

from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage


password = ''
maxpages = 0
codec = 'utf-8'
caching = True
pagenos = set() # type: ignore
laparams = LAParams(detect_vertical=True)


def clean_text(text, language_tag):
    if language_tag == 'ro':
        return text.replace('\\u00DB', '\\u0219').replace('\\u0015', '\\u021B')
    return text.replace('\\u0015', '').replace('\\u0002', '')


def isProperText(txt):
    if len(txt) < 3:
        return False
    nrOfAlphaNum = 0
    nrOfNonAlphaNum = 0
    for ch in txt:
        if ch.isalpha() is True:
            nrOfAlphaNum += 1
        else:
            nrOfNonAlphaNum += 1
    if nrOfAlphaNum > nrOfNonAlphaNum:
        return True
    return False


def convert_pdf_to_text(filepath, outputfile):
    rsrcmgr = PDFResourceManager(caching=caching)
    error_occured = False
    error = ''
    res_pages = []
    fp = file(filepath, 'rb')
    try:
        pages = PDFPage.get_pages(
            fp, pagenos,
            maxpages=maxpages, password=password,
            caching=caching, check_extractable=True
        )
        for page in pages:
            retstr = StringIO()
            device = TextConverter(
                rsrcmgr, retstr, codec=codec, laparams=laparams, imagewriter=None
            )
            try:
                interpreter = PDFPageInterpreter(rsrcmgr, device)
                interpreter.process_page(page)
                retstr.seek(0)
                isPrevEmptyLine = True
                raw_lines = [str(line.strip(), codec) for line in retstr.readlines()]
                paragraphs = []
                lines = []
                for raw_line in raw_lines:
                    if isProperText(raw_line):
                        lines.append(raw_line)
                        isPrevEmptyLine = False
                    if not raw_line and not isPrevEmptyLine:
                        paragraphs.append(' '.join(lines))
                        lines = []
                        isPrevEmptyLine = True
                res_pages.append('\n\n'.join(paragraphs))
                device.close()
            except:
                device.close()
                raise
    except:
        error_occured = True
        error = traceback.format_exc()
    fp.close()

    if error_occured is False:
        text = '\n\n\n'.join(res_pages)
        tag = detect(text)
        text = clean_text(text, tag)
        with open(outputfile, 'wb+') as openfile:
            openfile.write(text.encode('utf-8'))
        return {
            'converted': True,
            'error': error
        }
    else:
        return {
            'converted': False,
            'error': error
        }

#!/usr/bin/env python3
# -*- coding: utf-8 -*-



from celery import shared_task

from .pdf_parser import convert_pdf_to_text
from .ppt_parser import convert_pptx_to_text
from .txt_parser import convert_text_to_text

@shared_task
def pdf2text(source_file, out_file):
    convert_pdf_to_text(source_file, out_file)


@shared_task
def pptx2text(source_file, out_file):
    convert_pptx_to_text(source_file, out_file)


@shared_task
def text2text(source_file, out_file):
    convert_text_to_text(source_file, out_file)

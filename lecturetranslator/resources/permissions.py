#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from rest_framework import permissions

from lecturetranslator.event.models import Event
from lecturetranslator.lecture.permissions import can_upload_files


class FilespaceAccessPermission(permissions.BasePermission):
    message = 'Accessing filespace not allowed.'

    def has_permission(self, request, view):
        return request.user.has_perm('resources.can_upload')


class FilespaceAccessOrReadOnlyPermission(permissions.BasePermission):
    message = 'Accessing filespace not allowed.'

    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True
        return request.user.has_perm('resources.can_upload')


class UploadPermission(permissions.BasePermission):
    message = 'Uploading not allowed.'

    def has_permission(self, request, view):
        parent = request.data.get('parent', None)
        event = request.data.get('event', None)
        if event and not parent:
            try:
                event = Event.objects \
                    .select_related('lecture_term') \
                    .prefetch_related('lecture_term__lecturers') \
                    .get(id=int(event))
            except Event.DoesNotExist:
                return False
            return can_upload_files(request, event.lecture_term)
        elif not event:
            return request.user.has_perm('resources.can_upload')
        return False


class IsOwnerPermission(permissions.BasePermission):
    message = 'No Ownership for this object.'

    def has_object_permission(self, request, view, obj):
        if obj.user == request.user:
            return True
        if obj.event is None:
            return False
        if request.method in permissions.SAFE_METHODS:
            return True
        return can_upload_files(request, obj.event.lecture_term)

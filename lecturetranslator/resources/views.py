#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.db.models import Case, Count, IntegerField, When
from django.http import Http404

from django_filters.rest_framework import DjangoFilterBackend

from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.generics import (
    ListAPIView,
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView,
)
from rest_framework.permissions import IsAuthenticated
from rest_framework.parsers import FormParser
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED
from rest_framework.views import APIView

from lecturetranslator.common.generics import BulkListDestroyAPIView

from .filters import NodeFilter, ResourceFilterBackend
from .models import DocumentTag, Node, Resource
from .parsers import MultiPartJSONParser
from .permissions import (
    FilespaceAccessPermission,
    FilespaceAccessOrReadOnlyPermission,
    IsOwnerPermission,
    UploadPermission
)
from .serializers import (
    DirectoryCreateSerializer,
    DocumentCreateSerializer,
    DocumentTagSerializer,
    NodeCopySerializer,
    NodeMoveSerializer,
    NodeDetailSerializer,
    NodeSimpleSerializer,
    NodeSerializer,
    ResourceDetailSerializer,
    ResourceHashOnlySerializer,
)


class NodeList(ListCreateAPIView):
    permission_classes = (FilespaceAccessOrReadOnlyPermission,)
    filter_backends = (DjangoFilterBackend, SearchFilter, OrderingFilter)
    filterset_class = NodeFilter
    search_fields = ('name',)
    ordering_fields = ('name', 'created')
    ordering = ('name',)

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return DirectoryCreateSerializer
        return NodeSerializer

    def get_queryset(self):
        nodes = Node.objects
        if self.request.query_params.get('type', None) == 'DIR':
            nodes = nodes.annotate(number_of_children=Count(Case(
                When(children__node_type=Node.DIR, then=1),
                output_field=IntegerField(),
            )))
        else:
            nodes = nodes.annotate(number_of_children=Count('children'))
        nodes = nodes.prefetch_related('tags') \
            .select_related('document', 'directory', 'document__resource')
        if 'event' in self.request.query_params:
            nodes = nodes.filter(event_id=int(self.request.query_params['event']))
        elif 'lterm' in self.request.query_params:
            nodes = nodes.filter(event__lecture_term_id=int(self.request.query_params['lterm']))
        else:
            nodes = nodes.filter(user=self.request.user)
        return nodes


class NodeDetail(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsOwnerPermission, )
    serializer_class = NodeDetailSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_class = NodeFilter

    def get_queryset(self):
        return Node.objects \
            .annotate(number_of_children=Count('children')) \
            .prefetch_related('tags') \
            .select_related('document', 'directory', 'document__resource') \
            .all()


class NodeTree(ListAPIView):
    permission_classes = (FilespaceAccessPermission,)
    serializer_class = NodeSimpleSerializer

    def get_queryset(self):
        root_ids = []
        if 'id' in self.request.query_params:
            node_id = int(self.request.query_params['id'])
            try:
                node = Node.objects.get(user=self.request.user, id=node_id, node_type=Node.DIR)
            except Node.DoesNotExist:
                raise Http404
            node = node.parent
            while node:
                root_ids.insert(0, node.id)
                node = node.parent
        nodes = list(
            Node.objects.annotate(number_of_children=Count(Case(
                When(children__node_type=Node.DIR, then=1),
                output_field=IntegerField(),
            )))
            .filter(parent=None, user=self.request.user, node_type=Node.DIR)
            .order_by('name')
        )
        for root_id in root_ids:
            nodes += list(
                Node.objects
                .annotate(number_of_children=Count(Case(
                    When(children__node_type=Node.DIR, then=1),
                    output_field=IntegerField(),
                )))
                .filter(parent_id=root_id, node_type=Node.DIR)
                .order_by('name')
            )
        return nodes


class NodeSet(BulkListDestroyAPIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = NodeSerializer

    def get_queryset(self):
        return Node.objects.annotate(number_of_children=Count('children')) \
            .prefetch_related('tags') \
            .select_related('document', 'directory', 'document__resource') \
            .filter(user=self.request.user)



class NodeCopy(APIView):
    permission_classes = (FilespaceAccessPermission,)

    def post(self, request, format=None): # pylint: disable=W0613,W0622
        serializer = NodeCopySerializer(
            data=request.data,
            context={'request': request}
        )
        serializer.is_valid(raise_exception=True)
        nodes = serializer.save()
        result = [NodeSerializer(instance=n).data for n in nodes]
        return Response(result, status=HTTP_201_CREATED)


class NodeMove(APIView):
    permission_classes = (FilespaceAccessPermission,)

    def post(self, request, format=None): # pylint: disable=W0613,W0622
        serializer = NodeMoveSerializer(
            data=request.data,
            context={'request': request}
        )
        serializer.is_valid(raise_exception=True)
        nodes = serializer.save()
        result = [NodeSerializer(instance=n).data for n in nodes]
        return Response(result)


class ResourceList(ListAPIView):
    permission_classes = ()
    filter_backends = (ResourceFilterBackend,)

    def get_queryset(self):
        return Resource.objects.all()

    def get_serializer_class(self):
        if 'id_only' in self.request.query_params:
            return ResourceHashOnlySerializer
        return ResourceDetailSerializer


class DocumentUpload(APIView):
    parser_classes = (MultiPartJSONParser, FormParser)
    permission_classes = (IsAuthenticated, UploadPermission,)
    serializer_class = DocumentCreateSerializer

    def post(self, request, format=None): # pylint: disable=W0613,W0622
        if 'parent' not in request.data:
            request.data['parent'] = None
        if 'event' not in request.data:
            request.data['event'] = None
        serializer = DocumentCreateSerializer(
            data=request.data,
            context={'request': request}
        )
        serializer.is_valid(raise_exception=True)
        document = serializer.save()
        serializer = NodeSerializer(instance=document)
        return Response(serializer.data, status=HTTP_201_CREATED)


class DocumentTagList(ListCreateAPIView):
    queryset = DocumentTag.objects.all()
    permission_classes = (FilespaceAccessPermission,)
    serializer_class = DocumentTagSerializer
    filter_backends = (SearchFilter, OrderingFilter)
    search_fields = ('name', 'description')
    ordering_fields = ('name', 'description')

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.contrib import admin
from guardian.admin import GuardedModelAdmin

from .models import Document, Resource, DocumentTag, Node


admin.site.register(Document, GuardedModelAdmin)
admin.site.register(DocumentTag, GuardedModelAdmin)
admin.site.register(Resource, GuardedModelAdmin)
admin.site.register(Node, GuardedModelAdmin)

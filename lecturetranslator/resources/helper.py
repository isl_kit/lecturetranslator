#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from lecturetranslator.event.models import Event

from .models import Node


def get_nodes(*args, **kwargs):
    def _get_nodes(request):
        if request.user.is_authenticated:
            return Node.objects.filter(user=request.user, **kwargs)
        else:
            return Node.objects.none()
    return _get_nodes

#!/usr/bin/env python3
# -*- coding: utf-8 -*-



from traceback import format_exc

from pptx import Presentation


def convert_pptx_to_text(filepath, outputfile):
    try:
        prs = Presentation(filepath)
        res_pages = []
        for slide in prs.slides:
            lines = []
            for shape in slide.shapes:
                text = getattr(shape, 'text', '')
                if text:
                    lines.append(text)
            res_pages.append('\n\n'.join(lines))
        text = '\n\n\n'.join(res_pages)
        with open(outputfile, 'wb+') as openfile:
            openfile.write(text.encode('utf-8'))
        return {
            'converted': True,
            'error': ''
        }
    except:
        return {
            'converted': False,
            'error': format_exc()
        }

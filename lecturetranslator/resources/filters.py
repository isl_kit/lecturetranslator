#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.db.models import Q

from django_filters.filters import (
    ChoiceFilter,
    DateTimeFilter,
    ModelChoiceFilter,
    NumberFilter
)
from django_filters.rest_framework import FilterSet

from rest_framework.filters import BaseFilterBackend

from .helper import get_nodes
from .models import Directory, Node


class NodeFilter(FilterSet):
    ids = NumberFilter(field_name='id')
    created = DateTimeFilter()
    parent = ModelChoiceFilter(queryset=get_nodes(node_type=Node.DIR))
    type = ChoiceFilter(field_name='node_type', choices=Node.FILE_TYPE_CHOICES)
    class Meta:
        model = Node
        fields = {
            'created': ['lt', 'gt'],
            'ids': [],
            'parent': ['isnull'],
            'type': ['in']
        }


class ResourceFilterBackend(BaseFilterBackend):

    def filter_queryset(self, request, queryset, view):
        lterm = request.query_params.get('lterm', 0)
        event = request.query_params.get('event', 0)
        if event and lterm == 0:
            queryset = queryset.filter(documents__event_id=event).distinct()
        else:
            queryset = queryset.filter(documents__event__lecture_term_id=lterm).distinct()
        return queryset

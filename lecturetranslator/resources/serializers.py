#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from typing  import Tuple
from hashlib import sha256
from os.path import exists

from django.db import IntegrityError, transaction

from magic import from_buffer as magic_from_buffer

from rest_framework.serializers import (
    CurrentUserDefault,
    FileField,
    IntegerField,
    ListField,
    ModelSerializer,
    PrimaryKeyRelatedField,
    Serializer,
    SerializerMethodField,
    SlugField,
    ValidationError,
)

from lecturetranslator.common.fields import CreatableSlugRelatedField, DynamicPrimaryKeyRelatedField
from lecturetranslator.event.models import Event

from .helper import get_nodes
from .models import Directory, Document, DocumentTag, Node, Resource


class ResourceSerializer(ModelSerializer):
    text_exists = SerializerMethodField()
    class Meta:
        model = Resource
        fields = ('hash', 'size', 'text_exists')

    def get_text_exists(self, obj):
        return exists(obj.get_text_path())


class ResourceDetailSerializer(ModelSerializer):
    text = SerializerMethodField()
    class Meta:
        model = Resource
        fields = ('hash', 'text')

    def get_text(self, obj):
        text_path = obj.get_text_path()
        if not exists(text_path):
            return ''
        with open(text_path, 'r') as f:
            text = f.read()
        return text.decode('utf-8')


class ResourceHashOnlySerializer(ModelSerializer):
    class Meta:
        model = Resource
        fields = ('hash',)


class DocumentTagSerializer(ModelSerializer):
    class Meta:
        model = DocumentTag
        fields = ('name', 'description')


class DirectorySerializer(ModelSerializer):
    class Meta:
        model = Directory
        fields = ()


class DocumentSerializer(ModelSerializer):
    resource = ResourceSerializer(read_only=True)

    class Meta:
        model = Document
        fields = ('resource',)


class NodeSimpleSerializer(ModelSerializer):
    number_of_children = IntegerField(default=0, read_only=True, required=False)
    parent = DynamicPrimaryKeyRelatedField(
        allow_null=True,
        queryset=get_nodes(node_type=Node.DIR),
    )
    class Meta:
        model = Node
        fields = (
            'created',
            'id',
            'name',
            'node_type',
            'number_of_children',
            'parent',
        )


class NodeSerializer(ModelSerializer):
    directory = DirectorySerializer(read_only=True)
    document = DocumentSerializer(read_only=True)
    number_of_children = IntegerField(default=0, read_only=True, required=False)
    event = DynamicPrimaryKeyRelatedField(
        allow_null=True,
        queryset=Event.objects.all()
    )
    parent = DynamicPrimaryKeyRelatedField(
        allow_null=True,
        queryset=get_nodes(node_type=Node.DIR),
    )
    tags = CreatableSlugRelatedField(
        many=True,
        slug_field='name',
        queryset=DocumentTag.objects.all().order_by('name')
    )
    user = PrimaryKeyRelatedField(
        default=CurrentUserDefault(),
        read_only=True
    )
    class Meta:
        model = Node
        fields: Tuple[str, ...] = (
            'created',
            'directory',
            'document',
            'id',
            'name',
            'node_type',
            'number_of_children',
            'parent',
            'tags',
            'user',
            'event'
        )
        read_only_fields = (
            'created',
            'id',
            'node_type',
            'number_of_children'
        )


class DirectoryCreateSerializer(ModelSerializer):
    number_of_children = IntegerField(default=0, read_only=True, required=False)
    parent = DynamicPrimaryKeyRelatedField(
        allow_null=True,
        queryset=get_nodes(node_type=Node.DIR),
    )
    tags = CreatableSlugRelatedField(
        many=True,
        slug_field='name',
        queryset=DocumentTag.objects.all().order_by('name')
    )
    user = PrimaryKeyRelatedField(
        default=CurrentUserDefault(),
        read_only=True
    )
    class Meta:
        model = Directory
        fields = (
            'created',
            'id',
            'name',
            'node_type',
            'number_of_children',
            'parent',
            'tags',
            'user',
        )
        read_only_fields = (
            'created',
            'id',
            'node_type',
            'number_of_children'
        )


class NodeDetailSerializer(NodeSerializer):
    ancestors = SerializerMethodField()
    parent = DynamicPrimaryKeyRelatedField(
        allow_null=True,
        read_only=True,
    )

    class Meta(NodeSerializer.Meta):
        fields = NodeSerializer.Meta.fields + ('ancestors',)

    def get_ancestors(self, obj):
        result = []
        parent = obj.parent
        while parent:
            serializer = NodeSerializer(instance=parent)
            result.append(serializer.data)
            parent = parent.parent
        return result


class NodeCopySerializer(Serializer):
    nodes = DynamicPrimaryKeyRelatedField(
        many=True,
        queryset=get_nodes()
    )
    destination = DynamicPrimaryKeyRelatedField(
        allow_null=True,
        queryset=get_nodes(node_type=Node.DIR)
    )

    def validate(self, data):
        parent = data['nodes'][0].parent
        for node in data['nodes'][1:]:
            if node.parent != parent:
                raise ValidationError('all nodes must have the same parent')
        return data

    def copy_node(self, node, parent, done_ids):
        if node.id in done_ids:
            return None
        if node.node_type == Node.DIR:
            node_copy = Directory(
                name=node.name,
                user=node.user,
                parent=parent,
                node_type=Node.DIR
            )
        else:
            node_copy = Document(
                name=node.name,
                user=node.user,
                parent=parent,
                node_type=Node.DOC,
                resource=node.document.resource
            )
        node_copy.save()
        node_copy.tags.set(node.tags.all())
        if node.node_type == Node.DIR:
            for sub_node in Node.objects.filter(parent=node):
                self.copy_node(sub_node, node_copy, done_ids + [node_copy.id])
        return node_copy

    def save(self):
        copy_nodes = []
        parent = self.validated_data['destination']
        for node in self.validated_data['nodes']:
            nopy_copy = self.copy_node(node, parent, [])
            copy_nodes.append(nopy_copy)
        return copy_nodes


class NodeMoveSerializer(NodeCopySerializer):
    def validate(self, data):
        data = super(NodeMoveSerializer, self).validate(data)
        p_node = data['destination']
        while p_node:
            if p_node in data['nodes']:
                raise ValidationError('cannot move node into itself')
            p_node = p_node.parent
        return data

    def save(self):
        ids = [node.id for node in self.validated_data['nodes']]
        nodes = Node.objects.filter(id__in=ids)
        nodes.update(parent=self.validated_data['destination'])
        return list(nodes)


class DocumentCreateSerializer(Serializer):
    file = FileField()
    tags = ListField(child=SlugField(max_length=64, min_length=2))
    event = DynamicPrimaryKeyRelatedField(
        allow_null=True,
        queryset=Event.objects.all()
    )
    parent = PrimaryKeyRelatedField(
        allow_null=True,
        queryset=get_nodes(node_type=Node.DIR),
    )
    user = PrimaryKeyRelatedField(
        default=CurrentUserDefault(),
        read_only=True
    )

    def validate(self, data):
        MAX_FILE_SIZE = 52428800 # 50MB
        MIME_TYPES = [
            'application/pdf',
            'application/vnd.openxmlformats-officedocument.presentationml.presentation'
        ]
        ufile = data['file']
        size = ufile.size
        if size > MAX_FILE_SIZE:
            raise ValidationError('exeeded file size limit (50MB)')
        mimetype = magic_from_buffer(ufile.read(), mime=True)
        ufile.seek(0)
        if mimetype not in MIME_TYPES and not mimetype.startswith('text/'):
            raise ValidationError('not supported file type')
        return data

    def save(self):
        ufile = self.validated_data['file']
        resource = get_or_create_resource(ufile)
        document = Document(
            resource=resource,
            name=str(ufile),
            user=self.validated_data['user'],
            parent=self.validated_data['parent'],
            event=self.validated_data['event'],
            node_type=Node.DOC
        )
        document.save()
        for tag_name in self.validated_data['tags']:
            tag = get_or_create_tag(tag_name)
            document.tags.add(tag)
        return document


def create_hash(ufile):
    sha256_hash = sha256()
    for chunk in iter(lambda: ufile.read(128 * sha256_hash.block_size), b''):
        sha256_hash.update(chunk)
    return sha256_hash.hexdigest()


def get_or_create_resource(ufile):
    file_hash = create_hash(ufile)
    try:
        with transaction.atomic():
            resource = Resource(hash=file_hash, size=ufile.size)
            resource.save(ufile=ufile)
    except IntegrityError:
        resource = Resource.objects.get(hash=file_hash)
    return resource


def get_or_create_tag(name):
    try:
        tag = DocumentTag.objects.get(name=name)
    except DocumentTag.DoesNotExist:
        tag = DocumentTag(name=name, description='')
        tag.save()
    return tag

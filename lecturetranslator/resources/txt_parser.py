#!/usr/bin/env python3
# -*- coding: utf-8 -*-



from traceback import format_exc

import chardet


def convert_text_to_text(filepath, outputfile):
    try:
        with open(filepath, 'r') as f:
            text = f.read()
        try:
            text = text.decode('utf-8')
        except UnicodeDecodeError:
            encoding = chardet.detect(text)['encoding']
            text = text.decode(encoding)
        with open(outputfile, 'wb+') as f:
            f.write(text.encode('utf-8'))
        return {
            'converted': True,
            'error': ''
        }
    except UnicodeDecodeError:
        return {
            'converted': False,
            'error': format_exc()
        }

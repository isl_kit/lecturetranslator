#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from os import chmod, makedirs
from os.path import join, exists
from shutil import rmtree

from django.db import models
from django.conf import settings

from magic import from_file as magic_from_file

from lecturetranslator.event.models import Event

from .tasks import pdf2text, pptx2text, text2text


class Resource(models.Model):
    hash = models.CharField(max_length=64, unique=True, db_index=True)
    mimetype = models.CharField(max_length=255, blank=True)
    size = models.IntegerField()

    def get_path(self):
        return join(settings.MEDIA_ROOT, 'files', self.hash)

    def get_orig_path(self):
        return join(self.get_path(), 'orig')

    def get_text_path(self):
        return join(self.get_path(), 'content.txt')

    def save(self, ufile=None, *args, **kwargs):
        if ufile is not None:
            path = self.get_path()
            orig_path = self.get_orig_path()
            if not exists(path):
                makedirs(path, mode=0o777)
                chmod(path, 0o777)
                with open(orig_path, 'wb+') as destination:
                    for chunk in ufile.chunks():
                        destination.write(chunk)
                self.mimetype = magic_from_file(orig_path, mime=True)
                self.process()
        return super(Resource, self).save(*args, **kwargs)

    def process(self):
        if exists(self.get_text_path()) or not settings.CELERY_ENABLED:
            return
        if self.mimetype == 'application/pdf':
            pdf2text.delay(self.get_orig_path(), self.get_text_path())
        elif self.mimetype == 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
            pptx2text.delay(self.get_orig_path(), self.get_text_path())
        elif self.mimetype.startswith('text/'):
            text2text.delay(self.get_orig_path(), self.get_text_path())

    def delete(self, *args, **kwargs):
        path = self.get_path()
        if exists(path):
            rmtree(path)
        return super(Resource, self).delete(*args, **kwargs)

    def __unicode__(self):
        return self.hash


class DocumentTag(models.Model):
    name = models.SlugField(max_length=64, unique=True, db_index=True)
    description = models.TextField(blank=True)

    class Meta:
        ordering = ['name']

    def __unicode__(self):
        return self.name


class Node(models.Model):
    DOC = 'DOC'
    DIR = 'DIR'
    FILE_TYPE_CHOICES = (
        (DOC, 'Document'),
        (DIR, 'Directory')
    )
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=255)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE, related_name='nodes')
    parent = models.ForeignKey('Node', models.CASCADE, null=True, related_name='children')
    node_type = models.CharField(max_length=3, choices=FILE_TYPE_CHOICES, default=DIR)
    tags = models.ManyToManyField(DocumentTag, related_name='nodes')
    event = models.ForeignKey(
        Event, models.SET_NULL,
        related_name='nodes', null=True, blank=True
    )

    @property
    def number_of_children(self):
        return getattr(self, '_number_of_children', 0)

    @number_of_children.setter
    def number_of_children(self, value):
        self._number_of_children = value

    def __unicode__(self):
        return self.name

    def delete(self, *args, **kwargs):
        for node in Node.objects.filter(parent=self):
            node.delete()
        del_resource = self.node_type == Node.DOC and self.document.resource.documents.count() <= 1
        if del_resource:
            resource = self.document.resource
        return_val = super(Node, self).delete(*args, **kwargs)
        if del_resource:
            resource.delete()
        return return_val


class Directory(Node):

    def __unicode__(self):
        return self.name


class Document(Node):
    resource = models.ForeignKey(Resource, on_delete=models.CASCADE, related_name='documents')

    class Meta:
        permissions = (
            ('can_upload', 'Can upload Documents'),
        )

    def __unicode__(self):
        return self.name

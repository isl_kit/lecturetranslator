#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.db import models


class LectureHall(models.Model):
    name = models.CharField(unique=True, max_length=200)

    def __unicode__(self):
        return self.name


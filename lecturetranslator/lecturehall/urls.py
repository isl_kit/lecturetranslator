#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.conf.urls import url
from .views import LectureHallDetail, LectureHallList

urlpatterns = [
    url(r'^$', LectureHallList.as_view()),
    url(r'^(?P<pk>[0-9]+)/$', LectureHallDetail.as_view())
]

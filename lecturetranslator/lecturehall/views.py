#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.db.models import Count

from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.filters import SearchFilter, OrderingFilter

from lecturetranslator.common.permissions import IsStaffOrReadOnly

from .models import LectureHall
from .serializers import LectureHallSerializer


class LectureHallList(ListCreateAPIView):
    queryset = LectureHall.objects \
        .annotate(lecture_count=Count('events__lecture_term', distinct=True)).all()
    serializer_class = LectureHallSerializer
    filter_backends = (SearchFilter, OrderingFilter)
    search_fields = ('name',)
    ordering_fields = ('name',)
    ordering = ('name',)
    permission_classes = [IsStaffOrReadOnly]


class LectureHallDetail(RetrieveUpdateDestroyAPIView):
    queryset = LectureHall.objects \
        .annotate(lecture_count=Count('events__lecture_term', distinct=True)).all()
    serializer_class = LectureHallSerializer
    permission_classes = [IsStaffOrReadOnly]


#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from rest_framework import serializers

from .models import LectureHall


class LectureHallSerializer(serializers.ModelSerializer):
    lecture_count = serializers.IntegerField(read_only=True)
    class Meta:
        model = LectureHall
        fields = ('id', 'name', 'lecture_count')
        read_only_fields = ('id',)


class LectureHallRelatedSerializer(serializers.ModelSerializer):
    class Meta:
        model = LectureHall
        fields = ('id', 'name')

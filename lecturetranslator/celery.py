

import os
from time import sleep
from celery import Celery
from django.apps import apps, AppConfig
from django.conf import settings


if not settings.configured:
    # set the default Django settings module for the 'celery' program.
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings.local')


app = Celery('lecturetranslator', broker='amqp://')


class CeleryConfig(AppConfig):
    #name = 'lecturetranslator.taskapp'
    name = 'celery'
    verbose_name = 'Celery Config'

    def ready(self):
        app.config_from_object('django.conf:settings')
        installed_apps = [app_config.name for app_config in apps.get_app_configs()]
        app.autodiscover_tasks(lambda: installed_apps, force=True)


@app.task(bind=True)
def debug_task(self):
    sleep(10)
    print('Request: {0!r}'.format(self.request))



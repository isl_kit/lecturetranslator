from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group, Permission
from django.utils.translation import ugettext_lazy as _

from guardian.admin import GuardedModelAdmin

from .models import LTUser, AuthEntity

class LTUserAdmin(UserAdmin):

    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'is_subscriber', 'has_logged_in', 'is_webdav_initialized', 'webdav_alias', 'has_enabled_logging', 'languages')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}
        ),
    )

    #form = EmailUserChangeForm
    #add_form = EmailUserCreationForm
    list_display = ('email', 'first_name', 'last_name', 'is_staff', 'is_subscriber', 'has_logged_in', 'is_webdav_initialized', 'has_enabled_logging')
    search_fields = ('email', 'first_name', 'last_name')
    ordering = ('email',)

admin.site.register(LTUser)
admin.site.register(Permission)
admin.site.register(AuthEntity, GuardedModelAdmin)

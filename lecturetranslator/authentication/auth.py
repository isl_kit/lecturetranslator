#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from guardian.shortcuts import assign_perm, get_objects_for_user

from lecturetranslator.lecturer.models import Lecturer

from .models import AuthEntity


def check_login(request, auth_entity):
    # check is_authenticated == True needed because returning Django's CallableBool() creates internal problems
    # (CallableBool(False) is False) evaluates as False (correct but in this case not expected since apparently implicitly used)
    # (CallableBool(False) == False) evaluates as True
    # CallableBool will probably removed sometime during 2.x
    return ((
        "logged_in_ids" in request.session and auth_entity.id in request.session["logged_in_ids"]
    ) or request.user.has_perm("can_access", auth_entity) or (
        (request.user.is_authenticated == True) and Lecturer.objects.filter( 
            lectureterm__auth=auth_entity, user=request.user
        ).exists()
    ))


def authenticate(request, auth_entity, password):
    if not auth_entity.authenticate(password):
        return False
    if "logged_in_ids" not in request.session:
        request.session["logged_in_ids"] = []
    if auth_entity.id not in request.session["logged_in_ids"]:
        request.session["logged_in_ids"].append(auth_entity.id)
        request.session.modified = True
    if request.user.is_authenticated:
        assign_perm("can_access", request.user, auth_entity)
    return True


def get_all_authenticated(request):
    ids1 = list(get_objects_for_user(
        request.user,
        "can_access",
        AuthEntity.objects.all()
    ).values_list('id', flat=True))
    ids2 = request.session["logged_in_ids"] if "logged_in_ids" in request.session else []
    return list(set(ids1 + ids2))

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
from django.contrib.auth import get_user_model, login, authenticate

from rest_framework import serializers

from .auth import check_login
from .models import AuthEntity, add_user_to_group


all_perms = set([
    'session.can_record',
    'resources.can_upload'
])


class LoginSerializer(serializers.Serializer):
    name    = serializers.CharField(max_length=30)
    password    = serializers.CharField()

    def validate(self, data):
        name = data['name']
        password = data['password']
        user = authenticate(username=name, password=password)

        if not user:
            raise serializers.ValidationError('Could not authenticate')
        if not user.is_active:
            raise serializers.ValidationError('The user is not active')

        return {'user': user}

    def save(self):
        user = self.validated_data['user']
        login(self.context['request'], user)
        return user


class AuthEntitySerializer(serializers.ModelSerializer):
    authenticated = serializers.SerializerMethodField()

    class Meta:
        model = AuthEntity
        fields = ('id', 'password', 'authenticated')
        extra_kwargs = {
            'id': {'read_only': True},
            'password': {'write_only': True}
        }

    def get_authenticated(self, obj):
        if 'request' not in self.context:
            return False
        return check_login(self.context['request'], obj)

    def create(self, validated_data):
        authEntity = AuthEntity()
        authEntity.set_password(validated_data['password'])
        authEntity.save()
        return authEntity


class UserSerializer(serializers.ModelSerializer):
    groups = serializers.SerializerMethodField()
    permissions = serializers.SerializerMethodField()

    class Meta:
        model = get_user_model()
        fields = (
            'id', 'name', 'first_name', 'last_name',
            'groups', 'permissions', 'is_staff', 'is_transcript_corrector'
        )

    def get_groups(self, obj):
        return [group.name for group in obj.groups.all()]

    def get_permissions(self, obj):
        return [x.split('.')[1] for x in obj.get_all_permissions() if x in all_perms]


class UserGetOrCreateSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=30)

    def create(self, validated_data):
        User = get_user_model()
        username = validated_data['name']
        try:
            user = User.objects.get(name=username)
        except User.DoesNotExist:
            user = User.objects.create_user(username)
            user.external = False
            user.save()
            if re.match('^[a-z]{2}[0-9]{4}$', username):
                add_user_to_group(user, 'employee')
            if re.match('^u[a-z]{4}$', username):
                add_user_to_group(user, 'student')
        return user

    def to_representation(self, user):
        return UserSerializer(user).data

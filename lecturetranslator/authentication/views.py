#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json

from django.http import JsonResponse
from django.views.decorators.http import require_POST
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required

from rest_framework.generics import RetrieveAPIView, ListCreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated

from lecturetranslator.common.permissions import IsStaffOrReadOnly, IsStaff

from .auth import authenticate as authenticateEntity
from .models import AuthEntity, LTUser
from .serializers import AuthEntitySerializer, UserGetOrCreateSerializer, UserSerializer, LoginSerializer

from django.contrib.sessions.models import Session
from django.contrib.auth.models import User

from django.conf import settings


class Login(APIView):
    def post(self, request, format=None):
        # Deserialize request and validate it
        serializer = LoginSerializer(
            data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        # Is valid
        # Login and return user data
        user = serializer.save()
        result = UserSerializer(user).data
        return Response(result)


class Logout(APIView):
    def post(self, request, format=None):
        logout(request)
        return Response()


class Me(RetrieveAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = UserSerializer

    def get_object(self):
        return self.request.user


class AuthEntityDetail(RetrieveAPIView):
    queryset = AuthEntity.objects.all()
    permission_classes = [IsStaffOrReadOnly]
    serializer_class = AuthEntitySerializer

    def get_serializer_context(self):
        return {
            'request': self.request
        }


class AuthEntityAuthenticate(APIView):
    def post(self, request, format=None, pk=0):
        try:
            auth_entity = AuthEntity.objects.get(pk=pk)
        except AuthEntity.DoesNotExist:
            return Response(status=400)
        password = request.data['password']
        if not authenticateEntity(request, auth_entity, password):
            return Response(status=403)
        return Response({'authenticated': True})


class UserView(ListCreateAPIView):
    queryset = LTUser.objects.all()
    permission_classes = [IsStaff]

    def get_serializer_class(self):
        if self.request.method == 'POST':
            # TODO Modify Serializer to CreateSerializer
            return UserGetOrCreateSerializer
        else:
            return UserSerializer


class IsCorrectorView(APIView):
    def get(self, request, format=None):
        session_key = request.COOKIES.get(settings.SESSION_COOKIE_NAME)
        try:
            session = Session.objects.get(session_key=session_key)
        except Session.DoesNotExist:
            return Response(False)
        session_data = session.get_decoded()
        uid = session_data.get('_auth_user_id')
        user = LTUser.objects.get(id=uid)
        return Response(user.is_transcript_corrector or user.is_staff)

from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.db import models
from django.utils import timezone

class LTUserManager(BaseUserManager):

    def _create_user(self, name, password, is_staff, is_superuser, **extra_fields):
        now = timezone.now()
        user = self.model(
            name=name,
            is_staff=is_staff,
            is_active=True,
            is_superuser=is_superuser,
            last_login=now,
            date_joined=now,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, name, password=None, **extra_fields):
        return self._create_user(name, password, False, False, **extra_fields)

    def create_superuser(self, name, password, **extra_fields):
        return self._create_user(name, password, True, True, **extra_fields)


class LTUser(AbstractBaseUser, PermissionsMixin):
    name = models.CharField(max_length=30, unique=True)
    first_name = models.CharField(max_length=30, blank=True)
    last_name = models.CharField(max_length=30, blank=True)
    is_staff = models.BooleanField(default=False)
    is_transcript_corrector = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=timezone.now)
    has_logged_in = models.BooleanField(default=False)
    password_was_resetted = models.BooleanField(default=False)
    external = models.BooleanField(default=True)

    objects = LTUserManager()

    USERNAME_FIELD = "name"
    EMAIL_FIELD = ""
    REQUIRED_FIELDS = [] # type: ignore

    class Meta:
        verbose_name = "user"
        verbose_name_plural = "users"

    def get_full_name(self):
        return self.name

    def get_short_name(self):
        return self.name


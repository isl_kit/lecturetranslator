#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
from os import urandom
from hashlib import sha256

# The custom user model needs to be known before some of the django imports
from .model_user import LTUserManager, LTUser

from django.db import models
from django.contrib.auth.models import Group
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth import get_user_model

from .ldap_service import authenticate as ldap_authenticate


def add_user_to_group(user, group_name):
    if not user.groups.filter(name=group_name).exists():
        try:
            group = Group.objects.get(name=group_name)
        except:
            group = Group(name=group_name)
            group.save()
        user.groups.add(group)


class LDAPBackend(ModelBackend):

    def verify_credentials(self, name, password):
        return ldap_authenticate(name, password)

    def authenticate(self, request, username=None, password=None):
        User = get_user_model()
        try:
            user = User.objects.get(name=username)
        except User.DoesNotExist:
            user = None

        if user and user.external and user.check_password(password):
            return user

        if not self.verify_credentials(username, password):
            return None

        if user and user.external:
            return None

        if not user:
            user = User.objects.create_user(username)
            user.external = False
            user.save()
            if re.match("^[a-z]{2}[0-9]{4}$", username):
                add_user_to_group(user, "employee")
            if re.match("^u[a-z]{4}$", username):
                add_user_to_group(user, "student")
        return user

    def get_user(self, user_id):
        User = get_user_model()
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


class AuthEntity(models.Model):
    password = models.CharField(max_length=64, blank=True)
    salt = models.CharField(max_length=64, blank=True)

    class Meta:
        permissions = (
            ("can_access", "Can access"),
        )

    def __init__(self, *args, **kwargs):
        pw = None
        if 'password' in kwargs and 'salt' not in kwargs:
            pw = self._generate_password(kwargs['password'])
            del kwargs['password']

        super().__init__(*args, **kwargs)
        if pw is not None:
            self.salt = pw[1]
            self.password = pw[0]

    def authenticate(self, password):
        return sha256(str(self.salt + password).encode("utf-8")).hexdigest() == self.password

    def _generate_password(self, password):
        salt = sha256(urandom(64)).hexdigest()
        hash = sha256(str(salt + password).encode("utf-8")).hexdigest()
        return (hash, salt)

    def set_password(self, password):
        pw = self._generate_password(password)

        self.salt = pw[1]
        self.password = pw[0]

    def __unicode__(self):
        return str(self.id)

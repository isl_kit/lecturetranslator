#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.conf import settings

import ldap


ldap_server = "kit-ldap-01.scc.kit.edu"
base_dn = "ou=unix,ou=IDM,dc=kit,dc=edu"


def get_user_dn(uid):
    dn = None
    connect = ldap.initialize("ldap://" + ldap_server)
    connect.start_tls_s()
    try:
        connect.bind_s(settings.LDAP_DN, settings.LDAP_PASSWORD)
        result = connect.search_s(base_dn, ldap.SCOPE_SUBTREE, "uid=" + uid)
        if len(result) > 0:
            dn = result[0][0]
        connect.unbind_s()
    except ldap.LDAPError:
        connect.unbind_s()
    return dn

def check_password(dn, password):
    connect = ldap.initialize("ldap://" + ldap_server)
    connect.start_tls_s()
    try:
        connect.bind_s(dn, password)
        return True
    except ldap.LDAPError:
        connect.unbind_s()
        return False

def authenticate(username, password):
    if len(password) == 0:
        return False
    if not settings.LDAP_DN:
        return False
    dn = get_user_dn(username)
    if not dn:
        return False
    return check_password(dn, password)

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import getpass

from django.core.management.base import BaseCommand, CommandError

from ...models import add_user_to_group
from django.contrib.auth import get_user_model

class Command(BaseCommand):

    def get_user(self, username):
        User = get_user_model()
        try:
            user = User.objects.get(name=username)
            return user
        except User.DoesNotExist:
            return None

    def handle(self, *args, **options):
        User = get_user_model()
        username = input("Username: ")
        while self.get_user(username):
            print("User with name " + username + " already exists!")
            username = input("Username: ")

        p1 = getpass.getpass("Choose a password: ")
        p2 = getpass.getpass("Retype password: ")
        while p1 != p2:
            print("Passwords do not match. Try again")
            p1 = getpass.getpass("Password: ")
            p2 = getpass.getpass("Retype password: ")

        user = User.objects.create_user(username, password=p1)
        user.external = True
        user.save()
        add_user_to_group(user, "guest")


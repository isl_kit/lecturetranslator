#!/usr/bin/env python
# -*- coding: utf-8 -*-

import traceback
import ldap
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):


    def get_user_dn(self, uid):
        ldap_server = "kit-ldap-01.scc.kit.edu"
        user_dn = "uid=iar-lt,ou=ProxyUser,ou=IDM,dc=kit,dc=edu"
        password = "navtokCicofsyamdigci"
        base_dn = "ou=unix,ou=IDM,dc=kit,dc=edu"
        search_filter = "uid=" + uid
        dn = None
        connect = ldap.initialize("ldap://" + ldap_server)
        connect.start_tls_s()
        try:
            connect.bind_s(user_dn, password)
            result = connect.search_s(base_dn,ldap.SCOPE_SUBTREE,search_filter)
            if len(result) > 0:
                dn = result[0][0]
            connect.unbind_s()
        except ldap.LDAPError:
            connect.unbind_s()
        return dn

    def check_password(self, dn, password):
        ldap_server = "kit-ldap-01.scc.kit.edu"
        connect = ldap.initialize("ldap://" + ldap_server)
        connect.start_tls_s()
        try:
            connect.bind_s(dn, password)
            return True
        except ldap.LDAPError:
            connect.unbind_s()
            return False

    def handle(self, *args, **options):
        dn = self.get_user_dn("os1911")
        auth = self.check_password(dn, "wrong password")
        print(auth)
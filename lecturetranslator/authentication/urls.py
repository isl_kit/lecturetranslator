#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^login/$', views.Login.as_view()),
    url(r'^logout/$', views.Logout.as_view()),
    url(r'^me/$', views.Me.as_view()),
    url(r'^user/$', views.UserView.as_view()),
    url(r'^authItem/(?P<pk>[0-9]+)/$', views.AuthEntityDetail.as_view()),
    url(r'^authItem/(?P<pk>[0-9]+)/auth/$', views.AuthEntityAuthenticate.as_view()),
    url(r'^isCorrector/$', views.IsCorrectorView.as_view())
]

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from os import path

from django.conf import settings

from rest_framework.generics import RetrieveAPIView
from rest_framework.views import APIView
from rest_framework.response import Response

from lecturetranslator.common.permissions import IsStaffOrReadOnly
from lecturetranslator.session.permissions import IsSessionAccessible, IsSessionAuthenticated
from lecturetranslator.session.models import Stream

from .serializers import (
    StreamDataSerializer,
    StreamDataCorrectionsSerializer,
    StreamDataCorrectionsUpdateSerializer,
    StreamHistorySerializer,
    StreamDataSerializerDeprecated,
    StreamDataUpdateSerializerDeprecated
)

from .interface import (
    get_corrections_dir,
    get_public_dir,
    transfer_data
)

class StreamData(APIView):
    """
    Get or patch StreamData for session `pk` and fingerprint `fingerprint`.
    Having the WEBSOCKET_ACCESS_SECRET_KEY will override the permission_classes. This is needed for the offline-editing websocket server which needs access to the data.
    """
    permission_classes = [IsStaffOrReadOnly, IsSessionAccessible, IsSessionAuthenticated]

    def check_permissions(self, request):
        """
        Override permission check via permission_classes to first check for the secret key.
        Django Rest does not support complex permission concatenation without plugins so we do a one-time override for now.
        """
        if request.data == settings.WEBSOCKET_ACCESS_SECRET_KEY:
            return True
        elif request.user.is_authenticated and request.user.is_transcript_corrector:
            return True
        else:
            return super(type(self), self).check_permissions(request)

    def get(self, request, pk=0, fingerprint='', format=None):
        session_id = int(pk)
        stream = Stream.objects.get(
            session_id=session_id,
            fingerprint=fingerprint,
            type='text'
        )
        serializer = StreamDataSerializer(stream)
        return Response(serializer.data)


    def patch(self, request, pk=0, fingerprint='', format=None):
        session_id = int(pk)
        if not settings.EDITOR_ALLOW_COMMIT:
            print("Detected attempt to transfer corrections data to public transcript, but this feature was disabled. Session ID: {}, fingerprint: {}".format(session_id, fingerprint))
            return
        corrections_dir = get_corrections_dir(session_id, fingerprint)
        public_dir = get_public_dir(session_id, fingerprint)

        committer = None
        if request.user and request.user.is_authenticated:
            committer = request.user.name
        transfer_data(corrections_dir, public_dir, committer)
        return Response("OK")



class StreamDataHistory(RetrieveAPIView):
    permission_classes = [IsStaffOrReadOnly, IsSessionAccessible, IsSessionAuthenticated]
    queryset = Stream.objects.all()
    serializer_class = StreamHistorySerializer

    def get_object(self):
        session_id = int(self.kwargs['pk'])
        fingerprint = self.kwargs['fingerprint']
        return Stream.objects.get(session_id=session_id, fingerprint=fingerprint, type='text')


class StreamDataCorrections(APIView):
    """
    Get or patch StreamData for session `pk` and fingerprint `fingerprint`.
    Having the WEBSOCKET_ACCESS_SECRET_KEY will override the permission_classes. This is needed for the offline-editing websocket server which needs access to the data.
    """
    permission_classes = [IsStaffOrReadOnly, IsSessionAccessible, IsSessionAuthenticated]

    def check_permissions(self, request):
        """
        Override permission check via permission_classes to first check for the secret key.
        Django Rest does not support complex permission concatenation without plugins so we do a one-time override for now.
        """
        if request.data == settings.WEBSOCKET_ACCESS_SECRET_KEY:
            return True
        elif request.user.is_authenticated and request.user.is_transcript_corrector:
            return True
        else:
            return super(type(self), self).check_permissions(request)

    def get(self, request, pk=0, fingerprint='', format=None):
        session_id = int(pk)
        stream = Stream.objects.get(
            session_id=session_id,
            fingerprint=fingerprint,
            type='text'
        )
        serializer = StreamDataCorrectionsSerializer(stream)
        return Response(serializer.data)


    def patch(self, request, pk=0, fingerprint='', format=None):
        session_id = int(pk)
        serializer = StreamDataCorrectionsUpdateSerializer(data=request.data, context={
            'request': request,
            'session_id': session_id,
            'fingerprint': fingerprint
        })
        serializer.is_valid(raise_exception=True)
        data = serializer.save()
        return Response(data)


# DEPRECATED! This should be removed once we are sure that there are no more dependencies on it.
class StreamDataDeprecated(APIView):
    """
    Get or patch StreamData for session `pk` and fingerprint `fingerprint`.
    Having the WEBSOCKET_ACCESS_SECRET_KEY will override the permission_classes. This is needed for the offline-editing websocket server which needs access to the data.
    """
    permission_classes = [IsStaffOrReadOnly, IsSessionAccessible, IsSessionAuthenticated]

    def check_permissions(self, request):
        """
        Override permission check via permission_classes to first check for the secret key.
        Django Rest does not support complex permission concatenation without plugins so we do a one-time override for now.
        """
        if request.data == settings.WEBSOCKET_ACCESS_SECRET_KEY:
            return True
        elif request.user.is_authenticated and request.user.is_transcript_corrector:
            return True
        else:
            return super(type(self), self).check_permissions(request)

    def get(self, request, pk=0, fingerprint='', format=None):
        session_id = int(pk)
        stream = Stream.objects.get(
            session_id=session_id,
            fingerprint=fingerprint,
            type='text'
        )
        serializer = StreamDataSerializerDeprecated(stream)
        return Response(serializer.data)


    def patch(self, request, pk=0, fingerprint='', format=None):
        session_id = int(pk)
        serializer = StreamDataUpdateSerializerDeprecated(data=request.data, context={
            'request': request,
            'session_id': session_id,
            'fingerprint': fingerprint
        })
        serializer.is_valid(raise_exception=True)
        data = serializer.save()
        return Response(data)

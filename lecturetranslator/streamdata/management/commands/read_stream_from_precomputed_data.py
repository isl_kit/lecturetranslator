#!/usr/bin/env python
# -*- coding: utf-8 -*-

import shutil
import re

from django.conf import settings
from django.core.management.base import BaseCommand

from lecturetranslator.session.models import Session, Stream, DataSegment

from ...interface import get_bare_dir, get_public_dir
from ...tasks import fix_whitespaces, init_stream_data


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('sessionid', type=int)
        parser.add_argument('fingerprint', type=str)
        parser.add_argument('type', type=str)
        parser.add_argument('source', type=str)

    def handle(self, *args, **options):
        stream_type = options['type']
        fingerprint = options['fingerprint']
        session = Session.objects.get(id=options['sessionid'])
        with open(options['source'], 'r', encoding="utf-8") as f:
            lines = [t.strip() for t in f.readlines()]
        try:
            stream = Stream.objects.get(
                session=session,
                type=stream_type,
                fingerprint=fingerprint
            )
        except Stream.DoesNotExist:
            stream = Stream.objects.create(
                session=session,
                ident='{}_{}'.format(fingerprint, stream_type),
                type=stream_type,
                fingerprint=fingerprint,
                label=settings.GET_STREAM_LABEL(fingerprint),
                displayname='MANUAL {}/{}'.format(fingerprint, stream_type),
                control='l',
                sid='speech'
            )
            if session.input_fingerprint.split('-')[0] == fingerprint:
                session.transcript_stream = stream
                session.save()
            if session.transcript_stream == None:
                print("WARNING! The session does not have a transcript stream set. You will need to manually set the field 'transcript_stream' in the session to the stream with the correct fingerprint.")
            print("A new stream with id {} was created for the session.".format(stream.id))
        stream.data_segments.all().delete()
        for idx, line in enumerate(lines):
            self.handle_line(stream, line, (idx + 1) % 5 == 0)
        clear_data_stream(session.id, fingerprint)
        create_data_stream(session.id, fingerprint)

    def handle_line(self, stream, line, end_of_paragraph):
        parts = re.split(r'\s+', line, 2)
        start = int(float(parts[0]) * 1000)
        stop = int(float(parts[1]) * 1000)
        text = parts[2].strip()
        text = re.sub(r'\s+(\.|,|!|\?)', '\\1', text)
        text = re.sub(r'(¿)\s+', '\\1', text)
        text = re.sub(r'^(¿)?.', capitalize, text)
        text = re.sub(r'\s(%?)-(%?)\s', fix_dash, text)
        if end_of_paragraph:
            text += '<hr>'
        DataSegment.objects.create(
            stream=stream,
            start_abs=start,
            stop_abs=stop,
            start=start,
            stop=stop,
            offset=start,
            text=text,
            creator='MANUAL',
            end_of_paragraph=end_of_paragraph,
            version=1
        )


def create_data_stream(session_id, fingerprint):
    data = DataSegment.objects \
        .filter(stream__session_id=session_id, stream__fingerprint=fingerprint, stream__type='text') \
        .order_by('start') \
        .values(
            'text', 'start', 'stop', 'creator'
        )
    fix_whitespaces(data)
    init_stream_data(session_id, fingerprint, data)


def clear_data_stream(session_id, fingerprint):
    bare_dir = get_bare_dir(session_id, fingerprint)
    public_dir = get_public_dir(session_id, fingerprint)
    shutil.rmtree(bare_dir)
    shutil.rmtree(public_dir)


def capitalize(match):
    return match.group(0).upper()


def fix_dash(match):
    res = '-'
    if match.group(1) == '%':
        res = ' ' + res
    if match.group(2) == '%':
        res += ' '
    return res

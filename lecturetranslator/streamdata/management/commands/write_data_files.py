#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from lecturetranslator.session.models import Stream, DataSegment

from ...tasks import fix_whitespaces, init_stream_data


class Command(BaseCommand):

    def handle(self, *args, **options):
        session_id = 187694420859468997
        for stream in Stream.objects.filter(session_id=session_id, type='text'):
            data = DataSegment.objects \
                .filter(stream=stream) \
                .order_by('start_abs') \
                .values(
                    'text', 'start', 'stop', 'start_abs', 'stop_abs',
                    'offset', 'creator', 'created'
                )
            fix_whitespaces(data)
            init_stream_data(session_id, stream.fingerprint, data)


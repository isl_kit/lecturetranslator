#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime

from django.core.management.base import BaseCommand

from ...interface import read_data, deserialize_segment
from ...vc import read_file


class Command(BaseCommand):

    def handle(self, *args, **options):
        count = 10

        d1 = datetime.now()
        for _ in range(count):
            read_data(187694420859468997, 'de')
        d2 = datetime.now()
        for _ in range(count):
            full_text = read_file('/home/bkrueger/dev/LectureTranslator/lecturetranslator/media/streams/187694420859468997/data/de', 'HEAD', 'data.txt')
            data = []
            c = 0
            for line in full_text.split('\n'):
                segment, c = deserialize_segment(line, c)
                data.append(segment)
        d3 = datetime.now()

        print('FILE : {}ms'.format((d2 - d1).total_seconds() / count * 1000))
        print('GIT  : {}ms'.format((d3 - d2).total_seconds() / count * 1000))


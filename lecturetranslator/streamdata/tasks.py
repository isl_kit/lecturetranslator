#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import os.path
import re

from .vc import (
    init_repo,
    clone_repo,
    update,
    push_master
)

from .interface import (
    get_bare_dir,
    get_public_dir,
    write_data,
    get_data_dir,
    write_vtt
)

from .diff import (
    text_diff,
    apply_diff,
    DiffError
)

from .vc import (
    update
)

from .vtt import create_vtt_file_from_segments

def fix_whitespaces(data):
    count = len(data)
    for i, segment in enumerate(data):
        text = segment['text']
        if not text.endswith('<hr>') and i < count - 1:
            text += ' '
        if text.startswith('<hr>') and i > 0:
            data[i - 1]['text'] = data[i - 1]['text'].rstrip()
        segment['text'] = text


def init_stream_data(session_id, fingerprint, data):
    bare_dir = get_bare_dir(session_id, fingerprint)
    public_dir = get_public_dir(session_id, fingerprint)
    init_repo(bare_dir)
    clone_repo(bare_dir, public_dir)
    write_data(public_dir, data)
    update(public_dir, 'initial commit')
    push_master(public_dir)
    set_permissions(get_data_dir(session_id))


def get_full_text(data):
    return ''.join([s['text'] for s in data])


def sanitize_session_text(text):
    return re.sub(r'\s+', ' ', re.sub(r'\s*\n\s*', '<hr>', text))


def set_permissions(base_dir):
    os.chmod(base_dir, 0o777)
    for root, dirs, files in os.walk(base_dir):
        for d in dirs:
            os.chmod(os.path.join(root, d), 0o777)
        for f in files:
            os.chmod(os.path.join(root, f), 0o666)

def getUsersFromOperations(operations):
    users = set()
    for op in operations:
        if "change" in op:
            users.add(op["change"]["user"])
    if None in users:
        users.remove(None)
    return users


def keepOperation(op, usersToConsider):
    if "change" not in op:
        return True
    elif not op["change"]["user"]:
        # Operations by None are always added, especially in the first iteration
        return True
    else:
        changeType = op["change"]["changeType"]
        user = op["change"]["user"]
        wantedInsertion = changeType == "added" and user in usersToConsider
        unwantedRemoval = changeType == "removed" and user not in usersToConsider
        return wantedInsertion or unwantedRemoval


def operationsToText(operations, usersToConsider):
    operationsToKeep = list(
        filter(lambda op: keepOperation(op, usersToConsider), operations))
    insertions = list(map(lambda op: op["insert"], operationsToKeep))
    return "".join(insertions)

def commitChanges(prev_segments, new_text, message, username, committer, repo_dir, session_id, fingerprint):
    prevText = get_full_text(prev_segments)
    diff = text_diff(prevText, new_text)

    if diff is None:
        raise DiffError("Text contains too many differences, could not merge")
    elif diff == []:
        # No changes to the previous commit => we're done
        return prev_segments
    new_segments = apply_diff(prev_segments, diff)

    new_subtitle = create_vtt_file_from_segments(new_segments)
    write_vtt(session_id, fingerprint, new_subtitle)

    new_segments = [s for s in new_segments if len(s['text'].strip()) > 0]
    write_data(repo_dir, new_segments)

    update(repo_dir, message, username, committer)
    return new_segments

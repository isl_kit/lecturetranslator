#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from git import Repo
from git import Actor
from git import InvalidGitRepositoryError
import string


def init_repo(dir_path):
    Repo.init(dir_path, bare=True)


def get_head_sha(dir_path):
    repo = Repo(dir_path)
    return repo.heads.master.commit.hexsha

def sanitize_username(username):
    return ''.join(char.lower() for char in str(username) if char in string.printable and not char in string.whitespace)

def update(dir_path, message, author=None, committer=None):
    try:
        repo = Repo(dir_path)
    except InvalidGitRepositoryError:
        repo = Repo.init(dir_path)
    repo.git.add(A=True)
    if author:
        sanitized_author = sanitize_username(author)
        git_author = Actor(sanitized_author, sanitized_author + "@lecture-translator.kit.edu")
        git_committer = git_author  # If the committer is not specified, we just use the author as the committer
        if committer:
            sanitized_committer = sanitize_username(committer)
            git_committer = Actor(sanitized_committer, sanitized_committer + "@lecture-translator.kit.edu")

        repo.index.commit(message, author=git_author, committer=git_committer)
    else:
        repo.index.commit(message)


def read_file(dir_path, sha, file_path):
    repo = Repo.init(dir_path)
    return repo.git.show('{}:{}'.format(sha, file_path))


def switch_to_new_branch(dir_path, name, sha):
    repo = Repo(dir_path)
    repo.git.checkout(sha, b=name)


def merge_branch_to_master(dir_path, branch_name):
    repo = Repo(dir_path)
    master = repo.heads.master
    repo.head.reference = master
    repo.head.reset(index=True, working_tree=True)
    repo.git.merge(branch_name, squash=True)


def add_all(dir_path):
    repo = Repo(dir_path)
    repo.git.add(u=True)


def clone_repo(dir_path, cloned_path):
    Repo.clone_from(dir_path, cloned_path)


def push_master(dir_path):
    repo = Repo(dir_path)
    master = repo.heads.master
    repo.head.reference = master
    repo.head.reset(index=True, working_tree=True)
    repo.remotes.origin.push(refspec='master:master')


def pull_master(dir_path):
    repo = Repo(dir_path)
    master = repo.heads.master
    repo.head.reference = master
    repo.head.reset(index=True, working_tree=True)
    repo.remotes.origin.pull(refspec='master:master')

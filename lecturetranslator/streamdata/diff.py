#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import diff_match_patch as dmp_module
import difflib


def apply_diff(segments, diff):
    s_idx = 0
    segment = segments[0]
    for change in diff:
        idx1 = change['source'][0]
        idx2 = idx1 + change['source'][1]
        while segment and segment['last_idx'] < idx1:
            s_idx += 1
            segment = segments[s_idx] if s_idx < len(segments) else None
        while segment and segment['last_idx'] < idx2 - 1:
            segment = combine_segments(segment, segments[s_idx + 1])
            segments[s_idx] = segment
            del segments[s_idx + 1]
        if segment is None:
            segments[-1]['text'] += change['target']
            break
        local_idx1 = idx1 - segment['first_idx']
        local_idx2 = idx2 - segment['first_idx']
        new_text = '{}{}{}'.format(
            segment['text'][:local_idx1],
            change['target'],
            segment['text'][local_idx2:]
        )
        dl = len(new_text) - len(segment['text'])
        segment['first_idx'] -= dl
        segment['text'] = new_text

    # Patch holes in the segments (caused by removing entire segments) by adapting start and stop times
    ''' TODO: Find another way to do this. Right now it breaks the subtitles, e.g. when segments don't start at 00:00:00 initially (due to silence etc)
    prev_stop = 0  # Start at 0 so that the start of the first segment is always 00:00:00
    for segment in sorted(segments, key=lambda seg : seg['start']):
        time_diff = segment['start'] - prev_stop  # Hole between current and last segment
        segment['start'] -= time_diff
        segment['stop'] -= time_diff
        prev_stop = segment['stop']
    '''

    return segments


def combine_segments(s1, s2):
    return {
        'text': s1['text'] + s2['text'],
        'first_idx': s1['first_idx'],
        'last_idx': s2['last_idx'],
        'start': s1['start'],
        'stop': s2['stop'],
        'creator': s1['creator']
    }

DIFF_TYPE_EQUAL = 0
DIFF_TYPE_INSERT = 1
DIFF_TYPE_REMOVE = -1

def diff_to_op_and_pos(dmpDiff, cur_pos, prev_op, prev_op_is_delete):
    diff_type = dmpDiff[0]
    diff_target = dmpDiff[1]
    if diff_type == DIFF_TYPE_INSERT:
        source = (cur_pos, 0)
        if prev_op and prev_op_is_delete:
            # Combine delete and insert into one replace
            source = prev_op["source"]
        return({
            "source": source,
            "target": diff_target
        }, cur_pos + source[1])
    elif diff_type == DIFF_TYPE_REMOVE:
        return ({
            "source": (cur_pos, len(diff_target)),
            "target": ''
        }, cur_pos) # Do not update the position yet, this will be done in the next iteration
    else:
        # DIFF_TYPE_EQUAL
        return (None, cur_pos + len(diff_target))

def dmp_diffs_to_replace_diffs(dmpDiffs):
    cur_pos = 0
    prev_op = None
    prev_op_is_delete = False
    cur_op = None
    result_diffs = []
    # Iteratively convert dmp diffs into replace diffs, combining consecutive delete and inserts into one replace
    for i in range(0, len(dmpDiffs)):
        diff = dmpDiffs[i]
        diff_type = diff[0]
        diff_target = diff[1]
        if diff_type != DIFF_TYPE_INSERT and prev_op and prev_op_is_delete:
            # This delete was not added in the previous iteration and it cannot be combined with an insert, so add it now
            result_diffs.append(prev_op)
            cur_pos += prev_op["source"][1]

        (cur_op, cur_pos) = diff_to_op_and_pos(diff, cur_pos, prev_op, prev_op_is_delete)

        if not diff_type == DIFF_TYPE_REMOVE and cur_op:
            result_diffs.append(cur_op)
        prev_op = cur_op
        prev_op_is_delete = (diff_type == DIFF_TYPE_REMOVE)
    if prev_op and prev_op_is_delete:
        result_diffs.append(prev_op)
    return result_diffs

def text_diff(text1, text2):
    dmp = dmp_module.diff_match_patch()
    diffs = dmp.diff_main(text1, text2)
    dmp.diff_cleanupSemantic(diffs)

    return dmp_diffs_to_replace_diffs(diffs)

class DiffError(Exception):
    """ Raised when comparing two strings results in an invalid diff """
    pass

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

class StreamDataConfig(AppConfig):
    name = 'lecturetranslator.streamdata'
    verbose_name = _('streamdata')

    def ready(self):
        import lecturetranslator.streamdata.signals  # noqa

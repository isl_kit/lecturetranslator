#!/usr/bin/env python3
# -*- coding: utf-8 -*-



import codecs
import os
import os.path
from random import random

from django.conf import settings

from .vc import (
    get_head_sha,
    update
)

def write_data(dir_path, data):
    data_file = get_data_file(dir_path)
    with codecs.open(data_file, 'w+', encoding='utf-8') as f:
        for segment in data:
            f.write(serialize_segment(segment))


def read_data(dir_path):
    data = []
    data_file = get_data_file(dir_path)
    with codecs.open(data_file, 'r', encoding='utf-8') as f:
        c = 0
        for line in f:
            segment, c = deserialize_segment(line, c)
            data.append(segment)
    return data


def serialize_segment(segment):
    return '{start}|{stop}|{creator}|{text}\n'.format(**segment)


def deserialize_segment(text, c):
    parts = text.rstrip('\n').split('|')
    l = len(parts[3])
    segment = {
        'text': parts[3],
        'first_idx': c,
        'last_idx': c + l - 1,
        'start': int(parts[0]),
        'stop': int(parts[1]),
        'creator': parts[2]
    }
    return segment, c + l


def get_data_file(dir_path):
    return os.path.join(dir_path, 'data.txt')

def write_vtt(session_id, fingerprint, vttString):
    dir_path = get_vtt_dir(session_id)
    vtt_path = os.path.join(dir_path, '{}.vtt'.format(fingerprint))
    try:
        f = open(vtt_path, 'w')
    except IOError:
        print("Info: Could not correct vtt for session {} on path {}. This is not a problem if the vtt does not exist.".format(session_id, vtt_path))
        return
    f.write(vttString)
    f.close()

def get_public_dir(session_id, fingerprint):
    dir_path = os.path.join(get_data_dir(session_id), 'public', fingerprint)
    return ensure(dir_path)


def get_corrections_dir(session_id, fingerprint):
    """
    Return the path to the corrections directory for the stream specified by
    the session id and the fingerprint.
    It is NOT guaranteed that the corrections directory exists.
    """
    dir_path = os.path.join(get_data_dir(session_id), 'corrections', fingerprint)
    return dir_path


def get_corrections_dir_ensure(session_id, fingerprint):
    """
    Return the path to the corrections directory for the stream specified by
    the session id and the fingerprint.
    If the corrections directory does not exist, it will be created.
    """
    dir_path = os.path.join(get_data_dir(session_id), 'corrections', fingerprint)
    return ensure(dir_path)

def get_bare_dir(session_id, fingerprint):
    dir_path = os.path.join(get_data_dir(session_id), 'bares', '{}.git'.format(fingerprint))
    return ensure(dir_path)


def get_tmp_dir(session_id, fingerprint):
    base_dir = os.path.join(get_data_dir(session_id), 'tmp')
    while True:
        unique_dir_name = '{}_{}'.format(fingerprint, int(random() * 1000000))
        dir_path = os.path.join(base_dir, unique_dir_name)
        if not os.path.exists(dir_path):
            break
    return ensure(dir_path)

def get_data_dir(session_id):
    return os.path.join(settings.STREAMS_DIR, str(session_id), 'data')

def get_vtt_dir(session_id):
    return os.path.join(settings.STREAMS_DIR, str(session_id), 'vtt')

def get_id_and_data_from_repo(repo_dir):
    try:
        commit_id = get_head_sha(repo_dir)
    except Exception as e:
        # If this happens something is very wrong with the repository. Error handling is not really useful but give some more information.
        print("Error: Could not get head sha for repo '{}'. Original error follows.".format(repo_dir))
        raise e
    data = read_data(repo_dir)
    return {
        'id': commit_id,
        'data': data
    }

def transfer_data(source_dir, target_dir, committer):
    """
    Transfer the current data in the source directory to the target
    directory and commit it.
    """
    data = read_data(source_dir)
    write_data(target_dir, data)
    update(target_dir, "Update from corrections", committer, committer)

def ensure(dir_path):
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    return dir_path

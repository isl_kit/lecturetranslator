#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.dispatch import receiver

from lecturetranslator.mediator.signals import session_destroy
from lecturetranslator.session.models import Stream, DataSegment

from .tasks import fix_whitespaces, init_stream_data


@receiver(session_destroy, dispatch_uid='streamdata_session_destroy')
def session_destroy_handler(sender, session, **kwargs): #pylint: disable=W0613
    for stream in Stream.objects.filter(session=session, type='text'):
        data = DataSegment.objects \
            .filter(stream=stream) \
            .order_by('start') \
            .values(
                'text', 'start', 'stop', 'creator'
            )
        fix_whitespaces(data)
        init_stream_data(session.id, stream.fingerprint, data)

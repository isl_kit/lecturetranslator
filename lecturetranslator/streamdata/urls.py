#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.conf.urls import url
from .views import (
    StreamData,
    StreamDataCorrections,
    StreamDataHistory,
)

urlpatterns = [
    url(r'^public/(?P<pk>[0-9]+)/(?P<fingerprint>[^/]+)/$', StreamData.as_view()),
    url(r'^corrections/(?P<pk>[0-9]+)/(?P<fingerprint>[^/]+)/$', StreamDataCorrections.as_view()),
    url(r'^public/(?P<pk>[0-9]+)/(?P<fingerprint>[^/]+)/history/$', StreamDataHistory.as_view()),
    #url(r'^(?P<pk>[0-9]+)/(?P<fingerprint>[^/]+)/lock/$', StreamDataLock.as_view()),
]

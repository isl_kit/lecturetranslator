#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os.path
import shutil

from git.cmd import GitCommandError

from rest_framework import serializers

from lecturetranslator.session.models import Stream

from .diff import (
    DiffError
)

from .vc import (
    get_head_sha
)

from .interface import (
    get_tmp_dir,
    get_public_dir,
    get_corrections_dir,
    get_corrections_dir_ensure,
    read_data,
    get_id_and_data_from_repo
)

from .tasks import (
    sanitize_session_text,
    getUsersFromOperations,
    operationsToText,
    commitChanges
)


class StreamDataSerializer(serializers.ModelSerializer):

    class Meta:
        model = Stream
        fields = ()

    def to_representation(self, obj):
        repo_dir = get_public_dir(obj.session_id, obj.fingerprint)
        return get_id_and_data_from_repo(repo_dir)

class StreamDataCorrectionsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Stream
        fields = ()

    def to_representation(self, obj):
        repo_dir = get_corrections_dir(obj.session_id, obj.fingerprint)
        corrected = True
        if not os.path.exists(repo_dir):
            # The purpose of this serializer is to get the "freshest" transcript.
            # If there are no corrections, then the public transcript is the freshest.
            repo_dir = get_public_dir(obj.session_id, obj.fingerprint)
            corrected = False
        resultObj = get_id_and_data_from_repo(repo_dir)
        resultObj["corrected"] = corrected
        return resultObj

class OperationChangeSerializer(serializers.Serializer):
    changeType = serializers.CharField(allow_blank=True, trim_whitespace=False)
    user = serializers.CharField(allow_blank=True, trim_whitespace=False, allow_null=True)


class OperationSerializer(serializers.Serializer):
    insert = serializers.CharField(allow_blank=True, trim_whitespace=False)
    change = OperationChangeSerializer(required=False)


class StreamDataCorrectionsUpdateSerializer(serializers.Serializer):
    commit_id = serializers.CharField()
    operations = OperationSerializer(many=True)

    def validate(self, data):
        operations = data['operations']

        session_id = self.context['session_id']
        fingerprint = self.context['fingerprint']
        request_user = self.context['request'].user

        corrections_dir = get_corrections_dir_ensure(session_id, fingerprint)

        if not os.path.exists(corrections_dir):
            raise serializers.ValidationError('No stream data found', code=500)

        users_who_changed = getUsersFromOperations(operations)
        users_to_consider = set()
        committer = request_user.name
        new_user = committer

        # do while loop
        while True:
            try:
                prev_segments = read_data(corrections_dir)
            except FileNotFoundError:
                public_dir = get_public_dir(session_id, fingerprint)
                prev_segments = read_data(public_dir)
            new_text = operationsToText(operations, users_to_consider)
            new_text = sanitize_session_text(new_text)
            new_user_name = "<UNKNOWN>" # User can be None if it's unknown where the change came from
            if new_user:
                new_user_name = new_user
            commit_message = 'Changes by {}'.format(new_user_name) if users_to_consider else 'New snapshot sent by {}'.format(new_user_name)
            try:
                commitChanges(
                    prev_segments, new_text, commit_message, new_user_name, committer, corrections_dir, session_id, fingerprint)
            except DiffError:
                raise serializers.ValidationError(
                    'text contains too many differences, could not merge',
                    code=501
                )
            if users_who_changed:
                new_user = users_who_changed.pop()
                users_to_consider.add(new_user)
            else:
                # Quit once we've considered all users
                break

        return {
            'corrections_dir': corrections_dir
        }

    def save(self):
        corrections_dir = self.validated_data['corrections_dir']

        """
        Do not use bare repositories for now because we should not need them with the new offline editor.
        In general exact need for the bares is currently not understood.
        """
        #push_master(corrections_dir)
        return {
            'data':  read_data(corrections_dir),
            'id': get_head_sha(corrections_dir)
        }


class StreamHistorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Stream
        fields = ()

    def to_representation(self, obj):
        return {
            'history': []
        }

# DEPRECATED! This should be removed once we are sure that there are no more dependencies on it.
class StreamDataSerializerDeprecated(serializers.ModelSerializer):

    class Meta:
        model = Stream
        fields = ()

    def to_representation(self, obj):
        repo_dir = get_public_dir(obj.session_id, obj.fingerprint)
        try:
            commit_id = get_head_sha(repo_dir)
        except Exception as e:
            # If this happens something is very wrong with the repository. Error handling is not really useful but give some more information.
            print("Error: Could not get head sha for session {} and repo '{}'. Original error follows.".format(obj.session_id, repo_dir))
            raise e
        data = read_data(repo_dir)
        return {
            'id': commit_id,
            'data': data
        }

# DEPRECATED! This should be removed once we are sure that there are no more dependencies on it.
class StreamDataUpdateSerializerDeprecated(serializers.Serializer):
    commit_id = serializers.CharField()
    operations = OperationSerializer(many=True)

    def validate(self, data):
        operations = data['operations']

        session_id = self.context['session_id']
        fingerprint = self.context['fingerprint']
        request_user = self.context['request'].user

        public_dir = get_public_dir(session_id, fingerprint)
        tmp_dir = get_tmp_dir(session_id, fingerprint)

        if not os.path.exists(public_dir):
            raise serializers.ValidationError('No stream data found', code=500)

        users_who_changed = getUsersFromOperations(operations)
        users_to_consider = set()
        committer = request_user.name
        new_user = committer

        # do while loop
        while True:
            prev_segments = read_data(public_dir)
            new_text = operationsToText(operations, users_to_consider)
            new_text = sanitize_session_text(new_text)
            new_user_name = "<UNKNOWN>" # User can be None if it's unknown where the change came from
            if new_user:
                new_user_name = new_user
            commit_message = 'Changes by {}'.format(new_user_name) if users_to_consider else 'New snapshot sent by {}'.format(new_user_name)
            try:
                commitChanges(
                    prev_segments, new_text, commit_message, new_user_name, committer, public_dir, session_id, fingerprint)
            except DiffError:
                raise serializers.ValidationError(
                    'text contains too many differences, could not merge',
                    code=501
                )
            if users_who_changed:
                new_user = users_who_changed.pop()
                users_to_consider.add(new_user)
            else:
                # Quit once we've considered all users
                break

        return {
            'tmp_dir': tmp_dir,
            'public_dir': public_dir
        }

    def save(self):
        public_dir = self.validated_data['public_dir']

        """
        Do not use bare repositories for now because we should not need them with the new offline editor.
        In general exact need for the bares is currently not understood.
        """
        #push_master(public_dir)
        return {
            'data':  read_data(public_dir),
            'id': get_head_sha(public_dir)
        }

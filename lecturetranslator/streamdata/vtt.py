#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import copy


def create_vtt_file_from_stream(stream):
    res = ''
    segments = []
    for data_segment in stream.data_segments.order_by('start_abs').exclude(text=''):
        text = data_segment.text
        start = data_segment.start
        stop = data_segment.stop
        segments.append({
            'start': start,
            'stop': stop,
            'text': text
        })
    return create_vtt_file_from_segments(segments)


def create_vtt_file_from_segments(segments):
    if len(segments) == 0:
        return ''

    # Make a deepcopy of the segments so we can change them as needed without modifying the original segments
    segments = copy.deepcopy(segments)
    for segment in segments:
        segment["start"] /= 1000.0
        segment["stop"] /= 1000.0
        segment["text"] = segment["text"].replace("<hr>", '')

    # TODO: Figure out if this is really needed. Patching "holes" between segments shouldn't be necessary.
    # In fact these holes are often intentional. Also, why have a minimum delta of 1 millisecond?
    max_offset = 1
    stop_before = 0

    for segment_before, segment in keep_previous(segments):
        if segment['start'] > stop_before:
            delta = min((segment['start'] - stop_before) // 2, max_offset)
            segment['start'] -= delta
            if segment_before is not None:
                segment_before['stop'] += delta
        stop_before = segment['stop']

    segments[-1]['stop'] += max_offset

    new_segments = []

    for segment_before, segment in keep_previous(segments):
        if segment_before is None or segment['start'] - segment_before['stop'] > 0.1:
            new_segments.append([])
        new_segments[-1].append(segment)

    segments = []
    max_len = 60

    for part in new_segments:
        ind = 0
        segment_before = None
        for segment in part:
            if not segment_before:
                segment_before = segment
                segments.append(segment)
                continue

            txt = segment_before['text']
            new_txt = segment['text']
            if len(txt) + len(new_txt) + 1 > max_len * 2:
                segment_before = segment
                segments.append(segment)
            else:
                segment_before['text'] = segment_before['text'].strip() + ' ' + new_txt.strip()
                segment_before['stop'] = segment['stop']

    for segment in segments:
        txt = segment['text']
        leng = len(txt)
        if leng > max_len:
            ind1 = leng // 2
            ind2 = ind1 + 1
            while txt[ind1] != ' ' and txt[ind2] != ' ':
                if ind1 > 0:
                    ind1 -= 1
                if ind2 < leng - 1:
                    ind2 += 1
            if txt[ind1] == ' ':
                ind = ind1
            else:
                ind = ind2
            segment['lines'] = [txt[:ind], txt[ind + 1:]]
        else:
            segment['lines'] = [txt]

    res = 'WEBVTT\n\n'
    for segment in segments:
        res += create_vtt_segment(segment['start'], segment['stop'], segment['lines'])
    return res


def create_vtt_segment(start, stop, lines):
    start = seconds_to_str(start)
    #print(start)
    stop = seconds_to_str(stop)
    #print(stop)
    all_lines = [start + ' --> ' + stop] + lines + ['\n']
    return '\n'.join(all_lines)


def zero_pad(num, l=2):
    snum = str(int(num))
    if len(snum) < l:
        return ''.join(['0' for _ in range(l - len(snum))]) + snum
    else:
        return snum


def seconds_to_str(seconds):
    hours, seconds = divmod(seconds, 3600)
    minutes, seconds = divmod(seconds, 60)
    seconds, milliseconds = divmod(seconds, 1)
    return '{}:{}:{}.{}'.format(
        zero_pad(hours),
        zero_pad(minutes),
        zero_pad(seconds),
        zero_pad(milliseconds, l=3)
    )


def keep_previous(l):
    last_item = None
    for item in l:
        yield last_item, item
        last_item = item

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json

from django.views.decorators.csrf import ensure_csrf_cookie
from django.shortcuts import render
from django.shortcuts import redirect
from django.conf import settings
from django.utils.safestring import mark_safe


@ensure_csrf_cookie
def index(request):
    return render(request, "index.html", {
        "websocket_url": settings.WEBSOCKET_EXT_URL,
        "editor_websocket_url": settings.EDITOR_WEBSOCKET_EXT_URL,
        "editor_allow_commit": settings.EDITOR_ALLOW_COMMIT,
        "backchannel": settings.BACKCHANNEL,
        "emoji_enabled": settings.EMOJI_ENABLED,
        "links_enabled": settings.LINKS_ENABLED,
        "limit_streams": mark_safe(json.dumps(settings.LIMIT_STREAMS))
    })


def angular_redirect(request, path=""):
    return redirect("/#/" + path)

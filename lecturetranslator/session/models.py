#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.db import models

from lecturetranslator.authentication.models import AuthEntity
from lecturetranslator.event.models import Event


class Session(models.Model):
    id = models.BigIntegerField(primary_key=True)
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=200)
    desc = models.TextField(blank=True)
    input_fingerprint = models.CharField(max_length=200)
    input_type = models.CharField(max_length=50)
    auth = models.ForeignKey(
        AuthEntity, models.SET_NULL,
        null=True, blank=True, related_name='sessions'
    )
    source = models.CharField(max_length=100, blank=True)

    compound_key = models.CharField(max_length=100, blank=True)
    meta = models.TextField(default='{}')
    extras = models.TextField(default='{}')
    params = models.TextField(default='{}')

    hidden = models.BooleanField(default=False)
    active = models.BooleanField(default=True)
    touched = models.BooleanField(default=True)

    audio_file = models.CharField(max_length=4096, blank=True)
    ext_video_url = models.URLField(blank=True)

    event = models.ForeignKey(
        Event, models.SET_NULL,
        related_name='sessions', null=True, blank=True
    )
    start = models.BigIntegerField(default=0)
    transcript_stream = models.ForeignKey(
        'Stream', models.SET_NULL,
        related_name='+', null=True, blank=True
    )

    class Meta:
        permissions = (
            ('can_access', 'Can access session'),
            ('can_record', 'Can record a session')
        )

    @property
    def id_as_string(self):
        return '{}'.format(self.id)

    def __unicode__(self):
        return '{}'.format(self.id)


class Stream(models.Model):
    ident = models.CharField(max_length=251, editable=False)
    session = models.ForeignKey(
        Session, models.CASCADE,
        related_name='streams'
    )
    fingerprint = models.CharField(max_length=200)
    type = models.CharField(max_length=50)
    label = models.CharField(max_length=100)
    displayname = models.CharField(max_length=200)
    control = models.CharField(max_length=50)
    sid = models.CharField(max_length=50)

    class Meta:
        unique_together = (('ident', 'session'), ('fingerprint', 'type', 'session'))
        index_together = [['ident', 'session'], ['fingerprint', 'type', 'session']]

    def __unicode__(self):
        return '{} ({} / {})'.format(
            self.session.id,
            self.fingerprint,
            self.type
        )


class SessionPart(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    session = models.ForeignKey(
        Session, models.CASCADE,
        related_name='parts'
    )
    audio_file = models.CharField(max_length=4096, blank=True)
    start_greater = models.BigIntegerField()

    def __unicode__(self):
        return '{} ({})'.format(
            self.session.id,
            self.start_greater
        )


class DataSegment(models.Model):
    stream = models.ForeignKey(
        Stream, models.CASCADE,
        related_name='data_segments'
    )
    start_abs = models.BigIntegerField()
    stop_abs = models.BigIntegerField()
    start = models.BigIntegerField()
    stop = models.BigIntegerField()
    offset = models.BigIntegerField(default=0)
    text = models.TextField()
    created = models.BigIntegerField(default=0)
    creator = models.CharField(max_length=100)
    end_of_paragraph = models.BooleanField(default=False)
    version = models.PositiveSmallIntegerField(default=0)

    class Meta:
        unique_together = (('start_abs', 'stream'))
        index_together = [['start_abs', 'stream']]

    @property
    def created_as_string(self):
        return '{}'.format(self.created)

    def __unicode__(self):
        return '{} ({} / {}): {}'.format(
            self.stream.session.id,
            self.stream.fingerprint,
            self.stream.type,
            self.start
        )


class ComputeNode(models.Model):
    name = models.CharField(max_length=200)
    creator = models.CharField(max_length=100)
    input_fingerprint = models.CharField(max_length=200)
    input_type = models.CharField(max_length=50)
    output_fingerprint = models.CharField(max_length=200)
    output_type = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name


class ComputeStep(models.Model):
    index = models.PositiveSmallIntegerField(default=0)
    name = models.CharField(max_length=200)
    fingerprint = models.CharField(max_length=200)
    type = models.CharField(max_length=50)
    node = models.ForeignKey(
        ComputeNode, models.SET_NULL,
        null=True, blank=True, related_name='compute_steps'
    )
    stream = models.ForeignKey(
        Stream, models.CASCADE,
        related_name='compute_steps'
    )

    def __unicode__(self):
        return self.name

#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from ...serializers import StreamSerializer
from ...models import Stream

class Command(BaseCommand):

    def handle(self, *args, **options):
        data = {
            'session': 123456,
            'fingerprint': 'de',
            'type': 'text'
        }
        serializer = StreamSerializer(data=data)
        print(serializer.is_valid())
        print(serializer.validated_data)
        print(serializer.validated_data['session_id'].title)
        print(Stream.objects.get(**serializer.validated_data))

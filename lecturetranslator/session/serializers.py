#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json

from django.conf import settings

from rest_framework import serializers

from lecturetranslator.authentication.models import AuthEntity
from lecturetranslator.authentication.serializers import AuthEntitySerializer

from lecturetranslator.event.serializers import EventRelatedSerializer, EventDetailSerializer

from .models import Session, Stream, DataSegment, ComputeNode, ComputeStep
from .permissions import is_accessible


class ComputeNodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ComputeNode
        fields = (
            'name', 'creator',
            'input_fingerprint', 'input_type',
            'output_fingerprint', 'output_type'
        )


class ComputeStepSerializer(serializers.ModelSerializer):
    node = ComputeNodeSerializer(read_only=True)
    class Meta:
        model = ComputeStep
        fields = (
            'index', 'name',
            'fingerprint', 'type', 'node'
        )


class StreamSerializer(serializers.ModelSerializer):
    # This is overwriting the actual label field. Renaming would be a lot of work so for now we keep the current, possibly bugged field.
    label = serializers.SerializerMethodField() # type: ignore

    class Meta:
        model = Stream
        fields = ('ident', 'fingerprint', 'type', 'label')
        read_only_fields = ('ident',)

    def get_label(self, obj):
        tag = obj.fingerprint.split('-')[0]
        return settings.GET_STREAM_LABEL(tag)


class StreamIdSerializer(serializers.ModelSerializer):
    session = serializers.PrimaryKeyRelatedField(read_only=True)
    class Meta:
        model = Stream
        fields = ('ident',)


class StreamNodeSerializer(serializers.ModelSerializer):
    compute_steps = ComputeStepSerializer(many=True, read_only=True)
    class Meta:
        model = Stream
        fields = ('ident', 'control', 'displayname', 'sid', 'fingerprint', 'type', 'compute_steps')


class SessionListSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField(source='id_as_string')
    auth = AuthEntitySerializer(read_only=True)
    event = EventRelatedSerializer(read_only=True)
    class Meta:
        model = Session
        fields = (
            'id', 'created', 'title', 'start',
            'input_fingerprint', 'input_type',
            'desc', 'auth', 'event', 'active', 'hidden'
        )


class SessionBroadcastSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField(source='id_as_string')
    auth = serializers.PrimaryKeyRelatedField(read_only=True)
    event = EventRelatedSerializer(read_only=True)
    streams = StreamSerializer(many=True)
    transcript_stream = serializers.SlugRelatedField(read_only=True, slug_field='ident')
    default_streams = serializers.SerializerMethodField()

    class Meta:
        model = Session
        fields = (
            'id', 'created', 'title', 'start',
            'input_fingerprint', 'input_type',
            'desc', 'auth', 'event', 'active', 'hidden',
            'streams', 'transcript_stream', 'default_streams'
        )

    def get_default_streams(self, obj):
        result = []
        if obj.transcript_stream:
            result.append(obj.transcript_stream.ident)
        params = json.loads(obj.params)
        if 'T' in params:
            s = obj.streams.filter(fingerprint=params['T'], type='text')
            if s.exists():
                result.append(s[0].ident)
        return result


class SessionDetailSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField(source='id_as_string')
    auth = AuthEntitySerializer()
    event = EventDetailSerializer(read_only=True)
    can_access = serializers.SerializerMethodField()
    streams = StreamSerializer(many=True)
    transcript_stream = serializers.SlugRelatedField(read_only=True, slug_field='ident')
    default_streams = serializers.SerializerMethodField()

    class Meta:
        model = Session
        fields = (
            'id', 'created', 'title', 'start', 'event',
            'input_fingerprint', 'input_type', 'can_access', 'default_streams',
            'desc', 'auth', 'active', 'hidden', 'streams', 'transcript_stream'
        )
        read_only_fields = ('id', 'created', 'active')

    def get_can_access(self, obj):
        if obj.active:
            return True
        return is_accessible(self.context['request'], obj)

    def get_default_streams(self, obj):
        result = []
        if obj.transcript_stream:
            result.append(obj.transcript_stream.ident)
        params = json.loads(obj.params)
        if 'T' in params:
            s = obj.streams.filter(fingerprint=params['T'], type='text')
            if s.exists():
                result.append(s[0].ident)
        return result


    def create(self, validated_data):
        validated_data['auth'] = AuthEntity.objects.create(**validated_data['auth'])
        streams_data = validated_data.pop('streams')
        session = Session.objects.create(**validated_data)
        for stream_data in streams_data:
            Stream.objects.create(session=session, **stream_data)
        return session

    def update(self, session, validated_data):
        streams = validated_data.pop('streams')
        session.title = validated_data.get('title', session.title)
        session.input_fingerprint = validated_data.get(
            'input_fingerprint', session.input_fingerprint
        )
        session.input_type = validated_data.get('input_type', session.input_type)
        session.desc = validated_data.get('desc', session.desc)
        session.active = True
        session.save()
        for stream_data in streams:
            if not Stream.objects.filter(session=session, **stream_data).exists():
                Stream.objects.create(session=session, **stream_data)
        return session


class SessionNodesSerializer(serializers.ModelSerializer):
    streams = StreamNodeSerializer(many=True)
    transcript_stream = serializers.SlugRelatedField(read_only=True, slug_field='ident')
    class Meta:
        model = Session
        fields = (
            'id', 'streams', 'transcript_stream'
        )


class DataSegmentListSerializer(serializers.ModelSerializer):
    created = serializers.ReadOnlyField(source='created_as_string')
    class Meta:
        model = DataSegment
        fields = (
            'id', 'text', 'creator', 'version', 'created',
            'end_of_paragraph', 'start', 'stop', 'start_abs'
        )


class DataSegmentSerializer(serializers.ModelSerializer):
    created = serializers.ReadOnlyField(source='created_as_string')
    stream = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = DataSegment
        fields = (
            'id', 'stream', 'text', 'creator', 'version', 'created',
            'end_of_paragraph', 'start', 'stop', 'start_abs'
        )


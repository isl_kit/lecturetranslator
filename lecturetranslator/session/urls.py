#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.SessionList.as_view()),
    url(r'^(?P<pk>[0-9]+)/$', views.SessionDetail.as_view()),
    url(r'^(?P<pk>[0-9]+)/nodes/$', views.SessionNodes.as_view()),
    url(r'^(?P<pk>[0-9]+)/(?P<stream_ident>[^/]+)/$', views.StreamDetail.as_view()),
    url(r'^(?P<pk>[0-9]+)/(?P<stream_ident>[^/]+)/data/$', views.DataSegmentList.as_view())
]

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from lecturetranslator.authentication.auth import check_login
from lecturetranslator.authentication.models import AuthEntity


def check_session_auth(request, session_id):
    try:
        auth = AuthEntity.objects.get(sessions__id=session_id)
    except AuthEntity.DoesNotExist:
        return True
    return check_login(request, auth)

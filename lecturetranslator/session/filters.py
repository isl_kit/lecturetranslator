#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.conf import settings

from django_filters.rest_framework import FilterSet, BooleanFilter, ModelChoiceFilter

from rest_framework import filters

from lecturetranslator.event.models import Event

from .models import Session


class SessionFilterBackend(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        event_id = request.query_params.get('event', None)
        if event_id is not None:
            queryset = queryset.filter(event_id=event_id)
        else:
            queryset = queryset.filter(active=True)
            if not settings.SHOW_HIDDEN_SESSIONS:
                queryset = queryset.filter(hidden=False)
        return queryset



class SessionFilter(FilterSet):
    event = ModelChoiceFilter(
        field_name='event',
        to_field_name='id',
        queryset=Event.objects.all(),
        help_text='ID of related Event'
    )
    hidden = BooleanFilter(
        help_text='show hidden sessions'
    )
    active = BooleanFilter(
        help_text='show active sessions'
    )

    class Meta:
        model = Session
        fields = ['event', 'hidden', 'active']

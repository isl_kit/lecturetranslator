#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.contrib import admin
from guardian.admin import GuardedModelAdmin

from .models import Session, Stream, DataSegment, ComputeNode, ComputeStep


admin.site.register(Session, GuardedModelAdmin)
admin.site.register(Stream, GuardedModelAdmin)
admin.site.register(DataSegment, GuardedModelAdmin)
admin.site.register(ComputeNode, GuardedModelAdmin)
admin.site.register(ComputeStep, GuardedModelAdmin)

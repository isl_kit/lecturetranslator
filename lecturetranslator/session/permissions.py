#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.conf import settings

from rest_framework import permissions

from lecturetranslator.lecture.permissions import (
    has_archive_access,
    has_media_access
)

from .auth import check_session_auth
from .models import Session


def is_accessible(request, session):
    if settings.GRANT_ARCHIVE_TO_ALL or session.active:
        return True
    if request.user.has_perm('can_access', session):
        return True
    if not session.event:
        return False
    return has_archive_access(request, session.event.lecture_term)


def is_media_accessible(request, session):
    if settings.GRANT_MEDIA_TO_ALL or request.user.has_perm('can_access', session):
        return True
    if not session.event:
        return False
    return has_media_access(request, session.event.lecture_term)


class IsSessionAccessible(permissions.BasePermission):
    def has_permission(self, request, view):
        if 'pk' not in view.kwargs: # hack for swagger DOC
            return True
        session_id = int(view.kwargs['pk'])
        try:
            session = Session.objects.get(id=session_id)
        except Session.DoesNotExist:
            return False
        return is_accessible(request, session)


class IsSessionMediaAccessible(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return is_media_accessible(request, obj)


class IsSessionAuthenticated(permissions.BasePermission):
    def has_permission(self, request, view):
        session_id = int(view.kwargs.get('pk', 0))
        return check_session_auth(request, session_id)

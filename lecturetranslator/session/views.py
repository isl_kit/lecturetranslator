#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.db.models import Prefetch

from rest_framework.generics import ListCreateAPIView, RetrieveAPIView, RetrieveUpdateDestroyAPIView

from django_filters.rest_framework import DjangoFilterBackend

from lecturetranslator.common.permissions import IsStaffOrReadOnly, IsStaff

from .filters import SessionFilter
from .models import Session, Stream, DataSegment, ComputeStep
from .permissions import IsSessionAccessible, IsSessionAuthenticated
from .serializers import (
    SessionListSerializer,
    SessionDetailSerializer,
    SessionNodesSerializer,
    StreamSerializer,
    DataSegmentSerializer
)


class SessionList(ListCreateAPIView):
    permission_classes = [IsStaffOrReadOnly]
    filter_backends = (DjangoFilterBackend,)
    filterset_class = SessionFilter

    def get_queryset(self):
        sessions = Session.objects \
            .select_related(
                'auth', 'event',
                'event__lecture_term',
                'event__lecture_term__auth',
                'event__lecture_term__lecture',
                'event__lecture_hall') \
            .order_by('title')
        return sessions

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return SessionDetailSerializer
        return SessionListSerializer

    def get_serializer_context(self):
        return {
            'request': self.request
        }


class SessionDetail(RetrieveUpdateDestroyAPIView):
    queryset = Session.objects \
        .select_related(
            'auth', 'event',
            'event__lecture_term',
            'event__lecture_term__lecture',
            'event__lecture_hall'
        ) \
        .prefetch_related(
            Prefetch('streams', queryset=Stream.objects.order_by('fingerprint'))
        ).all()
    serializer_class = SessionDetailSerializer
    permission_classes = [IsStaffOrReadOnly]

    def get_serializer_context(self):
        return {
            'request': self.request
        }


class SessionNodes(RetrieveAPIView):
    queryset = Session.objects \
        .prefetch_related(
            Prefetch('streams__compute_steps', queryset=ComputeStep.objects.order_by('-index')),
            'streams__compute_steps__node'
        ).all()
    serializer_class = SessionNodesSerializer
    permission_classes = [IsStaff]


class StreamDetail(RetrieveUpdateDestroyAPIView):
    queryset = Stream.objects.all()
    serializer_class = StreamSerializer
    permission_classes = [IsStaffOrReadOnly]

    def get_object(self):
        session_id = int(self.kwargs['pk'])
        ident = self.kwargs['stream_ident']
        return Stream.objects.get(session_id=session_id, ident=ident)


class DataSegmentList(ListCreateAPIView):
    serializer_class = DataSegmentSerializer
    permission_classes = [IsStaffOrReadOnly, IsSessionAccessible, IsSessionAuthenticated]

    def get_queryset(self):
        session_id = int(self.kwargs['pk'])
        ident = self.kwargs['stream_ident']
        return DataSegment.objects \
            .filter(stream__session_id=session_id, stream__ident=ident) \
            .order_by('start_abs')

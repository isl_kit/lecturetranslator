#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import string
import random

from django.http import JsonResponse
from django.views.decorators.http import require_POST, require_GET
from django.core.mail import send_mail


def generate_id(size=6, chars=string.ascii_lowercase + string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


@require_POST
def send_feedback(request):
    data = json.loads(request.body)
    try:
        name = data["name"]
        message = data["message"]
        email = data["email"]
        topic = data["topic"]
    except KeyError:
        return JsonResponse({"message": "missing parameter"}, status=404)
    tid = generate_id(size=11)
    request.session["C" + tid] = True
    send_mail(
        "Feedback (" + topic + ")",
        "FROM: " + name + " (" + email + ")\n\n" + message,
        "info@lecture-translator.kit.edu",
        ["info@lecture-translator.kit.edu"],
    )
    return JsonResponse({"tid": tid})


@require_GET
def verify_feedback(request):
    try:
        tid = request.GET["tid"]
    except KeyError:
        return JsonResponse({"message": "missing parameter"}, status=404)
    if "C" + tid not in request.session:
        return JsonResponse({"message": "message not found"}, status=404)
    del request.session["C" + tid]
    return JsonResponse({})

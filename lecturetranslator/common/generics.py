#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.http import Http404

from rest_framework.generics import GenericAPIView

from .mixins import BulkDestroyModelMixin, BulkListModelMixin


class BulkListDestroyAPIView(BulkDestroyModelMixin, BulkListModelMixin, GenericAPIView):

    def get(self, request, *args, **kwargs):
        return self.bulk_list(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.bulk_destroy(request, *args, **kwargs)

    def get_ids(self):
        if 'ids' not in self.request.query_params:
            return []
        try:
            ids = [int(i) for i in self.request.query_params.getlist('ids')]
        except ValueError:
            raise Http404
        return list(set(ids))

    def reduce_queryset(self, queryset):
        ids = self.get_ids()
        result = queryset.filter(pk__in=ids)
        if result.count < len(ids):
            raise Http404
        return result


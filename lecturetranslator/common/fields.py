#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Manager
from django.db.models.query import QuerySet
from django.utils.encoding import smart_text

from rest_framework.serializers import PrimaryKeyRelatedField, SlugRelatedField


class CreatableSlugRelatedField(SlugRelatedField):
    def to_internal_value(self, data):
        try:
            return self.get_queryset().get_or_create(**{self.slug_field: data})[0]
        except ObjectDoesNotExist:
            self.fail('does_not_exist', slug_name=self.slug_field, value=smart_text(data))
        except (TypeError, ValueError):
            self.fail('invalid')


class DynamicPrimaryKeyRelatedField(PrimaryKeyRelatedField):
    def get_queryset(self):
        queryset = self.queryset
        if callable(queryset):
            queryset = queryset(self.context.get('request', None))
        if isinstance(queryset, (QuerySet, Manager)):
            queryset = queryset.all()
        return queryset

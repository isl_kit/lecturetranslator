#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from rest_framework.response import Response
from rest_framework.status import HTTP_204_NO_CONTENT


class BulkListModelMixin(object):

    def bulk_list(self, request, *args, **kwargs): # pylint: disable=W0613
        queryset = self.filter_queryset(self.reduce_queryset(self.get_queryset()))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class BulkDestroyModelMixin(object):

    def bulk_destroy(self, request, *args, **kwargs): # pylint: disable=W0613
        queryset = self.filter_queryset(self.reduce_queryset(self.get_queryset()))
        self.perform_bulk_destroy(queryset)
        return Response(status=HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        instance.delete()

    def perform_bulk_destroy(self, objects):
        for obj in objects:
            self.perform_destroy(obj)

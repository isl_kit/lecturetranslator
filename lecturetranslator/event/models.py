#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.db import models

from lecturetranslator.lecturehall.models import LectureHall
from lecturetranslator.lecture.models import LectureTerm


class Event(models.Model):
    lecture_term = models.ForeignKey(
        LectureTerm, models.CASCADE,
        related_name='events'
    )
    lecture_hall = models.ForeignKey(
        LectureHall, models.SET_NULL,
        null=True, blank=True, related_name='events'
    )
    fingerprint = models.CharField(max_length=200)
    title = models.CharField(max_length=200, blank=True)
    '''
    origin = models.ForeignKey(
        'Event', related_name='children',
        null=True, blank=True
    )
    '''
    start = models.DateTimeField()
    stop = models.DateTimeField(null=True, blank=True)
    session = models.ForeignKey(
        'session.Session', models.SET_NULL,
        related_name='+', null=True, blank=True
    )

    def __unicode__(self):
        return '{} {}-{}'.format(
            self.lecture_term.lecture.slug,
            self.start.strftime("%a %d.%m.%Y %H:%M"),
            self.stop.strftime("%H:%M") if self.stop else "?"
        )

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime

from lecturetranslator.lecture.permissions import has_archive_access, can_upload_files


def can_access(request, event):
    if not event.session:
        if event.stop is None:
            return False
        now = datetime.now()
        return (now - event.stop).total_seconds() < 3600
    return event.session.active or has_archive_access(request, event.lecture_term)


def can_upload(request, event):
    return can_upload_files(request, event.lecture_term)

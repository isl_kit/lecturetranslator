#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime, timedelta

from django_filters.rest_framework import (
    FilterSet,
    ModelChoiceFilter,
    ModelMultipleChoiceFilter,
    CharFilter,
    NumberFilter,
    IsoDateTimeFilter
)

from lecturetranslator.lecture.models import Lecture
from lecturetranslator.lecturer.models import Lecturer
from lecturetranslator.lecturehall.models import LectureHall

from .models import Event


class EventFilter(FilterSet):
    slug = ModelChoiceFilter(
        field_name='lecture_term__lecture',
        to_field_name='slug',
        queryset=Lecture.objects.all(),
        help_text='slug of related Lecture'
    )
    term = CharFilter(
        field_name='lecture_term__term',
        help_text='term name (i.e. SS2016)'
    )
    lecturer = ModelMultipleChoiceFilter(
        field_name='lecture_term__lecturers',
        to_field_name='id',
        queryset=Lecturer.objects.all(),
        help_text='ID of related Lecturer (multiple values allowed)'
    )
    hall = ModelMultipleChoiceFilter(
        field_name='lecture_hall',
        to_field_name='id',
        queryset=LectureHall.objects.all(),
        help_text='ID of related LectureHall (multiple values allowed)'
    )
    offset_pre = NumberFilter(
        label='offset_pre',
        method='offset_pre_filter',
        help_text='\n'.join((
            '### Filter events by start/stop time',
            'Only shows events that have been/will be stopped after current_time-offset_pre. (In minutes)',
            'Together with offset_post, defines a time interval that centers around the current time and reaches `offset_pre` minutes into the past and `offset_post` minutes into the future. All events are matched that overlap with this interval.'
        ))
    )
    offset_post = NumberFilter(
        label='offset_post',
        method='offset_post_filter',
        help_text='\n'.join((
            '### Filter events by start/stop time',
            'Only shows events that will be started before current_time+offset_post. (In minutes)',
            'Together with offset_pre, defines a time interval that centers around the current time and reaches `offset_pre` minutes into the past and `offset_post` minutes into the future. All events are matched that overlap with this interval.'
        ))
    )

    # Filters an event by its start date. gte from a, lte until a date.
    # Names have been chosen as to not break older functionality
    start__lte = IsoDateTimeFilter(field_name='start', lookup_expr='lte')
    start__gte = IsoDateTimeFilter(field_name='start', lookup_expr='gte')

    class Meta:
        model = Event
        fields = ['slug', 'term', 'hall', 'lecturer', 'offset_pre', 'offset_post', 'start__gte', 'start__lte']

    # TODO these might be useless since the IsoDateTimeFilters are doing the same thing. Should probably be removed
    # but it is unclear who might still be using offset_* or start__*
    def offset_pre_filter(self, queryset, name, value):
        d = datetime.now() - timedelta(minutes=int(value))
        return queryset.filter(stop__gte=d)

    def offset_post_filter(self, queryset, name, value):
        d = datetime.now() + timedelta(minutes=int(value))
        return queryset.filter(start__lte=d)

'''
class EventFilterBackend(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        lecturer_id = request.query_params.get('lecturer', None)
        hall_id = request.query_params.get('hall', None)
        slug = request.query_params.get('slug', None)
        term = request.query_params.get('term', None)
        offset_pre = int(request.query_params.get('offset_pre', 0))
        offset_post = int(request.query_params.get('offset_post', 0))
        start_gte = request.query_params.get('start__gte', None)
        start_lte = request.query_params.get('start__lte', None)

        origin_isnull = 'origin__isnull' in request.query_params
        origin_id = request.query_params.get('origin', None)

        if origin_isnull:
            queryset = queryset.filter(origin=None)
        elif origin_id is not None:
            queryset = queryset.filter(origin_id=origin_id)

        if lecturer_id is not None:
            queryset = queryset.filter(lecture_term__lecturers__id=lecturer_id).distinct()
        if hall_id is not None:
            queryset = queryset.filter(lecture_hall_id=hall_id)
        if slug is not None:
            queryset = queryset.filter(lecture_term__lecture__slug=slug)
        if term is not None:
            queryset = queryset.filter(lecture_term__term=term)
        if offset_pre > 0 or offset_post > 0:
            d1 = datetime.now() - timedelta(minutes=offset_pre)
            d2 = datetime.now() + timedelta(minutes=offset_post)
            queryset = queryset.filter(stop__gte=d1, start__lte=d2)
        else:
            if start_gte is not None:
                try:
                    d = datetime.strptime(start_gte, '%Y-%m-%dT%H:%M')
                    queryset = queryset.filter(start__gte=d)
                except ValueError:
                    pass
            if start_lte is not None:
                try:
                    d = datetime.strptime(start_lte, '%Y-%m-%dT%H:%M')
                    queryset = queryset.filter(start__lte=d)
                except ValueError:
                    pass
        return queryset
'''

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.conf.urls import url
from .views import (
    EventList,
    EventDetail,
)

urlpatterns = [
    url(r'^$', EventList.as_view()),
    url(r'^(?P<pk>[0-9]+)/$', EventDetail.as_view())
]

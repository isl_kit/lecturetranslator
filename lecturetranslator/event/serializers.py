#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from rest_framework import serializers

from lecturetranslator.common.fields import CreatableSlugRelatedField

from lecturetranslator.lecture.serializers import (
    LectureTermListSerializer,
    LectureTermRelatedSerializer
)
from lecturetranslator.lecture.models import LectureTerm

from lecturetranslator.lecturehall.serializers import (
    LectureHallSerializer,
    LectureHallRelatedSerializer
)
from lecturetranslator.lecturehall.models import LectureHall
from lecturetranslator.session.models import Session

from .models import Event
from .permissions import can_access, can_upload


class EventRelatedSerializer(serializers.ModelSerializer):
    lecture_term = LectureTermRelatedSerializer(read_only=True)
    lecture_hall = LectureHallRelatedSerializer(read_only=True)

    class Meta:
        model = Event
        fields = ('id', 'lecture_term', 'lecture_hall', 'start', 'stop', 'fingerprint', 'title')


class EventListSerializer(serializers.ModelSerializer):
    lecture_term = LectureTermListSerializer(read_only=True)
    lecture_hall = LectureHallRelatedSerializer(read_only=True)
    session = serializers.SlugRelatedField(read_only=True, slug_field='id_as_string')
    can_access = serializers.SerializerMethodField()

    class Meta:
        model = Event
        fields = (
            'id', 'lecture_term', 'lecture_hall', 'fingerprint',
            'start', 'stop', 'session', 'can_access', 'title'
        )

    def get_can_access(self, obj):
        return can_access(self.context['request'], obj)


class EventCreateSerializer(serializers.ModelSerializer):
    lecture_term = serializers.PrimaryKeyRelatedField(queryset=LectureTerm.objects.all())
    lecture_hall = CreatableSlugRelatedField(
        queryset=LectureHall.objects.all(),
        slug_field='name',
        allow_null=True
    )

    class Meta:
        model = Event
        fields = ('id', 'lecture_term', 'lecture_hall', 'start', 'stop', 'fingerprint')

    def to_representation(self, obj):
        return EventListSerializer(obj, context=self.context).data


class EventDetailSerializer(serializers.ModelSerializer):
    lecture_term = LectureTermListSerializer(read_only=True)
    lecture_hall = LectureHallSerializer(read_only=True)
    session = serializers.SlugRelatedField(read_only=True, slug_field='id_as_string')
    can_access = serializers.SerializerMethodField()
    can_upload = serializers.SerializerMethodField()

    class Meta:
        model = Event
        fields = (
            'id', 'lecture_term', 'lecture_hall', 'fingerprint',
            'start', 'stop', 'session', 'can_access', 'can_upload', 'title'
        )

    def get_can_access(self, obj):
        if 'request' not in self.context:
            return False
        return can_access(self.context['request'], obj)

    def get_can_upload(self, obj):
        if 'request' not in self.context:
            return False
        return can_upload(self.context['request'], obj)


class EventUpdateSerializer(serializers.ModelSerializer):
    lecture_hall = CreatableSlugRelatedField(
        queryset=LectureHall.objects.all(),
        slug_field='name'
    )
    session = serializers.PrimaryKeyRelatedField(queryset=Session.objects.all())

    class Meta:
        model = Event
        fields = ('lecture_hall', 'start', 'stop', 'session', 'fingerprint')

    def to_representation(self, obj):
        return EventListSerializer(obj, context=self.context).data

from django.core.management.base import BaseCommand, CommandError

from lecturetranslator.event.models   import Event
from lecturetranslator.session.models import Session

import random

class Command(BaseCommand):
    help = 'Creates an empty session for the given event. This is useful if you need to add Events that never ran on the instance with data that was computed offline.\nThe session id can be specified optionally on the command line, but it is recommended to let the command use a random one'

    def add_arguments(self, parser):
        parser.add_argument('event_id', type=int, help="The event the new session will belong to")
        parser.add_argument('audio_fingerprint', type=str, help="The fingerprint of the input language.")
        parser.add_argument('--sessionid', type=int, required=False, action="store", dest="sessionid", help="Manually specify the id of the session to be created. Otherwise one will be generated for you.")

    def handle(self, *args, **options):
        try:
            event = Event.objects.get(id=options["event_id"])
        except Exception as e:
            print("Could not get an event with the id {}. Reason:".format(options["event_id"]))
            print(e)
            return 1

        new_s_id = None
        if options["sessionid"] is None:
            # Generate random id with collision checking
            new_s_id = random.getrandbits(62) + 1 # Taken from mediator.preprocess.compound
            while Session.objects.filter(id=new_s_id).exists():
                print("Session id collision detected. Retrying...")
                new_s_id = random.getrandbits(62) + 1
        else:
            new_s_id = options["sessionid"]
            if Session.objects.filter(id=new_s_id).exists():
                print("The session id you specified already exists in the database.")
                return 1

        session = Session.objects.create(id=new_s_id, active=False, input_fingerprint=options["audio_fingerprint"], event=event)
        event.session = session
        session.save()
        event.save()
        print("Created a new session with id {} and linked it with event {}".format(session.id, event.id))

from django.core.management.base import BaseCommand, CommandError

from lecturetranslator.event.models   import Event
from lecturetranslator.session.models import Session

import random

class Command(BaseCommand):
    help = 'Get the session id whose session is linked to a specific event'

    def add_arguments(self, parser):
        parser.add_argument('event_id', type=int, help="The event the new session will belong to")

    def handle(self, *args, **options):
        try:
            event = Event.objects.get(id=options["event_id"])
        except Exception as e:
            print("Could not get an event with the id {}. Reason:".format(options["event_id"]))
            print(e)
            return 1
        try:
            session = event.session
            print("The session with id {} is linked to the event {}".format(session.id, event.id))
        except Exception as e:
            print("There is no session yet linked to this event")
            return 1

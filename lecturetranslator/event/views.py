#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from django_filters.rest_framework import DjangoFilterBackend

from lecturetranslator.common.permissions import IsStaffOrReadOnly

from .filter import EventFilter
from .models import Event
from .serializers import (
    EventCreateSerializer,
    EventUpdateSerializer,
    EventListSerializer,
    EventDetailSerializer,
)


class EventList(ListCreateAPIView):
    """
    get:
    Return a list of all the existing events.

    post:
    Create a new event.
    """

    queryset = Event.objects \
        .select_related(
            'session',
            'lecture_term',
            'lecture_term__lecture',
            'lecture_term__auth',
            'lecture_hall') \
        .prefetch_related('lecture_term__lecturers') \
        .order_by('start').all()
    permission_classes = [IsStaffOrReadOnly]
    filter_backends = (DjangoFilterBackend,)
    filterset_class = EventFilter

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return EventCreateSerializer
        return EventListSerializer

    def get_serializer_context(self):
        return {
            'request': self.request
        }


class EventDetail(RetrieveUpdateDestroyAPIView):
    queryset = Event.objects \
        .select_related(
            'session',
            'lecture_term',
            'lecture_term__lecture',
            'lecture_term__auth',
            'lecture_hall') \
        .prefetch_related('lecture_term__lecturers') \
        .order_by('start').all()
    permission_classes = [IsStaffOrReadOnly]

    def get_serializer_class(self):
        if self.request.method in ['PUT', 'PATCH']:
            return EventUpdateSerializer
        return EventDetailSerializer

    def get_serializer_context(self):
        return {
            'request': self.request
        }

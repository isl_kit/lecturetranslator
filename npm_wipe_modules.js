const path = require('path')
const fs = require('fs')
const child_process = require('child_process')
const readline = require('readline')
var rl = readline.createInterface(process.stdin, process.stdout);

// First remove node_modules in the root folder
console.log(`Removing node_modules inside root folder`)
child_process.execSync('rm -rf node_modules', { cwd: ".", env: process.env, stdio: 'inherit' })

// Then go through all folders containing JS app files
const appRoot = "lecturetranslator/static/app"
process.chdir(appRoot)
const root = process.cwd()
remove_modules_recursive(root)
process.exit(0)

// Recursively delete node_modules starting at the specified folder
function remove_modules_recursive(folder)
{
    const has_package_json = fs.existsSync(path.join(folder, 'package.json'))

    // If there is `package.json` in this folder, then remove node_modules
    if (has_package_json && folder !== root)
    {
        console.log(`Removing node_modules inside ${folder === root ? 'root folder' : './' + appRoot + "/" + path.relative(root, folder)}`)

        remove_modules(folder)
    }

    // Recurse into subfolders
    for (let subfolder of subfolders(folder))
    {
        remove_modules_recursive(subfolder)
    }
}

// Remove node_modules inside the specified folder
function remove_modules(where)
{
    child_process.execSync('rm -rf node_modules', { cwd: where, env: process.env, stdio: 'inherit' })
}

// List subfolders in a folder
function subfolders(folder)
{
    return fs.readdirSync(folder)
        .filter(subfolder => fs.statSync(path.join(folder, subfolder)).isDirectory())
        .filter(subfolder => subfolder !== 'node_modules' && subfolder[0] !== '.')
        .map(subfolder => path.join(folder, subfolder))
}

var path = require("path");

module.exports = {

    static_dir: "lecturetranslator/static",
    build_dir: "build",
    compile_dir: "bin",

    config_files: {
        local: "app/config/local.json",
        production: "app/config/production.json"
    },

    app_files: {
        modules: ["app/**/*.module.js"],

        js: [ "app/**/*.module.js", "app/**/*.js", "!app/**/node_modules/**/*.js", "!app/**/session-editor/**/*.js" ],

        js_es15: ["../../node_modules/@babel/polyfill/browser.js", "app/**/session-editor/**/*.js", "!app/**/node_modules/**/*.js" ],

        jsunit: [ "test/**/*.js" ],

        tpl: [ "app/**/*.tpl.html" ],
        html: [ "app/index.html" ],

        main_less: "assets/css/main.less",
        less: ["**/*.desktop.less", "**/*.less"],
        css: ["assets/css/**/*.css"],

        singlejs: [ "assets/js/*" ],

        wav: [ "assets/wav/*" ],

        fonts: [ "assets/fonts/*" ],

        images: [ "assets/images/*" ],

        index: "app/index.html"

    },

    test_files: {
        js: [
            "vendor/angular-mocks/angular-mocks.js"
        ]
    },

    vendor_files: {

        js: [
            "vendor/adapterjs/publish/adapter.debug.js",
            "vendor/underscore/underscore-min.js",
            "vendor/jquery/dist/jquery.js",
            "vendor/blueimp-file-upload/js/vendor/jquery.ui.widget.js",
            "vendor/blueimp-file-upload/js/jquery.fileupload.js",
            "vendor/blueimp-file-upload/js/jquery.iframe-transport.js",
            "vendor/javascript-detect-element-resize/jquery.resize.js",
            "vendor/fingerprintjs2/fingerprint2.js",

            "vendor/angular/angular.js",
            "vendor/angular-animate/angular-animate.js",
            "vendor/angular-cookies/angular-cookies.js",
            "vendor/angular-sanitize/angular-sanitize.js",
            "vendor/angular-aria/angular-aria.js",
            "vendor/angular-messages/angular-messages.js",
            "vendor/angular-material/angular-material.js",

            "vendor/video.js/dist/video.js",

            "vendor/vjs-video/dist/vjs-video.js",
            "vendor/angular-scroll-glue/src/scrollglue.js",
            "vendor/angular-ui-router/release/angular-ui-router.js",
            "vendor/ngInfiniteScroll/build/ng-infinite-scroll.js",

	    //"vendor/rangeslider-videojs/rangeslider.js"
        ],
        fonts: [
        ],
        css: [
            "vendor/angular-material/angular-material.css",
            "vendor/video.js/dist/video-js.css",
	    //"vendor/rangeslider-videojs/rangeslider.css"
        ],
        less: [
        ],
        swf: [
            "vendor/video.js/dist/video-js.swf"
        ]
    },
};

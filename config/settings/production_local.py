#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from .production import *  # noqa

STATICFILES_DIRS = (
    str(BASE_DIR.path('static/build')),
)
TEMPLATES[0]['DIRS'] = [str(BASE_DIR.path('static/build'))]
STREAMS_DIR = join(MEDIA_ROOT, 'streams')
MEDIA_ROOT = str(BASE_DIR.path('media'))

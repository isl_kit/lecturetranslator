#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from os.path import join, exists
from hashlib import md5
from typing  import Tuple, List, Dict, Any
import environ

ROOT_DIR = environ.Path(__file__) - 3  # (/a/b/myfile.py - 3 = /)
BASE_DIR = ROOT_DIR.path("lecturetranslator")

env = environ.Env()

env_file = str(ROOT_DIR.path(".env"))
if exists(env_file):
    environ.Env.read_env(env_file)

langdict = {
    "en": "English",
    "de": "German",
    "fr": "French",
    "it": "Italian",
    "xx": "Nothing",
    "lt": "Lithuanian",
    "zh": "Chinese",
    "lv": "Latvian",
    "et": "Estonian",
    "vi": "Vietnamese",
    "es": "Spanish",
    "ro": "Romanian",
    "mk": "Macedonian",
    "hi": "Hindi",
    "id": "Indonese",
    "hu": "Hungarian",
    "sl": "Slovenian",
    "el": "Greek",
    "sk": "Slovak",
    "tr": "Turkish",
    "cs": "Czech",
    "ar": "Arabic",
    "uk": "Ukrainian",
    "ru": "Russian",
    "fa": "Persian",
    "hy": "Armenian",
    "ca": "Catalan",
    "da": "Danish",
    "he": "Hebrew",
    "fi": "Finnish",
    "eo": "Esperanto",
    "sv": "Swedish",
    "pt": "Portuguese",
    "sq": "Albanian",
    "pl": "Polish",
    "bg": "Bulgarian",
    "nl": "Dutch",
    "nb": "Norwegian",
    "am": "English2",
    "ja": "Japanese",
    "sb": "Schwäbisch"
}

# APP CONFIGURATION
# ------------------------------------------------------------------------------
DJANGO_APPS = (
    # Default Django apps:
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Useful template tags:
    # 'django.contrib.humanize',

    # Admin
    'django.contrib.admin',
)

THIRD_PARTY_APPS = (
    'socketiodjangoapp',
    'guardian',
    'slidetransitiondetector',
    'rest_framework',
    'rest_framework_swagger',
    'django_celery_beat',
    'django_filters',
)

# Apps specific for this project go here.
LOCAL_APPS = (
    'lecturetranslator.common',
    'lecturetranslator.streamdata',
    'lecturetranslator.diva',
    'lecturetranslator.main',
    'lecturetranslator.lecture',
    'lecturetranslator.lecturer',
    'lecturetranslator.lecturehall',
    'lecturetranslator.event',
    'lecturetranslator.logger',
    'lecturetranslator.authentication',
    'lecturetranslator.contact',
    'lecturetranslator.recorder',
    'lecturetranslator.mediator',
    'lecturetranslator.mediaprocessor',
    'lecturetranslator.resources',
    'lecturetranslator.serve',
    'lecturetranslator.session',
    'lecturetranslator.celery.CeleryConfig'
)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS
# END APP CONFIGURATION

# MIDDLEWARE CONFIGURATION
# ------------------------------------------------------------------------------
MIDDLEWARE: Tuple[str, ...] = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)
# END MIDDLEWARE CONFIGURATION



# DEBUG
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG: bool = env.bool("DJANGO_DEBUG", False)

# FIXTURE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-FIXTURE_DIRS
FIXTURE_DIRS = (
    str(BASE_DIR.path("fixtures")),
)

# EMAIL CONFIGURATION
# ------------------------------------------------------------------------------
EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND', default='django.core.mail.backends.smtp.EmailBackend')
SERVER_EMAIL = env("DJANGO_SERVER_EMAIL", default="root@localhost")
EMAIL_HOST = env("DJANGO_EMAIL_HOST", default="localhost")
EMAIL_PORT = env.int("DJANGO_EMAIL_PORT", default=1025)
EMAIL_HOST_USER = env("DJANGO_EMAIL_HOST_USER", default="")
EMAIL_HOST_PASSWORD = env("DJANGO_EMAIL_HOST_PASSWORD", default="")
EMAIL_USE_TLS = env.bool("DJANGO_EMAIL_USE_TLS", default=False)
EMAIL_SUBJECT_PREFIX = env("DJANGO_EMAIL_SUBJECT_PREFIX", default="[LectureTranslator] ")

# MANAGER CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = (
    ('Joshua Schauer', 'joshua.schauer@karlsruhe-it-solutions.com'),
)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS

# DATABASE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    # Raises ImproperlyConfigured exception if DATABASE_URL not in os.environ
    'default': env.db("DATABASE_URL"),
}
DATABASES['default']['ATOMIC_REQUESTS'] = True

# GENERAL CONFIGURATION
# ------------------------------------------------------------------------------
# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Berlin'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = 'en-us'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = False

# TEMPLATE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES: List[Dict[str, Any]] = [
    {
        # See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-TEMPLATES-BACKEND
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-dirs
        'DIRS': [
            str(BASE_DIR.path('static/build')),
        ],
        'OPTIONS': {
            # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-debug
            'debug': DEBUG,
            # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-loaders
            # https://docs.djangoproject.com/en/dev/ref/templates/api/#loader-types
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            # See: https://docs.djangoproject.com/en/dev/ref/settings/#template-context-processors
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                # Your stuff: custom template context processors go here
            ],
        },
    },
]

# STATIC FILE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = str(ROOT_DIR.path('staticfiles'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = '/static/'

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = (
    str(BASE_DIR.path('static/build')),
)

# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

# MEDIA CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = str(BASE_DIR.path('media'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = '/media/'


TEMP_DIR = str(BASE_DIR.path('temp'))


# SENDFILE settings
SENDFILE_BACKEND = "django_sendfile.backends.simple"
SENDFILE_ROOT = MEDIA_ROOT
SENDFILE_URL = "/media"


# URL Configuration
# ------------------------------------------------------------------------------
ROOT_URLCONF = 'config.urls'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = 'config.wsgi.application'

# AUTHENTICATION CONFIGURATION
# ------------------------------------------------------------------------------
AUTHENTICATION_BACKENDS = (
    'lecturetranslator.authentication.models.LDAPBackend',
    'guardian.backends.ObjectPermissionBackend',
    #'django.contrib.auth.backends.ModelBackend',
    #'allauth.account.auth_backends.AuthenticationBackend',
)

# Custom user app defaults
# Select the correct user model
AUTH_USER_MODEL = 'authentication.LTUser'

# LOGGING CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#logging
# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s '
                      '%(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        '''
        'django.db.backends': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        '''
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'loggers': {
            'django.security.DisallowedHost': {
                'level': 'ERROR',
                'handlers': ['console', 'mail_admins'],
                'propagate': True,
            },
        },
    }
}


# CUSTOM CONFIGURATION
# ------------------------------------------------------------------------------

# LOGGING
LOGGING_DIR = str(BASE_DIR.path('log'))

# PERMISSIONS
GRANT_ARCHIVE_TO_ALL=env.bool("GRANT_ARCHIVE_TO_ALL", default=False)
GRANT_MEDIA_TO_ALL=env.bool("GRANT_MEDIA_TO_ALL", default=False)

# SOCKETIO
SOCKETIO_INT_URL = env("SOCKETIO_INT_URL")
SOCKETIO_EXT_URL = env("SOCKETIO_EXT_URL")
SOCKETIO_WEB_URL = env("SOCKETIO_WEB_URL")
SOCKETIO_PATH_PREFIX = "socket/"
SOCKET_APP_ID = env("SOCKET_APP_ID", default="lt")

# MEDIATOR
SESSION_DEFAULT_PASSWORD = "secret"
SESSION_NAMESPACE = env("SESSION_NAMESPACE", default="")
LIMIT_STREAMS = env.list("LIMIT_STREAMS", default=[])
SHOW_HIDDEN_SESSIONS = env.bool("SHOW_HIDDEN_SESSIONS", default=False)
MEDIATOR_PRINT_LOG = env.bool("MEDIATOR_PRINT_LOG", default=True)
MEDIATOR_LOG_DIR = join(LOGGING_DIR, 'mediator')

# DISPLAY CLIENT
UNIX_IN_SOCKET = env("UNIX_IN_SOCKET", default="/tmp/unix_in.sock")
UNIX_OUT_SOCKET = env("UNIX_OUT_SOCKET", default="/tmp/unix_out.sock")

# RECORDING CLIENT
MEDIATOR_HOST = env("MEDIATOR_HOST", default="141.3.25.30")
MEDIATOR_PORT = env.int("MEDIATOR_PORT", default=4443)

# WEBSOCKET (audio streaming)
WEBSOCKET_INT_URL = env("WEBSOCKET_INT_URL")
WEBSOCKET_EXT_URL = env("WEBSOCKET_EXT_URL")

# EDITOR WEBSOCKET (transcript editing)
EDITOR_WEBSOCKET_EXT_URL = env("EDITOR_WEBSOCKET_EXT_URL")
EDITOR_ALLOW_COMMIT = env.bool("EDITOR_ALLOW_COMMIT", default=False)

# BACKCHANNEL
BACKCHANNEL = env.bool("BACKCHANNEL", default=False)

# AUDIO PLAYER
STREAMS_DIR = join(MEDIA_ROOT, 'streams')

# CERTIFICATES
LOCALHOST_CERT_FILE = env("LOCALHOST_CERT_FILE", default="")
LOCALHOST_KEY_FILE = env("LOCALHOST_KEY_FILE", default="")

# LDAP
LDAP_DN = env("LDAP_DN", default=None)
LDAP_PASSWORD = env("LDAP_PASSWORD", default=None)

# EMOJI
EMOJI_ENABLED = env.bool("EMOJI_ENABLED", default=False)

# WEBLINKS
LINKS_ENABLED = env.bool("LINKS_ENABLED", default=False)

# CLASSIFICATION
CLASSIFICATION = env.bool("CLASSIFICATION", default=False)

# CELERY
CELERY_ENABLED = env.bool("CELERY_ENABLED", default=False)
BROKER_URL = env('CELERY_BROKER_URL', default='amqp://')
CELERY_RESULT_BACKEND = 'cache'
CELERY_CACHE_BACKEND = 'memory'

# SWAGGER
SWAGGER_SETTINGS = {
    'DOC_EXPANSION': 'list',
    'JSON_EDITOR': True
}

# CORRECTIONS
CORRECTIONS_DIR = join(MEDIA_ROOT, 'corrections')

# TTS (DEPRECATED)
#TTS_FINGERPRINTS = env.list("TTS_FINGERPRINTS", default=[])
#FLITEVOX_EXEC = "/home/bkrueger/dev/festival/flite-1.9.2-current/bin/flite"
#FESTIVAL_EXEC = "/home/bkrueger/dev/festival/festival/festival/bin/text2wave"
#VOICES_DIR = join(MEDIA_ROOT, 'voices')
#DATA_ROOT = str(BASE_DIR.path('data'))

# NEW FILE PERMISSIONS
# Allow group full access too
# Not sure if this applies to media dir
FILE_UPLOAD_PERMISSIONS = 0o664
#FILE_UPLOAD_DIRECTORY_PERMISSIONS = 0o775 # Not sure if needed, default is None


def GET_STREAM_LABEL(language_tag):
    if language_tag in langdict:
        return langdict[language_tag] + " (" + language_tag.upper()[:2] + ")"
    return language_tag


def FILTER_SESSIONS(session):
    from django.conf import settings
    if settings.SESSION_NAMESPACE and "N" in session["params"]:
        namespace = session["params"]["N"]
        if (type(settings.SESSION_NAMESPACE) is list and namespace in settings.SESSION_NAMESPACE) or namespace == settings.SESSION_NAMESPACE:
            return True
        return False
    else:
        if not settings.SESSION_NAMESPACE:
            return True
        return False

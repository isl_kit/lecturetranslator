#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Production Configurations
'''


from .common import *  # noqa
from os.path import join

# SECRET CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
# Raises ImproperlyConfigured exception if DJANGO_SECRET_KEY not in os.environ
SECRET_KEY = env("DJANGO_SECRET_KEY")

WEBSOCKET_ACCESS_SECRET_KEY= env('WEBSOCKET_ACCESS_SECRET_KEY', default='YOUSHOULDCHANGMETOO!!!')

# This ensures that Django will be able to detect a secure connection
# properly on Heroku.
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

SECURITY_MIDDLEWARE = (
    'django.middleware.security.SecurityMiddleware',
)

# Make sure djangosecure.middleware.SecurityMiddleware is listed first
MIDDLEWARE = SECURITY_MIDDLEWARE + MIDDLEWARE

# set this to 60 seconds and then to 518400 when you can prove it works
SECURE_HSTS_SECONDS = 60
SECURE_HSTS_INCLUDE_SUBDOMAINS = env.bool(
    "DJANGO_SECURE_HSTS_INCLUDE_SUBDOMAINS", default=True)
SECURE_FRAME_DENY = env.bool("DJANGO_SECURE_FRAME_DENY", default=True)
SECURE_CONTENT_TYPE_NOSNIFF = env.bool(
    "DJANGO_SECURE_CONTENT_TYPE_NOSNIFF", default=True)
SECURE_BROWSER_XSS_FILTER = True
SESSION_COOKIE_SECURE = True
SESSION_COOKIE_HTTPONLY = True
SECURE_SSL_REDIRECT = env.bool("DJANGO_SECURE_SSL_REDIRECT", default=True)

# SITE CONFIGURATION
# ------------------------------------------------------------------------------
# Hosts/domain names that are valid for this site
# See https://docs.djangoproject.com/en/1.6/ref/settings/#allowed-hosts
ALLOWED_HOSTS = env.list('DJANGO_ALLOWED_HOSTS', default=[])

# STATIC FILES
# ------------------------------------------------------------------------------
STATIC_ROOT = str(ROOT_DIR.path('static'))
MEDIA_ROOT = str(ROOT_DIR.path('media'))
TEMP_DIR = str(ROOT_DIR.path('temp'))
# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = (
    str(BASE_DIR.path('static/bin')),
)

# SENDFILE settings
SENDFILE_BACKEND = "django_sendfile.backends.xsendfile"
SENDFILE_ROOT = MEDIA_ROOT


# EMAIL
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND', default='django.core.mail.backends.console.EmailBackend')


# TEMPLATE CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES[0]['DIRS'] = [str(BASE_DIR.path('static/bin'))]
TEMPLATES[0]['OPTIONS']['loaders'] = [
    ('django.template.loaders.cached.Loader', [
        'django.template.loaders.filesystem.Loader', 'django.template.loaders.app_directories.Loader', ]),
]

# DATABASE CONFIGURATION
# ------------------------------------------------------------------------------
# Raises ImproperlyConfigured exception if DATABASE_URL not in os.environ
DATABASES['default'] = env.db("DATABASE_URL")

# CACHING
# ------------------------------------------------------------------------------
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

# CUSTOM CONFIGURATION
# ------------------------------------------------------------------------------

# MEDIATOR APP
MEDIATOR_LOG_DIR = join(str(ROOT_DIR.path('log')), 'mediator')

# AUDIO PLAYER
STREAMS_DIR = join(MEDIA_ROOT, 'streams')

# TTS (DEPRECATED)
#VOICES_DIR = join(MEDIA_ROOT, 'voices')
#FLITEVOX_EXEC = "/var/www/tts/flite-1.9.2-current/bin/flite"
#FESTIVAL_EXEC = "/var/www/tts/festival/festival/bin/text2wave"

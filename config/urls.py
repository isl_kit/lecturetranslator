# -*- coding: utf-8 -*-


from django.conf.urls import include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin

from rest_framework_swagger.views import get_swagger_view

from lecturetranslator.main.views import index, angular_redirect


admin.autodiscover()

urlpatterns = [
    url(r'^' + settings.SOCKETIO_PATH_PREFIX, include('socketiodjangoapp.urls')),
    url(r'^media/', include('lecturetranslator.serve.urls')),
    url(r'^diva/', include('lecturetranslator.diva.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^logging/', include('lecturetranslator.logger.urls')),
    url(r'^auth/', include('lecturetranslator.authentication.urls')),
    url(r'^contact/', include('lecturetranslator.contact.urls')),
    url(r'^streamdata/', include('lecturetranslator.streamdata.urls')),
    url(r'^resources/', include('lecturetranslator.resources.urls')),
    url(r'^sessions/', include('lecturetranslator.session.urls')),
    url(r'^session-media/', include('lecturetranslator.mediaprocessor.urls')),
    url(r'^halls/', include('lecturetranslator.lecturehall.urls')),
    url(r'^lecturers/', include('lecturetranslator.lecturer.urls')),
    url(r'^lectures/', include('lecturetranslator.lecture.urls')),
    url(r'^events/', include('lecturetranslator.event.urls')),
    url(r'^docs/', get_swagger_view(title='Lecture Translator API')),
    url(r'^$', index, name="index"),
    url(r'^(?P<path>.*)/$', angular_redirect, name="angular_redirect"),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) # type: ignore
urlpatterns += static("media/", document_root=settings.MEDIA_ROOT)


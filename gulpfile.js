// gulpfile.js
var gulp = require("gulp"),
    glob = require("glob"),
    _ = require("lodash"),
    path = require("path"),
    del = require("del"),
    fs = require("fs"),
    vm = require("vm"),

    ngConstant = require('gulp-ng-constant'),
	log	= require('fancy-log'),
    rename = require("gulp-rename"),
    jshint = require("gulp-jshint"),
    concat = require("gulp-concat"),
    uglify = require("gulp-uglify"),
    header = require("gulp-header"),
    stylish = require("jshint-stylish"),
    ngAnnotate = require("gulp-ng-annotate"),
    template = require("gulp-template"),
    less = require("gulp-less"),
    minifyCSS = require('gulp-clean-css'),
    wrap = require("gulp-wrap"),
    ngHtml2Js = require("gulp-ng-html2js"),
    minifyHtml = require("gulp-htmlmin"),
    webkitAssign = require('webkit-assign/gulp'),
    babel = require("gulp-babel"),
    webpack = require('webpack-stream'),
    terser = require("gulp-terser"),

    Server = require("karma").Server,

    streamqueue = require("streamqueue"),

    config = require("./build.config.js"),
    pkg = require("./package.json"),

    finalCSSFile = pkg.name + "-" + pkg.version + ".css",
    finalJSFile = pkg.name + "-" + pkg.version + ".js",
    templateJSFile = "templates.html.js",
    configFile = "config.module.js",

    banner, bannerInner, bannerHtml;

require("date-utils");

bannerInner =
    " * " + pkg.name + " - v" + pkg.version + " - " + getCurrentDate() + "\n" +
    " * " + pkg.homepage + "\n" +
    " *\n" +
    " * Author: " + pkg.author + "\n" +
    " * Copyright (c) " + getCurrentYear() + " KIT\n";

banner = "/**\n" + bannerInner + " */\n";

bannerHtml = "<!--\n" + bannerInner + " -->\n";

function getPath(x) {
    var joinPath = function(str) {
        if(str[0] == '!') 
        {
            // String represents an exclusion (folder/files that should be ignored), so make sure to add the exclamation mark before the path
            return '!' + path.join(config.static_dir, str.substring(1));
        }
        else return path.join(config.static_dir, str);
    }

    if (_.isArray(x)) {
        return x.map(joinPath);
    }
    return joinPath(x);
}

/* Clean */

gulp.task("clean", function () {
    return del(del.sync([
        getPath(config.build_dir),
        getPath(config.compile_dir),
    ], {force: true}));
});

/* Karma */

gulp.task("test", function (done) {
    new Server({
        configFile: path.join(__dirname, getPath(config.build_dir), "karma.conf.js"),
        singleRun: true
    }, done).start();
});

/* Build Subtasks */

gulp.task("build_config", function(done) {
	if (_.isEmpty(config.config_files.local)) {
		log("No local config files; Skipping!");
		return done();
	}
    return gulp.src(getPath(config.config_files.local))
        .pipe(ngConstant({
          name: "app.config",
          deps: []
        }))
        .pipe(rename(configFile))
        .pipe(gulp.dest(getPath(config.build_dir + "/conf")));
});

gulp.task("build_vendor_js", function(done) {
	if (_.isEmpty(config.vendor_files.js)) {
		log("No vendor js files; Skipping!");
		return done();
	}
    return gulp.src(getPath(config.vendor_files.js), { base: config.static_dir })
        .pipe(gulp.dest(getPath(config.build_dir)));
});

gulp.task("webkit_assign", function() {
    return gulp.src(getPath("vendor/angular/angular.js"))
        .pipe(webkitAssign())
        .pipe(gulp.dest(getPath(config.build_dir + "/vendor/angular")));
});

gulp.task("build_vendor_css", function(done) {
	if (_.isEmpty(config.vendor_files.css)) {
		log("No vendor css files; Skipping!");
		return done();
	}
    return gulp.src(getPath(config.vendor_files.css), { base: config.static_dir })
        .pipe(gulp.dest(getPath(config.build_dir)));
});

gulp.task("build_vendor_fonts", function(done) {
	if (_.isEmpty(config.vendor_files.fonts)) {
		log("No vendor fonts; Skipping!");
		return done();
	}
    return gulp.src(getPath(config.vendor_files.fonts))
        .pipe(gulp.dest(getPath(config.build_dir + "/assets/fonts")));
});

gulp.task("build_assets_js", function(done) {
	if (_.isEmpty(config.app_files.singlejs)) {
		log("No singlejs app_files; Skipping!");
		return done();
	}
    return gulp.src(getPath(config.app_files.singlejs), { base: config.static_dir })
        .pipe(gulp.dest(getPath(config.build_dir)));
});

gulp.task("build_assets_wav", function(done) {
	if (_.isEmpty(config.app_files.wav)) {
		log("No wav app_files; Skipping!");
		return done();
	}
    return gulp.src(getPath(config.app_files.wav), { base: config.static_dir })
        .pipe(gulp.dest(getPath(config.build_dir)));
});


gulp.task("build_js", function(done) {
	if (_.isEmpty(config.app_files.js)) {
		log("No js app_files; Skipping!");
		return done();
	}
    return gulp.src(getPath(config.app_files.js), { base: config.static_dir })
        .pipe(jshint())
        .pipe(jshint.reporter(stylish))
        .pipe(jshint.reporter("fail"))
        .pipe(gulp.dest(getPath(config.build_dir)));
});

gulp.task("build_js_es15", function(done) {
	if (_.isEmpty(config.app_files.js_es15)) {
		log("No js es15 app_files; Skipping!");
		return done();
	}
    return gulp.src(getPath(config.app_files.js_es15), { base: config.static_dir })
        .pipe(webpack(require('./webpack.config.js')))
        .pipe(gulp.dest(getPath(config.build_dir)));
});

gulp.task("build_html2js", function(done) {
	if (_.isEmpty(config.app_files.tpl)) {
		log("No tpl app_files; Skipping!");
		return done();
	}
    return gulp.src(getPath(config.app_files.tpl))
        .pipe(minifyHtml({
					ignoreCustomFragments: [/{{.*?}}/] // Fix for inline angular
		}))
        .pipe(ngHtml2Js({
            moduleName: "app.templates"
        }))
        .pipe(concat(templateJSFile))
        .pipe(gulp.dest(path.join(getPath(config.build_dir), "tpl")));
});

gulp.task("build_css", function() {
    var lessFiles = [], vendorFiles = getVendorLessFiles(),
        modules = getAngularModules();

    _.each(modules, function (module) {
        lessFiles = _.union(lessFiles, getLessFiles(path.dirname(module.path)));
    });
    /**
    return gulp.src(getPath(config.app_files.main_less))
        .pipe(template({
            lessFiles: lessFiles,
            vendorFiles: vendorFiles
        }))
        .pipe(less())
        .on("error", function (error) {
            gutil.log(gutil.colors.red(error.message));
        })
        .pipe(rename(finalCSSFile))
        .pipe(gulp.dest(path.join(getPath(config.build_dir), "assets", "css")));**/

    return streamqueue({ objectMode: true },
        gulp.src(getPath(config.app_files.css)),
        gulp.src(getPath(config.app_files.main_less))
        .pipe(template({
            lessFiles: lessFiles,
            vendorFiles: vendorFiles
        }))
        .pipe(less())
        .on("error", function (error) {
            log(gutil.colors.red(error.message));
        }))
    .pipe(concat(finalCSSFile))
    .pipe(gulp.dest(path.join(getPath(config.build_dir), "assets", "css")));
});

gulp.task("build_fonts", function(done) {
	if (_.isEmpty(config.app_files.fonts)) {
		log("No font app_files; Skipping!");
		return done();
	}
    return gulp.src(getPath(config.app_files.fonts))
        .pipe(gulp.dest(getPath(config.build_dir) + "/assets/fonts"));
});

gulp.task("build_images", function(done) {
	if (_.isEmpty(config.app_files.images)) {
		log("No image app_files; Skipping!");
		return done();
	}
    return gulp.src(getPath(config.app_files.images))
        .pipe(gulp.dest(getPath(config.build_dir) + "/assets/images"));
});

gulp.task("build_karma", function() {
    var karmaFiles = _.union(config.vendor_files.js, config.test_files.js.map(function (fileName) {
        return "../" + fileName;
    }));

    return gulp.src("karma.conf.js")
        .pipe(template({
            karmaFiles: karmaFiles
        }))
        .pipe(gulp.dest(getPath(config.build_dir)));
});

gulp.task("build_test", function(done) {
	if (_.isEmpty(config.app_files.jsunit)) {
		log("No jsunit app_files; Skipping!");
		return done();
	}
    return gulp.src(getPath(config.app_files.jsunit), { base: config.static_dir })
        .pipe(jshint())
        .pipe(jshint.reporter(stylish))
        .pipe(jshint.reporter("fail"))
        .pipe(gulp.dest(getPath(config.build_dir)));
});

gulp.task("build_swf", function(done) {
	if (_.isEmpty(config.vendor_files.swf)) {
		log("No vendor swf files; Skipping!");
		return done();
	}
    return gulp.src(getPath(config.vendor_files.swf))
        .pipe(gulp.dest(path.join(getPath(config.build_dir), "assets", "swf")));
});

gulp.task("build_index", function(done) {
	if (_.isEmpty(config.app_files.index)) {
		log("No index app_file; Skipping!");
		return done();
	}

    var jsFiles = getBuildJSFiles().map(function (js_path) {
            return path.relative(getPath(config.build_dir), js_path);
        }),
        cssFiles = getBuildCSSFiles().map(function (css_path) {
            return path.relative(getPath(config.build_dir), css_path);
        });

    return gulp.src(getPath(config.app_files.index))
        .pipe(template({
            cssFiles: cssFiles,
            jsFiles: jsFiles,
            strict: false
        }))
        .pipe(gulp.dest(getPath(config.build_dir)));
});

/* Compile Subtasks */

gulp.task("compile_css", function() {
    var files = _.union(getPath(config.vendor_files.css), [getPath(config.build_dir) + "/assets/**/*.css"]);
    return gulp.src(files)
        .pipe(concat(finalCSSFile))
        //.pipe(minifyCSS())
        .pipe(header(banner))
        .pipe(gulp.dest(getPath(config.compile_dir) + "/assets"));
});

gulp.task("compile_js", function(){
    return streamqueue({ objectMode: true },
        gulp.src(getPath(config.vendor_files.js)),
        gulp.src(getPath(config.app_files.singlejs)),
        gulp.src(getPath(config.config_files.production))
        .pipe(ngConstant({
          name: "app.config",
          deps: []
        })),
        gulp.src(getPath(config.build_dir) + "/tpl/**/*.js"),
        gulp.src([getPath(config.build_dir) + "/app/**/*.module.js", getPath(config.build_dir) + "/app/**/*.js"])
        .pipe(wrap({ src: "module.template"}))
        .pipe(ngAnnotate()))
    .pipe(concat(finalJSFile))
    .pipe(terser()) // TODO: Changed from .pipe(uglify()). Check if this was a good choice
    .pipe(header(banner))
    .pipe(gulp.dest(getPath(config.compile_dir) + "/assets"))
});

gulp.task("compile_wav", function() {
    return gulp.src(getPath(config.build_dir) + "/assets/wav/*")
        .pipe(gulp.dest(getPath(config.compile_dir) + "/assets/wav"));
});

gulp.task("compile_fonts", function() {
    return gulp.src(getPath(config.build_dir) + "/assets/fonts/*")
        .pipe(gulp.dest(getPath(config.compile_dir) + "/assets/fonts"));
});

gulp.task("compile_images", function() {
    return gulp.src(getPath(config.build_dir) + "/assets/images/*")
        .pipe(gulp.dest(getPath(config.compile_dir) + "/assets/images"));
});

gulp.task("compile_swf", function() {
    var files = getPath(config.vendor_files.swf);
    return gulp.src(files)
        .pipe(gulp.dest(getPath(config.compile_dir) + "/assets/swf"));
});

gulp.task("compile_index", function() {
    var jsFiles = ["assets/" + finalJSFile],
        cssFiles = ["assets/" + finalCSSFile];

    return gulp.src(getPath(config.app_files.index))
        .pipe(template({
            cssFiles: cssFiles,
            jsFiles: jsFiles,
            strict: true
        }))
        .pipe(header(bannerHtml))
        .pipe(gulp.dest(getPath(config.compile_dir)));
});

/* Main Tasks */

gulp.task("build",
	// Run one after another
	gulp.series(
		"clean",
		gulp.parallel(	// Run these in parallel
			"build_vendor_fonts",
			"build_vendor_js",
			"build_vendor_css",
			"build_assets_js",
			"build_assets_wav",
			"build_config",
            "build_js",
            "build_js_es15",
			"build_html2js",
			"build_css",
			"build_swf",
			"build_fonts",
			"build_images",
			"build_test",
			"build_karma"
		),
		"webkit_assign",
		"build_index",
		"test"
	)
);

gulp.task("compile",
    gulp.series(
        "build",
		gulp.parallel(
			"compile_js",
			"compile_css",
			"compile_swf",
			"compile_fonts",
			"compile_images",
			"compile_wav",
			"compile_index"
		)
	)
);

gulp.task("default", gulp.series("compile", function () {}));

/* build automatically on changes in the js files */
gulp.task('watch',function() {
	gulp.watch('lecturetranslator/static/app/**/*.js',gulp.series('build'));
})

/* Helper functions */

function getAngularModules() {
    var sandbox, modules = [], moduleFiles;
    sandbox = {
        angular: {
            module: function (name, deps) {
                this.cache[name] = {
                    name: name,
                    path: this.tempPath,
                    deps: deps
                };
            },
            tempPath: "",
            cache: {},
            done: {}
        }
    };
    moduleFiles = _.union.apply(_, _.map(config.app_files.modules, function (g) {
        return glob.sync(getPath(g));
    }));
    _.each(moduleFiles, function (path) {
        sandbox.angular.tempPath = path;
        vm.runInNewContext(fs.readFileSync(path, "utf-8"), sandbox);
    });
    while (_.size(sandbox.angular.cache) > 0) {
        _.each(sandbox.angular.cache, function (modObj, name) {
            var allStates = _.map(modObj.deps, function (dep) {
                return _.has(sandbox.angular.done, dep) || !_.has(sandbox.angular.cache, dep);
            });
            var canBeAdded = !_.includes(allStates, false);
            if (canBeAdded) {
                modules.push(modObj);
                delete sandbox.angular.cache[name];
                sandbox.angular.done[name] = true;
            }
        });
    }
    return modules;
}

function getLessFiles(rootPath) {
    return _.map(_.union.apply(this, _.map(config.app_files.less, function (g) {
        return glob.sync(rootPath + "/" + g);
    })), function (p) {
        return path.relative(path.dirname(getPath(config.app_files.main_less)), p)
    });
}

function getVendorLessFiles() {
    return _.map(_.union.apply(this, _.map(config.vendor_files.less, function (g) {
        return glob.sync(getPath(g));
    })), function (p) {
        return path.relative(path.dirname(getPath(config.app_files.main_less)), p)
    });
}

function getBuildJSFiles() {
    return _.union.apply(this, _.map(_.union(config.vendor_files.js, config.app_files.singlejs, ["conf/" + configFile], ["tpl/" + templateJSFile], config.app_files.js), function (g) {
        return glob.sync(getPath(config.build_dir) + "/" + g);
    }));
}

function getBuildCSSFiles() {
    return _.union.apply(this, _.map(_.union(config.vendor_files.css, [path.join("assets", "css", finalCSSFile)]), function (g) {
        return glob.sync(getPath(config.build_dir) + "/" + g);
    }));
}

function getCurrentYear() {
    return Date.today().toFormat("YYYY");
}

function getCurrentDate() {
    return Date.today().toFormat("DD/MM/YYYY");
}

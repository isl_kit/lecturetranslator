const path = require('path');
const glob = require('glob');

function getEntries(pattern) {
    const entries = {};
  
    glob.sync(pattern, { ignore: '**/node_modules/**' }).forEach((file) => {
      entries[file.replace('lecturetranslator/static/', '')] = path.join(__dirname, file);
      console.log(path.join(__dirname, file));
    });
    return entries;
  }

module.exports = {
entry: getEntries('lecturetranslator/static/app/**/session-editor/**/*.js'),
    output: {
        filename: '[name]'
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
                loader: 'babel-loader',
                options: {
                  presets: ['@babel/preset-env']
                }
              }
        }]
   }
};